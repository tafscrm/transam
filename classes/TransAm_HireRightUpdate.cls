/* Class Name   : TransAm_HireRightUpdate Helper
* Description  : Webservice Class to update the status of HireRight order from HireRight
* Created By   : Monalisa
* Created On   : 09-Apr-2017
*
*  Modification Log :
*  --------------------------------------------------------------------------------------
*  * Developer                    Date                    Description
*  * ------------------------------------------------------------------------------------                 
*  * Monalisa               09-Apr-2017              Initial version.
*
*****************************************************************************************/
@RestResource(urlMapping='/transam/hireright/*')
global class TransAm_HireRightUpdate {
    
/****************************************************************************************
* Created By      :  Monalisa
* Create Date     :  09-Apr-2017
* Description     :  Update the Status of HireRight order
* Modification Log:  Initial version.
***************************************************************************************/
    /*@HttpPost(urlMapping='/HireRightOrderID')
    global static void updateHireRightOrders()
    {
        try{
            RestRequest req = RestContext.request;
            String orderId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            TransAm_HireRight_Orders__c order = updateOrder(orderId);
            List<TransAm_HireRight_Orders__c> orders = new List<TransAm_HireRight_Orders__c>();
            orders.add(order);
            TransAm_HireRightHelper.populateWeblinkOnHireRightOrders(orders);
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightUpdate',TransAm_Method_Name__c = 'updateHireRightOrders',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }   */
    
    //@future(callout=true)
    /*public static void test(String orderId) {
        System.debug('in test method');
        TransAm_HireRight_Orders__c order = updateOrder(orderId);
        System.debug('updated order');
        List<TransAm_HireRight_Orders__c> orders = new List<TransAm_HireRight_Orders__c>();
        orders.add(order);
        System.debug('about to call web link');
        TransAm_HireRightHelper.populateWeblinkOnHireRightOrders(orders);
        System.debug('called web link');
       // return res;
    }
    
    private static TransAm_HireRight_Orders__c updateOrder(String orderId) {
        
        TransAm_HireRight_Orders__c order = new TransAm_HireRight_Orders__c();
        try{
            order = [SELECT TransAm_Order_Status__C, TransAm_Hireright_Order_ID__c
                     FROM  TransAm_HireRight_Orders__c WHERE Name = :orderId];
            order.TransAm_Order_Status__c = 'Completed';
            Database.update(order);
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightUpdate',TransAm_Method_Name__c = 'updateOrder',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        return order;
    }*/
    
}