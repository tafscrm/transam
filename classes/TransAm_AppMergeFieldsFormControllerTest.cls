/* CLass Name   : TransAm_AppMergeFormControllerTest
 * Description  : Test class for TransAm_AppMergeFormController
 * Created By   : Monalisa Das
 * Created On   : 11-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                11-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
 @isTest(seeAllData=false)
public class TransAm_AppMergeFieldsFormControllerTest {
/************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
@testSetup static void setup() {
        Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        acc.TransAm_Driving_School_Code__c='C-2830';
        update acc;
        system.debug('account+++'+acc);
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        TransAm_Application__c masterApp =new TransAm_Application__c();
        masterApp = TransAm_Test_DataUtility.createApplications(1, 'Independent', 'Application Received',con, True)[0];  
        masterApp.TransAm_Primary_Application__c=false;
        masterApp.TransAm_DriverID__c = '';
        masterApp.TransAm_EBE_Entry_ID__c = '';
        update masterApp;
        
        List<TransAm_Application__c> childAppList =new List<TransAm_Application__c>();
        childAppList = TransAm_Test_DataUtility.createApplications(3, 'Dependant', 'Application Received',con, True);
        List<TransAm_Application__c> childAppUpdateList =new List<TransAm_Application__c>();
        for(TransAm_Application__c childApp: childAppList){
            childApp.TransAm_Primary_Application__c=false;
            childApp.TransAm_DriverID__c = '';
            childApp.TransAm_EBE_Entry_ID__c = '';
            childApp.TransAm_Master_Application__c = masterApp.Id;
            childAppUpdateList.add(childApp);
        }
        update childAppUpdateList;
        
        /*TransAm_Application__c childApp1 =new TransAm_Application__c();
        childApp1 = TransAm_Test_DataUtility.createApplications(1, 'Dependant', 'Application Received',con, True)[0];  
        childApp1.TransAm_Primary_Application__c=false;
        childApp1.TransAm_DriverID__c = '';
        childApp1.TransAm_EBE_Entry_ID__c = '';
        childApp1.TransAm_Master_Application__c = masterApp.Id;
        update childApp1;
        
        TransAm_Application__c childApp2 =new TransAm_Application__c();
        childApp2 = TransAm_Test_DataUtility.createApplications(1, 'Dependant', 'Application Received',con, True)[0];  
        childApp2.TransAm_Primary_Application__c=false;
        childApp2.TransAm_DriverID__c = '';
        childApp2.TransAm_EBE_Entry_ID__c = '';
        childApp2.TransAm_Master_Application__c = masterApp.Id;
        update childApp2;*/    
        TransAm_Application__c cApp = [SELECT ID,TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_Master_Application__c =: masterApp.Id LIMIT 1];
        system.debug('cApp+++'+cApp);
        List<TransAm_Violation__c> voilations = TransAm_Test_DataUtility.createViolations(2,cApp, TRUE);
        system.debug('voilations+++'+voilations);
        List<TransAm_Misdemeanor__c> misdemeanor = TransAm_Test_DataUtility.createMisdemeanor(2,cApp, TRUE);
        List<TransAm_Employer__c> employer = TransAm_Test_DataUtility.createEmployers(2,TRUE);
        TransAm_Employer__c emp = [SELECT ID FROM TransAm_Employer__c LIMIT 1];
        List<TransAm_Job_History__c> jobHistory = TransAm_Test_DataUtility.createJobHistory(2,cApp,emp, TRUE);
        List<TransAm_License__c> licenses = TransAm_Test_DataUtility.createLicenses(2,cApp, TRUE);
        List<TransAm_Felony__c> felonies = TransAm_Test_DataUtility.createFelonies(2,cApp, TRUE);
        List<TransAm_Accident__c> accidents = TransAm_Test_DataUtility.createAccidents(2,cApp, TRUE);
        List<TransAm_PreviousAddress__c> addresses = TransAm_Test_DataUtility.createPreviousAddresses(2,cApp, TRUE);
        
    }
       /************************************************************************************
    * Method       :    mergeFieldsFormPageTest
    * Description  :    Test Method to create mergeFieldsFormPage
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void mergeFieldsFormPageTest(){
    
        Test.startTest();
        
         //try{
            TransAm_Application__c masterApp = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c where TransAm_Application_Restriction__c='Independent' LIMIT 1];
            List<TransAm_Application__c> childAppList = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c where TransAm_Application_Restriction__c='Dependant' LIMIT 3];
            PageReference pageRef = Page.TransAm_Application_Merge_Fields_Form;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', masterApp.id);
            ApexPages.currentPage().getParameters().put('fieldAPIName', 'TransAm_Address__c');
            ApexPages.currentPage().getParameters().put('id1', childAppList[0].id);
            ApexPages.currentPage().getParameters().put('id2', childAppList[1].id);
            ApexPages.currentPage().getParameters().put('id3', childAppList[1].id);
            
            ApexPages.StandardController sc3 = new ApexPages.StandardController(masterApp);
            TransAm_AppMergeFieldsFormController  mergeApps = new TransAm_AppMergeFieldsFormController(sc3);
           
            Set<id> appIdSet = new set<id>();
            for(TransAm_Application__c childAppRec: childAppList){
                 appIdSet.add(childAppRec.Id);
            }
            mergeApps.selectedAppsIdSet = appIdSet;
            mergeApps.childAppList = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c where TransAm_Application_Restriction__c='Dependant' LIMIT 3];
            //mergeApps.app3Wrapper appwrapper=   new mergeApps.app3Wrapper();
            
            mergeApps.mergeMaster();
            mergeApps.mergeVoilations();
            mergeApps.mergeFelonies();
            mergeApps.mergeAddresses();
            mergeApps.mergeAccidents();
            mergeApps.mergeJobHistories();
            mergeApps.mergeLicenses();
            mergeApps.mergeMisdemeanors();
            mergeApps.UpdateMasterApplication();
            mergeApps.cancelMergeOperation();
            mergeApps.NextSection();
        //}
        //catch(Exception e){
            //System.debug('Exception e ::'+e.getMessage() + e.getStackTraceString() + e );
        //}
        
        Test.stopTest();
}

}