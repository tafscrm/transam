/******************************************************************************************
* Created By   :     Nitesh Halliyal
* Create Date  :     6/27/2017
* Description  :     Controller class for contact photo upload VF page.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Nitesh Halliyal                     06/27/2017         Initial version.
*****************************************************************************************/
Public class TransAM_ContactPhotoController{
    
    public transient blob attachBody{get;set;}
    public String name{get;set;}
    public id contId;
    public TransAM_ContactPhotoController(ApexPages.StandardController controller){
        
        contId = controller.getRecord().id;
    }
    /****************************************************************************************
    * Created By      :  Nitesh Halliyal
    * Create Date     :  06/27/2017
    * Description     :  Called from Page to upload attachment and populate attachment Id on contact.
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public PageReference upload(){
        pagereference pg = new pagereference('/'+contId);
        Attachment contactPic =  new attachment();
        String attachId = '';
        String parContactId = '';
        contactPic.ParentId= contId;
        contactPic.OwnerId = UserInfo.getUserId();
        contactPic.body=attachBody;
        contactPic.name= 'ContactPic'+String.valueof(Math.Random()).remove('.')+'.jpg';
        
        
        try {
            insert contactPic;
            attachId = contactPic.Id;
            contactPic.body=null;
            contactPic = null;
            attachBody = null;
            
            List<contact> parContactList= new list<contact>([SELECT Id,TransAM_Contact_Photo_Attachment_Id__c 
                                                             FROM contact
                                                             WHERE Id = :contId]);
            if(!parContactList.isEmpty() && String.isNotBlank(attachId)){
                parContactList[0].TransAM_Contact_Photo_Attachment_Id__c = attachId;
                update parContactList;  
            }
            pg.setRedirect(true);
        	return pg;
        } 
        catch (DMLException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Contact Photo Could Not be uploaded'));
            return null;
        } 
    }
}