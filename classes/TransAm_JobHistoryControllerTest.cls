/* CLass Name   : JobHistoryControllerTest
 * Description  : Test class for controller JobHistoryController
 * Created By   : Neha Jain
 * Created On   : 30-Mar-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Neha Jain                28-Mar-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TransAm_JobHistoryControllerTest{
    
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        
        Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        acc.TransAm_Driving_School_Code__c='C-2830';
        update acc;
        system.debug('account+++'+acc);
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        TransAm_Application__c objApp =new TransAm_Application__c();
        objApp = TransAm_Test_DataUtility.createApplications(1, 'Independent', 'Application Received',con, True)[0];  
        objApp.TransAm_Primary_Application__c=false;
        objApp.TransAm_DriverID__c = '';
        objApp.TransAm_EBE_Entry_ID__c = '';
        update objApp;
        TransAm_Employer__c objEmp =new TransAm_Employer__c(Name='Neha',TransAm_City__c='Bangalore');
        insert objEmp;
        TransAm_Job_History__c objJobHistory = new TransAm_Job_History__c(TransAm_Employed_From__c=Date.newInstance(2017,3,13),
                                                TransAm_Employed_To__c=Date.newInstance(2017,3,28),TransAm_Application__c=objApp.id,
                                                TransAm_Employer2__c=objEmp.id,TransAm_Current_Employer__c='TRUE');                                  
        insert objJobHistory;
        
        TransAm_Job_History__c objJobHistory2 = new TransAm_Job_History__c(TransAm_Employed_From__c=Date.newInstance(2017,3,13),
                                                TransAm_Employed_To__c=Date.newInstance(2017,3,28),TransAm_Application__c=objApp.id,
                                                TransAm_Current_Employer__c='FALSE',TransAm_Other_Employer__c='Deloitte');
        insert objJobHistory2;

     }

     /************************************************************************************
    * Method       :    testEmployerAssignment
    * Description  :    Test Method to create Job history
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testEmployerAssignment(){
     
        Test.startTest();
        try{
        
            TransAm_Job_History__c objJobHistory = [SELECT ID,TransAm_Employer2__c  FROM TransAm_Job_History__c where TransAm_Current_Employer__c='TRUE' LIMIT 1];
            system.debug('objJobHistory----'+objJobHistory );
            
            ApexPages.StandardController sc3 = new ApexPages.StandardController(objJobHistory);
            TransAm_JobHistoryController  jobHisCon= new TransAm_JobHistoryController(sc3);
    
            PageReference pageRef = Page.TransAm_JobHistory; 
            pageRef.getParameters().put('id', String.valueOf(objJobHistory.Id));
            Test.setCurrentPage(pageRef);
            
            jobHisCon.save();
            jobHisCon.saveNew();
            jobHisCon.cancel();
            jobHisCon.addressInfo();
            
            
        
        }
        catch(Exception e){
        Test.stopTest();
        }
    }    
    
    public static testmethod void testEmployerAssignment2(){
    
        TransAm_Job_History__c objJobHistory2 = [SELECT ID, TransAm_Employer2__c FROM TransAm_Job_History__c where TransAm_Current_Employer__c='FALSE' LIMIT 1];
         objJobHistory2.TransAm_Employer2__c =null;
         update objJobHistory2;
         ApexPages.StandardController sc2 = new ApexPages.StandardController(objJobHistory2);
         //TransAm_JobHistoryController  objJobs2 = new TransAm_JobHistoryController(sc2);
         //objJobs2.otherEmployer();
         
        
        
    }
    
    public static testmethod void testJobHistoryException() {
        
            List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);  
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
            TransAm_Job_History__c jobList = TransAm_Test_DataUtility.createJobHistory(2, lstApplications[0], lstEmployers[0], false)[0];
           
            Test.startTest();
            
                ApexPages.StandardController sc3 = new ApexPages.StandardController(jobList);
                TransAm_JobHistoryController  jobHisCon= new TransAm_JobHistoryController(sc3);
            Test.stopTest();
         }
    
    
}