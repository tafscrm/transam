/******************************************************************************************
* Create By    :    Suresh M
* Create Date  :    02/14/2016
* Description  :    Handler class for TransAm_ContactTrigger trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_ContactHandler{
   
    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    public static boolean prohibitAfterUndeleteTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static profile LoggedInUserProfile;
   /*******************************************************************************************        
   * Method        :   beforeInsertHandler
   * Description   :   Purpose of this method is to call updateContacts method from TransAm_ContactHelper class.
   * Parameter     :   list<contact>
   * Return Type   :   void
   ******************************************************************************************/
    public static void beforeInsertHandler(list<contact> newContactList){
        TransAm_ContactHelper.populateFieldsForProspect(newContactList);                
        if(prohibitBeforeInsertTrigger){
            return;
        }
        LoggedInUserProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];      
        if(LoggedInUserProfile.Name != 'TransAm_Integration_User'){
           system.debug('preventing duplicates insert::');
           TransAm_ContactHelper.preventDuplicateContactsOnBeforeInsert(newContactList);
        }        
          
        prohibitBeforeInsertTrigger = true;
        TransAm_ContactHelper.populateKeyFieldsOnContactBeforeInsert(newContactList);
        TransAm_ContactHelper.updateContacts(newContactList);   
        TransAm_ContactHelper.populateDummyAccountOnContact(newContactList);
        TransAm_ContactHelper.populateRelatedSchoolOnContact_Insert(newContactList);

        //TransAm_ContactHelper.validateMethod(newContactList,null);
    }
    
    /*******************************************************************************************        
   * Method        :   beforeUpdateHandler
   * Description   :   Purpose of this method is to call updateContacts method from TransAm_ContactHelper class.
   * Parameter     :   list<contact>
   * Return Type   :   void
   ******************************************************************************************/
    public static void beforeUpdateHandler(list<contact> newContactList, Map<id, Contact> oldContactMap){
        
        if(prohibitBeforeUpdateTrigger){
            return;
        }
        
        LoggedInUserProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        if(LoggedInUserProfile.Name != 'TransAm_Integration_User'){
            TransAm_ContactHelper.preventDuplicateContactsOnBeforeUpdate(newContactList, oldContactMap);
        }
        
        prohibitBeforeUpdateTrigger = true;
        TransAm_ContactHelper.updateContacts(newContactList); 
        TransAm_ContactHelper.populateDummyAccountOnContact_Update(newContactList,oldContactMap); 
        //TransAm_ContactHelper.validateMethod(newContactList,null);
        TransAm_ContactHelper.populateKeyFieldsOnContactBeforeUpdate(newContactList, oldContactMap);                
        TransAm_ContactHelper.populateRelatedSchoolOnContact_Update(newContactList, oldContactMap);
                
        
    }
    
    /*******************************************************************************************        
   * Method        :   afterUndeleteHandler
   * Description   :   Purpose of this method is to call updateContacts method from TransAm_ContactHelper class.
   * Parameter     :   list<contact>
   * Return Type   :   void
   ******************************************************************************************/
    public static void afterUndeleteHandler(list<contact> newContactList){
        
        if(prohibitAfterUndeleteTrigger ){
            return;
        }
        
        prohibitAfterUndeleteTrigger = true;
        TransAm_ContactHelper.updateContacts(newContactList);        
    }
    
     /*******************************************************************************************        
   * Method        :   afterInsertHandler
   * Description   :   Purpose of this method is to call Helper class methods from TransAm_ContactHelper class for after insert event.
   * Parameter     :   list<contact>
   * Return Type   :   void
   ******************************************************************************************/
    public static void afterInsertHandler(list<Contact> newContactList, Map<id, Contact> newContactMap, Map<id, Contact> oldContactMap){
        if(prohibitAfterInsertTrigger){
            return;
        }
        prohibitAfterInsertTrigger = true;  
        TransAm_ContactHelper.deleteDuplicateContactsOnAfterInsert(newContactList, newContactMap);       
        //TransAm_ContactHelper.deleteDuplicateDriversAfterInsert(newContactList, newContactMap);
    }
    
    
      /*******************************************************************************************        
   * Method        :   afterUpdateHandler
   * Description   :   Purpose of this method is to call updateContacts method from TransAm_ContactHelper class.
   * Parameter     :   list<contact>
   * Return Type   :   void
   ******************************************************************************************/
   /* public static void afterUpdateHandler(list<Contact> newContactList, Map<id, Contact> newContactMap, Map<id, Contact> oldContactMap){
        if(prohibitAfterUpdateTrigger){
            return;
        }
        prohibitAfterUpdateTrigger = true;
        TransAm_ContactHelper.updateLastName(newContactList);        
    } */
    
}