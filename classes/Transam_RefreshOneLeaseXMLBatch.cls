/******************************************************************************************
* Create By    :     Raushan Anand	
* Create Date  :     09/27/2017
* Description  :     Batch class to regenerate the RVI XML for the one lease.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class Transam_RefreshOneLeaseXMLBatch implements Database.Batchable<sObject> {
    
    global String query;
    global Transam_RefreshOneLeaseXMLBatch(){
        
    }
    global Transam_RefreshOneLeaseXMLBatch(String query){
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(query==null){
            //DateTime todayDateTime=system.now();
            Date dt = Date.today();
            System.Debug(dt);
            query='Select id,RecordType.Name from TransAm_ONE_Leasing__c where Transam_Archive_Date__c =:dt';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<TransAm_ONE_Leasing__c> scope){
        System.debug('Scope-->'+ scope);
        for(TransAm_ONE_Leasing__c objLease : scope){
            Transam_RVIExportForLease.generateXML(objLease.Id, objLease.RecordType.Name);
        }
    }
    global void finish(Database.BatchableContext BC){
    }

}