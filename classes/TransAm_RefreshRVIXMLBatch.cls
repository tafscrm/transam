/******************************************************************************************
* Create By    :     Pranjal Singh
* Create Date  :     05/06/2017
* Description  :     Batch class to regenerate the RVI XML for the applications.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_RefreshRVIXMLBatch implements Database.Batchable<sObject> {
    
   global String query;
    global TransAm_RefreshRVIXMLBatch(){
      
   }
   global TransAm_RefreshRVIXMLBatch(String query){
      this.query=query;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
       if(query==null){
           //DateTime todayDateTime=system.now();
           Date dt = Date.today();
           query='Select id from TransAm_Application__c where TransAm_Archived_Date__c=:dt';
       }
      return Database.getQueryLocator(query);
   }
    
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> scope){
        for(TransAm_Application__c app : scope){
          TransAm_RVIExport.generateXML(app.Id); 
        }
    }
     global void finish(Database.BatchableContext BC){
   }

}