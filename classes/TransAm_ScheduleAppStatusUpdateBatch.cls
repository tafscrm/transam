/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     03/14/2017
* Description  :     Schedule class for TransAm_ApplicationStatusUpdateBatch batch class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_ScheduleAppStatusUpdateBatch implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_ApplicationStatusUpdateBatch());
   }
}