global class UpdateApplicationsForSortOneTimeBatch implements Database.Batchable<sObject> {
    
    /*******************************************************************************************
    * Method        :   start
    * Description   :   This is the start() method of the batch class, used to query 
    *                   and retrieve all Applications where TransAm_Sort_Applications__c=null.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('select id,TransAm_Applicant__c,TransAm_Received_Date__c,TransAm_Sort_Applications__c,TransAm_Primary_Application__c from TransAm_Application__c where TransAm_Sort_Applications__c=null'); //exectue query
    }
   
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   This is the execute method of the batch class, used to process and update the count of attachments under RD
    * Parameter     :   Database.BatchableContext, List<TransAm_Application__c>
    * Return Type   :   Void
    * Modification Log:
    * ----------------------------------------------------------------------------
    *  * Developer                          Date               Description
    *  * ----------------------------------------------------------------------------                 
    *  * Ajay B R                         09/04/2017           Initial version.
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> applications) {
        
        Set<ID> applicants = new Set<ID>();
        Set<TransAm_Application__c> setToBeUpdated   = new Set<TransAm_Application__c>();
        List<TransAm_Application__c> listToBeUpdated   = new List<TransAm_Application__c>();
        Map<Id,List<TransAm_Application__c>> appMap = new Map<Id,List<TransAm_Application__c>>();
        for(TransAm_Application__c app : applications){
            applicants.add(app.TransAm_Applicant__c);
        }
        for(TransAm_Application__c app : [select id,TransAm_Applicant__c,TransAm_Received_Date__c,TransAm_Sort_Applications__c,
                                TransAm_Primary_Application__c from TransAm_Application__c where TransAm_Applicant__c IN : 
                                    applicants order by TransAm_Received_Date__c ]){
            if(appMap.containsKey(app.TransAm_Applicant__c)){
                List<TransAm_Application__c> listApplication = appMap.get(app.TransAm_Applicant__c);
                listApplication.add(app);
                appMap.put(app.TransAm_Applicant__c,listApplication);
            }
            else{
                List<TransAm_Application__c> listApplications = new List<TransAm_Application__c>();
                listApplications.add(app);
                appMap.put(app.TransAm_Applicant__c,listApplications);
            }
        }
        Integer count=0;
        for(Id conId: appMap.KeySet()){
            
            for(TransAm_Application__c app : appMap.get(conId)){
                
                if(app.TransAm_Primary_Application__c){
                    if(count<10){
                        app.TransAm_Sort_Applications__c='M'+'0'+count;
                    }
                    else{
                        app.TransAm_Sort_Applications__c='M'+count;
                    }
                }
                else{
                    if(count<10){
                        app.TransAm_Sort_Applications__c='C'+'0'+count;
                    }
                    else{
                        app.TransAm_Sort_Applications__c='C'+count;
                    }
                }
                setToBeUpdated.add(app);
                count++;
            }
            count=0;
        }
        listToBeUpdated.addAll(setToBeUpdated);
        try{
            for(TransAm_Application__c app : listToBeUpdated){
                app.TransAm_Update_child_app_flag__c=TRUE;
            }
            update listToBeUpdated;
        }
        catch(Exception e){
        
            TransAm_Error_Log__c el = new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_ApplicationHelper',TransAm_Method_Name__c = 'sortApplicationRealtedList',TransAm_Module_Name__c = 'Application',TransAm_Exception_Message__c = e.getMessage());
            insert el;
        }
    }    

    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Finish() method of the batch class,generally used to implement post commit logics.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
    //completed
    }
}