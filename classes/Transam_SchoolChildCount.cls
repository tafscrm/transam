/******************************************************************************************
* Create By    :     Raushan Anand
* Create Date  :     04/06/2017
* Description  :     Batch Class to delete archived attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Raushan Anand                    04/06/2017           Initial version.
*****************************************************************************************/
global class Transam_SchoolChildCount implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC){
		String query = 'SELECT Id,Transam_Total_Applicants__c,Transam_Total_Drivers__c,Transam_Total_Prospects__c,Transam_Current_Active_Drivers__c FROM Account';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<Account> schools = (List<Account>)scope;
        Map<Id,INTEGER> prospectMap = new Map<Id,INTEGER>();
        Map<Id,INTEGER> applicantMap = new Map<Id,INTEGER>();
        Map<Id,INTEGER> driverMap = new Map<Id,INTEGER>();
        Map<Id,INTEGER> activeDriverMap = new Map<Id,INTEGER>();
        for(Account acc : schools){
            prospectMap.put(acc.Id,0);
            applicantMap.put(acc.Id,0);
            driverMap.put(acc.Id,0);
            activeDriverMap.put(acc.Id,0);
        }
        Date last2Year = Date.today().addYears(-2);
        List<Contact> contactList = [Select Id,RecordType.Name,TransAm_Driver_Status__c,AccountId from Contact where AccountId IN: schools AND LASTMODIFIEDDATE >: last2Year];
        for(Contact objCon : contactList){
            if(objCon.RecordType.Name.equalsIgnoreCase('Prospect')){
                prospectMap.put(objCon.AccountId,prospectMap.get(objCon.AccountId)+1);
            }
            if(objCon.RecordType.Name.equalsIgnoreCase('Applicant')){
                applicantMap.put(objCon.AccountId,applicantMap.get(objCon.AccountId)+1);
            }
            if(objCon.RecordType.Name.equalsIgnoreCase('Driver') && objCon.TransAm_Driver_Status__c == 'Hired'){
                activeDriverMap.put(objCon.AccountId,activeDriverMap.get(objCon.AccountId)+1);
            }
            if(objCon.RecordType.Name.equalsIgnoreCase('Driver')){
                driverMap.put(objCon.AccountId,driverMap.get(objCon.AccountId)+1);
            }
            
        }
        for(Account acc : schools){
            acc.Transam_Total_Prospects__c = prospectMap.get(acc.Id);
            acc.Transam_Total_Applicants__c = applicantMap.get(acc.Id);
            acc.Transam_Total_Drivers__c = driverMap.get(acc.Id);
            acc.Transam_Current_Active_Drivers__c = activeDriverMap.get(acc.Id);
        }
        Database.update(schools);
    }
    global void finish(Database.BatchableContext BC){
        
    }
}