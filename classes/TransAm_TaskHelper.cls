/******************************************************************************************
* Class Name   :     TransAm_TaskHelper
* Created By   :     Ajay B R
* Create Date  :     08/16/2017
* Description  :     Task Helper class contains the logics that will be executed in Task Trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                         08/16/2017           Initial version.
*****************************************************************************************/
public class TransAm_TaskHelper{
    
    public static Map<String, TransAm_Generic_Custom_Setting__c> genericCustomSettingsMap; 
    public static String taskType;
    public static String taskStatusOpen;
    public static String taskStatusCompleted;
    public static String taskRecTypeNonSchool;
    public static String appObjAPIName;
    public static Id taskRecTypeId;
    
    /*******************************************************************************************
    * Method        :   getCustomSettingsData()
    * Description   :   This method will retreive the custom setting dataset
    * Parameter     :   nill
    * Return Type   :   void
    ******************************************************************************************/
    public static void getCustomSettingsData(){
        genericCustomSettingsMap = TransAm_Generic_Custom_Setting__c.getAll();
        if(genericCustomSettingsMap!=null){
            if(genericCustomSettingsMap.containsKey('TaskRecordTypeNonSchool'))
                taskRecTypeNonSchool = genericCustomSettingsMap.get('TaskRecordTypeNonSchool').TransAm_Message_String__c;
            if(genericCustomSettingsMap.containsKey('TaskStatusOpen'))
                taskStatusOpen = genericCustomSettingsMap.get('TaskStatusOpen').TransAm_Message_String__c;
            if(genericCustomSettingsMap.containsKey('TaskTypeFollowUp'))
                taskType = genericCustomSettingsMap.get('TaskTypeFollowUp').TransAm_Message_String__c;
            if(genericCustomSettingsMap.containsKey('TaskStatusCompleted'))
                taskStatusCompleted = genericCustomSettingsMap.get('TaskStatusCompleted').TransAm_Message_String__c;
            if(genericCustomSettingsMap.containsKey('AppObjAPIName'))
                appObjAPIName = genericCustomSettingsMap.get('AppObjAPIName').TransAm_Message_String__c;       
        }
    }
    
    /*******************************************************************************************
    * Method        :   getTaskRecordTypeID()
    * Description   :   This method will retreive the Task RecordTypeID
    * Parameter     :   nill
    * Return Type   :   void
    ******************************************************************************************/
    public static void getTaskRecordTypeID(){
        if(String.isNotBlank(taskRecTypeNonSchool))    
            taskRecTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(taskRecTypeNonSchool).getRecordTypeId();
    }
    
    /*******************************************************************************************
    * Method        :   populateFollowUpDateOnApplication_Insert()
    * Description   :   This method has the logics to populate the Task Due date on Application Follow-Up Date
    *                   On inserting a new Task, if the task record Type is Non-School and type is Follow-Up
    * Parameter     :   List<Task>
    * Return Type   :   void
    ******************************************************************************************/
    public static void populateFollowUpDateOnApplication_Insert(list<Task> taskList){
        Set<Id> appIdSet = new Set<Id>();
        Set<Id> recurrenceTaskAppIdSet = new Set<Id>();
        List<Task> existingTaskList = new List<Task>();
        List<Task> newTaskList = new List<Task>();
        Set<Transam_Application__C> appsListToBeUpdated = new Set<Transam_Application__C>();
        Map<Id, List<Task>> mapAppIdTaskList = new Map<Id, List<Task>>();
        Map<Id, List<Task>> mapAppIdRecTaskList = new Map<Id, List<Task>>();
        List<TransAm_Application__c> finalListAppsToBeUpdated = new List<TransAm_Application__c >();
        List<Task> listRecurrenceTask = new List<Task>();
        List<Task> existingRecTaskList = new List<Task>();
        
        getCustomSettingsData();
        if(genericCustomSettingsMap!=null){
            getTaskRecordTypeID();
            
            for(Task taskObj : taskList){
                if(taskObj.recordTypeId != null && taskObj.recordTypeId == taskRecTypeId){
                    if(taskObj.whatId != null && String.valueOf(taskObj.whatId).startsWith(TransAm_Application__C.sobjecttype.getDescribe().getKeyPrefix())){
                        if(taskObj.type!= null && String.isNotBlank(taskType) && taskObj.type == taskType){
                            if(taskObj.isRecurrence){
                                recurrenceTaskAppIdSet.add(taskObj.whatId);
                                listRecurrenceTask.add(taskObj);
                            }
                            else{
                                appIdSet.add(taskObj.whatId);
                                newTaskList.add(taskObj);
                            }
                        }
                    }
                }
            }
            System.debug('*** recordType: '+taskRecTypeId );
            System.debug('*** appIDSet: '+appIdSet);
            System.debug('*** newTaskList: '+newTaskList);
            if(!appIdSet.isEmpty() && String.isNotBlank(appObjAPIName ) && String.isNotBlank(taskType) && String.isNotBlank(taskStatusOpen)){
                existingTaskList = [SELECT Id, type, recordTypeId, Subject, whatId, activitydate, status FROM TASK 
                                       WHERE What.Type = :appObjAPIName and WhatId IN :appIdSet and 
                                       recordTypeId = :taskRecTypeId and type = :taskType and status = :taskStatusOpen 
                                       and isRecurrence = false order by activitydate asc];
                                       
                if(!existingTaskList.isEmpty()){
                    System.debug('*** existingTaskList: '+existingTaskList);
                    for(Task taskObj : existingTaskList){
                        if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(taskObj.WhatId)){
                            mapAppIdTaskList.get(taskObj.WhatId).add(taskObj);
                        }
                        else{
                            List<Task> appTaskList = new List<Task>();
                            appTaskList.add(taskObj);
                            mapAppIdTaskList.put(taskObj.WhatId, appTaskList);
                        }
                    }
                }
            }
            System.debug('*** mapAppIdTaskList: '+mapAppIdTaskList);
            
            if(!recurrenceTaskAppIdSet.isEmpty() && String.isNotBlank(appObjAPIName ) && String.isNotBlank(taskType) && String.isNotBlank(taskStatusOpen)){
                existingRecTaskList = [SELECT Id, type, recordTypeId, Subject, whatId, activitydate, status FROM TASK 
                                       WHERE What.Type = :appObjAPIName and WhatId IN :appIdSet and 
                                       recordTypeId = :taskRecTypeId and type = :taskType and status = :taskStatusOpen 
                                       and isRecurrence = true order by activitydate asc];
                if(!existingRecTaskList.isEmpty()){
                    for(Task taskObj : existingRecTaskList){
                        if(!mapAppIdRecTaskList.isEmpty() && mapAppIdRecTaskList.containsKey(taskObj.WhatId)){
                            mapAppIdRecTaskList.get(taskObj.WhatId).add(taskObj);
                        }
                        else{
                            List<Task> appTaskList = new List<Task>();
                            appTaskList.add(taskObj);
                            mapAppIdRecTaskList.put(taskObj.WhatId, appTaskList);
                        }
                    }
                    
                }
            }
            
            if(!newTaskList.isEmpty()){
                for(Task taskObj : newTaskList){
                    Transam_Application__C appObj = new Transam_Application__C(id = taskObj.WhatId);
                    if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(taskObj.WhatId)){
                        if(mapAppIdTaskList.get(taskObj.WhatId).size() > 0){
                            if(taskObj.ActivityDate <= mapAppIdTaskList.get(taskObj.WhatId)[0].ActivityDate){
                                appObj.TransAm_Follow_Up_Date__c = taskObj.ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(taskObj.WhatId)[0].ActivityDate;
                            }
                        }
                    }
                    else{
                        appObj.TransAm_Follow_Up_Date__c = taskObj.ActivityDate;
                    }
                    if(appObj != null){
                        appsListToBeUpdated.add(appObj);
                    }
                }
            }
            System.debug('*** appsListToBeUpdated: '+appsListToBeUpdated);
            
            if(!listRecurrenceTask.isEmpty()){
                
                Transam_Application__C appObj = new Transam_Application__C(id = listRecurrenceTask[0].WhatId);
                    if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            if(listRecurrenceTask[0].ActivityDate <= mapAppIdTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate){
                                appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                            }
                        }
                    }
                    if(!mapAppIdRecTaskList.isEmpty() && mapAppIdRecTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            if(listRecurrenceTask[0].ActivityDate <= mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate){
                                appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                            }
                        }
                    }
                    else{
                        appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                    }
                    if(appObj != null){
                        try{
                            update appObj;
                        }catch(System.dmlException dmlExcpn){
                            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                                    el.TransAm_Class_Name__c = 'TransAm_TaskHelper';
                                    el.TransAm_Method_Name__c = 'populateFollowUpDateOnApplication_Insert';
                                    el.TransAm_Module_Name__c = 'Task';
                                    el.TransAm_Exception_Message__c = dmlExcpn.getMessage();
                                    el.TransAm_Line_Number__c = dmlExcpn.getLineNumber();
                                    insert el;  //insert error log instance
                        }
                    }
            }
            
            if(!appsListToBeUpdated.isEmpty()){
                for(TransAm_Application__C appObj : appsListToBeUpdated){
                    finalListAppsToBeUpdated.add(appObj);
                }
                if(!finalListAppsToBeUpdated.isEmpty()){
                    Database.SaveResult[] updateResults = Database.update(finalListAppsToBeUpdated, false);
                    //for every result captured
                    for (Database.SaveResult sr : updateResults) {
                        //check if it is a failure
                        if (!sr.isSuccess()){
                            for(Database.Error err : sr.getErrors()) {
                                //create an error log instance
                                TransAm_Error_Log__c el = new TransAm_Error_Log__c();el.TransAm_Class_Name__c = 'TransAm_TaskHelper';el.TransAm_Method_Name__c = 'populateFollowUpDateOnApplication';el.TransAm_Module_Name__c = 'Task';el.TransAm_Exception_Message__c = 'Error Record ID: '+ sr.getID() + '; -- Error Fields: ' + err.getFields() + '; -- Error Message: ' + err.getMessage();insert el;  //insert error log instance
                            }
                        }
                    }
                }
            }
        }
   }
   
   /*******************************************************************************************
    * Method        :   populateFollowUpDateOnApplication_Update()
    * Description   :   This method has the logics to populate the Task Due date on Application Follow-Up Date
    *                   On Updating a Task, if the task record Type is Non-School and type is Follow-Up
    * Parameter     :   List<Task>, Map<ID, Task>
    * Return Type   :   void
    ******************************************************************************************/
   public static void populateFollowUpDateOnApplication_Update(list<Task> taskList, Map<Id, Task> taskOldMap){
        Set<Id> appIdSet = new Set<Id>();
        Set<Id> recurrenceTaskAppIdSet = new Set<Id>();
        List<Task> existingTaskList = new List<Task>();
        List<Task> newTaskList = new List<Task>();
        Set<Transam_Application__C> appsListToBeUpdated = new Set<Transam_Application__C>();
        Map<Id, List<Task>> mapAppIdRecTaskList = new Map<Id, List<Task>>();
        Map<Id, List<Task>> mapAppIdTaskList = new Map<Id, List<Task>>();
        List<TransAm_Application__c> finalListAppsToBeUpdated = new List<TransAm_Application__c >();
        List<Task> listRecurrenceTask = new List<Task>();
        List<Task> existingRecTaskList = new List<Task>();
        
        getCustomSettingsData();
        if(genericCustomSettingsMap!=null){
            getTaskRecordTypeID();
            
            for(Task taskObj : taskList){
                if(taskObj.recordTypeId != null & taskObj.recordTypeId == taskRecTypeId){
                    if(taskObj.whatId != null && String.valueOf(taskObj.whatId).startsWith(TransAm_Application__C.sobjecttype.getDescribe().getKeyPrefix())){
                        if(taskObj.type!= null && String.isNotBlank(taskType) && taskObj.type == taskType){
                            if(taskObj.ActivityDate != taskOldMap.get(taskObj.id).ActivityDate || taskObj.status != taskOldMap.get(taskObj.id).status){
                                if(taskObj.isRecurrence){
                                    recurrenceTaskAppIdSet.add(taskObj.whatId);
                                    listRecurrenceTask.add(taskObj);
                                }
                                else{
                                    appIdSet.add(taskObj.whatId);
                                    newTaskList.add(taskObj);
                                }
                            }
                        }
                    }
                }
            }
            if(!appIdSet.isEmpty() && String.isNotBlank(appObjAPIName ) && String.isNotBlank(taskType) && String.isNotBlank(taskStatusOpen)){
                    existingTaskList = [SELECT Id, type, recordTypeId, Subject, whatId, activitydate, status FROM TASK 
                                           WHERE What.Type = :appObjAPIName and WhatId IN :appIdSet and 
                                           recordTypeId = :taskRecTypeId and type = :taskType and status = :taskStatusOpen 
                                           and isRecurrence = false order by activitydate asc];
                                       
                if(!existingTaskList.isEmpty()){
                    System.debug('*** existingTaskList: '+existingTaskList);
                    for(Task taskObj : existingTaskList){
                        if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(taskObj.WhatId)){
                            mapAppIdTaskList.get(taskObj.WhatId).add(taskObj);
                        }
                        else{
                            List<Task> appTaskList = new List<Task>();
                            appTaskList.add(taskObj);
                            mapAppIdTaskList.put(taskObj.WhatId, appTaskList);
                        }
                    }
                }
            }
            
            if(!recurrenceTaskAppIdSet.isEmpty() && String.isNotBlank(appObjAPIName ) && String.isNotBlank(taskType) && String.isNotBlank(taskStatusOpen)){
                existingRecTaskList = [SELECT Id, type, recordTypeId, Subject, whatId, activitydate, status FROM TASK 
                                       WHERE What.Type = :appObjAPIName and WhatId IN :appIdSet and 
                                       recordTypeId = :taskRecTypeId and type = :taskType and status = :taskStatusOpen 
                                       and isRecurrence = true order by activitydate asc];
                if(!existingRecTaskList.isEmpty()){
                    for(Task taskObj : existingRecTaskList){
                        if(!mapAppIdRecTaskList.isEmpty() && mapAppIdRecTaskList.containsKey(taskObj.WhatId)){
                            mapAppIdRecTaskList.get(taskObj.WhatId).add(taskObj);
                        }
                        else{
                            List<Task> appTaskList = new List<Task>();
                            appTaskList.add(taskObj);
                            mapAppIdRecTaskList.put(taskObj.WhatId, appTaskList);
                        }
                    }
                    
                }
            }
            
            if(!newTaskList.isEmpty()){
                for(Task taskObj : newTaskList){
                    Transam_Application__C appObj = new Transam_Application__C(id = taskObj.WhatId);
                    if(taskObj.ActivityDate != taskOldMap.get(taskObj.id).ActivityDate){
                        if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(taskObj.WhatId)){
                            if(mapAppIdTaskList.get(taskObj.WhatId).size() > 0){
                                if(taskObj.ActivityDate <= mapAppIdTaskList.get(taskObj.WhatId)[0].ActivityDate){
                                    appObj.TransAm_Follow_Up_Date__c = taskObj.ActivityDate;
                                }
                                else{
                                    appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(taskObj.WhatId)[0].ActivityDate;
                                }
                            }
                        }
                        else{
                            appObj.TransAm_Follow_Up_Date__c = taskObj.ActivityDate;
                        }
                    }
                    else if(String.isNotBlank(taskStatusCompleted) && taskObj.status == taskStatusCompleted){
                        if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(taskObj.WhatId)){
                            if(mapAppIdTaskList.get(taskObj.WhatId).size() > 0){
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(taskObj.WhatId)[0].ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = null;
                            }
                        }
                        else{
                            appObj.TransAm_Follow_Up_Date__c = null;
                        }
                    }
                    if(appObj != null){
                        appsListToBeUpdated.add(appObj);
                    }
                }
            }
            
            System.debug('*** appsListToBeUpdated: '+appsListToBeUpdated);
            
            if(!listRecurrenceTask.isEmpty()){
                Transam_Application__C appObj = new Transam_Application__C(id = listRecurrenceTask[0].WhatId);
                if(listRecurrenceTask[0].ActivityDate != taskOldMap.get(listRecurrenceTask[0].id).ActivityDate){
                    if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            if(listRecurrenceTask[0].ActivityDate <= mapAppIdTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate){
                                appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                            }
                        }
                    }
                    if(!mapAppIdRecTaskList.isEmpty() && mapAppIdRecTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            if(listRecurrenceTask[0].ActivityDate <= mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate){
                                appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                            }
                            else{
                                appObj.TransAm_Follow_Up_Date__c = mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                            }
                        }
                    }
                    else{
                        appObj.TransAm_Follow_Up_Date__c = listRecurrenceTask[0].ActivityDate;
                    }
                }
                else if(String.isNotBlank(taskStatusCompleted) && listRecurrenceTask[0].status == taskStatusCompleted){
                    if(!mapAppIdTaskList.isEmpty() && mapAppIdTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            appObj.TransAm_Follow_Up_Date__c = mapAppIdTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                        }
                        else{
                            appObj.TransAm_Follow_Up_Date__c = null;
                        }
                    }
                    if(!mapAppIdRecTaskList.isEmpty() && mapAppIdRecTaskList.containsKey(listRecurrenceTask[0].WhatId)){
                        if(mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId).size() > 0){
                            appObj.TransAm_Follow_Up_Date__c = mapAppIdRecTaskList.get(listRecurrenceTask[0].WhatId)[0].ActivityDate;
                        }
                        else{
                            appObj.TransAm_Follow_Up_Date__c = null;
                        }
                    }
                    else{
                        appObj.TransAm_Follow_Up_Date__c = null;
                    }
                }
                if(appObj != null){
                    try{
                        update appObj;
                    }catch(System.dmlException dmlExcpn){
                        TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                                el.TransAm_Class_Name__c = 'TransAm_TaskHelper';
                                el.TransAm_Method_Name__c = 'populateFollowUpDateOnApplication_Insert';
                                el.TransAm_Module_Name__c = 'Task';
                                el.TransAm_Exception_Message__c = dmlExcpn.getMessage();
                                el.TransAm_Line_Number__c = dmlExcpn.getLineNumber();
                                insert el;  //insert error log instance
                    }
                }
            }
            
            if(!appsListToBeUpdated.isEmpty()){
                for(TransAm_Application__c appObj : appsListToBeUpdated ){
                    finalListAppsToBeUpdated.add(appObj);
                }
                if(!finalListAppsToBeUpdated.isEmpty()){
                    Database.SaveResult[] updateResults = Database.update(finalListAppsToBeUpdated , false);
                    //for every result captured
                    for (Database.SaveResult sr : updateResults) {
                        //check if it is a failure
                        if (!sr.isSuccess()){
                            for(Database.Error err : sr.getErrors()) {
                                //create an error log instance
                                TransAm_Error_Log__c el = new TransAm_Error_Log__c();el.TransAm_Class_Name__c = 'TransAm_TaskHelper';el.TransAm_Method_Name__c = 'populateFollowUpDateOnApplication';el.TransAm_Module_Name__c = 'Task';el.TransAm_Exception_Message__c = 'Error Record ID: '+ sr.getID() + '; -- Error Fields: ' + err.getFields() + '; -- Error Message: ' + err.getMessage();insert el;  //insert error log instance
                            }
                        }
                    }
                }
            }
        }
    }
    /*******************************************************************************************
    * Method        :   populateContactOnTask_Insert()
    * Description   :   This method has the logics to populate the contact on Task that is created 
    *                   under a master application.
    * Parameter     :   List<Task>
    * Return Type   :   void
    ******************************************************************************************/
    public static void populateContactOnTask_Insert(list<Task> taskList){
    
        Set<Id> appIdSet = new Set<Id>();
        List<Task> newTaskList = new List<Task>();
        Map<ID,ID> appIdContactIdMap = new Map<ID,ID>();
        Map<ID, String> appIDConNameMap = new Map<ID, String>();
        map<Id, list<Task>> appTaskMap = new map<Id, list<Task>>();
        List<Task> taskToBeUpdatedList = new List<Task>();
        set<Id> taskIdSet = new set<Id>();
        String appPrefix = TransAm_Application__C.sobjecttype.getDescribe().getKeyPrefix();
        for(Task taskObj : [SELECT ID,Whoid, WhatId FROM Task WHERE ID IN:taskList]){           
            if(taskObj.whatId != null && String.valueOf(taskObj.whatId).startsWith(appPrefix)){
                newTaskList.add(taskObj);
                if(appTaskMap.containskey(taskObj.whatId)){
                    appTaskMap.get(taskObj.WhatId).add(taskObj);
                }
                else{
                    List<Task> appTaskList = new List<Task>();
                    appTaskList.add(taskObj);
                    appTaskMap.put(taskObj.WhatId, appTaskList);
                }                
            }
        }
        for(TransAm_Application__C appObj : [Select ID, TransAm_Applicant__c, TransAm_Primary_Application__c from TransAm_Application__C 
                                             where ID IN :appTaskMap.keyset() and TransAm_Primary_Application__c = True]){
            if(appTaskMap.containsKey(appObj.Id)){
                for(Task taskObj : appTaskMap.get(appObj.Id)){
                    taskObj.whoId = appObj.TransAm_Applicant__c;
                    taskToBeUpdatedList.add(taskObj);
                }                
            }
        }
        try{
            update taskToBeUpdatedList;
        }catch(System.dmlException dmlExcpn){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                    el.TransAm_Class_Name__c = 'TransAm_TaskHelper';
                    el.TransAm_Method_Name__c = 'populateContactOnTask_Insert';
                    el.TransAm_Module_Name__c = 'Task';
                    el.TransAm_Exception_Message__c = dmlExcpn.getMessage();
                    el.TransAm_Line_Number__c = dmlExcpn.getLineNumber();
                    insert el;  //insert error log instance
        }
    }
}