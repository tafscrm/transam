/* Class Name   : TransAm_Leasing_DocuSignUtility 
 * Description  : Utility class for sending lease documents to drivers associated with application
 * Created By   : Karthik Gulla
 * Created On   : 09-Aug-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               09-Aug-2017             Initial version.
 *
 *****************************************************************************************/
public with sharing class TransAm_Leasing_DocuSignUtility {
    public string leaseId           { get; set; }
    public string applicationId     { get; set; }
    public String packetType        { get; set; }
    private string recipientList = '';
    private decimal routingNo = 0;
    
    public TransAm_Leasing_DocuSignUtility(){
        leaseId = ApexPages.currentPage().getParameters().get('Id');
        applicationId = ApexPages.currentPage().getParameters().get('appId');
        packetType  = ApexPages.currentPage().getParameters().get('packetType');
    }
    public PageReference sendLeasingDocsToDocuSign(){
        TransAm_Application__c applicationObj;
        applicationObj = [Select Id, Owner.Name, TransAm_Driver_Type__c, TransAm_State__c, TransAm_First_Name__c, Owner.Email,TransAm_Last_Name__c,TransAm_Email__c from TransAm_Application__c WHERE ID =: applicationId];        
    
        string templateId = '';
        string RC = '';
        string RSL='';
        string RSRO='';
        string RROS='';
        string CCRM='';
        string CCTM='';
        string CCNM='';
        string CRCL=''; 
        string CRL='';
        string OCO='';
        string DST='';
        string LA='';
        string CEM='';
        string CES='';
        string STB='';
        string SSB='';
        string SES='';
        string SEM='';string SRS='';string SCS ='';string RES=''; 
        Organization orgDetails = [SELECT IsSandbox FROM Organization LIMIT 1];
        Map<String,TransAm_DocuSign_Template_Mappings__mdt> mapDocusignTemplateMappings = new Map<String,TransAm_DocuSign_Template_Mappings__mdt>();
        List<TransAm_DocuSign_Template_Mappings__mdt> lstDSTemplateMappings = [SELECT MasterLabel, TransAm_Docusign_TemplateId__c, TransAm_Is_Sandbox__c FROM TransAm_DocuSign_Template_Mappings__mdt WHERE TransAm_Is_Sandbox__c =:orgDetails.IsSandbox];
        
        for(TransAm_DocuSign_Template_Mappings__mdt dsMappings:lstDSTemplateMappings){
            String strMasterLabel = dsMappings.MasterLabel;
            if(orgDetails.IsSandbox)
                strMasterLabel = strMasterLabel.replace('_Demo','');
            mapDocusignTemplateMappings.put(strMasterLabel, dsMappings);       
        }
        
        recipientList = recipientList +'Email~'+applicationObj.TransAm_Email__c+';FirstName~'+applicationObj.TransAm_First_Name__c+';LastName~'+applicationObj.TransAm_Last_Name__c+';Role~Signer1;RoutingOrder~1;';
        Id userId = UserInfo.getUserId();
        User activeUser = new User();
        if(userId!=null){
            activeUser = [Select Id, Email, FirstName, LastName From User where Id = : userId LIMIT 1];
        }
        if(activeUser.Email != null && activeUser.FirstName != null && activeUser.LastName != null)
            recipientList = recipientList +','+'Email~'+activeUser.Email+';FirstName~'+activeUser.FirstName+';LastName~'+activeUser.LastName+';Role~Signer2;RoutingOrder~2';

        if(packetType == 'LeasingIC' && mapDocusignTemplateMappings.containsKey('LeasingIC')){
            templateId = mapDocusignTemplateMappings.get('LeasingIC').TransAm_Docusign_TemplateId__c;
            
        }else if(packetType == 'LeasingELA' && mapDocusignTemplateMappings.containsKey('LeasingELA')){
            templateId = mapDocusignTemplateMappings.get('LeasingELA').TransAm_Docusign_TemplateId__c;
        }

        //Adding Notes & Attachments 
        LA='0'; 
        CRL= recipientList + 'LoadDefaultContacts~0';
        
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+leaseId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+templateId+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}