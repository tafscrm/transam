/******************************************************************************************
* Create By    :     Karthik Gulla
* Create Date  :     08/10/2017
* Description  :     Helper class for TransAm_ONELeasing_Trigger
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                      08/10/2017         Initial version.
*****************************************************************************************/

public with sharing class TransAm_ONELeasing_Helper{
    /*******************************************************************************************
    * Method        :   createLeasingDocuments
    * Description   :   Method to create Leasing Document records when Lease is created.
    * Parameter     :   list<TransAm_ONE_Leasing__c>
    * Return Type   :   void
    ******************************************************************************************/
    public static void createLeasingDocuments(List<TransAm_ONE_Leasing__c> newONELeasingList){
        Map<Id,Id> mapLeaseAppIds = new Map<Id,Id>();

        List<String> pickvalsList = new List<String>();
        List<TransAm_ONE_Leasing_Document__c> newLeasingDocsList = new list<TransAm_ONE_Leasing_Document__c>();
        String statusLabel;

        Schema.DescribeFieldResult fieldResult = TransAm_ONE_Leasing_Document__c.TransAm_Lease_Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();            
         
        for(Schema.PicklistEntry f : pickListValues){
            pickvalsList.add(f.getValue());
        }

        List<Id> lstNewLeaseIds = new List<Id>();
        for(TransAm_ONE_Leasing__c newTLease : newONELeasingList){
            lstNewLeaseIds.add(newTLease.Id);
        }
        
        for(TransAm_ONE_Leasing__c newLease : [SELECT RecordType.Name, Id, Name, TransAm_Application_IC__c, TransAm_Application_IC__r.TransAm_One_Leasing_Status__c, TransAm_Application_ELA__c,TransAm_Application_ELA__r.TransAm_One_Leasing_Status__c FROM TransAm_ONE_Leasing__c WHERE Id In :lstNewLeaseIds]){
            String appAPIName;
            System.debug('### newLease.RecordType.Name ###'+newLease.RecordType.Name);
            if(newLease.RecordType.Name == 'Lease Agreement')
                appAPIName = 'TransAm_Application_ELA__c';
            else if (newLease.RecordType.Name == 'IC Agreement')
                appAPIName = 'TransAm_Application_IC__c'; 
            if(newLease.get(appAPIName) != null){
                mapLeaseAppIds.put(newLease.Id, (Id)newLease.get(appAPIName));
            }
        }

        Map<Id, TransAm_Application__c> mapApps = new Map<Id, TransAm_Application__c>([SELECT Id, TransAm_One_Leasing_Status__c, TransAm_Email__c,TransAm_Phone__c, TransAm_SSN__c, TransAm_First_Name__c, TransAm_Last_Name__c  FROM TransAm_Application__c WHERE Id In :mapLeaseAppIds.values()]); 
        
        for(TransAm_ONE_Leasing__c newOneLease : newONELeasingList){
            for(String doctype : pickvalsList){
                if(mapLeaseAppIds.containsKey(newOneLease.Id) && mapApps.containsKey(mapLeaseAppIds.get(newOneLease.Id))){             
                    TransAm_ONE_Leasing_Document__c leasingDoc = new TransAm_ONE_Leasing_Document__c();                   
                    leasingDoc.Name = newOneLease.Name + ' ' + doctype;
                    leasingDoc.TransAm_Lease_Document_Type__c = doctype;
                    leasingDoc.TransAm_Lease__c = newOneLease.Id;
                    leasingDoc.TransAm_Email__c = mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_Email__c;
                    leasingDoc.TransAm_Phone__c = mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_Phone__c;
                    leasingDoc.TransAm_SSN__c = mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_SSN__c;
                    if(!string.isBlank(mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_First_Name__c)){
                        leasingDoc.TransAm_Lease_Applicant_Name__c = mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_First_Name__c+' ' + mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_Last_Name__c;
                    } else {
                        leasingDoc.TransAm_Lease_Applicant_Name__c = mapApps.get(mapLeaseAppIds.get(newOneLease.Id)).TransAm_Last_Name__c;
                    }                   
                    newLeasingDocsList.add(leasingDoc);
                }
            }
        }

        System.debug('## newLeasingDocsList size is:'+newLeasingDocsList.size());
        if(newLeasingDocsList.size() > 0){
            try{
                Database.insert(newLeasingDocsList);
            } catch(exception e){
                system.debug('##Error occured:'+e.getMessage());
                  
                TransAm_Error_Log__c el = new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_ONELeasing_Helper', TransAm_Method_Name__c = 'createLeasingDocuments',TransAm_Module_Name__c = 'Leasing',TransAm_Exception_Message__c = e.getMessage());
                insert el;
            }
       }
   }

  /*******************************************************************************************
    * Method        :   updateInsuranceDetails
    * Description   :   Method to update Insurance details once 
    * Parameter     :   list<TransAm_ONE_Leasing__c>
    * Return Type   :   void
    ******************************************************************************************/
  public static void updateInsuranceDetails(List<TransAm_ONE_Leasing__c> newLeasingList, Map<Id, TransAm_ONE_Leasing__c> oldLeasingMap, Boolean flag){
      Set<String> setInsuranceFieldDetails = new Set<String>();
      setInsuranceFieldDetails.add('TransAm_DeadheadBobtail_Declaration__c'+';'+'TransAm_NonTruckingInsurance__c'+';'+'TransAm_DeadheadInsuranceOptionDate__c'+';'+'TransAm_InsDHB_Declaration__c'+';');
      setInsuranceFieldDetails.add('TransAm_PhysicalDamage_Declaration__c'+';'+'TransAm_Physical_Damage_Insurance__c'+';'+'TransAm_PhysicalDamageInsuranceOptinDate__c'+';'+'TransAm_InsPDI_Declaration__c'+';');
      setInsuranceFieldDetails.add('TransAm_OccupationalAccident_Declaration__c'+';'+'TransAm_OccupationalAccidentInsurance__c'+';'+'TransAm_OccAccInsuranceOptInDate__c'+';'+'TransAm_InsOAI_Declaration__c'+';');
      setInsuranceFieldDetails.add('TransAm_DeductibleBuydown_Declaration__c'+';'+'TransAm_Deductible_Buydown__c'+';'+'TransAm_DeductibleBuydownInsurance_Date__c'+';'+'TransAm_InsDB_Declaration__c'+';');
      
      Set<String> setOptions = new Set<String>();
      setOptions.add('TransAm_PrePassInsurance_Declaration__c'+';'+'TransAm_Prepass__c'+';'+'TransAm_Prepass_Insurance_Opt_in_Date__c'+';');
      setOptions.add('TransAm_FuelOptimization_Declaration__c'+';'+'TransAm_Fuel_Optimization__c'+';'+'TransAm_FuelOptimizationInsurance_Date__c'+';');
      setOptions.add('TransAm_Breakdown_Declaration__c'+';'+'TransAm_Breakdown_Insurance__c'+';'+'TransAm_Breakdown_Insurance_Opt_in_Date__c'+';');

      Map<String,String> mapAdditionalInsFormDetails = new Map<String,String>();
      mapAdditionalInsFormDetails.put('TransAm_InsDHB_Declaration__c','TransAm_DeadheadBobtail_Declaration__c');
      mapAdditionalInsFormDetails.put('TransAm_InsPDI_Declaration__c','TransAm_PhysicalDamage_Declaration__c');
      mapAdditionalInsFormDetails.put('TransAm_InsOAI_Declaration__c','TransAm_OccupationalAccident_Declaration__c');
      mapAdditionalInsFormDetails.put('TransAm_InsDB_Declaration__c','TransAm_DeductibleBuydown_Declaration__c');
      mapAdditionalInsFormDetails.put('TransAm_InsLS_Declaration__c','TransAm_Lifestyle__c');

      Map<String,List<String>> mapInsDetails = new Map<String,List<String>>();
      for(String ins:setInsuranceFieldDetails){
          List<String> lstInsFields = ins.split(';');
          mapInsDetails.put(lstInsFields[0],lstInsFields);
          mapInsDetails.get(lstInsFields[0]).remove(0);
      }

      Map<String,List<String>> mapOptDetails = new Map<String,List<String>>();
      for(String opt:setOptions){
          List<String> lstOptFields = opt.split(';');
          mapOptDetails.put(lstOptFields[0],lstOptFields);
          mapOptDetails.get(lstOptFields[0]).remove(0);
      }
      
      List<Id> lstLeaseIds = new List<Id>(); 
      for(TransAm_ONE_Leasing__c nLease:newLeasingList){
          lstLeaseIds.add(nLease.Id);
      }
      List<TransAm_ONE_Leasing__c> lstUpdatedLeasingList = [SELECT TransAm_DeadheadBobtail_Declaration__c, TransAm_PhysicalDamage_Declaration__c, TransAm_FuelOptimization_Declaration__c,
                                                                   TransAm_OccupationalAccident_Declaration__c, TransAm_DeductibleBuydown_Declaration__c, TransAm_PrePassInsurance_Declaration__c,
                                                                   TransAm_Lifestyle__c, TransAm_Breakdown_Declaration__c, TransAm_InsDHB_Declaration__c, TransAm_InsPDI_Declaration__c,
                                                                   TransAm_InsOAI_Declaration__c, TransAm_InsDB_Declaration__c, TransAm_InsLS_Declaration__c
                                                                   FROM TransAm_ONE_Leasing__c WHERE Id In :lstLeaseIds];

      List<TransAm_ONE_Leasing__c> lstToBeUpdated = new List<TransAm_ONE_Leasing__c>();
      if(!flag){
          for(TransAm_ONE_Leasing__c updLease:lstUpdatedLeasingList){
              for(String key:mapInsDetails.keySet()){
                  if(updLease.get(key)!= null && updLease.get(key) == 'Yes' && oldLeasingMap.get(updLease.Id).get(key) != 'Yes' && mapInsDetails.containsKey(key)){
                      updLease.put(mapInsDetails.get(key)[0] , 'Purchased through TransAm');
                      updLease.put(mapInsDetails.get(key)[1] , System.Today());
                      updLease.put(mapInsDetails.get(key)[2] , 'X');
                  }
                  else if(updLease.get(key)!= null && updLease.get(key) == 'No' && oldLeasingMap.get(updLease.Id).get(key) != 'No' && mapInsDetails.containsKey(key)){
                      if(!key.equals('TransAm_DeductibleBuydown_Declaration__c'))
                          updLease.put(mapInsDetails.get(key)[0] , 'Purchased through Third Party');
                      else 
                          updLease.put(mapInsDetails.get(key)[0] , null);
                      updLease.put(mapInsDetails.get(key)[1] , null);
                      updLease.put(mapInsDetails.get(key)[2] , null);
                  }
              }
          
              for(String key:mapOptDetails.keySet()){
                  if(updLease.get(key)!= null && updLease.get(key) == 'X' && oldLeasingMap.get(updLease.Id).get(key) != 'X' && mapOptDetails.containsKey(key)){
                      updLease.put(mapOptDetails.get(key)[0] , true);
                      updLease.put(mapOptDetails.get(key)[1] , System.Today());
                  }
                  else if(updLease.get(key) == null && oldLeasingMap.get(updLease.Id).get(key) != null && mapOptDetails.containsKey(key)){
                      updLease.put(mapOptDetails.get(key)[0] , false);
                      updLease.put(mapOptDetails.get(key)[1] , null);
                  }
              }

              //Adding Lifestyle details only for Insurance Forms
              List<String> lstLSdetails = new List<String>();
              lstLSdetails.add('TransAm_Lifestyle_Opt_in_Date__c');
              lstLSdetails.add('TransAm_InsLS_Declaration__c');
              mapInsDetails.put('TransAm_Lifestyle__c', lstLSdetails);
              
              for(String key:mapAdditionalInsFormDetails.keySet()){
                  if(updLease.get(key)!= null && updLease.get(key) == 'X' && oldLeasingMap.get(updLease.Id).get(key) == null && mapAdditionalInsFormDetails.containsKey(key)){
                      updLease.put(mapAdditionalInsFormDetails.get(key), 'Yes');
                      if(mapAdditionalInsFormDetails.get(key).equals('TransAm_Lifestyle__c') && mapInsDetails.containsKey(mapAdditionalInsFormDetails.get(key))){
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[0] , System.Today());
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[1] , 'X');
                      }else if(!mapAdditionalInsFormDetails.get(key).equals('TransAm_Lifestyle__c')){
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[0] , 'Purchased through TransAm');
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[1] , System.Today());
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[2] , 'X');
                      }
                  } 
                  else if(updLease.get(key) == null 
                    && mapAdditionalInsFormDetails.containsKey(key) 
                    && oldLeasingMap.get(updLease.Id).get(mapAdditionalInsFormDetails.get(key)) == 'Yes'){
                          updLease.put(mapAdditionalInsFormDetails.get(key), 'No');

                      if(mapAdditionalInsFormDetails.get(key).equals('TransAm_Lifestyle__c') && mapInsDetails.containsKey(mapAdditionalInsFormDetails.get(key))){
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[0] , null);
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[1] , null);
                      }
                      else if(!mapAdditionalInsFormDetails.get(key).equals('TransAm_Lifestyle__c') && mapInsDetails.containsKey(mapAdditionalInsFormDetails.get(key))){
                          if(!key.equals('TransAm_DeductibleBuydown_Declaration__c'))
                              updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[0] , 'Purchased through Third Party');
                          else 
                              updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[0] , null);
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[1] , null);
                          updLease.put(mapInsDetails.get(mapAdditionalInsFormDetails.get(key))[2] , null);
                      }
                  }
              }
              lstToBeUpdated.add(updLease);

              if(mapInsDetails.containsKey('TransAm_Lifestyle__c'))
                  mapInsDetails.remove('TransAm_Lifestyle__c');
          }
      }
      if(lstToBeUpdated.size() > 0)
          update lstToBeUpdated;
  }
  /*******************************************************************************************
       * Method        :   sendMailtoRiskMgmt
       * Description   :   Method to Send mail to risk Management
       * Parameter     :   list<TransAm_ONE_Leasing__c>
       * Return Type   :   void
  	 * author        :   Nitesh Halliyal
  ******************************************************************************************/
  Public static void sendMailtoRiskMgmt(List<TransAm_ONE_Leasing__c> updatedList){
    
    /*
    Algorithm:
    - Check for the field TransAM_Risk_Mgmt_Document_Count__c if it is same as the number of required documents
    - if yes add the one elase id to set
    - query all the one lease documents which have the parent id in the above set and are the required one lease doc types.
    - also query the inner attachments and save their ids in a set
    - construct a map with one lease req id and set of attachment ids to be sent in a mail
    - query the attachment from the ids constructed in the above set
    - iterate on the one lease map values
    - run one more for loop on the values of the above map on attachment ids
    - Also do a get from the attachment map constructed earlier with key from the loop variable
    - construct the email attachment and put it into email attachment list
    - send email at the end of the loop iteration.
    -then update the risk mgmt sent email flag to stop sending of emails on further update on one lease record.
    */
    
    String docTypeNumber = '';
    String docTypeNames = '';
    String leaseDocTypes = '';
    string riskMgmtEmail = '';
    Set<Id> oneLeaseIdSet  = new set<Id>();
    set<Id> attachIdSet = new set<Id>();
    map<Id,Set<Id>> oneLeaseAttachmap = new map<Id,Set<Id>>();
    map<Id,Attachment> attachmentMap = new map<Id,Attachment>();
    map<Id,String> oneLeaseDrCodeMap = new map<Id,String>();
    list<Messaging.EmailFileAttachment> attMailList;
    list<Messaging.SingleEmailMessage> mailsToBeSent = new list<Messaging.SingleEmailMessage>();
    list<Messaging.SendEmailResult> emailResult = new list<Messaging.SendEmailResult>();
    integer intDocTypeNum = 0;
    set<String> docnames;
    set<String> leaseDocTypeSet;
    set<String> updateEmailSentflagSet = new set<string>();
    list<TransAm_ONE_Leasing__c> oneleaseFlagUpdateList = new list<TransAm_ONE_Leasing__c>();
    map<String,TransAm_Generic_Custom_Setting__c> genricCustSettingMap = TransAm_Generic_Custom_Setting__c.getAll();
    if(genricCustSettingMap.containsKey('Risk Mgmt Docs Number'))
        docTypeNumber = TransAm_Generic_Custom_Setting__c.getInstance('Risk Mgmt Docs Number').TransAm_Message_String__c;
    if(genricCustSettingMap.containsKey('Risk Mgmt Email Docs'))
        docTypeNames = TransAm_Generic_Custom_Setting__c.getInstance('Risk Mgmt Email Docs').TransAm_Message_String__c;
    if(genricCustSettingMap.containsKey('Risk Mgmt Lease Doc Types'))
        leaseDocTypes  = TransAm_Generic_Custom_Setting__c.getInstance('Risk Mgmt Lease Doc Types').TransAm_Message_String__c;
    if(genricCustSettingMap.containsKey('Risk Mgmt Email Address'))
        riskMgmtEmail = TransAm_Generic_Custom_Setting__c.getInstance('Risk Mgmt Email Address').TransAm_Message_String__c; 
    if(String.isNotBlank(leaseDocTypes))
        leaseDocTypeSet = new set<String>(leaseDocTypes.split(','));    
    if(String.isNotBlank(docTypeNames))
        docnames = new set<String>(docTypeNames.split(','));    
    if(String.isNotBlank(docTypeNumber))
        intDocTypeNum = Integer.valueOf(docTypeNumber); 

    for(TransAm_ONE_Leasing__c oneLease : updatedList){
        if(oneLease.TransAM_Risk_Mgmt_Document_Count__c == intDocTypeNum && !oneLease.TransAm_Risk_Mgmt_Email_Sent__c){
            oneLeaseIdSet.add(onelease.Id);
        }
    }
    system.debug('one lease id equl to count$$$$'+oneLeaseIdSet);
    if(!oneLeaseIdSet.isEmpty()){
        for(TransAm_ONE_Leasing_Document__c leaseDoc :[SELECT Id,TransAm_Lease__c,TransAm_Lease__r.TransAm_Application_IC__r.TransAm_DriverID__c,TransAm_Lease__r.TransAm_Application_ELA__r.TransAm_DriverID__c,(SELECT Id,name FROM Attachments) FROM TransAm_ONE_Leasing_Document__c WHERE TransAm_Lease__c IN :oneLeaseIdSet AND 
                                                       TransAm_Lease_Document_Type__c IN :leaseDocTypeSet]){
            if(!leaseDoc.attachments.isEmpty()){
                if(String.isNotBlank(leaseDoc.TransAm_Lease__r.TransAm_Application_IC__r.TransAm_DriverID__c))
                    oneLeaseDrCodeMap.put(leaseDoc.TransAm_Lease__c,leaseDoc.TransAm_Lease__r.TransAm_Application_IC__r.TransAm_DriverID__c);
                else if(String.isNotBlank(leaseDoc.TransAm_Lease__r.TransAm_Application_ELA__r.TransAm_DriverID__c))
                    oneLeaseDrCodeMap.put(leaseDoc.TransAm_Lease__c,leaseDoc.TransAm_Lease__r.TransAm_Application_ELA__r.TransAm_DriverID__c);         
                for(Attachment att :leaseDoc.attachments){
                    if(docnames.contains(att.name)){
                        if(oneLeaseAttachMap.containskey(leaseDoc.TransAm_Lease__c)){
                            Set<Id> mailAttachSet = new Set<Id>(oneLeaseAttachMap.get(leaseDoc.TransAm_Lease__c));
                            mailAttachSet.add(att.Id);
                            oneLeaseAttachMap.put(leaseDoc.TransAm_Lease__c,mailAttachSet);
                            attachIdSet = new set<Id>(mailAttachSet);
                        }
                        else{
                            oneLeaseAttachMap.put(leaseDoc.TransAm_Lease__c,new Set<Id>{att.id});
                            attachIdSet.add(att.id);
                        }   
                    }
                }
            }
        }
        map<Id,Attachment> attachMap = new map<Id,Attachment>([SELECT Id,name,contentType,body,parentId FROM Attachment WHERE Id IN :attachIdSet]);
        for(String onelease : oneLeaseAttachMap.keyset()){
            attMailList = new list<Messaging.EmailFileAttachment>();
            for(String attachId : oneLeaseAttachMap.get(onelease)){
                if(attachMap.containsKey(attachId)){
                    Attachment mailAttach = attachMap.get(attachId);
                    Messaging.EmailFileAttachment riskMgmtAtt = new Messaging.EmailFileAttachment();
                    riskMgmtAtt.setBody(mailAttach.Body);
                    riskMgmtAtt.setContentType(mailAttach.ContentType);
                    riskMgmtAtt.setFileName(mailAttach.Name);
                    attMailList.add(riskMgmtAtt);
                }
            }
            if(!attMailList.isEmpty() && oneLeaseDrCodeMap.containsKey(onelease)){
                Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
                emailMessage.setToAddresses(new List<String>{riskMgmtEmail});
                emailMessage.setSubject(oneLeaseDrCodeMap.get(onelease)+' - Insurance Documents');
                emailMessage.setHtmlBody('');
                emailMessage.setFileAttachments(attMailList);
                mailsToBeSent.add(emailMessage);
                updateEmailSentflagSet.add(onelease);
            }
        }
        system.debug('mails send list####'+mailsToBeSent);
        if(!mailsToBeSent.isEmpty())
            emailResult = Messaging.sendEmail(mailsToBeSent);
        for(Messaging.SendEmailResult emr: emailResult){
                if(!emr.isSuccess()){
                    TransAm_Error_Log__c el = new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_ONELeasing_Helper',
                                                                        TransAm_Method_Name__c = 'sendMailtoRiskMgmt',
                                                                        TransAm_Module_Name__c = 'Risk Management Email',
                                                                        TransAm_Exception_Message__c = String.valueOf(emr.getErrors()));
                    insert el;
                }
            }
            list<TransAm_Error_Log__c> errorLogList = new list<TransAm_Error_Log__c>();
            if(!updateEmailSentflagSet.isEmpty()){
                for(String oneLeaseid :updateEmailSentflagSet){
                    TransAm_ONE_Leasing__c onelease = new TransAm_ONE_Leasing__c(Id = oneLeaseid);
                    onelease.TransAm_Risk_Mgmt_Email_Sent__c = TRUE;
                    oneleaseFlagUpdateList.add(onelease);
                }
                list<Database.SaveResult> srListupdate = new list<Database.SaveResult>();
                if(!oneleaseFlagUpdateList.isEmpty())
                    srListupdate = database.update(oneleaseFlagUpdateList, FALSE);
                for(Database.SaveResult sr : srListupdate){
                    if (!sr.isSuccess()) {
                         TransAm_Error_Log__c el = new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_ONELeasing_Helper',
                                                                        TransAm_Method_Name__c = 'sendMailtoRiskMgmt',
                                                                        TransAm_Module_Name__c = 'Risk Management Email',
                                                                        TransAm_Exception_Message__c = 'Update risk mgmt flag - one leasing error');
                       errorLogList.add(el);    
                    }
                }
                if(!errorLogList.isEmpty())
                    insert errorLogList;    
            }
        }                                                        
    }

    /*******************************************************************************************
    * Method        :   updateAssigneeDetails
    * Description   :   Method to update Assignee details on application 
    * Parameter     :   list<TransAm_ONE_Leasing__c>, Boolean
    * Return Type   :   void
    ******************************************************************************************/
    public static void updateAssigneeDetails(List<TransAm_ONE_Leasing__c> newLeasingList, Map<Id, TransAm_ONE_Leasing__c> oldLeasingMap, Boolean isInsert){
        Map<Id, Map<Id,List<LeasingWrapper>>> mapLeaseAppDetails = new Map<Id, Map<Id,List<LeasingWrapper>>>();
        Map<Id,List<LeasingWrapper>> mapLeaseRecordsByRecType;
        Map<Id,List<TransAm_ONE_Leasing__c>> mapLeaseRecordsByApp = new Map<Id,List<TransAm_ONE_Leasing__c>>();
        List<Id> lstAppRecordIds = new List<Id>();
        Set<Id> setLeaseRecordIds = new Set<Id>();
        Map<Id,TransAm_ONE_Leasing__c> mapAllLeaseRecords = new Map<Id,TransAm_ONE_Leasing__c>();
        Map<Id,TransAm_ONE_Leasing__c> mapNewLeaseRecords = new Map<Id,TransAm_ONE_Leasing__c>();
        Id appId = null;

        for(TransAm_ONE_Leasing__c lease: newLeasingList){
            setLeaseRecordIds.add(lease.Id);
        }

        for(TransAm_ONE_Leasing__c lease: [SELECT Id, RecordType.Name, RecordTypeId, TransAm_LA_Assignee__c, TransAm_IC_Assignee__c, TransAm_Application_IC__c, TransAm_Application_ELA__c 
                                            FROM TransAm_ONE_Leasing__c
                                            WHERE Id In :setLeaseRecordIds]){
            Id newLeaseAppId;
            if((isInsert && lease.RecordType.Name == 'Lease Agreement')
              || (!isInsert 
                && oldLeasingMap.get(lease.Id).TransAm_LA_Assignee__c != lease.TransAm_LA_Assignee__c
                && lease.RecordType.Name == 'Lease Agreement')){
                newLeaseAppId = lease.TransAm_Application_ELA__c;
                lstAppRecordIds.add(newLeaseAppId);
                mapNewLeaseRecords.put(lease.Id, lease);
            }
            else if((isInsert && lease.RecordType.Name == 'IC Agreement')
              || (!isInsert 
                && oldLeasingMap.get(lease.Id).TransAm_IC_Assignee__c != lease.TransAm_IC_Assignee__c
                && lease.RecordType.Name == 'IC Agreement')){
                newLeaseAppId = lease.TransAm_Application_IC__c;
                lstAppRecordIds.add(newLeaseAppId);
                mapNewLeaseRecords.put(lease.Id, lease);
            }
        }

        for(TransAm_ONE_Leasing__c oneLease:[SELECT Id, RecordType.Name, RecordTypeId, TransAm_LA_Assignee__c, TransAm_IC_Assignee__c, TransAm_Application_IC__c, TransAm_Application_ELA__c, LastModifiedDate, CreatedDate 
                                            FROM TransAm_ONE_Leasing__c
                                            WHERE TransAm_Application_IC__c In :lstAppRecordIds
                                            OR TransAm_Application_ELA__c In :lstAppRecordIds]){
            Id leaseAppId;
            if(oneLease.RecordType.Name == 'IC Agreement'){
                leaseAppId = oneLease.TransAm_Application_IC__c;
            }else if(oneLease.RecordType.Name == 'Lease Agreement'){
                leaseAppId = oneLease.TransAm_Application_ELA__c;
            }
            
            if(mapLeaseRecordsByApp.containsKey(leaseAppId)){
                mapLeaseRecordsByApp.get(leaseAppId).add(oneLease);
            }
            else{
                List<TransAm_ONE_Leasing__c> lstLeaseRecords = new List<TransAm_ONE_Leasing__c>();
                lstLeaseRecords.add(oneLease);
                mapLeaseRecordsByApp.put(leaseAppId, lstLeaseRecords);
            }
        }

        for(Id applicationId:mapLeaseRecordsByApp.keySet()){
            mapLeaseRecordsByRecType = new Map<Id,List<LeasingWrapper>>();
            for(TransAm_ONE_Leasing__c trnsmLease:mapLeaseRecordsByApp.get(applicationId)){
                if(mapLeaseRecordsByRecType.containsKey(trnsmLease.RecordTypeId)){
                    mapLeaseRecordsByRecType.get(trnsmLease.RecordTypeId).add(new LeasingWrapper(trnsmLease));
                }
                else{
                    List<LeasingWrapper> lstLeaseRecords = new List<LeasingWrapper>();
                    lstLeaseRecords.add(new LeasingWrapper(trnsmLease));
                    mapLeaseRecordsByRecType.put(trnsmLease.RecordTypeId, lstLeaseRecords);
                }
            }
            mapLeaseAppDetails.put(applicationId, mapLeaseRecordsByRecType);
        }

        for(Id applicationId:mapLeaseAppDetails.keyset()){
            if(mapLeaseAppDetails.containsKey(applicationId) && mapLeaseAppDetails.get(applicationId).containsKey(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('Lease Agreement').getRecordTypeId()))
                mapLeaseAppDetails.get(applicationId).get(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('Lease Agreement').getRecordTypeId()).sort();
            if(mapLeaseAppDetails.containsKey(applicationId) && mapLeaseAppDetails.get(applicationId).containsKey(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('IC Agreement').getRecordTypeId()))
                mapLeaseAppDetails.get(applicationId).get(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('IC Agreement').getRecordTypeId()).sort();
        }

        List<TransAm_Application__c> lstAssigneeUpdateApps = new List<TransAm_Application__c>();
        List<TransAm_Application__c> lstApps = [SELECT Id, TransAm_Lease_Assignee__c, TransAm_IC_Assignee__c FROM TransAm_Application__c WHERE Id In :mapLeaseAppDetails.keyset()];
        for(TransAm_Application__c tapp:lstApps){
            if(mapLeaseAppDetails.containsKey(tapp.Id) && mapLeaseAppDetails.get(tapp.Id).containsKey(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('IC Agreement').getRecordTypeId())){}
                tapp.TransAm_IC_Assignee__c = mapLeaseAppDetails.get(tapp.Id).get(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('IC Agreement').getRecordTypeId())[0].leaseRecWrapper.TransAm_IC_Assignee__c;
            if(mapLeaseAppDetails.containsKey(tapp.Id) && mapLeaseAppDetails.get(tapp.Id).containsKey(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('Lease Agreement').getRecordTypeId()))
                tapp.TransAm_Lease_Assignee__c = mapLeaseAppDetails.get(tapp.Id).get(Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('Lease Agreement').getRecordTypeId())[0].leaseRecWrapper.TransAm_LA_Assignee__c;
            lstAssigneeUpdateApps.add(tapp);
        }

        if(lstAssigneeUpdateApps.size() > 0)
            update lstAssigneeUpdateApps;
    }

   /************************************************************************************
    * Class        :    LeasingWrapper
    * Description  :    Wrapper class implementing Comparable to sort out lease records
    *************************************************************************************/
    public class LeasingWrapper implements Comparable {
        public TransAm_ONE_Leasing__c leaseRecWrapper;
      
        // Constructor
        public LeasingWrapper(TransAm_ONE_Leasing__c leaseWrapper) {
            leaseRecWrapper = leaseWrapper;
        }
      
        // Compare Lease Records based on the LastModifiedDate.
        public Integer compareTo(Object compareTo) {
            // Cast argument to LeasingWrapper
            LeasingWrapper compareToLeaseRec = (LeasingWrapper)compareTo;
          
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (leaseRecWrapper.createdDate > compareToLeaseRec.leaseRecWrapper.createdDate) {
                // Set return value to a negative value.
                returnValue = -1;
            } else if (leaseRecWrapper.createdDate < compareToLeaseRec.leaseRecWrapper.createdDate) {
                // Set return value to a positive value.
                returnValue = 1;
            }
            return returnValue;       
        }
    }

    /*******************************************************************************************
    * Method        :   updateLeaseContact
    * Description   :   Method to update Lease Contact details from application 
    * Parameter     :   list<TransAm_ONE_Leasing__c>
    * Return Type   :   void
    ******************************************************************************************/
    public static void updateLeaseContactDetails(List<TransAm_ONE_Leasing__c> newLeasingList){
        List<Id> lstAppIds = new List<Id>();
        List<Id> lstNewLeaseIds = new List<Id>();
        Map<Id,List<TransAm_ONE_Leasing__c>> mapAppLeases = new Map<Id,List<TransAm_ONE_Leasing__c>>();
        for(TransAm_ONE_Leasing__c trNewLease:newLeasingList){
            lstNewLeaseIds.add(trNewLease.Id); 
        }

        for(TransAm_ONE_Leasing__c trLease:[SELECT Id, RecordType.Name, RecordTypeId, TransAm_Application_IC__c, TransAm_Application_ELA__c, TransAm_Contact_ELA__c, TransAm_Contact_IC__c
                                          FROM TransAm_ONE_Leasing__c
                                          WHERE Id In :lstNewLeaseIds]){
            Id leaseAppId;
            if(trLease.RecordType.Name == 'IC Agreement'){
                leaseAppId = trLease.TransAm_Application_IC__c;
            }else if(trLease.RecordType.Name == 'Lease Agreement'){
                leaseAppId = trLease.TransAm_Application_ELA__c;
            }

            if(mapAppLeases.containsKey(leaseAppId)){
                mapAppLeases.get(leaseAppId).add(trLease);
            }else{
                List<TransAm_ONE_Leasing__c> lstLeases = new List<TransAm_ONE_Leasing__c>();
                lstLeases.add(trLease);
                mapAppLeases.put(leaseAppId, lstLeases);
            }
        }
        Map<Id,Id> mapAppDrivers = new Map<Id,Id>();
        Id driverRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Driver').getRecordTypeId();
        
        for(TransAm_Application__c app:[SELECT Id, TransAm_Applicant__c FROM TransAm_Application__c 
                                        WHERE Id In :mapAppLeases.keyset()
                                        AND TransAm_Applicant__r.RecordTypeId = :driverRecTypeId
                                        AND TransAm_Applicant__r.TransAm_Driver_Status__c = 'Hired']){
            mapAppDrivers.put(app.Id, app.TransAm_Applicant__c);
        }

        List<TransAm_ONE_Leasing__c> lstLeaseContactUpdates = new List<TransAm_ONE_Leasing__c>();
        for(Id appId:mapAppLeases.keySet()){
            for(TransAm_ONE_Leasing__c trLease:mapAppLeases.get(appId)){
                if(mapAppDrivers.containsKey(appId)){
                    if(trLease.RecordType.Name == 'IC Agreement'){
                        trLease.TransAm_Contact_IC__c = mapAppDrivers.get(appId);
                    }else if(trLease.RecordType.Name == 'Lease Agreement'){
                        trLease.TransAm_Contact_ELA__c = mapAppDrivers.get(appId);
                    }
                    lstLeaseContactUpdates.add(trLease);
                }
            }
        }
        System.debug('### lstLeaseContactUpdates ###'+lstLeaseContactUpdates.size());
        if(lstLeaseContactUpdates.size() > 0)
            update lstLeaseContactUpdates;
    }

   /*******************************************************************************************
    * Method        :   updateLifestyleDeclarationDetails
    * Description   :   Method to update Lifestyle details on Lease 
    * Parameter     :   list<TransAm_ONE_Leasing__c>
    * Return Type   :   void
    ******************************************************************************************/
    public static void updateLifestyleDeclarationDetails(List<TransAm_ONE_Leasing__c> newLeasingList){
        List<Id> lstLeaseIds = new List<Id>();
        for(TransAm_ONE_Leasing__c trLease:newLeasingList){
            lstLeaseIds.add(trLease.Id);
        }

        List<TransAm_ONE_Leasing__c> lstToBeUpdated = new List<TransAm_ONE_Leasing__c>();
        for(TransAm_ONE_Leasing__c trNewLease:[SELECT Id, RecordType.Name, TransAm_Lifestyle__c, TransAm_Lifestyle_Opt_in_Date__c, TransAm_InsLS_Declaration__c FROM TransAm_ONE_Leasing__c WHERE Id In :lstLeaseIds]){
            if(trNewLease.RecordType.Name == 'Lease Agreement'){
                if(trNewLease.TransAm_Lifestyle__c == 'Yes'){
                    trNewLease.TransAm_Lifestyle_Opt_in_Date__c = trNewLease.TransAm_Lifestyle_Opt_in_Date__c == null ? System.today() : trNewLease.TransAm_Lifestyle_Opt_in_Date__c;
                    trNewLease.TransAm_InsLS_Declaration__c = 'X';
                }
                else{
                    trNewLease.TransAm_Lifestyle_Opt_in_Date__c = null;
                    trNewLease.TransAm_InsLS_Declaration__c = null;
                }
                lstToBeUpdated.add(trNewLease);
            }
        }
        System.debug('### updateLifestyleDeclarationDetails.lstToBeUpdated ###'+lstToBeUpdated.size());
        if(lstToBeUpdated.size() > 0)
            update lstToBeUpdated;
    }
}
