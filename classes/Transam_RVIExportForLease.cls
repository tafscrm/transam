global class Transam_RVIExportForLease {
    webservice static void generateXML(Id oneLeaseId, String recType){
    	TransAm_ONE_Leasing__c oneLease;
        List<formatXML> xmlNodes = new List<formatXML>();
        List<Attachment> updateAttachList = new List<Attachment>();
        TransAm_Application__c currentApp;
        Map<String,String> docTypeMap = new Map<String,String>();       	
        String fileContent;
        if(recType == 'Lease Agreement'){
         	oneLease = [SELECT Id,
                        Name,
                        TransAm_Application_ELA__r.Transam_First_Name__c,
                        TransAm_Application_ELA__r.TransAm_Last_Name__c,
                        TransAm_Application_ELA__r.TransAm_Orientation_Date__c,
                        TransAm_Application_ELA__r.TransAm_Hire_Date__c,
                        TransAm_Application_ELA__r.TransAm_Status_Reason__c,
                        TransAm_Application_ELA__r.Name,
                        TransAm_Application_ELA__r.TransAm_DriverID__c,
                        TransAm_Application_ELA__r.TransAm_Driver_Type__c,
                        TransAm_RVI_Export__c,
                        TransAm_RVI_Export_Date__c,
                        TransAm_Application_ELA__r.TransAm_SSN__c,
                        TransAm_RVI_Export_Hour__c,
                        TransAm_Application_ELA__c
                        FROM TransAm_ONE_Leasing__c 
                        WHERE Id =: oneLeaseId LIMIT 1];
            
            	currentApp = [Select Id,
                              Transam_First_Name__c,
                              TransAm_Last_Name__c,
                              TransAm_Orientation_Date__c,
                              TransAm_Hire_Date__c,
                              Name,
                              TransAm_DriverID__c,
                              TransAm_SSN__c
                              FROM TransAm_Application__c
                              WHERE Id =: oneLease.TransAm_Application_ELA__c
                             ];
        }
        else if(recType == 'IC Agreement'){
            oneLease = [SELECT Id,
                        Name,
                        TransAm_Application_IC__r.Transam_First_Name__c,
                        TransAm_Application_IC__r.TransAm_Last_Name__c,
                        TransAm_Application_IC__r.TransAm_Orientation_Date__c,
                        TransAm_Application_IC__r.TransAm_Hire_Date__c,
                        TransAm_Application_IC__r.TransAm_Status_Reason__c,
                        TransAm_Application_IC__r.Name,
                        TransAm_Application_IC__r.TransAm_DriverID__c,
                        TransAm_Application_IC__r.TransAm_Driver_Type__c,
                        TransAm_RVI_Export__c,
                        TransAm_RVI_Export_Date__c,
                        TransAm_Application_IC__r.TransAm_SSN__c,
                        TransAm_RVI_Export_Hour__c,
                        TransAm_Application_IC__c
                        FROM TransAm_ONE_Leasing__c
                        WHERE Id =: oneLeaseId LIMIT 1];
        	currentApp = [Select Id,
                              Transam_First_Name__c,
                              TransAm_Last_Name__c,
                              TransAm_Orientation_Date__c,
                              TransAm_Hire_Date__c,
                              Name,
                          	  TransAm_SSN__c,
                              TransAm_DriverID__c
                              FROM TransAm_Application__c
                              WHERE Id =: oneLease.TransAm_Application_IC__c
                             ];
        }
        
    	List<TransAm_ONE_Leasing_Document__c> oneLeaseDocList = [SELECT Id, TransAm_Lease_Document_Type__c,TransAm_SSN__c,TransAm_Is_Exported__c FROM TransAm_ONE_Leasing_Document__c WHERE TransAm_Lease__c =: oneLeaseId ]; 
    	validateAttachmentNames(oneLease);
        List<DocumentTypeMapping__mdt> metaDocSetting = [SELECT Id,
                                                            MasterLabel,
                                                  			Doc_Type__c
                                                            FROM DocumentTypeMapping__mdt];
        for(DocumentTypeMapping__mdt objMD : metaDocSetting){
            docTypeMap.put(objMD.MasterLabel,objMD.Doc_Type__c);
        }
        List<Attachment> attachList = [SELECT Id, Name, ParentId FROM Attachment where ParentId IN: oneLeaseDocList];
        List<RVI_XML_Setting_For_Lease__mdt> metasetting = [SELECT Id,
                                                            TransAm_Document_Name__c,
                                                  			TransAm_Owner_Hired_System__c,
                                                            TransAm_RVI_Scan_Code__c
                                                  			FROM RVI_XML_Setting_For_Lease__mdt];
        
        for(Attachment objAttachment : attachList){
            for(TransAm_ONE_Leasing_Document__c objR : oneLeaseDocList){
                if(objR.Id == objAttachment.ParentId && objR.TransAm_Lease_Document_Type__c != 'System Document'){
                    formatXML objXML = new formatXML();
                    objXML.recruitDocType = objR.TransAm_Lease_Document_Type__c;
                    objXML.docName = objAttachment.Name;
                    objXML.docId = objR.Id;
                    String nameOfDoc = objAttachment.Name.substringAfterLast('.').toUpperCase();
                    if(docTypeMap.containsKey(nameOfDoc))
                    	objXML.typeofDoc = docTypeMap.get(nameOfDoc);
                    else
                        objXML.typeofDoc = 'O';
                    xmlNodes.add(objXML);
                    if(!System.isBatch())
                    	objR.TransAm_Is_Exported__c = true;
                    		                  
                }
            }
        }
        for(RVI_XML_Setting_For_Lease__mdt objSetting : metasetting){
            for(formatXML objXML : xmlNodes){
                if(String.isNotBlank(objSetting.TransAm_Owner_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                    objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                    objXML.folderLocation = objSetting.TransAm_Owner_Hired_System__c;
                    Date hireUpdatedDate=Date.today();
                    objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                    
                }
            }        
        }
        for(formatXML objXML : xmlNodes){
            objXML.SC=objXML.folderLocation;
            if(String.isBlank(fileContent)){
                fileContent=getxmlFormattedString(currentApp,objXML,oneLease.Name);     
            }
            else{
                fileContent +=getxmlFormattedString(currentApp,objXML,oneLease.Name);    
            }
            
        }
        
        TransAm_ONE_Leasing_Document__c objRecruit;
        objRecruit = [SELECT Id FROM TransAm_ONE_Leasing_Document__c WHERE TransAm_Lease_Document_Type__c = 'System Document' AND TransAm_Lease__c =: oneLeaseId LIMIT 1];
    	if(String.isNotBlank(fileContent)){
            fileContent = fileContent.remove('null');
            List<Attachment> existingSystemDoc = [SELECT ID FROM Attachment WHERE ParentId =: objRecruit.Id];
            if(existingSystemDoc.size() > 0){
                DELETE existingSystemDoc;
            }
            UPDATE oneLeaseDocList;
            Attachment att = new Attachment(Name = oneLease.Name+'_Index.xml', body = Blob.valueOf(fileContent), parentId = objRecruit.Id);
            updateAttachList.add(att);
        }
        if(!updateAttachList.isEmpty()){
            Database.INSERT(updateAttachList);
            oneLease.TransAm_RVI_Export_Date__c = Date.today();
            oneLease.TransAm_RVI_Export__c = true;
            oneLease.TransAm_RVI_Export_Hour__c = DateTime.now().Hour();
            UPDATE oneLease;
        }
    }
    private static void validateAttachmentNames(TransAm_ONE_Leasing__c oneLease){
        List<Attachment> attList = [SELECT Id, Name FROM Attachment 
                                    WHERE PARENTID IN
                                    (SELECT ID FROM TransAm_ONE_Leasing_Document__c 
                                    WHERE Transam_Lease__c =: oneLease.Id)];
        for(Attachment objAtt : attList){
            objAtt.Name = objAtt.Name.Replace(' ','_');
            Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9_.-]');
            Matcher matcher = nonAlphanumeric.matcher(objAtt.Name);
            objAtt.Name = matcher.replaceAll('');
            if(!objAtt.Name.contains(oneLease.Name)){
                objAtt.Name = oneLease.Name+'_'+objAtt.Name;
            }
            if(objAtt.Name.length()>Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)){
                objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')).substring(0,Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)-5)+'.'+objAtt.Name.substringafterlast('.');
            }
            for(Attachment dupAtt : attList){
                if(objAtt.Name == dupAtt.Name && objAtt.Id != dupAtt.Id){
                    if(objAtt.Name.length() < Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)-5){
                        objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')) + String.ValueOf(Math.random()).Substring(3,7)+'.'+objAtt.Name.substringafterlast('.');
                    }
                    else{
                        objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')).substring(0,28)+String.ValueOf(Math.random()).Substring(3,7)+'.'+objAtt.Name.substringafterlast('.');
                    }
                }
            }
        }
        UPDATE attList;
    }
    
    public static String getxmlFormattedString(TransAm_Application__c currentApp , formatXML xmlObj, String oneLeaseName){
        try{
            //String oneLeaseName = oneLease.Name;
            String resourceName='oneLeaseServer_'+xmlObj.SC;
            String content=[SELECT Body FROM StaticResource WHERE Name =:resourceName].Body.toString();
            // update value in content from current application
            if(currentApp!=null){
               Map<String, object> currentAppMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(currentApp));
               for(String currentAppField: currentAppMap.keyset()){
                   content=content.replace('{currentApp.'+currentAppField+'}', (JSON.serialize(currentAppMap.get(currentAppField))).replace('"',''));
               }
            }
            // update value in content from format xml object
            if(xmlObj!=null){
               Map<String, object> xmlObjMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(xmlObj));
               for(String xmlObjField: xmlObjMap.keyset()){
                   content=content.replace('{objXML.'+xmlObjField+'}', (JSON.serialize(xmlObjMap.get(xmlObjField))).replace('"',''));
               }
            }
            if(String.isNotBlank(oneLeaseName)){
                content=content.replace('{oneLeaseName}', (JSON.serialize(oneLeaseName).replace('"','')));
            }
            content=(content).replaceAll('\\{[0-9A-Za-z\\_.]*\\}','');
            return content;
        }catch(Exception e){
            return '';
        }
    }
   global class formatXML{
        public String recruitDocType{get;set;}
        public Id docId {get;set;}
        public String docName {get;set;}
        public String scanCode{get;set;}
        public String typeofDoc{get;set;}
        public String folderLocation{get;set;}
        public String driverType {get;set;}
        public String hireDate {get;set;}
        public String SC{get; set;}
        
    }
}