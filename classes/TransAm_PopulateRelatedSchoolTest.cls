/******************************************************************************************
* Class Name   :     TransAm_PopulateRelatedSchoolTest
* Create By    :     Ajay B R
* Create Date  :     06/21/2017
* Description  :     Test class for testing PopulateRelatedSchool_Insert & PopulateRelatedSchool_Update methods of ContactHelper class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                          06/29/2017          Initial version.
*****************************************************************************************/
@isTest(seeAllData = False)
private class TransAm_PopulateRelatedSchoolTest{

    /*******************************************************************************************
    * Method        :   dataSetup
    * Description   :   Purpose of this method is to setup test data.
    * Parameter     :   null
    * Return Type   :   void
    ******************************************************************************************/
    @testSetup
    private static void testDataSetUp(){
        Group recQueue = new Group(name = 'Driver Recruiter Queue');
        insert recQueue;
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        User integrationUser = new User();
        integrationUser.firstname = 'Arun';
        integrationUser.lastname = 'Tyagi';
        integrationUser.username = 'xyzrtyu@abc.com';
        integrationUser.email = 'xyz@deloitte.com';
        integrationUser.alias = 'xyz';
        integrationUser.TimeZoneSidKey = 'America/Los_Angeles';
        integrationUser.LocaleSidKey = 'En_US';
        integrationUser.profileID = prof.Id;
        integrationUser.LanguageLocaleKey = 'En_US';
        integrationUser.EmailencodingKey = 'UTF-8';
        
        insert integrationUser;
    }
    /************************************************************************************
    * Method       :    testPopulateRelatedSchool_Insert
    * Description  :    This method is used to test the PopulateRelatedSchool_Insert.
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testPopulateRelatedSchool_Insert(){
        List<TransAm_Hiring_Areas_Reference__c> hiringAreaRef = TransAm_Test_DataUtility.insertHiringAreasReferences(true);
        List<Account> accList = TransAm_Test_DataUtility.createAccounts(2, false);
            accList[1].TransAm_Driving_School_Code__c = 'C-1235';
            insert accList;
        List<Contact> conList = new List<Contact>();
        User intUser = [Select Id from User where FirstName='Arun' limit 1];
        system.runAs(intUser){
            test.startTest();
            conList = TransAm_Test_DataUtility.createContacts(1, accList[0].Id, 'School Contact',true);
            //test.stopTest();
            Contact conObj = [select Id, AccountID, TransAm_Related_School__c from contact where id=:conList[0].Id];
            System.assertEquals(conObj.AccountId, conObj.TransAm_Related_School__c);
            //test.startTest();
            conObj.accountId = null;
            update conObj;
            //test.stopTest();
            Contact conObj1 = [select Id, AccountID, TransAm_Related_School__c from contact where id=:conObj.Id];
            System.assertEquals(conObj1.AccountId, conObj1.TransAm_Related_School__c);
            //test.startTest();
            conObj1.accountId = accList[1].Id;
            update conList;
            
            Contact conObj2 = [select Id, AccountID, TransAm_Related_School__c from contact where id=:conList[0].Id];
            //System.assertEquals(conObj2.AccountId, conObj2.TransAm_Related_School__c);
            test.stopTest();
        }
    }
}