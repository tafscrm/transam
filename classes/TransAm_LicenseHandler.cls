/* Class Name   : TransAm_LicenseHandler
 * Description  : Handler class for the License trigger
 * Created By   : Pankaj Singh
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_LicenseHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_License__c> licenseList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_LicenseHelper.populateApplicationOnLicense(licenseList);
        //TransAm_LicenseHelper.validateLicense(licenseList);
        prohibitBeforeInsertTrigger = true;
    }
     /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Second version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<TransAm_License__c> licenseList){
        List<TransAm_License__c>  listFilteredLicense = new List<TransAm_License__c>();
        if(prohibitAfterInsertTrigger){
            return;
        }  
        Set<Id> appSet = new Set<Id>();
        Map<Id,String> appMap = new Map<Id,String>();      
        TransAm_LicenseHelper.updateExpirationdateOnApplication(licenseList);  
        for(TransAm_License__c lic : licenseList){
            appSet.add(lic.TransAm_Application__c);
        }   
        for(TransAm_Application__c app: [SELECT ID,TransAm_Status__c FROM TransAm_Application__c WHERE ID IN : appSet and  TransAm_Status__c != 'Application Received']){
            appMap.put(app.id,app.TransAm_Status__c);
        }
        
        for(TransAm_License__c lic :licenseList){
            if(appMap.containsKey(lic.TransAm_Application__c)){
                listFilteredLicense.add(lic);
            }
        } 
		if(!listFilteredLicense.isEmpty()){
			TransAm_LicenseHelper.validateLicense(listFilteredLicense);
		}
        
        prohibitAfterInsertTrigger = true;
    }
    
     /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  handler method for after Update event
    * Modification Log:  Second version.
    ***************************************************************************************/
    public static void afterUpdateHandler(list<TransAm_License__c> licenseList){
        List<TransAm_License__c>  listFilteredLicense = new List<TransAm_License__c>();
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        TransAm_LicenseHelper.updateExpirationdateOnApplication(licenseList); 
        Set<Id> appSet = new Set<Id>();
        Map<Id,String> appMap = new Map<Id,String>();
        for(TransAm_License__c lic : licenseList){
            appSet.add(lic.TransAm_Application__c);
        }   
        for(TransAm_Application__c app: [SELECT ID,TransAm_Status__c FROM TransAm_Application__c WHERE ID IN : appSet and  TransAm_Status__c != 'Application Received']){
            appMap.put(app.id,app.TransAm_Status__c);
        }
        
        for(TransAm_License__c lic :licenseList){
            if(appMap.containsKey(lic.TransAm_Application__c)){
                listFilteredLicense.add(lic);
            }
        } 
        if(!listFilteredLicense.isEmpty()){
			TransAm_LicenseHelper.validateLicense(listFilteredLicense);
		}
        prohibitAfterUpdateTrigger = true;
    }
    
    /****************************************************************************************
    * Created By      :  MJ
    * Create Date     :  04-April-2017
    * Description     :  handler method for before Update event
    * Modification Log:  Second version.
    ***************************************************************************************/
    public static void beforeUpdateHandler(list<TransAm_License__c> licenseList){
    
        if(prohibitBeforeUpdateTrigger){
            return;
        }        
        //TransAm_LicenseHelper.validateLicense(licenseList);                        
        prohibitBeforeUpdateTrigger = true;
    }

}