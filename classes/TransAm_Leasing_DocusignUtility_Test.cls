@isTest
private with sharing class TransAm_Leasing_DocusignUtility_Test {
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(2, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0], true);        
    } 

    private static testmethod void testAppDocSend() {       
        Test.startTest();
        TransAm_Application__c appObj = [SELECT ID FROM TransAm_Application__c LIMIT 1];
        System.debug('### appObj ###'+appObj);
        PageReference pageRef = Page.TransAm_Leasing_SendUsingDocusign;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('appId', appObj.id);
        TransAm_Leasing_DocuSignUtility newInstance = new TransAm_Leasing_DocuSignUtility();
        newInstance.sendLeasingDocsToDocuSign();
        Test.stopTest();
    }    
}