public class TransAm_DownloadAttachmentJob implements Queueable, Database.AllowsCallouts {

    public final TransAm_HireRight_Orders__c hireRightOrder;
    
    public TransAm_DownloadAttachmentJob(TransAm_HireRight_Orders__c hireRightOrder) {
        this.hireRightOrder = hireRightOrder;
    }
    
    public void execute(QueueableContext context) {
        try
        {
            String attachmentUrl = hireRightOrder.TransAm_HireRight_Reports__c;
            System.debug('Inside download attachment');
            HttpRequest req = new HttpRequest();
            req.setEndpoint(attachmentURL);
            req.setMethod('GET');
            Http h = new Http();                 
            HttpResponse res = h.send(req);
            String nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            System.debug('Status code for ows ' + res.getStatusCode());
            
            String reportType ;
            if(!String.ISBLANK(hireRightOrder.TransAm_Report_Type__c))
            {
                if(hireRightOrder.TransAm_Report_Type__c.contains('Express'))
                    reportType = 'MVRExpress';
                else if(hireRightOrder.TransAm_Report_Type__c.contains('Standard'))
                    reportType = 'MVRStandard';
            }
            //String searchString = '%'+hireRightOrder.TransAm_Application__r.Name + '%'+reportType;
            String appName = hireRightOrder.TransAm_Application__r.Name;
            TransAm_Recruitment_Document__c recDoc = [select id from TransAm_Recruitment_Document__c where TransAm_Application__r.Name = :appName and TransAm_Type__c = :reportType Limit 1];
            
            
            Attachment att = new Attachment();
            att.Body = res.getBodyAsBlob();
            att.name = 'test.pdf';
            att.ParentId = recDoc.Id;
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = true;
            Database.insert(att);
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = false;
            System.debug('Attachment Id :: '+att);
            
            TransAm_HireRight_Orders__c order = new TransAm_HireRight_Orders__c(Id=hireRightOrder.Id);
            order.TransAm_Recruitment_Documents__c = recDoc.Id;
            order.TransAm_HireRight_Reports__c=attachmentURL;
            order.TransAm_Order_Status__c = 'Completed';
            System.debug('Updating Hire Right Order :: '+hireRightOrder.Id);
            Database.update(order);
        }
        catch(Exception e)
        {
            System.debug('Exception in downloadAttachment::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightServiceConsumer',TransAm_Method_Name__c = 'downloadAttachment',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }
}