/* CLass Name   : TransAm_AttachmentTriggerTest
 * Description  : Test class for Attachment Trigger
 * Created By   : Karthik Gulla
 * Created On   : 12-Apr-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               12-Apr-2017              Initial version.
 *  * Nitesh Halliyal			  20-Jul-2017			   Updated Class for MVP3
 *****************************************************************************************/
@isTest
Public with sharing class TransAm_AttachmentTriggerTest {
    public static List<TransAm_Job_History__c> lstApplicationJobHistory;
    public static List<TransAm_Application__c> lstNewApplications;
    public static List<Attachment> lstAttachments;
    public static List<attachment> attList;
    public static List<attachment> attListSentFax;
    public static List<Account> lstAccounts;
    public static List<transam_application__c> lstApplications;


    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        TransAm_Test_DataUtility.createFormsEmailMappings();
        lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        TransAm_Test_DataUtility.createGenericCustomSetting();
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0],true);
        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);                              
        TransAm_Test_DataUtility.createJobHistory(1, lstApplications[0], lstEmployers[0], true);
        
        List<TransAm_Recruitment_Document__c> lstRecrDocuments = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplications[0].Id 
                                                                    AND (TransAm_Type__c = 'Background Check Release Form' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Release Form'
                                                                 		OR TransAm_Type__c = 'Telephone Reference')];
        
        
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[0].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[1].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[2].Id, true); 
        
        dsfs__DocuSign_Status__c d = new dsfs__DocuSign_Status__c();
        d.dsfs__Company__c = lstAccounts[0].Id;
        d.TransAm_Application__c = lstApplications[0].Id;
        d.dsfs__Envelope_Status__c = 'Completed';
        d.dsfs__DocuSign_Envelope_ID__c = '1001A123-1234-5678-1D84-F8D44652A382';
        d.dsfs__Subject__c = 'Document for eSignature';
        d.dsfs__Completed_Date_Time__c = System.now();
        insert d;
    } 

    /************************************************************************************
    * Method       :    testAttachmentTrigger
    * Description  :    Test attachment trigger Functionalities
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testAttachmentTrigger() {
        lstApplications = [SELECT id FROM transam_application__c];
        TransAm_Forms_EmailMapping__c custsetting = new TransAm_Forms_EmailMapping__c(
            name = 'IC-ECA One leasing',
            TransAm_TeamEmail__c = 'test@abc.com',
        	TransAm_Email_Subject__c = 'TestSUbject',
        	TransAm_Form_Type__c = 'IC/ECA');
        insert custsetting;
        test.startTest(); 
            lstApplications[0].TransAm_DriverID__c = 'TESTCD12345';
            update lstApplications[0];
            TransAm_ONE_Leasing__c onelease = new TransAm_ONE_Leasing__c(TransAm_Application_ELA__c =lstApplications[0].Id,TransAm_Application_IC__c=lstApplications[0].Id,recordTypeId = Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get('IC Agreement').getRecordTypeId());
            insert onelease;
            TransAm_ONE_Leasing_Document__c oneLsDoc = new TransAm_ONE_Leasing_Document__c(TransAm_Lease__c = onelease.Id,TransAm_Lease_Document_Type__c = 'IC Settlement Charges Cover Letter');
            insert oneLsDoc;
            TransAm_ONE_Leasing_Document__c oneLsDoc1 = new TransAm_ONE_Leasing_Document__c(TransAm_Lease__c = onelease.Id,TransAm_Lease_Document_Type__c = 'One Settlement Charges Cover letter');
            insert oneLsDoc1;
            List<Attachment> onelsAtt =  TransAm_Test_DataUtility.createAttachments(6, oneLsDoc.Id, false);
            List<Attachment> onelsAtt1 =  TransAm_Test_DataUtility.createAttachments(1, oneLsDoc1.Id, false);
            onelsAtt1[0].Name = 'ICA.docx';
        	onelsAtt[0].Name = 'OneBeaconEnrollForm.docx';
            onelsAtt[1].Name = 'TestNameOne.tif';
            onelsAtt[2].Name = 'TestNameTwo.jpg';
            onelsAtt[3].Name = 'TestNameThree.jpeg';
            onelsAtt[4].Name = 'TestNameFour.png';
            onelsAtt[5].Name = 'TestNameFive.xml';
        	insert onelsAtt;
            insert onelsAtt1;  
        

        
        lstApplicationJobHistory = [SELECT Id, TransAm_Application__c FROM TransAm_Job_History__c];
        List<Attachment> lstAttmts = TransAm_Test_DataUtility.createAttachments(1, lstApplicationJobHistory[0].Id, false);
        lstAttmts[0].Name = 'HR_Emp_Ver_Test.pdf';
        insert lstAttmts;
        list<dsfs__DocuSign_Status__c> docList = [SELECT Id,TransAm_ONE_Leasing__c FROM dsfs__DocuSign_Status__c];
        system.assert(docList[0].Id != null);
        docList[0].TransAm_ONE_Leasing__c = onelease.Id;
        update docList[0];
        List<Attachment> telRef = TransAm_Test_DataUtility.createAttachments(1, lstApplicationJobHistory[0].Id, false);
        telRef[0].name = 'Telephone_Ref_Form.pdf';
        insert telRef;
        TransAm_AttachmentHelper.transferAttachements(telRef);
                List<Attachment> docAtt = TransAm_Test_DataUtility.createAttachments(1, docList[0].Id, false);
        docAtt[0].name = 'Past_Emp_Verification.pdf';
        insert docAtt;
        TransAm_AttachmentHelper.transferAttachements(docAtt);
        TransAm_AttachmentHelper.transferLeasingAttachments(docAtt);
        lstAttmts = TransAm_Test_DataUtility.createAttachments(1, lstApplicationJobHistory[0].Id, false);
        TransAm_SendFax.sendFaxToEmployer(lstApplicationJobHistory[0].Id, '16157507314', 'HireRight: Test Subject. Please ignore');

        lstNewApplications = [SELECT Id FROM TransAm_Application__c];
        TransAm_SendFax.sendFaxToSchoolContact(lstNewApplications[0].Id);
        List<attachment> efaxatt = new list<attachment>(TransAm_Test_DataUtility.createAttachments(1, lstNewApplications[0].Id, true)); 
        efaxapp__Sent_Fax__c fax = new efaxapp__Sent_Fax__c(efaxapp__Fax_Number__c = '123456789',efaxapp__Subject__c = 'Test Fax',efaxapp__Send_Date__c = Datetime.now(),efaxapp__Status__c = 'Delivered',
                efaxapp__Attachment_ID__c    = efaxatt[0].Id,
                efaxapp__Sent_With_Outbound_Message__c = True, 
                efaxapp__Org_Fax_Number__c = '123456789', 
                efaxapp__Barcode_Position_Top__c = '11', 
                efaxapp__Barcode_Position_Left__c = '404',
                efaxapp__Barcode_Size_Width__c = '72'
            );
        insert fax;
        efaxapp__Received_Fax__c recfax = new efaxapp__Received_Fax__c(
            efaxapp__MCFID__c = '12345678',
            efaxapp__Sent_Fax__c = fax.Id,
            efaxapp__Fax_Number__c = '123456778',
            efaxapp__Received_Date__c = system.now(),
            efaxapp__Total_Pages__c = 3
        );
        insert recfax;
        system.assert(recfax.Id != null);
        attList = new list<Attachment>(TransAm_Test_DataUtility.createAttachments(6, recfax.Id, true));
        //attList[0].name = 'Test.pdf';
        //insert attList;
        system.assert(attList[0].Id != null);
		attListSentFax = new list<Attachment>(TransAm_Test_DataUtility.createAttachments(1, fax.Id, true));
        //TransAm_AttachmentHelper.transferReceivedFaxes(attList);
        try{
            TransAm_AttachmentHelper.transferDoctoRDFromSentFax(attListSentFax);
            List<TransAm_Recruitment_Document__c> lstRecrDocumentsTest = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplicationJobHistory[0].TransAm_Application__c 
                                                                    AND (TransAm_Type__c = 'Background Check Release Form' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Release Form')];
            lstAttachments = TransAm_Test_DataUtility.createAttachments(1, lstRecrDocumentsTest[0].Id, false);
            lstAttachments[0].Name = 'TestUnsupportedDocTestUnsupportedDocTestUnsupportedDoc.mpeg';
            insert lstAttachments;
            TransAm_AttachmentHelper.transferReceivedFaxes(attList);
            List<attachment> fileTypeAtt = TransAm_Test_DataUtility.createAttachments(1, lstRecrDocumentsTest[0].Id, false);
    		fileTypeAtt[0].Name = 'Test.Zip';
            TransAm_AttachmentHelper.CheckTheExtensionOfAttachment(fileTypeAtt);
            fileTypeAtt[0].Name = 'Applicant_Agreement_Contractor';
        }
        catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains(Label.TransAm_FileType) ? true : false;
        }
        test.stopTest();
    }

    /************************************************************************************
    * Method       :    testW4FormEmailTrigger
    * Description  :    Test W4 Form email Trigger
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testW4FormEmailTrigger() {
        lstApplicationJobHistory = [SELECT Id, TransAm_Application__c FROM TransAm_Job_History__c];
        List<TransAm_Recruitment_Document__c> lstRecrDocuments = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplicationJobHistory[0].TransAm_Application__c
                                                                    AND TransAm_Type__c = 'W-4'];
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[0].Id, true);
    }

    /************************************************************************************
    * Method       :    testTransferAttachments
    * Description  :    Test transfer Attachments
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testTransferAttachments() {
        lstApplicationJobHistory = [SELECT Id, TransAm_Application__c FROM TransAm_Job_History__c];
        List<TransAm_Recruitment_Document__c> lstRecrDocuments = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplicationJobHistory[0].TransAm_Application__c
                                                                    AND TransAm_Type__c = 'Previous Employment Verification'];
        TransAm_Test_DataUtility.createAttachments(1, lstApplicationJobHistory[0].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[0].Id, true);
    }
}