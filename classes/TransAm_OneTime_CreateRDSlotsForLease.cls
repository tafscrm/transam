/* Batch Job to create RD Slots for Blank ELA and IC - To be Removed after Go Live and Data Creation */
global class TransAm_OneTime_CreateRDSlotsForLease implements Database.Batchable<sObject> {
    public static Set<String> setAppStatus = new Set<String>();
    public static Set<String> setRDTypes = new Set<String>();
    static{
        setAppStatus.add('Application Received');
        setAppStatus.add('Recruiter Review');
        setAppStatus.add('Manager Review');
        setAppStatus.add('Schedule Orientation');
        setAppStatus.add('Pending Orientation');
        
        setRDTypes.add('Acknowledgement of Review of ICA');
        setRDTypes.add('Acknowledgement of Review of ELA');
        setRDTypes.add('Independent Contractor Agreement');
        setRDTypes.add('Equipment Lease Agreement');
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name, TransAm_Email__c, TransAm_Phone__c, TransAm_SSN__c, TransAm_First_Name__c, TransAm_Last_Name__c';
        query = query + ' FROM TransAm_Application__c';
        query = query + ' WHERE TransAm_Status__c In :setAppStatus';
        query = query + ' AND TransAm_Primary_Application__c = TRUE';
        return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> scope) {
        List<Id> lstAppIds = new List<Id>();
        for(TransAm_Application__c trApp:scope){
           lstAppIds.add(trApp.Id);   
        }
        
        Map<Id, Set<String>> mapAppRDs = new Map<Id, Set<String>>();
        for(TransAm_Recruitment_Document__c rd:[SELECT Id, Name, TransAm_Type__c, TransAm_Application__c FROM TransAm_Recruitment_Document__c 
                                                WHERE TransAm_Application__c In :lstAppIds]){
            if(mapAppRDs.containsKey(rd.TransAm_Application__c)){
                mapAppRDs.get(rd.TransAm_Application__c).add(rd.TransAm_Type__c);
            }else{
                Set<String> setRDType = new Set<String>();
                setRDType.add(rd.TransAm_Type__c);
                mapAppRDs.put(rd.TransAm_Application__c, setRDType);
            }
        }
      
        List<TransAm_Recruitment_Document__c> lstNewRDs = new List<TransAm_Recruitment_Document__c>();
        for(TransAm_Application__c application:scope){
            for(String rdType:setRDTypes){
                if(mapAppRDs.containsKey(application.Id) && !mapAppRDs.get(application.Id).contains(rdType)){
                    TransAm_Recruitment_Document__c recruitmentDoc = new TransAm_Recruitment_Document__c();
                    recruitmentDoc.Name = 'Active '+application.Name+' '+rdType;
                    recruitmentDoc.TransAm_Type__c = rdType;
                    recruitmentDoc.TransAm_Application__c = application.Id;
                    recruitmentDoc.TransAm_Email__c = application.TransAm_Email__c;
                    recruitmentDoc.TransAm_Phone__c = application.TransAm_Phone__c;
                    recruitmentDoc.TransAm_SSN__c = application.TransAm_SSN__c;
                    if(!string.isBlank(application.TransAm_First_Name__c)){
                        recruitmentDoc.TransAm_Applicant_Name__c = application.TransAm_First_Name__c+' '+application.TransAm_First_Name__c;
                    } else {
                        recruitmentDoc.TransAm_Applicant_Name__c = application.TransAm_Last_Name__c;
                    }
                    lstNewRDs.add(recruitmentDoc);
                }
            }  
        }
        
        if(lstNewRDs.size() > 0){
            insert lstNewRDs;
        }
    }
    global void finish(Database.BatchableContext BC) {
        
    }
}