global class TransAm_DriverConversion {
    webService static Id cloneApplication(Id appId) {
       TransAm_Application__c oTransAmApp = new TransAm_Application__c();
        String errorMessage = '';
        String driverRecQId = ''; 
        String ObjectFields = getFields('TransAm_Application__c');
        
        String sQuery = 'Select '+ObjectFields+' from TransAm_Application__c where id=: appId';
        System.debug('sQuery>>>>>>>>>>>>>>..'+sQuery );
        oTransAmApp = Database.Query(sQuery);
        String appAssignUsName = '';
        TransAm_Generic_Custom_Setting__c genericCustSet = TransAm_Generic_Custom_Setting__c.getInstance('Assign Inactive application');
        if(genericCustSet != null)
        	appAssignUsName = 	genericCustSet.TransAm_Message_String__c;
        TransAm_Application__c  cloneApp =  new TransAm_Application__c();
        cloneApp  = oTransAmApp.clone();
        if (cloneApp!=null){
			if(cloneApp.OwnerId != null){
        		map<Id,user> activeUserMap = new map<Id,user>([SELECT Id,isActive,userName FROM user WHERE (Id =:cloneApp.OwnerId OR userName =:appAssignUsName) AND isActive = TRUE]);
        		if(!activeUserMap.isEmpty() && !activeUserMap.containsKey(cloneApp.OwnerId)){
        			list<User> drvRecQ = new list<user>(activeUserMap.values());
        			if(!drvRecQ.isEmpty()){
        				cloneApp.OwnerId = 	drvRecQ[0].Id;
        				driverRecQId = drvRecQ[0].Id;
        			}
        		}
        	}
			cloneApp.TransAm_EBE_Entry_ID__c = '';
			cloneApp.TransAm_Sort_Applications__c = '';
			cloneApp.TransAm_DriverID__c = '';
			cloneApp.TransAm_Emp_Status__c = 'New';
			cloneApp.TransAm_Status_Reason__c = '';
			cloneApp.TransAm_Termination_Date__c = Null;
			cloneApp.TransAm_Status_Flag__c = false; 
			cloneApp.TransAm_Status__c = 'Application Received';
			cloneApp.TransAm_RVI_Export__c = FALSE;
			cloneApp.TransAm_RVI_Export_Date__c = NULL;
			cloneApp.TransAm_By_Pass_Validation__c = false;
			cloneApp.TransAm_One_Leasing_Status__c = '';     
			cloneApp.TransAm_RCom_Pin__c = '';
			cloneApp.TransAm_TERM_Reason__c  = '';
			cloneApp.TransAm_Driver_Manager__c  = '';
			cloneApp.TransAm_TERM_Code__c  = '';  
			cloneApp.TransAm_Hire_Date__c = Null; 
			cloneApp.TransAm_School_Recruiter_Driver__c = '';
			cloneApp.TransAm_Owner_Code__c = '';
			cloneApp.TransAm_Owner_Name__c = Null;       
                        cloneApp.TransAm_Truck_Number__c = NULL;
            		cloneApp.TransAm_Transportation_confirmation__c = false;
            try{
                insert cloneApp;
            }catch(exception e){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_DriverConversion';
                el.TransAm_Method_Name__c = 'cloneApplication';
                el.TransAm_Module_Name__c = 'Application';
                el.TransAm_Exception_Message__c = e.getMessage();
                insert el;
				 if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    String errstrChild = e.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,');    
                    errorMessage = errstrChild.substringBefore(': []');
            }
            else
                errorMessage = 'Not Validation Error';
            throw new TransAM_CustomException(errorMessage);
            }
        }
        
        list<Sobject>  listToUpdate = new list<Sobject>();
        list<TransAm_Recruitment_Document__c> listofRecDoc = new list<TransAm_Recruitment_Document__c>();
        
        list<TransAm_Application__c> appRelatedList= getRelatedList('TransAm_Application__c',appId,'TransAm_Master_Application__c');
        TransAm_Application__c oTranApp = new TransAm_Application__c();
        for(TransAm_Application__c oTransAm : appRelatedList){
			oTranApp = oTransAm.clone();
			oTranApp.TransAm_Master_Application__c = cloneApp.id; 
			oTranApp.TransAm_EBE_Entry_ID__c = '';
			oTranApp.TransAm_By_Pass_Validation__c = false;
			listToUpdate.Add(oTranApp);
			if(String.IsNotBlank(driverRecQId))
				oTranApp.ownerId = driverRecQId;
			else
				oTranApp.OwnerId = cloneApp.OwnerId;
        }
        
        list<TransAm_PreviousAddress__c> addRelatedList= getRelatedList('TransAm_PreviousAddress__c',appId,'TransAm_Application__c');
        TransAm_PreviousAddress__c TranAmAdd = new TransAm_PreviousAddress__c();
        for(TransAm_PreviousAddress__c oTransAmAdd : addRelatedList){
        TranAmAdd = oTransAmAdd.clone();
        TranAmAdd.TransAm_Application__c = cloneApp.id; 
        TranAmAdd.TransAm_EBE_Entry_Id__c = '';
        listToUpdate.Add(TranAmAdd );
        }
        
        list<TransAm_Job_History__c> jobrelatedList =getRelatedList ('TransAm_Job_History__c',appId,'TransAm_Application__c');
        TransAm_Job_History__c TransAmJH = new TransAm_Job_History__c();
        for(TransAm_Job_History__c oTransAmJobHistory : jobrelatedList ){
        TransAmJH = oTransAmJobHistory.clone();
        TransAmJH.TransAm_Application__c = cloneApp.id;
        TransAmJH.TransAm_EBE_Entry_ID__c = '';
        TransAmJH.TransAm_IsCloned__c = TRUE;//NHALLIYAL:added to by pass validation during clone for defect 589341
        listToUpdate.Add(TransAmJH);
        }
        
        list<TransAm_Accident__c> accrelatedList =getRelatedList ('TransAm_Accident__c',appId,'TransAm_Application__c');
        TransAm_Accident__c TransAmAcc = new TransAm_Accident__c();
        for(TransAm_Accident__c oTransAmAcc : accrelatedList ){
        TransAmAcc = oTransAmAcc.clone();
        TransAmAcc.TransAm_Application__c = cloneApp.id;
        TransAmAcc.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmAcc);
        }
        
        list<TransAm_Felony__c> felonyList =getRelatedList ('TransAm_Felony__c',appId,'TransAm_Application__c');
        TransAm_Felony__c TransAmFelony = new TransAm_Felony__c();
        for(TransAm_Felony__c oTransAmFelony : felonyList ){
        TransAmFelony = oTransAmFelony.clone();
        TransAmFelony.TransAm_Application__c = cloneApp.id;
        TransAmFelony.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmFelony);
        }
        
        list<TransAm_License__c> licenseList =getRelatedList ('TransAm_License__c',appId,'TransAm_Application__c');
        TransAm_License__c TransAmLicense = new TransAm_License__c();
        for(TransAm_License__c oTransAmLicense : licenseList){
        TransAmLicense = oTransAmLicense.clone();
        TransAmLicense.TransAm_Application__c = cloneApp.id;
        TransAmLicense.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmLicense);
        }
        
        list<TransAm_Misdemeanor__c> misdemList =getRelatedList ('TransAm_Misdemeanor__c',appId,'TransAm_Application__c');
        TransAm_Misdemeanor__c TransAmMisdem = new TransAm_Misdemeanor__c();
        for(TransAm_Misdemeanor__c oTransAmMisdem : misdemList){
        TransAmMisdem = oTransAmMisdem.clone();
        TransAmMisdem.TransAm_Application__c = cloneApp.id;
        TransAmMisdem.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmMisdem);
        }
        
        list<TransAm_Violation__c> violationList =getRelatedList ('TransAm_Violation__c',appId,'TransAm_Application__c');
        TransAm_Violation__c TransAmViolation = new TransAm_Violation__c();
        for(TransAm_Violation__c oTransAmviolation : violationList){
        TransAmViolation = oTransAmviolation.clone();
        TransAmViolation.TransAm_Application__c = cloneApp.id;
        TransAmViolation.TransAm_EBE_Entry_ID__c = '';
         
        listToUpdate.Add(TransAmViolation);
        }
        
        list<TransAm_Recruitment_Document__c> documentList = [Select id,name,TransAm_Type__c from TransAm_Recruitment_Document__c  where  TransAm_Type__c IN ('MVRStandard','Previous Employment Verification','School Verification Form','Telephone Reference') AND TransAm_Application__c = :appId];
        list<Id> DocIDs = new list<Id>();
        for(TransAm_Recruitment_Document__c oTranDoc :documentList ){
            DocIDs.add(oTranDoc.Id);            
        }
        map<String,String> mapOfDocNameType = new map<String,String>();
        for(TransAm_Recruitment_Document__c oTranDoc :documentList ){
         mapOfDocNameType.put(oTranDoc.Name,oTranDoc.TransAm_Type__c);
        }
         System.debug('mapOfDocNameType >>'+mapOfDocNameType );
        TransAm_Recruitment_Document__c TransAmRecDoc = new TransAm_Recruitment_Document__c();
        list<Attachment> lstAttchment = [Select id,Name,parentId,body,parent.Name from Attachment where parentId in : DocIDs ];
        map<string,list<Attachment> > mapDocTypeAtts = new map<string,list<Attachment> >(); 
        for(Attachment oAtt : lstAttchment){            
            list<Attachment> innerlist = new list<Attachment> ();
            system.debug('mapOfDocNameType.get(oAtt.parent.Name)>>>>>>>>>>>>>'+mapOfDocNameType.get(oAtt.parent.Name));
            if(!mapDocTypeAtts.containskey(mapOfDocNameType.get(oAtt.parent.Name))){              
                innerlist.add(oAtt);
                mapDocTypeAtts.put(mapOfDocNameType.get(oAtt.parent.Name),innerlist);
                
            }
            else{
                innerlist = mapDocTypeAtts.get(mapOfDocNameType.get(oAtt.parent.Name));
                innerlist.add(oAtt);
                mapDocTypeAtts.put(mapOfDocNameType.get(oAtt.parent.Name),innerlist);
            }
            
        }
        System.debug('mapDocTypeAtts >>'+mapDocTypeAtts );
        list<Attachment> lstAttchmentsToUpdate = new list<Attachment> ();
        Attachment oAtt1 = new Attachment();        
        list<TransAm_Recruitment_Document__c> documentListToUpdate = new  list<TransAm_Recruitment_Document__c>();
        
        lstAttchment.clear();
        
        
        for(TransAm_Recruitment_Document__c oTranDoc :  [Select id,name,TransAm_Type__c from TransAm_Recruitment_Document__c  where  TransAm_Type__c IN ('MVRStandard','Previous Employment Verification','School Verification Form','Telephone Reference') AND TransAm_Application__c = : cloneApp.id] ){
             documentListToUpdate.add(oTranDoc);
            
        }
        
        /*if(documentListToUpdate.size()>0){
            insert documentListToUpdate;    
        }*/
        //System.debug('documentListToUpdate  >>'+documentListToUpdate );
        for(TransAm_Recruitment_Document__c oTranDoc :documentListToUpdate ){
            lstAttchment = mapDocTypeAtts.get(oTranDoc.TransAm_Type__c);
            System.debug('lstAttchment 2nd loop >>'+lstAttchment );
            if(lstAttchment!=null){
                for(Attachment oAtt : lstAttchment){
                    oAtt1 = oAtt.Clone();
                    oAtt1.ParentId  = oTranDoc.Id;
                    lstAttchmentsToUpdate.Add(oAtt1);           
                }
                
            }
            
            
        }
         System.debug('lstAttchmentsToUpdate 2nd loop >>'+lstAttchmentsToUpdate );
        if(lstAttchmentsToUpdate.size()>0){
        
        insert lstAttchmentsToUpdate;
        }
        
        system.debug('cloneApp.id+++'+cloneApp.id);
        try{
            
            insert listToUpdate;
            
        }catch(exception e){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_DriverConversion';
            el.TransAm_Method_Name__c = 'cloneApplication';
            el.TransAm_Module_Name__c = 'Application';
            el.TransAm_Exception_Message__c = e.getMessage();
            insert el;
			if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                String errstrChild = e.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,');    
                errorMessage = errstrChild.substringBefore(': []');
            }
            else
                errorMessage = 'Not Validation Error';
            throw new TransAM_CustomException(errorMessage);
        }
        system.debug('cloneApp.id+++'+cloneApp.id);
        return cloneApp.id;
        
    }

    public static string  getFields( String sApi ){
        if(sApi !=''){
        String sQuery = '';
        SObjectType objType = Schema.getGlobalDescribe().get(sApi);      
        Map<string,Schema.SObjectField> mfield = objType.getDescribe().fields.getMap();
        System.debug('inserting'+mfield);
        
        for(Schema.sObjectField field :mfield.values()){
        System.debug('field >>'+field );
            if(sQuery=='' && string.valueof(field)!='TransAm_DriverID__c')   
            sQuery = ' '+field;
        else if(string.valueof(field)!='TransAm_DriverID__c')
            sQuery = sQuery+','+field;
        }
        System.debug('sQuery>>'+sQuery);
        Return sQuery;
        }
        return '';

}

    public static List<Sobject> getRelatedList( String ObjectName, ID AppID, String lookupField){

        list<Sobject>  lstOfRelated = new list<Sobject>();
         
        String  sRelatedObject = getFields(ObjectName);
        String sQuery = 'Select '+sRelatedObject+' from '+ObjectName+ ' where '+lookupField+' = : appId';
        
        lstOfRelated = Database.Query(sQuery);
        
        return lstOfRelated;

}
    
}