/******************************************************************************************
* Create By    :     Raushan Anand
* Create Date  :     04/06/2017
* Description  :     Scheduler Class to delete archived attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Raushan Anand                    04/06/2017           Initial version.
*****************************************************************************************/
global class TransAm_ArchivedDocumentDeleteScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_ArchivedDocumentDeleteBatch(),200);
    }

}