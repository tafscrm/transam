/******************************************************************************************
* Create By    :     Naveenkanth B
* Create Date  :     04/06/2017
* Description  :     Scheduler Class to delete DocuSign attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Naveenkanth B                    04/06/2017           Initial version.
*****************************************************************************************/
global class TransAm_DocuSignDocumentDeleteScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_DocuSignDocumentDeleteBatch(),200);
    }

}