/******************************************************************************************
* Created By    :     Karthik Gulla
* Created Date  :     21/08/2017
* Description   :     Webservice class to create and send the docusign envelope
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                      <Date>           Initial version.
*****************************************************************************************/
global class TransAm_SendApplicationSummary{
    //Docusign API Connect Details
    private static String docAccountId; 
    private static String docTemplateId;
    private static String baseUrl;
    public static Map<String,String> mapJHMappings = new Map<String,String>();
    public static Map<String,String> mapAppMappings = new Map<String,String>();
    public static Map<String,String> mapPAMappings = new Map<String,String>();
    public static Map<String,String> mapFelMappings = new Map<String,String>();
    public static Map<String,String> mapMdMappings = new Map<String,String>();
    public static Map<String,String> mapViolMappings = new Map<String,String>();
    public static Map<String,String> mapLicMappings = new Map<String,String>();
    public static Map<String,String> mapAccMappings = new Map<String,String>();
    public static Map<String,TransAm_DocusignAPI_Configurations__mdt> mapDocusignAPIDetails = new Map<String,TransAm_DocusignAPI_Configurations__mdt>();

    public static String referredByName;
    public static String schoolName;
    public static String appFont;
    public static String appFontSize;
    public static String signerOneRole;

    static{
        mapAppMappings = getApplicationMappings();
        mapJHMappings = getJobHistoryMappings();
        mapPAMappings = getPreviousAddressMappings();
        mapFelMappings = getFelonyMappings();
        mapMdMappings = getMisdemeanorMappings();
        mapViolMappings = getViolationsMappings();
        mapLicMappings = getLicenseMappings();
        mapAccMappings = getAccidentsMappings();
        mapDocusignAPIDetails = getDocusignAPICredentials();

        appFont = 'TimesNewRoman';
        appFontSize = 'size10';
        signerOneRole = 'Signer1';
    }

    Webservice static void sendAppSummary(Id appId) {
        TransAm_DocusignAPI_Configurations__mdt orgDocusignDetails;
        Organization orgDetails = [SELECT IsSandbox FROM Organization LIMIT 1];
        if(orgDetails.IsSandbox && mapDocusignAPIDetails.containsKey('DocusignDemo')){
            orgDocusignDetails = mapDocusignAPIDetails.get('DocusignDemo');       
        }
        else if(mapDocusignAPIDetails.containsKey('DocusignProd')){
            orgDocusignDetails = mapDocusignAPIDetails.get('DocusignProd');
        }

        String authenticationHeader =   '<DocuSignCredentials>' + 
                                            '<Username>' + orgDocusignDetails.TransAm_Username__c + '</Username>' +
                                            '<Password>' + orgDocusignDetails.TransAm_Password__c + '</Password>' + 
                                            '<IntegratorKey>' + orgDocusignDetails.TransAm_Integrator_Key__c  + '</IntegratorKey>' + 
                                        '</DocuSignCredentials>';

        baseUrl = orgDocusignDetails.TransAm_BaseUrl__c;
        docAccountId = orgDocusignDetails.TransAm_AccountId__c;
        docTemplateId = orgDocusignDetails.TransAm_TemplateId__c;

        List<TabWrapper> lstNewTextTabs = new List<TabWrapper>();

        // Login - Authentication - REST API Call - '/v2/login_information'
        if(!test.isRunningTest())
            sendRESTAPIRequest(authenticationHeader, 'GET', baseUrl + '/v2/login_information', null);

        String fields = String.join(new List<String>(TransAm_Application__c.sObjectType.getDescribe().fields.getMap().keySet()), ',');
        String queryString = 'SELECT ' + fields + ' FROM' + ' TransAm_Application__c WHERE Id = ';
        queryString += ':appId';

        TransAm_Application__c application = Database.query(queryString);
        for(String appMapping:mapAppMappings.keySet()){
            TabWrapper appTxtTabWrapper = new TabWrapper();
            appTxtTabWrapper.anchorString = appMapping;
            if(application.get(mapAppMappings.get(appMapping)) != null){
                if(appMapping.containsIgnoreCase('date'))
                    appTxtTabWrapper.value = String.valueOf(Date.valueOf(application.get(mapAppMappings.get(appMapping))));
                else
                    appTxtTabWrapper.value = String.valueOf(application.get(mapAppMappings.get(appMapping)));
            }
            appTxtTabWrapper.required = String.valueOf('false');
            appTxtTabWrapper.locked = String.valueOf('true');
            appTxtTabWrapper.font = appFont;
            appTxtTabWrapper.fontSize = appFontSize;
            lstNewTextTabs.add(appTxtTabWrapper);
        }

        Id referredById = (Id)application.get('transam_referredby__c');
        Id conId = (Id)application.get('transam_applicant__c');
        Contact conOne;
        Contact conTwo;

        if(referredById != null)
            conOne = [SELECT Id, Name, Docusign_School_Field__c FROM Contact WHERE Id = :referredById];
        if(conId != null)
            conTwo = [SELECT Id, Name, Docusign_School_Field__c FROM Contact WHERE Id = :conId];

        TabWrapper appTxtTabWrapperOne = new TabWrapper();
        appTxtTabWrapperOne.value = conTwo.Docusign_School_Field__c;
        appTxtTabWrapperOne.required = String.valueOf('false');
        appTxtTabWrapperOne.locked = String.valueOf('true');
        appTxtTabWrapperOne.anchorString = 'schoolname';
        appTxtTabWrapperOne.font = appFont;
        appTxtTabWrapperOne.fontSize = appFontSize;

        TabWrapper appTxtTabWrapperTwo = new TabWrapper();
        appTxtTabWrapperTwo.value = referredById != null ? conOne.Name : '';
        appTxtTabWrapperTwo.required = String.valueOf('false');
        appTxtTabWrapperTwo.locked = String.valueOf('true');
        appTxtTabWrapperTwo.anchorString = 'appReferredBy';
        appTxtTabWrapperTwo.font = appFont;
        appTxtTabWrapperTwo.fontSize = appFontSize;
        lstNewTextTabs.add(appTxtTabWrapperOne);
        lstNewTextTabs.add(appTxtTabWrapperTwo);

        //Job History
        List<TransAm_Job_History__c> lstJobHistories = [SELECT Id, Name, TransAm_Employed_From__c, TransAm_Employed_To__c, TransAm_Employer_for_Docusign__c,
                                                                TransAm_Phone__c, TransAm_Address__c, TransAm_City__c, TransAm_State__c, TransAm_Zip__c,
                                                                TransAm_Position_Held__c, TransAm_DOT_regulated__c, TransAm_Subject_to_DOT_Alcohol_Testing__c,
                                                                TransAm_Equipment_Driven__c, TransAm_Geographical_Areas_of_Operation__c, TransAm_Reason_for_Leaving__c,
                                                                TransAm_Leaving_Notes__c, TransAm_Type_of_Equipment_operated__c 
                                                        FROM TransAm_Job_History__c 
                                                        WHERE TransAm_Application__c = :appId
                                                        ORDER BY TransAm_Employed_From__c DESC];
        lstNewTextTabs.addAll(createTextTabs(lstJobHistories, mapJHMappings, 16));

        //Previous Address
        List<sObject> lstPreviousAddress = [SELECT TransAm_From_Date__c, TransAm_To_Date__c, TransAm_Address__c, 
                                                    TransAm_City__c, TransAm_State__c, TransAm_Zip__c 
                                            FROM TransAm_PreviousAddress__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_From_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstPreviousAddress, mapPAMappings, 4));

        //Felony
        List<sObject> lstFelony = [SELECT TransAm_Date__c, TransAm_Felony_Type__c, TransAm_Explain__c
                                            FROM TransAm_Felony__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstFelony, mapFelMappings, 4));

        //Misdemeanor
        List<sObject> lstMisdemeanor = [SELECT TransAm_Date__c, TransAm_Explain__c
                                            FROM TransAm_Misdemeanor__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstMisdemeanor, mapMdMappings, 4));

        //Violations
        List<sObject> lstViolations = [SELECT TransAm_Location__c, TransAm_Violation_Date__c, TransAm_Violation_State__c, TransAm_Charges__c, TransAm_Comments_Notes__c 
                                            FROM TransAm_Violation__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_Violation_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstViolations, mapViolMappings, 4));

        //License
        List<sObject> lstLicenses = [SELECT TransAm_LicenseNo__c, TransAm_License_Type__c, TransAm_State__c, TransAm_Expiration_Date__c, TransAm_Current__c 
                                            FROM TransAm_License__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_Expiration_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstLicenses, mapLicMappings, 4));

        //Accidents
        List<sObject> lstAccidents = [SELECT TransAm_Date__c, TransAm_Accident_Type__c, TransAm_Fatality__c, 
                                            TransAm_Injuries__c, TransAm_Preventable__c, TransAm_Vehicle_Type__c 
                                            FROM TransAm_Accident__c 
                                            WHERE TransAm_Application__c = :appId
                                            ORDER BY TransAm_Date__c DESC];

        lstNewTextTabs.addAll(createTextTabs(lstAccidents, mapAccMappings, 4));

        //Text Custom Fields
        List<TextCustomFieldsWrapper> lstTextCustomFields = new List<TextCustomFieldsWrapper>();
        TextCustomFieldsWrapper txtCFWrapperOne = new TextCustomFieldsWrapper();
        txtCFWrapperOne.name = 'PlatformName';
        txtCFWrapperOne.required = String.valueOf('false');
        txtCFWrapperOne.show = String.valueOf('false');
        txtCFWrapperOne.value = 'Salesforce';
        lstTextCustomFields.add(txtCFWrapperOne);

        TextCustomFieldsWrapper txtCFWrapperTwo = new TextCustomFieldsWrapper();
        txtCFWrapperTwo.name = 'DSFSSourceObjectId';
        txtCFWrapperTwo.required = String.valueOf('false');
        txtCFWrapperTwo.show = String.valueOf('false');
        txtCFWrapperTwo.value = appId+'~TransAm_Application__c';
        lstTextCustomFields.add(txtCFWrapperTwo);

        CustomFieldsWrapper custFieldsWrapper = new CustomFieldsWrapper();
        custFieldsWrapper.textCustomFields = lstTextCustomFields;

        //Sign Here Tabs
        List<TabWrapper> lstSignHereTabs = new List<TabWrapper>();
        TabWrapper signTabWrapper = new TabWrapper();
        signTabWrapper.anchorString = 'SignHere_1';
        signTabWrapper.required = String.valueOf('true');
        lstSignHereTabs.add(signTabWrapper);

        //Date Signed Tabs
        List<TabWrapper> lstDateSignedHereTabs = new List<TabWrapper>();
        TabWrapper dateSignedTabWrapper = new TabWrapper();
        dateSignedTabWrapper.anchorString = 'DS_1';
        dateSignedTabWrapper.font = appFont;
        dateSignedTabWrapper.required = String.valueOf('true');
        lstDateSignedHereTabs.add(dateSignedTabWrapper);

        //Form a list of all tabs
        DocusignTabsWrapper tbsWrapper = new DocusignTabsWrapper();
        tbsWrapper.textTabs = lstNewTextTabs;      
        tbsWrapper.signHereTabs = lstSignHereTabs;
        tbsWrapper.dateSignedTabs = lstDateSignedHereTabs;

        //Form a Template Roles
        TemplateRolesWrapper tempRolesWrapper = new TemplateRolesWrapper();
        if(application.get('TransAm_Email__c') != null)
            tempRolesWrapper.email = String.valueOf(application.get('TransAm_Email__c'));
        tempRolesWrapper.roleName = signerOneRole;

        if(application.get('TransAm_First_Name__c') != null && application.get('TransAm_Last_Name__c') != null)
            tempRolesWrapper.name = String.valueOf(application.get('TransAm_First_Name__c'))+' '+String.valueOf(application.get('TransAm_Last_Name__c'));

        List<TemplateRolesWrapper> lstTemplateRolesWrapper = new List<TemplateRolesWrapper>();
        lstTemplateRolesWrapper.add(tempRolesWrapper);

        tempRolesWrapper.tabs = tbsWrapper;

        //Form a Docusign REST API request (JSON)
        DocusignRequestWrapper docReqWrapper = new DocusignRequestWrapper();
        docReqWrapper.accountId = docAccountId;
        docReqWrapper.templateId = docTemplateId;
        docReqWrapper.templateRoles = lstTemplateRolesWrapper;
        docReqWrapper.customFields = custFieldsWrapper;
        docReqWrapper.status = 'sent';

        //Create Envelope REST API Call - '/v2/accounts/accountId/envelopes'
        if(!test.isRunningTest())
            sendRESTAPIRequest(authenticationHeader, 'POST', baseUrl + '/v2/accounts/'+docAccountId+'/envelopes', JSON.serializePretty(docReqWrapper));
    }

    public static void sendRESTAPIRequest(String authHeader, String methodType, String endPoint, String reqBody){
        HttpRequest httpReq = new HttpRequest();
        httpReq.setHeader('Content-Type', 'application/json');
        httpReq.setHeader('X-DocuSign-Authentication', authHeader);
        httpReq.setEndpoint(endPoint);
        httpReq.setMethod(methodType);
        if(reqBody != null && reqBody != '')
            httpReq.setBody(reqBody);

        Http restHttp = new Http();
        HttpResponse httpRes = restHttp.send(httpReq);
        String a = httpRes.getBody();
    }

    public static Map<String,String> getApplicationMappings(){
        Map<String,String> mapApplicationMappings = new Map<String,String>();
        mapApplicationMappings.put('lastname','TransAm_Last_Name__c');        
        mapApplicationMappings.put('firstname','TransAm_First_Name__c');
        mapApplicationMappings.put('ssn','TransAm_SSN_for_Docusign__c');               
        mapApplicationMappings.put('dateofbirth','TransAm_Date_Of_Birth__c');
        mapApplicationMappings.put('appaddress','TransAm_Address__c');
        mapApplicationMappings.put('app_addresstwo','TransAm_AddressLine2__c');              
        mapApplicationMappings.put('appCity','TransAm_City__c');
        mapApplicationMappings.put('appState','TransAm_State__c');           
        mapApplicationMappings.put('appZip','TransAm_Zip__c');            
        mapApplicationMappings.put('appPhone','TransAm_Phone__c');
        mapApplicationMappings.put('appEmail','TransAm_Email__c');
        mapApplicationMappings.put('appHazmat','TransAm_Hazardous_Material_Endorsement__c');

        mapApplicationMappings.put('ECFN','TransAm_Emergency_First_Name__c');
        mapApplicationMappings.put('ECLN','TransAm_Emergency_Last_Name__c');               
        mapApplicationMappings.put('ECP','TransAm_Emergency_Phone__c');
        mapApplicationMappings.put('ECAP','Emergency_Contact_1_Alternate_Phone__c');
        mapApplicationMappings.put('app_Felony','TransAm_Felony__c');

        mapApplicationMappings.put('app_Mis','TransAm_OrientationAdditionalCriminal__c');

        mapApplicationMappings.put('HUECDUI','TransAm_Have_you_had_a_DUI__c');            
        mapApplicationMappings.put('ExplnDWIDUIBAC','TransAm_ExplainDWIDUIBAC__c');

        mapApplicationMappings.put('LEBSOR','TransAm_Suspended_License__c');
        mapApplicationMappings.put('EILSOR','TransAm_ExplainSuspend__c');

        mapApplicationMappings.put('ExpDOTDate','TransAm_Expiration_Date__c');

        mapApplicationMappings.put('GIOOT','TransAm_Get_in_and_out_of_a_truck__c');        
        mapApplicationMappings.put('GIOOTR','TransAm_Get_in_and_out_of_a_trailer__c');
        mapApplicationMappings.put('GUUTPD','TransAm_GetUnderUnitDuty__c');               
        mapApplicationMappings.put('RLHCT','TransAm_RaiseLowerHood__c');
        mapApplicationMappings.put('RLTDUL','TransAm_RaiseLowerDollies__c');
        mapApplicationMappings.put('RFWP','TransAm_FifthWheelPin__c');              
        mapApplicationMappings.put('OCTD','TransAm_TrailerDoor__c');
        mapApplicationMappings.put('RCU70LBS','TransAm_Lift70Lbs__c');           
        mapApplicationMappings.put('SSIDSFLT','TransAm_DriveLongTime__c');            
        mapApplicationMappings.put('AEPTTTLTR','TransAm_TrailerLever__c');
        mapApplicationMappings.put('BODMHADOT','TransAm_DOTHours__c');
        mapApplicationMappings.put('AYWTCTDT','TransAm_ConsentDrugTest__c');
        return mapApplicationMappings;
    }

    public static Map<String,String> getJobHistoryMappings(){
        Map<String,String> mapJobHistoryMappings = new Map<String,String>();
        mapJobHistoryMappings.put('JH_StartDate', 'TransAm_Employed_From__c');
        mapJobHistoryMappings.put('JH_EndDate', 'TransAm_Employed_To__c');
        mapJobHistoryMappings.put('JH_Employer', 'TransAm_Employer_for_Docusign__c');
        mapJobHistoryMappings.put('JH_EmpPhone', 'TransAm_Phone__c');
        mapJobHistoryMappings.put('JH_Address', 'TransAm_Address__c');
        mapJobHistoryMappings.put('JH_City', 'TransAm_City__c');
        mapJobHistoryMappings.put('JH_State', 'TransAm_State__c');
        mapJobHistoryMappings.put('JH_Zip', 'TransAm_Zip__c');
        mapJobHistoryMappings.put('JH_PosTitle', 'TransAm_Position_Held__c');
        mapJobHistoryMappings.put('JH_DotR', 'TransAm_DOT_regulated__c');
        mapJobHistoryMappings.put('JH_DAT', 'TransAm_Subject_to_DOT_Alcohol_Testing__c');
        mapJobHistoryMappings.put('JH_VehiclesDriven', 'TransAm_Type_of_Equipment_operated__c');
        mapJobHistoryMappings.put('JH_AreasRan', 'TransAm_Geographical_Areas_of_Operation__c');
        mapJobHistoryMappings.put('JH_ReasonForLeaving', 'TransAm_Reason_for_Leaving__c');
        mapJobHistoryMappings.put('JH_ReasonForLeavingNotes', 'TransAm_Leaving_Notes__c');
        return mapJobHistoryMappings;
    }

    public static Map<String,String> getPreviousAddressMappings(){
        Map<String,String> mapPreviousAddressMappings = new Map<String,String>();
        mapPreviousAddressMappings.put('PA_FromDate', 'TransAm_From_Date__c');
        mapPreviousAddressMappings.put('PA_ToDate', 'TransAm_To_Date__c');
        mapPreviousAddressMappings.put('PA_Address', 'TransAm_Address__c');
        mapPreviousAddressMappings.put('PA_City', 'TransAm_City__c');
        mapPreviousAddressMappings.put('PA_State', 'TransAm_State__c');
        mapPreviousAddressMappings.put('PA_Zip', 'TransAm_Zip__c');
        return mapPreviousAddressMappings;
    }

    public static Map<String,String> getFelonyMappings(){
        Map<String,String> mapFelonyMappings = new Map<String,String>();
        mapFelonyMappings.put('AppFelonyDate', 'TransAm_Date__c');
        mapFelonyMappings.put('AppFelonyType', 'TransAm_Felony_Type__c');
        mapFelonyMappings.put('AppFelonyExplain', 'TransAm_Explain__c');
        return mapFelonyMappings;
    }

    public static Map<String,String> getMisdemeanorMappings(){
        Map<String,String> mapMisdemeanorMappings = new Map<String,String>();
        mapMisdemeanorMappings.put('AppMisdDate', 'TransAm_Date__c');
        mapMisdemeanorMappings.put('AppMisdExplain', 'TransAm_Explain__c');
        return mapMisdemeanorMappings;
    }

    public static Map<String,String> getViolationsMappings(){
        Map<String,String> mapViolationsMappings = new Map<String,String>();
        mapViolationsMappings.put('ViolationsCity', 'TransAm_Location__c');
        mapViolationsMappings.put('ViolationsState', 'TransAm_Violation_State__c');
        mapViolationsMappings.put('ViolationsDate', 'TransAm_Violation_Date__c');
        mapViolationsMappings.put('ViolationsCharges', 'TransAm_Charges__c');
        mapViolationsMappings.put('ViolationsComments', 'TransAm_Comments_Notes__c');
        return mapViolationsMappings;
    }
    
    public static Map<String,String> getLicenseMappings(){
        Map<String,String> mapLicenseMappings = new Map<String,String>();
        mapLicenseMappings.put('AppLicense#', 'TransAm_LicenseNo__c');
        mapLicenseMappings.put('AppLicenseType', 'TransAm_License_Type__c');
        mapLicenseMappings.put('AppLicenseState', 'TransAm_State__c');
        mapLicenseMappings.put('AppLicExpDate', 'TransAm_Expiration_Date__c');
        mapLicenseMappings.put('AppCurrentLicense', 'TransAm_Current__c');
        return mapLicenseMappings;
    }

    public static Map<String,String> getAccidentsMappings(){
        Map<String,String> mapAccidentsMappings = new Map<String,String>();
        mapAccidentsMappings.put('accidentdate', 'TransAm_Date__c');
        mapAccidentsMappings.put('AccidentType', 'TransAm_Accident_Type__c');
        mapAccidentsMappings.put('fatality', 'TransAm_Fatality__c');
        mapAccidentsMappings.put('AppAccInjuries', 'TransAm_Injuries__c');
        mapAccidentsMappings.put('AppAccPrev', 'TransAm_Preventable__c');
        mapAccidentsMappings.put('AppVehicleType', 'TransAm_Vehicle_Type__c');
        return mapAccidentsMappings;
    }

    public static List<TabWrapper> createTextTabs(List<sObject> lstSobjects, Map<String,String> mappings, Integer Count){
        List<TabWrapper> lstNewTextTabs = new List<TabWrapper>();
        Integer index = 1;       
        for(sObject trSobject:lstSobjects){
            for(String objMapping:mappings.keySet()){
                if(index <= count){
                    TabWrapper txtTabWrapper = new TabWrapper();
                    if(index > 9)
                        txtTabWrapper.anchorString = objMapping +'_c'+ String.valueOf(index);
                    else
                        txtTabWrapper.anchorString = objMapping +'_'+ String.valueOf(index);

                    if(objMapping.containsIgnoreCase('date')){
                        txtTabWrapper.value = String.valueOf(Date.valueOf(trSobject.get(mappings.get(objMapping))));
                    }
                    else
                        txtTabWrapper.value = String.valueOf(trSobject.get(mappings.get(objMapping)));
                    txtTabWrapper.required = String.valueOf('false');
                    txtTabWrapper.locked = String.valueOf('true');
                    txtTabWrapper.font = appFont;
                    txtTabWrapper.fontSize = appFontSize;
                    lstNewTextTabs.add(txtTabWrapper);
                }
            }
            index++;
        }
        return lstNewTextTabs;
    }

    public static Map<String,TransAm_DocusignAPI_Configurations__mdt> getDocusignAPICredentials(){
        Map<String,TransAm_DocusignAPI_Configurations__mdt> mapDocSignAPIDetails = new Map<String,TransAm_DocusignAPI_Configurations__mdt>();
        for(TransAm_DocusignAPI_Configurations__mdt docMapping : [SELECT Id, MasterLabel, TransAm_AccountId__c, TransAm_BaseUrl__c, TransAm_Integrator_Key__c,
                                                                    TransAm_Password__c, TransAm_TemplateId__c, TransAm_Username__c
                                                                FROM TransAm_DocusignAPI_Configurations__mdt]){
              mapDocSignAPIDetails.put(docMapping.MasterLabel, docMapping);                                                     
        }
        return mapDocSignAPIDetails;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  DocusignRequestWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to bind the Docusign API request data
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class DocusignRequestWrapper{        
        List<TemplateRolesWrapper> templateRoles;
        String accountId;
        String templateId;
        String status;
        CustomFieldsWrapper customFields;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  CustomFieldsWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to add custom field details
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class CustomFieldsWrapper{
        List<TextCustomFieldsWrapper> textCustomFields;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  CustomFieldsWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to add custom field details
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class TextCustomFieldsWrapper{
        String name;
        String show;
        String required;
        String value;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  TemplateRolesWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to bind template roles with tab details
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class TemplateRolesWrapper{
        String email;
        String name;
        String roleName;
        DocusignTabsWrapper tabs;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  DocusignTabsWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to bind different types of tab details
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class DocusignTabsWrapper{
        List<TabWrapper> textTabs;
        List<TabWrapper> signHereTabs;
        List<TabWrapper> dateSignedTabs;
    }

    /****************************************************************************************
    * Created By      :  Karthik Gulla
    * Class Name      :  TabWrapper
    * Created Date    :  20-Aug-2017
    * Description     :  Wrapper Class to bind tab details
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public class TabWrapper{
        String anchorString;
        String value;
        String required;
        String locked;
        String font;
        String fontSize;
        String show;
        String configurationType;
        String name;
    }
}