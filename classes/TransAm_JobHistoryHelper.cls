/* Class Name   : TransAm_JobHistoryHelper
 * Description  : Helper class for the Violation trigger
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_JobHistoryHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  16-Feb-2017
    * Description     :  populate the application Id on the Violation record
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateApplicationOnJobHistory(list<TransAm_Job_History__c> jobList){
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        if(UserInfo.getProfileId()==profileId){   
            map<string,Id> applicationMap = new map<string,Id>();
            set<String> parentIdSet = new set<String>();        
            try{
                for(TransAm_Job_History__c jobHistoryObj : jobList){
                    parentIdSet.add(jobHistoryObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_Job_History__c jobHistoryObj : jobList){
                    if(applicationMap.containsKey(jobHistoryObj.TransAm_EBE_Parent_ID__c)){
                        jobHistoryObj.TransAm_Application__c = applicationMap.get(jobHistoryObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_JobHistoryHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnJobHistory';
                el.TransAm_Module_Name__c = 'Job History';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el; 
            }
        }
    }
    
    
     /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  16-Feb-2017
    * Description     :  populate the Employer on Job History
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateEmployerOnJobHistory(list<TransAm_Job_History__c> jobList){
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        set<string> employerNames = new set<string>();
        map<string,Id> employerMap = new map<string,Id>();
        if(UserInfo.getProfileId()==profileId){   
            map<string,Id> applicationMap = new map<string,Id>();        
            try{
                for(TransAm_Job_History__c jobHistoryObj : jobList){
                    if(jobHistoryObj.TransAm_Other_Employer__c!=null){
                        employerNames.add(jobHistoryObj.TransAm_Other_Employer__c);
                    }
                }
                for(TransAm_Employer__c emp : [SELECT ID,Name FROM TransAm_Employer__c WHERE Name IN : employerNames]){
                    employerMap.put(emp.Name,emp.Id);
                }
                for(TransAm_Job_History__c jobHistoryObj : jobList){
                    if(employerMap.containsKey(jobHistoryObj.TransAm_Other_Employer__c)){
                        jobHistoryObj.TransAm_Employer2__c       = employerMap.get(jobHistoryObj.TransAm_Other_Employer__c);
                        //jobHistoryObj.TransAm_Other_Employer__c  = '';
                    }
                }                
            }catch(Exception excep){
                TransAm_Error_Log__c el           = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c          = 'TransAm_JobHistoryHelper';
                el.TransAm_Method_Name__c         = 'populateEmployerOnJobHistory';
                el.TransAm_Module_Name__c         = 'Job History';
                el.TransAm_Exception_Message__c   = excep.getMessage();
                insert el; 
            }
        }
    }
/****************************************************************************************
* Created By      :  Nitesh Halliyal
* Create Date     :  11-July-2017
* Description     :  Populate the background form from RD of parent application of
                     Job Histroy onto Job History for User story 588471. 
* Modification Log:  Initial version.
***************************************************************************************/
    public static Void createAttachmentFromRD(list<TransAm_Job_History__c> jobList){
        Set<String> appId = new set<String>();
        Map<String,String> applicationRDMap = new map<String,string>();
        Map<String,Attachment> appAttachMap = new Map<String,Attachment>();
        List<Attachment> jbHistAttachList = new list<Attachment>();
        
        for(TransAm_Job_History__c jbList :jobList){
            appId.add(jbList.TransAm_Application__c);
        }
        for(TransAm_Recruitment_Document__c RD : [SELECT Id,TransAm_Application__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Type__c= 'Background Check Release Form' AND TransAm_Application__c IN:appId]){
            applicationRDMap.put(RD.Id,RD.TransAm_Application__c);  
        }
        for(Attachment attach :[SELECT Id,name,body,contentType,parentId FROM Attachment WHERE ParentId IN:applicationRDMap.Keyset()]){
            if(applicationRDMap.containsKey(attach.parentId))
                appAttachMap.put(applicationRDMap.get(attach.parentId),attach);         
        }   
        for(TransAm_Job_History__c jbHistAttach : jobList){
            if(appAttachMap.containskey(jbHistAttach.TransAm_Application__c)){
                Attachment att = new attachment();
                att.name = 'backgroundForm-'+String.valueof(Math.Random()).remove('.')+'.pdf';
                att.body = appAttachMap.get(jbHistAttach.TransAm_Application__c).body;
                att.Contenttype = appAttachMap.get(jbHistAttach.TransAm_Application__c).contentType;
                att.parentId = jbHistAttach.Id;
                jbHistAttachList.add(att);
            }
        }
        system.debug('attach list**'+jbHistAttachList);
        try{
            if(!jbHistAttachList.isEmpty())
                Insert jbHistAttachList;
        }
        catch(Exception e){
            TransAm_Error_Log__c el           = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c          = 'TransAm_JobHistoryHelper';
                el.TransAm_Method_Name__c         = 'createAttachmentFromRD';
                el.TransAm_Module_Name__c         = 'Job History';
                el.TransAm_Exception_Message__c   = e.getMessage();
                insert el;    
        }
    }
}