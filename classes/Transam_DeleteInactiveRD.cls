global class Transam_DeleteInactiveRD implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    List<String> statusList = new List<String>();
    statusList.add('Application Complete');
    statusList.add('DQ');
    statusList.add('No Contact');
    statusList.add('No Show');
    statusList.add('Merged');    
    
    String query = 'SELECT Id,Transam_Application__c FROM TransAm_Recruitment_Document__c where TransAM_Application__r.TransAm_Status__c IN: statusList AND TransAM_Application__r.TransAm_Primary_Application__c =true';
    return database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Map<Id,Id> appIdRDMap = new Map<Id,Id>();
        //Set<Id> recScopeIdSet = new Set<Id>();
        List<TransAm_Recruitment_Document__c> rdScope = (List<TransAm_Recruitment_Document__c>) scope;
        for(TransAm_Recruitment_Document__c rd : rdScope){
            //recScopeIdSet.add(rd.Id);
            appIdRDMap.put(rd.Id,rd.Transam_Application__c);
        }
        List<Attachment> attachList = [Select Id,ParentId FROM Attachment WHERE ParentId IN: appIdRDMap.keySet()];
        Set<Id> attIdSet = new Set<Id>();
        List<TransAm_Recruitment_Document__c> objRDDel = new List<TransAm_Recruitment_Document__c>();
        for(Attachment att : attachList){
            attIdSet.add(appIdRDMap.get(att.ParentId));
        }
        
        for(TransAm_Recruitment_Document__c objRD : rdScope){
            if(!attIdSet.contains(appIdRDMap.get(objRD.Id))){
                objRDDel.add(objRD);
            }
        }
          
        System.debug('objRDDEL-->'+objRDDel.size()+' --> '+objRDDel);
        DELETE objRDDel;
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}