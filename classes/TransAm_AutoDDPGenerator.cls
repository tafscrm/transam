/******************************************************************************************
* Create By    :     Pankaj Singh
* Create Date  :     08/22/2017
* Description  :     Utility class for generating DDP from APEX trigger to stamp Fax status and time.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Pankaj Singh                     08/22/2017           Initial version.
*****************************************************************************************/
public class TransAm_AutoDDPGenerator{
    
    @future(callout=true)
    public static void generateDocument(set<Id> sentFaxIdSet, string sessionId){     
     try{
            map<Id,efaxapp__Sent_Fax__c> sentFaxMap = new map<Id,efaxapp__Sent_Fax__c>();
            map<string,Id> ddpNameIdMap = new map<string,Id>();
            map<Id,Id> deliveryOptionMap = new map<Id,Id>();
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = sessionId;
            String ddpId = '';
            String deliveryId = '';
            
            for(Loop__DDP__c ddp : [SELECT ID,Name,(SELECT Id,Name FROM Loop__Custom_Integration_Options__r WHERE Name = 'Attach') FROM Loop__DDP__c]){
                ddpNameIdMap.put(ddp.Name, ddp.Id);
                deliveryOptionMap.put(ddp.Id,ddp.Loop__Custom_Integration_Options__r[0].Id);
            }            
            for(efaxapp__Sent_Fax__c sf : [SELECT ID, efaxapp__Subject__c,efaxapp__Attachment_ID__c FROM efaxapp__Sent_Fax__c  WHERE ID IN:sentFaxIdSet]){
                sentFaxMap.put(sf.Id,sf);
            }             
            for(Id sentFaxId : sentFaxIdSet){
                ddpId = ddpNameIdMap.get(sentFaxMap.get(sentFaxId).efaxapp__Subject__c);
                deliveryId = deliveryOptionMap.get(ddpNameIdMap.get(sentFaxMap.get(sentFaxId).efaxapp__Subject__c));
                system.debug('inside for'+ddpId );
                String attachIds='';                
                attachIds = sentFaxMap.get(sentFaxId).efaxapp__Attachment_ID__c;
                system.debug('prinitng atachment ids::'+deliveryId);            
                //Below ID is of main "DDP"
                lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                sentFaxId, // MAIN RECORD ID - SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES
                ddpId,
                new Map<string, string> { 'deploy' => deliveryId,'attachIds'=>attachIds }
                ));
             }
             lm.sendAllRequests();
        }
        catch(Exception e){
            system.debug('printing exception message:'+e);
        }     
    }
}