public class TransAm_AppMergeFieldsFormController{

    public List<TransAm_Application__c> ChildAppList{get;set;}    
    public set<id> selectedAppsIdSet;
    public id masterId;
    public TransAm_Application__c masterApp;
    public string queryString;
    public list<string> fieldnames = new list<string>();
    public map<string, list<string>> dynamicFSetMap{get;set;}
    public map<string, string> fieldAPILabelMap = new map<string, string>();
    public map<string, masterWrapper> masterWrapperMap{get;set;}
    public map<string, app1Wrapper> app1WrapperMap{get;set;}
    public map<string, app2Wrapper> app2WrapperMap{get;set;}
    public map<string, app3Wrapper> app3WrapperMap{get;set;}    
    public boolean bChild1{get;set;}
    public boolean bChild2{get;set;}
    public boolean bChild3{get;set;}
    public list<string> fieldSetNamesList = new list<string>();
    public integer fSetCount = 0;
    public String fieldSetName{get;set;}
    public map<string, string> fSetDescriptionMap{get;set;}
    public Schema.DisplayType fDisplayType;
    
    public list<voilationsWrapper> listVoilations {get;set;}
    public list<addressesWrapper> listAddresses {get;set;}
    public list<accidentsWrapper> listAccidents {get;set;}
    public list<feloniesWrapper> listFelonies {get;set;}
    public list<jobHistoriesWrapper> listJobHistories {get;set;}
    public list<licensesWrapper> listLicenses {get;set;}
    public list<misdemeanorsWrapper> listmisdemeanors {get;set;}
    public List<TransAm_Violation__c> listVoilationsData;
    public List<TransAm_PreviousAddress__c> listAddressesData;
    public List<TransAm_Accident__c> listAccidentsData;
    public List<TransAm_Felony__c> listFeloniesData;
    public List<TransAm_Job_History__c> listJobHistoriesData;
    public List<TransAm_License__c> listLicensesData;
    public List<TransAm_Misdemeanor__c> listmisdemeanorsData;
    public boolean Selected{get;set;}
    public boolean addressBlock{get;set;}
    public boolean accidentBlock{get;set;}
    public boolean felonyBlock{get;set;} 
    public boolean jobHistoryBlock{get;set;}
    public boolean misdemeanorBlock{get;set;}  
    public boolean licenseBlock{get;set;}
    public boolean voilationBlock{get;set;}
    public boolean p2Block{get;set;}
    
    public List<TransAm_Violation__c> listMergeVoilations {get;set;}
    public List<TransAm_PreviousAddress__c> listMergeAddresses {get;set;}
    public List<TransAm_Felony__c> listMergeFelonies {get;set;}
    public List<TransAm_Job_History__c> listMergeJobHistories {get;set;}
    public List<TransAm_License__c> listMergeLicenses {get;set;}
    public List<TransAm_Misdemeanor__c> listMergemisdemeanors {get;set;}
    public List<TransAm_Accident__c> listMergeAccidents {get;set;}        
    
    public TransAm_AppMergeFieldsFormController(Apexpages.standardController controller){
        
        p2Block=True;
        ChildAppList = new List<TransAm_Application__c>();
        masterWrapperMap = new map<string, masterWrapper>();
        selectedAppsIdSet = new set<id>();
        app1WrapperMap = new map<string, app1Wrapper>();
        app2WrapperMap = new map<string, app2Wrapper>();
        app3WrapperMap = new map<string, app3Wrapper>();
        fSetDescriptionMap = new map<string, string>();
        bChild1 = false;
        bChild2 = false;
        bChild3 = false;
        
        system.debug('##currentpage url is:'+ApexPages.currentPage());
        
        if(ApexPages.currentPage().getParameters().get('Id') != null){
            masterId = ApexPages.currentPage().getParameters().get('Id');
        }
        
        if(ApexPages.currentPage().getParameters().get('id1') != null){
            selectedAppsIdSet.add(ApexPages.currentPage().getParameters().get('Id1'));
        }
        
        if(ApexPages.currentPage().getParameters().get('id2') != null){
            selectedAppsIdSet.add(ApexPages.currentPage().getParameters().get('id2'));
        }
        
        if(ApexPages.currentPage().getParameters().get('id3') != null){
            selectedAppsIdSet.add(ApexPages.currentPage().getParameters().get('id3'));
        }
        
        Set<String> setMergeFieldSets = new Set<String>();
        setMergeFieldSets.add('transam_address_information');   
        setMergeFieldSets.add('transam_application_details');       
        setMergeFieldSets.add('transam_contact_information');   
        setMergeFieldSets.add('transam_driver_details');    
        setMergeFieldSets.add('transam_military_experience');   
        setMergeFieldSets.add('transam_qualification_questions');
        setMergeFieldSets.add('transam_referral_details');

        //Map<String, Schema.FieldSet> fieldSetMap = SObjectType.transAm_application__c.FieldSets.getMap();
        Map<String, Schema.FieldSet> fieldSetMap = new Map<String, Schema.FieldSet>();
        
        for(String fSetName:setMergeFieldSets){
            fieldSetMap.put(fSetName, SObjectType.transAm_application__c.FieldSets.getMap().get(fSetName));
        }
        dynamicFSetMap = new map<string, list<string>>();
        
        for(string fSetName : fieldSetMap.keySet()){
            fSetDescriptionMap.put(fSetName, fieldSetMap.get(fSetName).getDescription());
            for(Schema.FieldSetMember fMember : fieldSetMap.get(fSetName).getFields()){
                if(!dynamicFsetMap.containsKey(fSetName)){
                    dynamicFsetMap.put(fSetName, new list<string>());
                    dynamicFsetMap.get(fSetName).add(fMember.getFieldPath());
                } else{
                    dynamicFsetMap.get(fSetName).add(fMember.getFieldPath());
                }
            }            
         }        
        
        for(Schema.FieldSet fSet : fieldSetMap.values()){            
            for(Schema.FieldSetMember fMember : fSet.getFields()){
                fieldnames.add(fMember.getFieldPath());
                fieldAPILabelMap.put(fMember.getFieldPath(),fMember.getLabel());
            }            
        }
        
        system.debug('##fieldnames size is:'+fieldnames.size());
        
        queryString = 'select id, ' + String.join(fieldNames, ',' ) + ' from' + ' TransAm_Application__c where id =';
        queryString+=':masterId';
        
        masterApp = Database.query(queryString);
        
        queryString = 'select id, TransAm_Status__c, ' + String.join(fieldNames, ',' ) + ' from' + ' TransAm_Application__c where id in';
        queryString+=':selectedAppsIdSet';
        
        ChildAppList = Database.query(queryString);
        
        system.debug('##ChildAppList is:'+ChildAppList.size());
        fieldSetNamesList.addAll(dynamicFSetMap.keySet());
        system.debug('##fieldSetNamesList is:'+fieldSetNamesList);
        
        for(string s : fieldNames){
            masterWrapper mWrapper = new masterWrapper();
            
            mWrapper.masterFieldValue = string.valueof(masterApp.get(s));
            mWrapper.masterFieldLabel = fieldAPILabelMap.get(s);
            mWrapper.masterFieldAPI = s;
            mWrapper.options.add(new SelectOption(s, ''));
            
            masterWrapperMap.put(s, mWrapper);
            
            if(!string.isBlank(mWrapper.masterFieldValue)){
                masterWrapperMap.get(s).isMasterSelected = s;
            }
        }
        
       if(ChildAppList.size() >= 1){
           for(string s : fieldNames){
                app1Wrapper a1Wrapper = new app1Wrapper();
                
                a1Wrapper.app1FieldValue = string.valueof(ChildAppList[0].get(s));
                a1Wrapper.app1FieldLabel = fieldAPILabelMap.get(s);
                a1Wrapper.app1FieldAPI = s;
                for(integer i=0; i < ChildAppList.size();i++){
                    a1Wrapper.options.add(new SelectOption(s, ''));
                }                                
                
                app1WrapperMap.put(s, a1Wrapper);
                
                if(string.isBlank(masterWrapperMap.get(s).masterFieldValue) && !string.isBlank(app1WrapperMap.get(s).app1FieldValue)){
                    app1WrapperMap.get(s).isApp1Selected = s;
                    masterApp.put(s, ChildAppList[0].get(s));
                }
            }
            bChild1 = true;
        }
        
        if(ChildAppList.size() >= 2){
           for(string s : fieldNames){
                app2Wrapper a2Wrapper = new app2Wrapper();
                
                a2Wrapper.app2FieldValue = string.valueof(ChildAppList[1].get(s));
                a2Wrapper.app2FieldLabel = fieldAPILabelMap.get(s);
                a2Wrapper.app2FieldAPI = s;
                
                app2WrapperMap.put(s, a2Wrapper);
                
                if(string.isBlank(masterWrapperMap.get(s).masterFieldValue) && string.isBlank(app1WrapperMap.get(s).app1FieldValue) && !string.isBlank(app2WrapperMap.get(s).app2FieldValue)){
                    app2WrapperMap.get(s).isApp2Selected = s;
                    masterApp.put(s, ChildAppList[1].get(s));
                }
            }
            bChild2 = true;
        }
        
        if(ChildAppList.size() >= 3){
           for(string s : fieldNames){
                app3Wrapper a3Wrapper = new app3Wrapper();
                
                a3Wrapper.app3FieldValue = string.valueof(ChildAppList[2].get(s));
                a3Wrapper.app3FieldLabel = fieldAPILabelMap.get(s);
                a3Wrapper.app3FieldAPI = s;
                
                app3WrapperMap.put(s, a3Wrapper);
                
                if(string.isBlank(masterWrapperMap.get(s).masterFieldValue) && string.isBlank(app1WrapperMap.get(s).app1FieldValue) && string.isBlank(app2WrapperMap.get(s).app2FieldValue) && !string.isBlank(app3WrapperMap.get(s).app3FieldValue)){
                    app3WrapperMap.get(s).isApp3Selected = s;
                    masterApp.put(s, ChildAppList[2].get(s));
                }
            }
            bChild3 = true;
        }
        
       fieldSetName =  fieldSetNamesList.get(fSetCount);                
        
        listVoilationsData =[Select id,TransAm_Application__r.Name,Name,TransAm_Charges__c,TransAm_Violation_Date__c,TransAm_Location__c,TransAm_Penalty__c,
                            TransAm_If_OverSpeeding_select_speed__c from TransAm_Violation__c where TransAm_Application__c in : selectedAppsIdSet];
        
        listVoilations = new list<voilationsWrapper>();
        for(TransAm_Violation__c obj : listVoilationsData ){
            listVoilations.add(new voilationsWrapper(obj, false));
        }
        
        listAccidentsData =[Select id,TransAm_Application__r.Name,Name,TransAm_Date__c,TransAm_Location__c,TransAm_Preventable__c, TransAm_Accident_Type__c,TransAm_Fatality__c,
                                TransAm_Trailer_Type__c,TransAm_Hazardous_Spills__c,TransAm_Injuries__c from TransAm_Accident__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listAccidents = new list<accidentsWrapper>();
        for(TransAm_Accident__c obj : listAccidentsData ){
            listAccidents.add(new accidentsWrapper(obj));
        }  
        
        listAddressesData =[Select id,TransAm_Application__r.Name,Name,TransAm_Address__c,TransAm_City__c,TransAm_State__c, TransAm_Zip__c
                                from TransAm_PreviousAddress__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listAddresses = new list<addressesWrapper>();
        for(TransAm_PreviousAddress__c obj : listAddressesData ){
            listAddresses.add(new addressesWrapper(obj));
        }
             
        listFeloniesData =[Select id,TransAm_Application__r.Name,Name,TransAm_Date__c,TransAm_Felony_Type__c,TransAm_Explain__c
                                from TransAm_Felony__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listFelonies = new list<feloniesWrapper>();
        for(TransAm_Felony__c obj : listFeloniesData ){
            listFelonies.add(new feloniesWrapper(obj));
        }  
        
        listJobHistoriesData =[Select id,TransAm_Application__r.Name,Name,TransAm_Current_Employer__c,TransAm_State__c, TransAm_Reason_for_Leaving__c,TransAm_Employed_From__c,
                                TransAm_Employed_To__c,TransAm_Phone__c from TransAm_Job_History__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listJobHistories = new list<jobHistoriesWrapper >();
        for(TransAm_Job_History__c obj : listJobHistoriesData ){
            listJobHistories.add(new jobHistoriesWrapper (obj));
        }  
        
        listlicensesData =[Select id,TransAm_Application__r.Name,Name,TransAm_LicenseNo__c,TransAm_License_Type__c, TransAm_State__c, TransAm_Expiration_Date__c,
                                TransAm_HazMat__c from TransAm_License__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listlicenses = new list<licensesWrapper >();
        for(TransAm_License__c obj : listlicensesData ){
            listlicenses.add(new licensesWrapper (obj));
        } 
        
        listMisdemeanorsData =[Select id,TransAm_Application__r.Name,Name,TransAm_Date__c, TransAm_Explain__c
                                from TransAm_Misdemeanor__c where TransAm_Application__c in:selectedAppsIdSet];
        
        listMisdemeanors = new list<misdemeanorsWrapper >();
        
        for(TransAm_Misdemeanor__c obj : listMisdemeanorsData ){
            listMisdemeanors.add(new misdemeanorsWrapper (obj));
        }     
    }        
    
    public class masterWrapper{
        public string isMasterSelected{get;set;}
        public List<SelectOption> options{get;set;}                
        public string masterFieldValue{get;set;}
        public string masterFieldLabel{get;set;}
        public string masterFieldAPI{get;set;}
        
        public masterWrapper(){
            options = new List<SelectOption>();
        }        
    }
    
     public class app1Wrapper{
        public string isApp1Selected{get;set;}
        public List<SelectOption> options{get;set;}        
        public string app1FieldValue{get;set;}
        public string app1FieldLabel{get;set;}
        public string app1FieldAPI{get;set;}
        
        public app1Wrapper(){
            options = new List<SelectOption>();
        }        
    }
    
    public class app2Wrapper{
        public string isApp2Selected{get;set;}
        public List<SelectOption> options{get;set;}        
        public string app2FieldValue{get;set;}
        public string app2FieldLabel{get;set;}
        public string app2FieldAPI{get;set;}                
        
    }
    
    public class app3Wrapper{
        public string isApp3Selected{get;set;}        
        public string app3FieldValue{get;set;}
        public string app3FieldLabel{get;set;}
        public string app3FieldAPI{get;set;}
    }
    
    public class voilationsWrapper{
        public boolean selected{get;set;}
        public TransAm_Violation__c voilationsSelected{get;set;}
        
        public voilationsWrapper(TransAm_Violation__c voilations, Boolean flag){       
            this.selected = flag;
            this.voilationsSelected = voilations;
        }                
    }
    public class addressesWrapper{
        public boolean selected{get;set;}
        public TransAm_PreviousAddress__c addressesSelected {get;set;}
        
        public addressesWrapper(TransAm_PreviousAddress__c addresses){       
            this.selected = false;
            this.addressesSelected  = addresses;
        }                
    }
     public class feloniesWrapper{
        public boolean selected{get;set;}
        public TransAm_Felony__c feloniesSelected {get;set;}
        
        public feloniesWrapper(TransAm_Felony__c addresses){       
            this.selected = false;
            this.feloniesSelected  = addresses;
        }                
    }
     public class jobHistoriesWrapper {
        public boolean selected{get;set;}
        public TransAm_Job_History__c jobHistoriesSelected {get;set;}
        
        public jobHistoriesWrapper (TransAm_Job_History__c jobHistory){       
            this.selected = false;
            this.jobHistoriesSelected  = jobHistory;
        }                
    }
    public class licensesWrapper {
        public boolean selected{get;set;}
        public TransAm_License__c licensesSelected {get;set;}
        
        public licensesWrapper (TransAm_License__c license){       
            this.selected = false;
            this.licensesSelected  = license;
        }                
    }
    public class misdemeanorsWrapper {
        public boolean selected{get;set;}
        public TransAm_Misdemeanor__c misdemeanorSelected {get;set;}
        
        public misdemeanorsWrapper (TransAm_Misdemeanor__c jobHistory){       
            this.selected = false;
            this.misdemeanorSelected  = jobHistory;
        }
    }
    public class accidentsWrapper{
        public boolean selected{get;set;}
        public TransAm_Accident__c accidentsSelected{get;set;}
        
        public accidentsWrapper(TransAm_Accident__c accidents){       
            this.selected = false;
            this.accidentsSelected = accidents;
        }                
    
    }
  
  public pageReference mergeMaster(){
      string fieldValue;
      string fieldAPIName;

      //if(Apexpages.currentPage().getParameters().get('fieldValue') != null){
          fieldValue = Apexpages.currentPage().getParameters().get('fieldValue');
          system.debug(' value from url:'+Apexpages.currentPage().getParameters().get('fieldValue'));
      //}
      
      if(Apexpages.currentPage().getParameters().get('fieldAPIName') != null){
          fieldAPIName = Apexpages.currentPage().getParameters().get('fieldAPIName');
      }     
     
     if(string.isBlank(fieldValue)){
         masterWrapperMap.get(fieldAPIName).masterFieldValue = '';                    
     } else {
         masterWrapperMap.get(fieldAPIName).masterFieldValue = fieldValue;
     }
     
     //Schema.SObjectType objType = Schema.getGlobalDescribe().get('TransAm_Application__c');
     
     //Schema.DescribeSObjectResult objResult = objType.getDescribe();          
     
     //fDisplayType = objResult.fields.getMap().get(fieldAPIName).getDescribe().getType();
     
     fDisplayType = Schema.getGlobalDescribe().get('TransAm_Application__c').getDescribe().fields.getMap().get(fieldAPIName).getDescribe().getType();
     
     if(fDisplayType == Schema.DisplayType.String || fDisplayType == Schema.DisplayType.picklist || fDisplayType == Schema.DisplayType.Reference){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else{
         system.debug('## Driver id and type should come here');
             masterApp.put(fieldAPIName, string.valueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue));
         }
     } else if(fDisplayType == Schema.DisplayType.Boolean){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         }else {
             masterApp.put(fieldAPIName, boolean.ValueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue));
         }
     } else if(fDisplayType == Schema.DisplayType.Date){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else{ 
             masterApp.put(fieldAPIName, Date.ValueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue));
         }
     } else if(fDisplayType == Schema.DisplayType.currency || fDisplayType == Schema.DisplayType.double || fDisplayType == Schema.DisplayType.percent){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else {
             masterApp.put(fieldAPIName, decimal.valueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue));
         }
     } else if(fDisplayType == Schema.DisplayType.Phone){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else {
             masterApp.put(fieldAPIName, string.valueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue.replaceAll('\\D','').trim()));
         }
     } else if(fDisplayType == Schema.DisplayType.Email){
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else {
             masterApp.put(fieldAPIName, string.valueOf(masterWrapperMap.get(fieldAPIName).masterFieldValue));
         }
     } else {
         if(string.isBlank(masterWrapperMap.get(fieldAPIName).masterFieldValue)){
             masterApp.put(fieldAPIName, '');
         } else {
             masterApp.put(fieldAPIName, masterWrapperMap.get(fieldAPIName).masterFieldValue);
         }
     }     
     return null;
  }
  
  public void UpdateMasterApplication(){
      try{
          for(TransAm_Application__c childApp : ChildAppList){
              if(childApp.TransAm_Status__c != 'Application Completed'){
                  childApp.TransAm_Status__c = 'Merged';
                  childApp.TransAm_Is_status_Changed_Through_Merge__c = true;
              }
          }
          update masterApp;
          update ChildAppList;
          
          
      } catch(exception e){
           TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_ApplicationHelper';
            el.TransAm_Method_Name__c = 'searchAndLinkAppContacts';
            el.TransAm_Module_Name__c = 'Application';
            el.TransAm_Exception_Message__c = e.getMessage();
            insert el;
      }
      
      //pageReference masterAppPage = new pageReference('/'+masterApp.id);      
      //return masterAppPage;
      //return null;
  }
  
    public pageReference NextSection(){
        system.debug('##dynamicFSetMap size is :'+ dynamicFSetMap.size());
        system.debug('##fSetCount is:'+fSetCount);
        
        if(fSetCount < dynamicFSetMap.size()-1){
            fSetCount++;
            //system.debug('## param value from list:'+fieldSetNamesList.get(fSetCount));
            
            fieldSetName = fieldSetNamesList.get(fSetCount);
            return null;
        } else{
           
          
          p2Block = false;
          accidentBlock = True;
          return null;
        }
        return null;
    }
    
     public pagereference mergeAccidents(){
         system.debug('## accedents called');
         listMergeAccidents = new List<TransAm_Accident__c>();
         for(accidentsWrapper obj : listAccidents ){
             if(obj.selected){
                 listMergeAccidents.add(obj.accidentsSelected);
             }
         }
         for(TransAm_Accident__c obj : listMergeAccidents){
             obj.TransAm_Application__c = masterApp.id;
         } 
         
         //update listMergeAccidents;
         
         addressBlock = true; 
         accidentBlock = false;
         system.debug('## accedents at the end');
         return null;
    }
    
    public pagereference mergeAddresses(){
         listMergeAddresses = new List<TransAm_PreviousAddress__c>();
         for(addressesWrapper obj : listAddresses ){
             if(obj.selected){
                 listMergeAddresses.add(obj.addressesSelected );
             }
         }
         
         for(TransAm_PreviousAddress__c obj : listMergeAddresses){
             obj.TransAm_Application__c=masterApp.id;
         }
         
         //update listMergeAddresses;
         
         addressBlock=false;
         felonyBlock = true;
         
         return null ;
     }
                
    
    public pagereference mergeFelonies(){
    
         listMergeFelonies = new List<TransAm_Felony__c>();
         for(feloniesWrapper obj : listFelonies ){
             if(obj.selected){
                 listMergeFelonies.add(obj.feloniesSelected );
             }
         }
         for(TransAm_Felony__c obj : listMergeFelonies){
             obj.TransAm_Application__c = masterApp.id;
         }
         
         //update listMergeFelonies; 
         felonyBlock = false;
         jobHistoryBlock = true; 
         return null;       
    }
    
    public pagereference mergeJobHistories(){
    
         listMergeJobHistories = new List<TransAm_Job_History__c>();
         for(jobHistoriesWrapper  obj : listJobHistories ){
             if(obj.selected){
                 listMergeJobHistories.add(obj.jobHistoriesSelected );
             }
         }
         for(TransAm_Job_History__c obj : listMergeJobHistories){
             obj.TransAm_Application__c=masterApp.id;
         } 
         //update listMergeJobHistories; 
         jobHistoryBlock = false;
         licenseBlock = true; 
         return null;
    }
    
     public pagereference mergeLicenses(){
    
         listMergeLicenses = new List<TransAm_License__c>();
         for(licensesWrapper  obj : listlicenses ){
             if(obj.selected){
                 listMergeLicenses.add(obj.licensesSelected );
             }
         }
         for(TransAm_License__c obj : listMergeLicenses){
             obj.TransAm_Application__c=masterApp.id;
         } 
         //update listMergeLicenses; 
         licenseBlock = false;
         misdemeanorBlock = true; 
         return null;  
    }
    
    public pagereference mergeMisdemeanors(){
    
         listMergemisdemeanors = new List<TransAm_Misdemeanor__c>();
         for(misdemeanorsWrapper  obj : listMisdemeanors ){
             if(obj.selected){
                 listMergemisdemeanors.add(obj.misdemeanorSelected );
             }
         }
         for(TransAm_Misdemeanor__c obj : listMergemisdemeanors){
             obj.TransAm_Application__c=masterApp.id;
         } 
         //update listMergemisdemeanors;
         misdemeanorBlock = false;
         voilationBlock = True;
         return null;
    }
    
    public PageReference mergeVoilations() {
    
        listMergeVoilations = new List<TransAm_Violation__c>();
         for(voilationsWrapper obj : listVoilations){
             if(obj.selected){
                 listMergeVoilations.add(obj.voilationsSelected);
             }
         }
         for(TransAm_Violation__c obj : listMergeVoilations){
             obj.TransAm_Application__c = masterApp.id;
         }
         try{
             UpdateMasterApplication();
             update listMergeAccidents;
             update listMergeAddresses;
             update listMergeFelonies;
             update listMergeJobHistories;
             update listMergeVoilations;
             update listMergeLicenses;
             update listMergemisdemeanors;
          } catch(exception e){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_AppMergeFieldsFormController';
                el.TransAm_Method_Name__c = 'mergeVoilations';
                el.TransAm_Module_Name__c = 'Application';
                el.TransAm_Exception_Message__c = e.getMessage();
                insert el;
          }
         //voilationBlock = false;
         return new PageReference('/'+masterid);
     }
     
     public pageReference cancelMergeOperation(){
         return new PageReference('/'+masterid);
     }

}