/* CLass Name   : TransAm_ONELeasingTrigger_Test
 * Description  : Test class for ONELeasing Trigger
 * Created By   : Karthik Gulla
 * Created On   : 19-Sep-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                19-Sep-2017            Initial version.
 *
 *****************************************************************************************/
@isTest
public class TransAm_ONELeasingTrigger_Test{
	/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
    	List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(2, lstAccounts[0].Id, 'Driver', true);
        lstContacts[0].TransAm_Driver_Status__c = 'Hired';
        update lstContacts;
        
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Pending Orientation', lstContacts[0], true);
        lstApplications[0].TransAm_DriverID__c = 'test1';
        update lstApplications;

        List<TransAm_ONE_Leasing__c> lstOneLeases = TransAm_Test_DataUtility.createLeases(1, lstApplications[0], 'IC Agreement', true, null);
        List<TransAm_Generic_Custom_Setting__c> lstGenCustomSettings = TransAm_Test_DataUtility.createGenericCustomSetting();
        
        List<Attachment> lstAttachments = new List<Attachment>();
        for(TransAm_ONE_Leasing_Document__c tld:[SELECT Id, Name, TransAm_Lease__c, TransAm_Lease_Document_Type__c FROM TransAm_ONE_Leasing_Document__c WHERE TransAm_Lease__c =:lstOneLeases[0].Id]){
            if(tld.TransAm_Lease_Document_Type__c == 'Independent Contractor Agreement')
                lstAttachments.add(new Attachment(        
                                    Name        = 'ICA.docx',
                                    Body        = Blob.valueOf('Unit Test Attachment Body - ICA'),
                                    ParentId    = tld.Id              
                                ));

            if(tld.TransAm_Lease_Document_Type__c == 'OneBeacon Enrollment Form')
                lstAttachments.add(new Attachment(        
                                    Name        = 'OneBeaconEnrollForm.docx',
                                    Body        = Blob.valueOf('Unit Test Attachment Body - OneBeacon Enrollment Form'),
                                    ParentId    = tld.Id              
                                ));
        }
        insert lstAttachments;
    }


    /************************************************************************************
    * Method       :    testONELeasingTriggerAfterInsert
    * Description  :    Test Method to test One leasing trigger after Insert
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testONELeasingTriggerAfterInsert() {
        Test.startTest();
  		  List<TransAm_ONE_Leasing__c> lstInsertedLeases = [SELECT Id, RecordType.Name, TransAm_Lifestyle__c FROM TransAm_ONE_Leasing__c];
  		  List<TransAm_Application__c> lstApps = [SELECT Id, Name FROM TransAm_Application__c];
  		 
  		  TransAm_Test_DataUtility.createLeases(1, lstApps[0], 'IC Agreement', true, lstInsertedLeases[0]);
        Test.stopTest();
    }

    /************************************************************************************
    * Method       :    testONELeasingTriggerAfterUpdateIC
    * Description  :    Test Method to test One leasing trigger after Update for IC Agreement
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testONELeasingTriggerAfterUpdateIC() {
      	Test.startTest();
      	List<TransAm_ONE_Leasing__c> lstNewICs = [SELECT Id, RecordType.Name, TransAm_Lifestyle__c FROM TransAm_ONE_Leasing__c WHERE RecordType.Name = 'IC Agreement' Order by CreatedDate];
	lstNewICs[0].TransAm_DeadheadBobtail_Declaration__c = 'Yes';
	lstNewICs[0].TransAm_PhysicalDamage_Declaration__c = 'Yes';
	lstNewICs[0].TransAm_FuelOptimization_Declaration__c = 'X';
	lstNewICs[0].TransAm_OccupationalAccident_Declaration__c = 'No';
	lstNewICs[0].TransAm_DeductibleBuydown_Declaration__c = 'No';
	lstNewICs[0].TransAm_PrePassInsurance_Declaration__c = null;
	update lstNewICs;
        lstNewICs[0].TransAm_InsDHB_Declaration__c = null;
        lstNewICs[0].TransAm_InsPDI_Declaration__c = null;
        lstNewICs[0].TransAm_InsOAI_Declaration__c = 'X';
        lstNewICs[0].TransAm_InsDB_Declaration__c = 'X';
    	update lstNewICs;
    }

    /************************************************************************************
    * Method       :    testONELeasingTriggerAfterUpdateLease
    * Description  :    Test Method to test One leasing trigger after Update for Lease agreement
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testONELeasingTriggerAfterUpdateLease() {
        Test.startTest();
        
        List<TransAm_ONE_Leasing__c> lstNewICsOne = [SELECT Id, RecordType.Name, TransAm_Lifestyle__c FROM TransAm_ONE_Leasing__c WHERE RecordType.Name = 'IC Agreement' Order By CreatedDate];
       
        List<TransAm_Application__c> lstAppsOne = [SELECT Id, Name FROM TransAm_Application__c];
        TransAm_Test_DataUtility.createLeases(1, lstAppsOne[0], 'Lease Agreement', true, lstNewICsOne[0]);

        List<TransAm_ONE_Leasing__c> lstNewLeases = [SELECT Id, RecordType.Name, TransAm_Lifestyle__c FROM TransAm_ONE_Leasing__c WHERE RecordType.Name = 'Lease Agreement'];
        lstNewLeases[0].TransAm_InsLS_Declaration__c = 'X';
        update lstNewLeases;

        Test.stopTest();
    }
}