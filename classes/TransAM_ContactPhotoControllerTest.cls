/* Class Name   : TransAM_ContactPhotoControllerTest
 * Description  : Test class for TransAM_ContactPhotoController
 * Created By   : Nitesh Halliyal
 * Created On   : 07-04-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Nitesh Halliyal              07-04-2017             Initial version.
 *
 *****************************************************************************************/
@isTest(seeAllData=False)
Public class TransAM_ContactPhotoControllerTest{
    
  Public static TestMethod Void testUpload(){
      Account acc = new account(name = 'Test Account');
      insert acc;
      Contact con = new contact(FirstName = 'Test', LastName = 'Contact', accountId = acc.Id);
      insert con;
      PageReference pageRef = Page.TransAM_ContactPicUpload;
      pageRef.getParameters().put('id',con.id);
      test.startTest();
      Test.setCurrentPage(pageRef);
      ApexPages.StandardController sc = new ApexPages.standardController(con);
      TransAM_ContactPhotoController contrl = new TransAM_ContactPhotoController(sc);
      contrl.attachBody = blob.valueOf('Test image');
      contrl.upload();
      TransAM_ContactPhotoController contrlNegative = new TransAM_ContactPhotoController(sc);
      contrlNegative.upload();
      Test.stopTest();
      Attachment att= [SELECT id FROM attachment WHERE parentId =:con.id LIMIT 1];
      system.assert(att.id != null);      
  }  
}