/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     03/14/2017
* Description  :     Batch class to update all the Applications with Status: No Contact whose Status = 'Recruiter Review' and Sub-status = 'Waiting on Release Form' and status/sub-status values are not changed for more than 30 days.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_ApplicationStatusUpdateBatch implements Database.Batchable<sObject>{
    
    list<TransAm_Application__c> appToUpdateList = new list<TransAm_Application__c>();
    
    TransAm_Status_duration__c durationCS = TransAm_Status_duration__c.getValues('Duration Value');
    integer duration = integer.valueof(durationCS.TransAm_Duration__c);
    long longduration = duration*24*60;
    DateTime dt = system.now();
    long longdt = dt.getTime()/(1000*60);
    long laststatuschange;

     /*******************************************************************************************
    * Method        :   start
    * Description   :   Purpose of this method is to execute a query and return the query results to Execute method.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([Select id, Name, TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Recruiter Review']);
    }
    
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   Purpose of this method is to update status field value of applications.
    * Parameter     :   Database.BatchableContext, List<TransAm_Application__c>
    * Return Type   :   Void
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> scope){
        try{
            for(TransAm_Application__c Application : scope){                
                laststatuschange = (Application.TransAm_Last_Status_Change__c.getTime())/(1000*60);
                if((longdt - laststatuschange) > longduration ){
                    Application.TransAm_Status__c = 'No Contact';
                    appToUpdateList.add(Application);
                }
            }
            
            if(appToUpdateList.size() > 0){
                update appToUpdateList;
            }
        } catch(Exception e){
            TransAm_Error_Log__c elog = new TransAm_Error_Log__c();elog.TransAm_Class_Name__c = 'TransAm_ApplicationStatusUpdate';elog.TransAm_Method_Name__c = 'execute';elog.TransAm_Module_Name__c = 'TransAm_Application__c';elog.TransAm_Exception_Message__c = e.getMessage();insert elog;
        }

    }
    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Purpose of this method is to perform any activities post status update Update.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
        System.debug('### Application Status is successfully updated to No Contact ###');
    }
}