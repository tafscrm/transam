/* Class Name   : TransAm_EventHelper
 * Description  : Helper class for the Event trigger
 * Created By   : Pankaj Singh
 * Created On   : 13-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               13-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_EventHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  populate the school visit count on driver
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateVisitCountOnDriver(list<Event> eventList){
    
        list<Contact> driverList = new list<Contact>();
        set<Id> driverIdSet = new set<Id>();
        try{
            for(Event eventObj :  eventList){   
                  driverIdSet.add(eventObj.TransAm_Driver__c);
            }
            for(Contact driver :  [SELECT ID,(SELECT ID FROM Activities__r) FROM Contact WHERE ID IN : driverIdSet ALL ROWS]){
                if(driver.Activities__r!=null){
                    driver.TransAm_School_Visit_Count__c = driver.Activities__r.size();
                    driverList.add(driver);
                }
            }
            if(!driverList.isEmpty()){
                Database.update(driverList);
            }
         }catch(Exception excep){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_EventHelper';
            el.TransAm_Method_Name__c = 'populateVisitCountOnDriver';
            el.TransAm_Module_Name__c = 'Event';
            el.TransAm_Exception_Message__c = excep.getMessage();
            insert el;  
         }
    }
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  populate the school visit count on school
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateVisitCountOnSchool(list<Event> eventList){
    
    String dummyAccountName = Label.TransAm_Dummy_Account_Name;
        list<Event> schoolVisitList = new list<Event>();
        try{
            if(eventList!=null){
                for(Event schoolVisit : eventList){
                    if(schoolVisit.RecordTypeId == Schema.SObjectType.Event.getRecordTypeInfosByName().get('School Visit').getRecordTypeId()){
                        schoolVisitList.add(schoolVisit);
                    }
                }
                Account dummySchool = [SELECT ID,Name,(SELECT ID FROM Events),TransAm_Count_of_Child_Activities__c FROM Account WHERE Name LIKE :dummyAccountName+'%' ORDER BY CreatedDate DESC LIMIT 1];
                if(dummySchool!=null){
                integer i =  Integer.valueOf(dummySchool.Name.right(2).trim())+1;
                    if(dummySchool.TransAm_Count_of_Child_Activities__c>9999){
                        Account newDummySchool = new Account(Name = dummyAccountName+' '+i, TransAm_Driving_School_Code__c = string.valueof(Math.random()).right(10), TransAm_Count_of_Child_Activities__c = schoolVisitList.size(), BillingCity=label.TransAm_Dummy_Account_City, BillingStreet = Label.TransAm_Dummy_Account_Street, BillingState = Label.TransAm_Dummy_Account_State, BillingPostalCode = Label.TransAm_Dummy_Account_Zipcode);
                        insert newDummySchool;
                        for(Event event : schoolVisitList){
                            if(event.WhatId==null){
                                event.WhatId = newDummySchool.Id;
                            }
                        }
                    }
                    else if(dummySchool.Events!=null){                        
                        dummySchool.TransAm_Count_of_Child_Activities__c = dummySchool.Events.size() + schoolVisitList.size();
                        update dummySchool;
                        for(Event event : schoolVisitList){
                            if(event.WhatId == null){
                                event.WhatId = dummySchool.Id;
                            }
                        }
                    }
                }
            }
            
         }catch(Exception excep){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_EventHelper';
            el.TransAm_Method_Name__c = 'populateVisitCountOnSchool';
            el.TransAm_Module_Name__c = 'Event';
            el.TransAm_Exception_Message__c = excep.getMessage();
            insert el;  
         }
    }
}