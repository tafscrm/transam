/******************************************************************************************
* Create By    :     Raushan Anand
* Create Date  :     09/28/2017
* Description  :     Schedule class for TransAm_RefreshOneLeaseXMLBatch batch class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class Transam_RefreshOneLeaseRVIScheduler implements Schedulable {
   global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_RefreshOneLeaseXMLBatch(),1);
   }
}