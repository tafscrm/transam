/******************************************************************************************
* Create By    :     Raushan Anand
* Create Date  :     04/06/2017
* Description  :     Batch Class to delete archived attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Raushan Anand                    04/06/2017           Initial version.
*****************************************************************************************/
global class TransAm_ArchivedDocumentDeleteBatch implements Database.Batchable<sObject>{
	
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date yesterdayDate = Date.today().addDays(-1); 
        String query = 'SELECT Id FROM TransAm_Recruitment_Document__c where TransAM_Application__r.TransAm_Archived_Date__c =: yesterdayDate';
    	return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Attachment> attachList = [Select Id FROM Attachment WHERE ParentId IN: scope];
        System.debug('attachList'+attachList);
        DELETE scope;
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}