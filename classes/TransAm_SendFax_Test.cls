/* CLass Name   : TransAm_SendFax_Test
 * Description  : Test class for TAFS_SendFax
 * Created By   : Karthik Gulla
 * Created On   : 12-Apr-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla               12-Apr-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private with sharing class TransAm_SendFax_Test {
    public static List<TransAm_Job_History__c> lstApplicationJobHistory;
    public static List<TransAm_Application__c> lstNewApplications;

    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0] ,true);
        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);                              
        TransAm_Test_DataUtility.createJobHistory(1, lstApplications[0], lstEmployers[0], true);
        for(TransAm_Recruitment_Document__c recrDoc:[SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: lstApplications[0].Id 
                                                                    AND (TransAm_Type__c = 'Background Check Release Form' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Release Form')]){

            TransAm_Test_DataUtility.createAttachments(1, recrDoc.Id, true);
        }
    } 

    /************************************************************************************
    * Method       :    testSendFax
    * Description  :    Test sendFax methods
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testSendFax() {
        lstApplicationJobHistory = [SELECT Id, TransAm_Application__c FROM TransAm_Job_History__c];
		Attachment att = new Attachment(name = 'HR_Emp_Ver_Test_RA.pdf', Body= Blob.valueOf('tesrtvuhjnkjmlk'), Parentid = lstApplicationJobHistory[0].Id);
        INSERT att;
        Attachment attPast = new Attachment(name = 'Past_Emp_Ver_Test_RA.pdf', Body= Blob.valueOf('tesrtvuhjnkjmlk'), Parentid = lstApplicationJobHistory[0].Id);
        INSERT attPast;
        lstNewApplications = [SELECT Id FROM TransAm_Application__c];
        Test.startTest();
        TransAm_SendFax.sendFaxToEmployer(lstApplicationJobHistory[0].Id, '1615750731','HireRight: Test Subject. Please ignore');
        TransAm_SendFax.sendFaxToEmployer(lstApplicationJobHistory[0].Id, '1234567890','Past Employer Verification');
        TransAm_SendFax.sendFaxToEmployer(lstApplicationJobHistory[0].Id, '','Past Employer Verification');
        TransAm_SendFax.sendFaxToSchoolContact(lstNewApplications[0].Id);
        Test.stopTest();
        system.assert([SELECT id from efaxapp__Sent_Fax__c] != null);
    }
}