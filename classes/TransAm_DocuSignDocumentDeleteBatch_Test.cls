@isTest(seeAllData=False)
public class TransAm_DocuSignDocumentDeleteBatch_Test {
    
    @testSetup
    private static void testSetUpArchivedData(){
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],true);
        lstApplications[0].TransAm_Archived_Date__c = Date.today().addDays(-50); 
        UPDATE lstApplications;
        dsfs__DocuSign_Status__c doc = new dsfs__DocuSign_Status__c(TransAm_Application__c= lstApplications[0].id);
        Insert Doc;
        Transam_recruitment_document__c docu = new Transam_recruitment_document__c(Name='Demo RD',TransAm_type__c='Print Only',
                                                                                    TransAm_Application__c=lstApplications[0].id);
        insert docu;
        
        system.debug('++++'+doc);
        //Attachment attach =TransAm_Test_DataUtility.createAttachments(1, doc.id ,True)[0];
        Attachment attach = new Attachment(Name='PrintOnly.pdf', ParentId=docu.Id, Body = Blob.valueOf('testDoc'));
        insert attach;
    }
 
   public static testmethod void testSchedule(){
        Attachment DocuAtt = [Select id, CreatedDate from attachment limit 1]; 
        Attachment DocuAtt1 = [Select id, CreatedDate from attachment limit 1];
        Test.setCreatedDate(DocuAtt.id, system.today().addDays(-50));
        Test.startTest();
        system.debug(DocuAtt1.CreatedDate);
        String sch = '0 0 23 * * ?'; 
        system.schedule('ertyuibn980_edfyugbhjTest Schedule', sch, new TransAm_DocuSignDocumentDeleteScheduler() );
        //TransAm_DocuSignDocumentDeleteBatch docClass = new TransAm_DocuSignDocumentDeleteBatch ();
        Test.stopTest();
    }
}