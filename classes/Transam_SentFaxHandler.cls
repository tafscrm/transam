public class Transam_SentFaxHandler {
    
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitTransferAttachment = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    
    public static void ddpGenerationHandler(set<Id> sentFaxIdSet, string sessionId){
        
        system.debug('calling ddp API'+prohibitAfterUpdateTrigger);
        if(prohibitAfterUpdateTrigger){
            return;
        } 
        prohibitAfterUpdateTrigger = true;               
        TransAm_AutoDDPGenerator.generateDocument(sentFaxIdSet , sessionId);
        
    }
    public static void afterUpdateHandler(List<efaxapp__Sent_Fax__c> sentFaxList, Map<Id,efaxapp__Sent_Fax__c> oldFaxMap){
        if(prohibitTransferAttachment){
            return;
        } 
        prohibitTransferAttachment = true;
        //Transam_SentFaxHelper.transferAttachmentsToRD(sentFaxList,oldFaxMap);
    }
}