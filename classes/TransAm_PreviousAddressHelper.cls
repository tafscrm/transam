/* Class Name   : TransAm_PreviousAddressHelper
 * Description  : Helper class for the Previous trigger
 * Created By   : Pankaj Singh
 * Created On   : 14-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               14-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_PreviousAddressHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  populate the school visit count on driver
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateApplicationOnAdress(list<TransAm_PreviousAddress__c> addressList){
        
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        if(UserInfo.getProfileId()==profileId){       
            map<string,Id> applicationMap = new map<string,Id>();
            set<String> parentIdSet = new set<String>();                 
            try{
                for(TransAm_PreviousAddress__c prevObjObj : addressList){
                    parentIdSet.add(prevObjObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_PreviousAddress__c addressObj : addressList){
                    if(applicationMap.containsKey(addressObj.TransAm_EBE_Parent_ID__c)){
                        addressObj.TransAm_Application__c = applicationMap.get(addressObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_PreviousAddressHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnAdress';
                el.TransAm_Module_Name__c = 'Previous Address';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el; 
            }
        }
    }
}