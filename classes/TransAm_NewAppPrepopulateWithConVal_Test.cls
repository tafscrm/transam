/* CLass Name   : TransAm_NewAppPrepopulateWithConVal_Test
 * Description  : Test class for Job History
 * Created By   : Karthik Gulla
 * Created On   : 03-JULY-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                03-JULY-2017            Initial version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_NewAppPrepopulateWithConVal_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(2, lstAccounts[0].Id, 'Applicant', true);
	
        TransAm_Test_DataUtility.createAppContactPrepopulateMappings();
    }

    /************************************************************************************
    * Method       :    testNewAppPrepopulateWithContactValues
    * Description  :    Test Method to Prepopulate contact values to Application
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testNewAppPrepopulateWithContactValues() {
        Test.startTest();
        List<Contact> lstCon = [Select Id, Name, TransAm_Driver_Id__c From Contact];
        TransAm_NewAppPrepopulateWithConValues.formStandardNewAppCreateUrl(lstCon[0].Id);
        Test.stopTest();
    }
}