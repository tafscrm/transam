/* CLass Name   : TransAm_AppMergeFieldsFormCtrl_Test
 * Description  : Test class for TransAm_AppMergeFieldsFormController
 * Created By   : Karthik Gulla
 * Created On   : 13-July-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                11-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/

@isTest(seeAllData=false)
public class TransAm_AppMergeFieldsFormCtrl_Test {
    public static List<TransAm_Violation__c> lstViolations;
    public static List<TransAm_Accident__c> lstAccidents;
    public static List<TransAm_PreviousAddress__c> lstAddresses;
    public static List<TransAm_Felony__c> lstFelonies;
    public static List<TransAm_License__c> lstLicenses;
    public static List<TransAm_Misdemeanor__c> lstMisdemeanors;
    public static List<TransAm_Job_History__c> lstJobHistories;
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        TransAm_Test_DataUtility.createGenericCustomSetting();
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(4, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],true);
        for(Integer i=1;i<lstApplications.size();i++){
            lstApplications[i].TransAm_Master_Application__c = lstApplications[0].Id;
            lstApplications[i].TransAm_Primary_Application__c = false;
        }
        lstApplications[0].TransAm_Address__c = '';
        lstApplications[0].TransAm_State__c = '';
        lstApplications[1].TransAm_State__c = '';
        lstApplications[0].TransAm_ZIP__c = '';
        lstApplications[1].TransAm_ZIP__c = '';
        lstApplications[2].TransAm_ZIP__c = '';    
        update lstApplications;

        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);
        lstViolations = TransAm_Test_DataUtility.createViolations(2, lstApplications[1], true);
        TransAm_Test_DataUtility.createAccidents(2, lstApplications[1], true);
        lstAddresses = TransAm_Test_DataUtility.createPreviousAddresses(2, lstApplications[1], true);
        lstFelonies = TransAm_Test_DataUtility.createFelonies(2, lstApplications[1], true);
        lstJobHistories = TransAm_Test_DataUtility.createJobHistory(2, lstApplications[1], lstEmployers[0], true);
        lstLicenses = TransAm_Test_DataUtility.createLicenses(2, lstApplications[1], true);
        lstMisdemeanors = TransAm_Test_DataUtility.createMisdemeanor(2, lstApplications[1], true);

        TransAm_Test_DataUtility.createViolations(2, lstApplications[2], true);
        TransAm_Test_DataUtility.createAccidents(2, lstApplications[2], true);
        TransAm_Test_DataUtility.createPreviousAddresses(2, lstApplications[2], true);
        TransAm_Test_DataUtility.createFelonies(2, lstApplications[2], true);
        TransAm_Test_DataUtility.createJobHistory(2, lstApplications[2], lstEmployers[0], true);
        TransAm_Test_DataUtility.createLicenses(2, lstApplications[2], true);
        TransAm_Test_DataUtility.createMisdemeanor(2, lstApplications[2], true);

        TransAm_Test_DataUtility.createViolations(2, lstApplications[3], true);
        TransAm_Test_DataUtility.createAccidents(2, lstApplications[3], true);
        TransAm_Test_DataUtility.createPreviousAddresses(2, lstApplications[3], true);
        TransAm_Test_DataUtility.createFelonies(2, lstApplications[3], true);
        TransAm_Test_DataUtility.createJobHistory(2, lstApplications[3], lstEmployers[0], true);
        TransAm_Test_DataUtility.createLicenses(2, lstApplications[3], true);
        TransAm_Test_DataUtility.createMisdemeanor(2, lstApplications[3], true);
    }
        
    /************************************************************************************
    * Method       :    mergeFieldsPageTest
    * Description  :    Test Method to create mergeFieldsPage
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void TestMergeFieldsForm(){
        List<TransAm_Application__c> lstMasterApps = [SELECT ID, TransAm_Master_Application__c, TransAm_City__c, 
                                                            TransAm_Primary_Application__c, TransAm_MilServedTo__c,
                                                            TransAm_Phone__c, TransAm_Email__c, TransAm_HaveYouSpokenToRecruiter__c
                                                            FROM TransAm_Application__c WHERE TransAm_Primary_Application__c = TRUE];
        List<TransAm_Application__c> lstChildApps = [SELECT ID, TransAm_Master_Application__c FROM TransAm_Application__c WHERE TransAm_Primary_Application__c = FALSE];
        PageReference pageRef = Page.TransAm_Application_Merge_Fields_Form;
        pageRef.getParameters().put('Id',lstMasterApps[0].Id);
        pageRef.getParameters().put('id1',lstChildApps[0].Id);
        pageRef.getParameters().put('id2',lstChildApps[1].Id);
        pageRef.getParameters().put('id3',lstChildApps[2].Id);
        Test.setCurrentPage(pageRef);
            
        ApexPages.StandardController sc = new ApexPages.StandardController(lstMasterApps[0]);
        TransAm_AppMergeFieldsFormController mergeApps = new TransAm_AppMergeFieldsFormController(sc);
        lstAccidents = [SELECT Id FROM TransAm_Accident__c];
        TransAm_AppMergeFieldsFormController.AccidentsWrapper accWrapper= new TransAm_AppMergeFieldsFormController.AccidentsWrapper(lstAccidents[0]);
        accWrapper.selected = true;
        
        mergeApps.mergeAccidents();
        mergeApps.mergeAddresses();
        mergeApps.mergeFelonies();
        mergeApps.mergeLicenses();
        mergeApps.mergeMisdemeanors();
        mergeApps.mergeVoilations();
        mergeApps.mergeJobHistories();
        
        mergeApps.NextSection();
        mergeApps.cancelMergeOperation();

        Set<String> setFieldAPINames = new Set<String>();
        setFieldAPINames.add('TransAm_City__c');
        setFieldAPINames.add('TransAm_Phone__c');
        setFieldAPINames.add('TransAm_Email__c');
        setFieldAPINames.add('TransAm_HaveYouSpokenToRecruiter__c');

        Test.startTest();
        for(String fieldName:setFieldAPINames){
            pageRef.getParameters().clear();
            pageRef.getParameters().put('fieldValue',String.valueOf(lstMasterApps[0].get(fieldName)));
            pageRef.getParameters().put('fieldAPIName',fieldName);
            Test.setCurrentPage(pageRef);
            mergeApps.mergeMaster();

            pageRef.getParameters().clear();
            pageRef.getParameters().put('fieldValue','');
            pageRef.getParameters().put('fieldAPIName',fieldName);
            Test.setCurrentPage(pageRef);

            mergeApps.mergeMaster();
        }
        Test.stopTest();
    }
}