/******************************************************************************************
* Create By    :     Karthik Gulla
* Create Date  :     08/10/2017
* Description  :     Handler class for TransAm ONELeasing trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                      08/10/2017         Initial version.
*****************************************************************************************/
public class TransAm_ONELeasing_Handler{

    Private Static Integer count=0;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitAfterDeleteTrigger = false;
  
    /*******************************************************************************************
     * Method        :   afterInsertHandler
     * Description   :   Purpose of this method is to call helper methods from TransAm_ONELeasing_Helper class.
     * Parameter     :   List<TransAm_ONE_Leasing__c>, Map<Id, TransAm_ONE_Leasing__c>, boolean
     * Return Type   :   Void
     ******************************************************************************************/
    public static void afterInsertHandler(List<TransAm_ONE_Leasing__c> newONELeasingList, Map<Id, TransAm_ONE_Leasing__c> newLeasingMap, boolean isAfterInsert){     
        if(prohibitAfterInsertTrigger){
            return;
        }      
        prohibitAfterInsertTrigger = true;
        TransAm_ONELeasing_Helper.createLeasingDocuments(newONELeasingList);
        TransAm_ONELeasing_Helper.updateAssigneeDetails(newONELeasingList, newLeasingMap, true);
        TransAm_ONELeasing_Helper.updateLeaseContactDetails(newONELeasingList);
        TransAm_ONELeasing_Helper.updateLifestyleDeclarationDetails(newONELeasingList);
    }
	/*******************************************************************************************
     * Method        :   afterUpdateHandler
     * Description   :   Purpose of this method is to call helper methods from TransAm_ONELeasing_Helper class.
     * Parameter     :   List<TransAm_ONE_Leasing__c>, Map<Id, TransAm_ONE_Leasing__c>
     * Return Type   :   Void
     ******************************************************************************************/
    public static void afterUpdateHandler(List<TransAm_ONE_Leasing__c> newONELeasingList, Map<Id, TransAm_ONE_Leasing__c> oldLeasingMap){
        if(prohibitAfterUpdateTrigger){
            return;
        }
        prohibitAfterUpdateTrigger = TRUE;
		TransAm_ONELeasing_Helper.sendMailtoRiskMgmt(newONELeasingList);//NHALLIYAL:added for user story 622364
        TransAm_ONELeasing_Helper.updateInsuranceDetails(newONELeasingList,oldLeasingMap, prohibitAfterInsertTrigger);
        TransAm_ONELeasing_Helper.updateAssigneeDetails(newONELeasingList, oldLeasingMap, false);
    }      
}
