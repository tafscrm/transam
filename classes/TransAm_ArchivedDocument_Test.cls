@isTest(seeAllData=False)
private class TransAm_ArchivedDocument_Test {
    
	@testSetup
    private static void testSetUpArchivedData(){
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],true);
        lstApplications[0].TransAm_Archived_Date__c = Date.today().addDays(-1);
        UPDATE lstApplications;
        List<TransAm_Recruitment_Document__c> recDocList = new List<TransAm_Recruitment_Document__c>();
        recDocList = TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Agreement',false);
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Application Summary',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Background Reports',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Fuel Card Agreement',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Hotel Agreement',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Initial Urinalysis Consent Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Kansas I-9',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Texas Arbitration',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Benefit Information',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Web In Transit Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Settlement Designation Authorization Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'System Document',false));
        
        INSERT recDocList;
        List<Attachment> lstAttachments = new List<Attachment>();
        for(TransAm_Recruitment_Document__c rec : recDocList){
            lstAttachments.add(new Attachment(        
                                    Name        = 'Test.pdf',
                                    Body        = Blob.valueOf('Unit Test Attachment Body'+ rec.TransAm_Type__c),
                                    ParentId    = rec.Id              
                                ));

        }
        
    }
    
    private static testmethod void testSchedule(){
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        system.schedule('ertyuibn980_edfyugbhjTest Schedule', sch, new TransAm_ArchivedDocumentDeleteScheduler() );
        Test.stopTest();
    }
    
}