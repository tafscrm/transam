/* CLass Name   : TransAm_HRRequestServiceTest
 * Description  : Test class for TransAm_HRRequestService
 * Created By   : Monalisa Das
 * Created On   : 09-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                09-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
@isTest(seeAllData=false)
public class TransAm_HRRequestServiceTest {
/************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);        
	
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Test';
        user_int.LastName = 'User';
        user_int.Username = 'testHRuser@abc.com';
        user_int.Email   = 'testuser@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
	
	
    }
    
     /************************************************************************************
    * Method       :    testHROrdersRequestService
    * Description  :    Test Method to create HR Orders Request Payload for non package report types
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/ 
    public static testmethod void testHROrdersRequestService() {
        
       List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
                
         User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Test' LIMIT 1];
        Test.startTest();
	
	List<PackageReport__c> custSetList= new List<PackageReport__c>();
        PackageReport__c reportCustSetting= new PackageReport__c(Name ='Standard Package',Is_Package__c =true,Package_ID__c='331742');
        custSetList.add(reportCustSetting);
        insert custSetList;
	
	List<TransAm_HireRight_Orders__c> lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'MVR Standard' ,True); 
	//insert lstHROrders;
	System.runAs(user_int){ 
            TransAm_HRRequestService.getCreateRequest(lstApplications[0],lstHROrders[0]);
            TransAm_HRRequestService.getGenerateWeblinkRequest(lstHROrders[0].TransAm_Hireright_Order_ID__c);

        Test.stopTest();
        }
    }
    
     /************************************************************************************
    * Method       :    testHROrdersPackageRequestService
    * Description  :    Test Method to create HR Orders Request Payload for package report types
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/ 
    public static testmethod void testHROrdersPackageRequestService() {
        
       List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
                
         User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Test' LIMIT 1];
        Test.startTest();
	
	List<PackageReport__c> custSetList= new List<PackageReport__c>();
        PackageReport__c reportCustSetting= new PackageReport__c(Name ='Standard Package',Is_Package__c =true,Package_ID__c='331742');
        custSetList.add(reportCustSetting);
        insert custSetList;
	
	List<TransAm_HireRight_Orders__c> lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'Standard Package' ,True); 
	//insert lstHROrders;
	System.runAs(user_int){ 
            TransAm_HRRequestService.getCreateRequest(lstApplications[0],lstHROrders[0]);
            TransAm_HRRequestService.getGenerateWeblinkRequest(lstHROrders[0].TransAm_Hireright_Order_ID__c);

        Test.stopTest();
        }
    }
}