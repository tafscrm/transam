/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/17/2017
* Description  :     Handler class for TransAm_ApplicationTrigger trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
public class TransAm_ApplicationHandler{

    Private Static Integer count=0;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitBeforeUpdateTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitAfterDeleteTrigger = false;
    
   /*******************************************************************************************      
   * Method        :   beforeInsertHandler
   * Description   :   Purpose of this method is to call updateApplicationStatus method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Application__c> newApplicationList,Map<id, TransAm_Application__c> newApplicationMap, Boolean isBeforeInsert){
        
        if(prohibitBeforeInsertTrigger){
            return;
        }
        
        prohibitBeforeInsertTrigger = true;
        
        for(TransAm_Application__c newApp : newApplicationList){
            newApp.TransAm_Status__c = 'Application Received';
        }
        
        TransAm_ApplicationHelper.updateApplicationStatus(newApplicationList, null, null, isBeforeInsert, false);   
        TransAm_ApplicationHelper.validateApplicantStatus(newApplicationList,newApplicationMap);
        //system.debug('printing user id::'+UserInfo.getUserId());
        //system.debug('printing integration user id::'+[SELECT ID FROM User WHERE Profile.Name = 'TransAm_Integration_User' LIMIT 1].Id);
        //Id profileId=userinfo.getProfileId();
        //String profileName=[Select Id,Name from Profile where Id=:profileId].Name;

        String profileName = 'TransAm_Integration_User';

        /*if(profileName == 'TransAm_Integration_User'){
            TransAm_ApplicationAssignmentHelper.assignApplication(newApplicationList);
        }*/
        //TransAm_ApplicationHelper.recruiterFieldsUpdate(newApplicationList, isBeforeInsert, false);
        TransAm_ApplicationHelper.calculateScoreonApplication(newApplicationList);
        TransAm_ApplicationHelper.updateDQStatusAndReason(newApplicationList);
        TransAm_ApplicationHelper.emailPhoneSSNCheck(newApplicationList);
        TransAm_ApplicationHelper.populateKeyFieldsOnApplicationBeforeInsert(newApplicationList);
        TransAm_ApplicationHelper.updateArchivalDate(newApplicationList);

        	TransAm_ApplicationHelper.updateOwnerName(newApplicationList);

        //TransAm_ApplicationHelper.sortApplicationRealtedList(newApplicationList);
    }
    
   /*******************************************************************************************
   * Method        :   beforeUpdateHandler
   * Description   :   Purpose of this method is to call updateApplicationStatus method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void beforeUpdateHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> newApplicationMap, Map<id, TransAm_Application__c> oldApplicationMap, boolean isBeforeUpdate){
        
        if(prohibitBeforeUpdateTrigger){
            return;
        }
        
        prohibitBeforeUpdateTrigger = true;
        
        TransAm_ApplicationHelper.updateApplicationStatus(newApplicationList, newApplicationMap, oldApplicationMap, false, isBeforeUpdate);        
        TransAm_ApplicationHelper.validateApplicantStatus(newApplicationList,newApplicationMap);
        TransAm_ApplicationHelper.updateApplicationOwner(newApplicationList, oldApplicationMap,newApplicationMap); 
        TransAm_ApplicationHelper.updateEmpStatus(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.calculateScoreonApplication(newApplicationList);
        TransAm_ApplicationHelper.updateDQStatusAndReason(newApplicationList);
        TransAm_ApplicationHelper.updateLastName(newApplicationList);
        TransAm_ApplicationHelper.updateConRecTypeToDriverForDriverCode(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.populateKeyFieldsOnApplicationBeforeUpdate(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.preventMultipleMasterApps(newApplicationList, newApplicationMap);
        TransAm_ApplicationHelper.updateArchivalDate(newApplicationList);

        	TransAm_ApplicationHelper.updateOwnerName(newApplicationList);

    }
    
    
   /*******************************************************************************************
   * Method        :   afterUpdateHandler
   * Description   :   Purpose of this method is to call updateApplicationOwner method from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterUpdateHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> oldApplicationMap, boolean isafterUpdate){     
       
       //list<TransAm_Application__c> sortAppList = new list<TransAm_Application__c>();
       set<Id> sortAppList = new set<ID>();
       list<TransAm_Application__c> childAppList = new list<TransAm_Application__c>();
       TransAm_ApplicationHelper.updateOwnerOnContactFromApplication(newApplicationlist, oldApplicationMap);

       Set<String> idList = new Set<String>();
       if(prohibitAfterUpdateTrigger ){
            return;
       }
        
        prohibitAfterUpdateTrigger = true;      
        TransAm_ApplicationHelper.updateContactFromPrimaryApp(newApplicationList, oldApplicationMap);
        //TransAm_ApplicationHelper.updateContactFromApplication(newApplicationList, oldApplicationMap);
        if(!prohibitAfterInsertTrigger){
            TransAm_ApplicationHelper.updateRecruitmentDocuments(newApplicationList, oldApplicationMap, isafterUpdate);
        }
        for(TransAm_Application__c childApp : newApplicationList){
            if(childApp.TransAm_Primary_Application__c != oldApplicationMap.get(childApp.Id).TransAm_Primary_Application__c){
                childAppList.add(childApp);
            }

            idList.add(childApp.Id);

        }
        if(childAppList.size()>0){
            TransAm_ApplicationHelper.countChildRecords(newApplicationList);
        }
        system.debug('term reason' + newApplicationList[0].TransAm_TERM_Reason__c); 
        //TransAm_ApplicationHelper.updateTerminationDate(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.updateConRecTypeOnAssociatingApp(newApplicationList, oldApplicationMap);
        TransAm_ApplicationHelper.updateGreyhoundDetails(newApplicationlist, oldApplicationMap);
        //TransAm_ApplicationHelper.updateHireDate(newApplicationlist, oldApplicationMap);
        TransAm_ApplicationHelper.updateApplicationLastStatusChangeOnUpdate(newApplicationlist, oldApplicationMap);
		TransAm_ApplicationHelper.updateDriverStatusChange(newApplicationList); 		
        for(TransAm_Application__c app : newApplicationList){    
            if(app.TransAm_Primary_Application__c!=oldApplicationMap.get(app.Id).TransAm_Primary_Application__c){
                sortAppList.add(app.Id);
                
            }
        } 
        if(sortAppList.size()>0 && (!System.isFuture())){
            TransAm_ApplicationHelper.sortApplicationRealtedList(sortAppList);
        }   

        //TransAm_ApplicationHelper.mergeAutoApps(idList);
		TransAm_ApplicationHelper.UpdateChildAppFromWebApp(newApplicationList);

    } 
 /*******************************************************************************************
   * Method        :   afterInsertHandler
   * Description   :   Purpose of this method is to call helper methods from TransAm_ApplicationHelper class.
   * Parameter     :   List<TransAm_Application__c>, Map<id, TransAm_Application__c>, boolean
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterInsertHandler(list<TransAm_Application__c> newApplicationList, Map<id, TransAm_Application__c> newApplicationMap, boolean isAfterInsert){
        Set<String> idList = new Set<String>();
        //TransAm_ApplicationHelper.sortApplicationRealtedList(newApplicationList);
        if(prohibitAfterInsertTrigger){
            if(count>0){
                TransAm_ApplicationHelper.childAppCreationIndication(newApplicationList);
                TransAm_ApplicationHelper.countChildRecords(newApplicationList);
            }
            return;
        }
        for(TransAm_Application__c objApp : newApplicationList){
            idList.add(objApp.Id);
        }
        prohibitAfterInsertTrigger = true;
        count++;
        system.debug('##calling helper');
        TransAm_ApplicationHelper.searchAndLinkAppContacts(newApplicationList, newApplicationMap, isAfterInsert);
        TransAm_ApplicationHelper.createRecruitmentDocuments(newApplicationList, isAfterInsert);
        TransAm_ApplicationHelper.childAppCreationIndication(newApplicationList);
        TransAm_ApplicationHelper.countChildRecords(newApplicationList);
        TransAm_ApplicationHelper.updateDriverStatusChange(newApplicationList);
        if(!System.isFuture()){
        	TransAm_ApplicationHelper.mergeAutoApps(idList);
			TransAm_ApplicationHelper.sortApplicationRealtedList(newApplicationMap.keyset());
		}
    }
    
    public static void afterDeleteHandler(list<TransAm_Application__c> oldApplicationList, Map<id, TransAm_Application__c> oldApplicationMap, boolean isAfterDelete){
    
        if(prohibitAfterDeleteTrigger){
            return;
        }
        prohibitAfterDeleteTrigger = true;
        TransAm_ApplicationHelper.countChildRecords(oldApplicationList);
    }        
}