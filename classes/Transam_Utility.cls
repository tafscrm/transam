public class Transam_Utility {
	public static List<String> getAllFields(String type) {
        
        List<String> getLabels = new List<String>();
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

        for (String fieldName: fieldMap.keySet()) { 
            
            Schema.DescribeFieldResult dfr = fieldMap.get(fieldName).getDescribe();
            
            if(!(dfr.getType() == Schema.DisplayType.Reference)) {
                getLabels.add(fieldName);
            }
        }
        //System.debug('getLabels'+ String.join(getLabels,','));
        return getLabels;
    }
}