/* CLass Name   : TransAm_ApplicationAssignmentHelperTest
 * Description  : Test class TransAm_ApplicationAssignmentHelper
 * Created By   : Neha Jain
 * Created On   : 17-Apr-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Neha Jain                17-Apr-2017              Initial version.
 *
 *****************************************************************************************/
@isTest(SeeAllData=false)
public class TransAm_ApplicationAssignmentHelperTest{

    @testSetup static void setup() {
        
        
        
       /* Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        List<TransAm_Application__c> objApp =new List<TransAm_Application__c>();
        objApp = TransAm_Test_DataUtility.createApplications(3, 'Independent', 'Application Received',con, True);  
        objApp[0].TransAm_Primary_Application__c=TRUE;
        objApp[0].TransAm_DriverID__c = '15263';
        objApp[0].TransAm_SSN__c='123434343';
        objApp[0].TransAm_EBE_Entry_ID__c = '';
        objApp[0].TransAm_Cell_Number__c='122424';
        objApp[0].TransAM_Email__c = 'test@tert.com';
        update objApp;*/
        
    }
    
 /************************************************************************************
 * Method       :    testApplicationAssignment
 * Description  :    Test Method to call Driver Conversion class
 * Parameter    :    NIL    
 * Return Type  :    void
 *************************************************************************************/
    
    public static testmethod void testDriverConversion(){
        
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        User user = new User();
        user.FirstName = 'Neha';
        user.LastName = 'Jain';
        user.Username = 'Nehaj@deloitte.com';
        user.Email   = 'Nehajain@deloitte.com';
        user.Alias   = 'Neha';
        user.TimeZoneSidKey   = 'America/Los_Angeles';
        user.LocaleSidKey   = 'en_US';
        user.EmailEncodingKey   = 'UTF-8';
        user.ProfileId = p.Id;                               
        user.LanguageLocaleKey = 'en_US'; 
        user.TransAm_Exclude_from_Assignment__c = FALSE;  
        user.TransAm_Last_Application_Assigned__c = FALSE; 
        insert user;

		Profile p1 = [SELECT id,name FROM Profile where name = 'Recruiter'];
        User user1 = new User();
        user1.FirstName = 'Test';
        user1.LastName = 'Jain';
        user1.Username = 'teasrt@deloitte.com';
        user1.Email   = 'Treta@deloitte.com';
        user1.Alias   = 'trfsh';
        user1.TimeZoneSidKey   = 'America/Los_Angeles';
        user1.LocaleSidKey   = 'en_US';
        user1.EmailEncodingKey   = 'UTF-8';
        user1.ProfileId = p1.Id;                               
        user1.LanguageLocaleKey = 'en_US'; 
        user1.TransAm_Exclude_from_Assignment__c = FALSE;  
        user1.TransAm_Last_Application_Assigned__c = TRUE; 
        
        insert user1;
        
        System.runAs(user){
            Account acc = new Account();
            acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
            Contact con = new Contact();
            con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
            List<TransAm_Application__c> objAppEx =new List<TransAm_Application__c>();
            
            objAppEx = TransAm_Test_DataUtility.createApplications(3, 'Independent', 'Application Received',con, FALSE);  
            objAppEx[0].TransAm_Primary_Application__c=TRUE;
            objAppEx[0].TransAm_DriverID__c = '15263';
            objAppEx[0].TransAm_SSN__c='123434343';
            objAppEx[0].TransAm_EBE_Entry_ID__c = '';
            objAppEx[0].TransAm_Cell_Number__c='122424';
            objAppEx[0].TransAM_Email__c = 'test@tert.com';
            objAppEx[0].TransAm_Primary_Application__c=TRUE;
            objAppEx[1].TransAm_DriverID__c = '23333';
            objAppEx[1].TransAm_SSN__c='676767675';
            objAppEx[1].TransAm_EBE_Entry_ID__c = '';
            objAppEx[1].TransAm_Cell_Number__c='122424';
            objAppEx[1].TransAM_Email__c = 'test@fgh.com';
            INSERT objAppEx;
            system.assert(!objAppEx.isEmpty());
            List<TransAm_Application__c> objApp =new List<TransAm_Application__c>();
            objApp = TransAm_Test_DataUtility.createApplications(3, 'Independent', 'Application Received',con, FALSE);  
            objApp[0].TransAm_Primary_Application__c=TRUE;
            objApp[0].TransAm_DriverID__c = '15263';
            objApp[0].TransAm_SSN__c='123434343';
            objApp[0].TransAm_EBE_Entry_ID__c = '';
            objApp[0].TransAm_Cell_Number__c='122424';
            objApp[0].TransAM_Email__c = 'test@tert.com';
            objApp[0].TransAm_Primary_Application__c=TRUE;
            objApp[1].TransAm_DriverID__c = '23333';
            objApp[1].TransAm_SSN__c='676767675';
            objApp[1].TransAm_EBE_Entry_ID__c = '';
            objApp[1].TransAm_Cell_Number__c='122424';
            objApp[1].TransAM_Email__c = 'test@fgh.com';
            INSERT objApp;
            system.assert(!objApp.isEmpty());
            Test.startTest();
            TransAm_ApplicationAssignmentHelper.assignApplication(objAppEx);
           	Test.stopTest();
        }
    }
}