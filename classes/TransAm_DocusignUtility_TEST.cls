@isTest
private with sharing class TransAm_DocusignUtility_TEST {

    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
         List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
         List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
         List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
         lstApplications = TransAm_Test_DataUtility.createApplications(2, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],true);
         
    } 
    private static testmethod void testAppDocSend() {
        
        TransAm_Application__c appObj = [SELECT ID FROM TransAm_Application__c LIMIT 1];       
        Test.startTest();
            PageReference pageRef = Page.TransAm_SendUsingDocusign;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', appObj.id);
            TransAm_DocuSignUtility newInstance = new TransAm_DocuSignUtility ();
            newInstance.sendToDocuSign();
        Test.stopTest();
    } 
    
}