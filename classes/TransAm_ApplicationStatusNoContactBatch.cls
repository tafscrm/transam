/******************************************************************************************
* Create By    :     Neha Jain
* Create Date  :     06/20/2017
* Description  :     Batch class to update status to 'No Contact' if Application is in 'Application Received' status for last 1 year.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_ApplicationStatusNoContactBatch implements Database.Batchable<sObject>{

    list<TransAm_Application__c> appToUpdateList = new list<TransAm_Application__c>();
    
    /*******************************************************************************************
    * Method        :   start
    * Description   :   Purpose of this method is to execute a query and return the query results to Execute method.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC){
       String customLabelValue = System.Label.TranAm_1_Year_Timespan;
       String status = 'Application Received';
       String queryString = 'SELECT id, Name, TransAm_Status__c, createddate from TransAm_Application__c WHERE TransAm_Status__c =: status AND TransAm_Received_Date__c = N_DAYS_AGO:' + customLabelValue;
       return Database.getQueryLocator(queryString);
       //return Database.getQueryLocator([Select id, Name, TransAm_Status__c, createddate from TransAm_Application__c where TransAm_Status__c = 'Application Received' AND createddate = N_DAYS_AGO:2 ]);
    }
    
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   Purpose of this method is to update status field value of applications.
    * Parameter     :   Database.BatchableContext, List<TransAm_Application__c>
    * Return Type   :   Void
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> scope){
        try{
            for(TransAm_Application__c Application : scope){                
                Application.TransAm_Status__c = 'No Contact';
                appToUpdateList.add(Application);
            }
            
            if(appToUpdateList.size() > 0){
                update appToUpdateList;
            }
        } catch(Exception e){
            TransAm_Error_Log__c elog = new TransAm_Error_Log__c();elog.TransAm_Class_Name__c = 'TransAm_ApplicationStatusToNoContactBatch';elog.TransAm_Method_Name__c = 'execute';elog.TransAm_Module_Name__c = 'TransAm_Application__c';elog.TransAm_Exception_Message__c = e.getMessage();insert elog;
        }

    }
    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Purpose of this method is to perform any activities post status update Update.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
        
    }

}