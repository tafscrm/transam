/****************************************************************************************
* Create By     :   Pankaj Singh
* Create Date   :   02/22/2017
* Description   :   Util class to setup test data for test class
* Modification Log:
*-----------------------------------------------------------------------------
** Developer                    Date          Description
** ----------------------------------------------------------------------------
** Pankaj Singh                 02/22/2017    Initial version.

*****************************************************************************************/
public with sharing class TransAm_Test_DataUtility {
    /************************************************************************************
    * Method       :    createAccounts
    * Description  :    create a list of accounts
    * Parameter    :    Integer, Boolean 
    * Return Type  :    list<Account>
    *************************************************************************************/
    public static List<Account> createAccounts(Integer total, Boolean insertRecords) {
        List<Account> lstAccounts = new List<Account>();
        
        for(Integer i = 0; i < total; i++){
            String uniqueness = DateTime.now()+':'+Math.random();
            try{ 
                throw new NullPointerException();
            }catch(Exception e){
                uniqueness += e.getStackTraceString();
            }
            String accountNumber = 'testaccountnumber-' + uniqueness.HashCode() + '-'+ i;
            lstAccounts.add(new Account( 
                                    Name                                    = 'Test Account - ' + uniqueness.HashCode(),
                                    AccountNumber                           =  accountNumber, 
                                    TransAm_Driving_School_Code__c          = 'C-1234'+String.valueOf(Math.random()).substring(3,9)+i,                             
                                    Phone                                   = '98765432'+i,
                                    BillingStreet                           = 'testBillingStreet',
                                    BillingState                            = 'testBillingState',
                                    BillingCity                             = 'testBillingCity',
                                    BillingCountry                          = 'US',
                                    BillingPostalCode                       = '56010',
                                    Fax                                     = '16157507314'
                                ));
        }
        if(insertRecords){
            insert lstAccounts;
        }
        return lstAccounts;
    }

    /************************************************************************************
    * Method       :    createContacts
    * Description  :    create a list of contacts
    * Parameter    :    Integer, Id, String, Boolean   
    * Return Type  :    list<Contact>
    *************************************************************************************/
    public static List<Contact> createContacts(Integer total, Id accountId, String recordTypeName, Boolean insertRecords) {
        Id contactRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        List<Contact> lstContacts = new List<Contact>();
        for(integer i = 0; i < total; i++){
            lstContacts.add(new Contact(AccountId                   = accountId,
                                     LastName                       = 'TestLNeh'+i,
                                     MobilePhone                    = '9898989898'+i,
                                     Phone                          = '16157185278'+i,
                                     FirstName                      = 'TestFNeh'+i,
                                     Salutation                     = 'Mr',
                                     TransAm_SSN__c                 = '22331144'+i,
                                     Email                          = 'testApplicant'+i+'@transam.com',
                                     RecordTypeId                   = contactRecTypeId,
                                     BirthDate                      = Date.today().addYears(-22),
                                     Emergency_contact_1_verified__c = true
                                    ));
        }
        
        if(insertRecords)           
            insert lstContacts;
        return lstContacts;
    }

    /************************************************************************************
    * Method       :    createEvents (School Visits)
    * Description  :    create a list of events
    * Parameter    :    Integer, Id, Id, String, Boolean   
    * Return Type  :    List<Event>
    *************************************************************************************/
    public static List<Event> createEvents(Integer total, Id contactId, Id accountId, String schoolVisitType, Boolean insertRecords) {
        List<Event> lstEvents = new List<Event>();
        for(integer i = 0; i < total; i++){
            lstEvents.add(new Event(Subject           = schoolVisitType+' '+i+contactId,                            
                                 OwnerId              = UserInfo.getUserId(),
                                 WhoId                = contactId,
                                 WhatId               = accountId,
                                 Type                 = schoolVisitType,
                                 DurationInMinutes    = 25,
                                 ActivityDateTime     = System.now()+1
                               ));
        }
        if(insertRecords) 
            insert lstEvents;
        return lstEvents;
    }

    /************************************************************************************
    * Method       :    createApplications
    * Description  :    create a list of Applications
    * Parameter    :    Integer, String, String, Boolean 
    * Return Type  :    List<TransAm_Application__c>
    *************************************************************************************/
    public static List<TransAm_Application__c> createApplications(Integer total, String appRestriction, String status, Contact con,Boolean insertRecords){
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        for(integer i = 0; i < total; i++){
            lstApplications.add(new TransAm_Application__c(
                                    TransAm_Application_Restriction__c = appRestriction,
                                    TransAm_City__c                    = 'testCity',
                                    TransAm_EBE_Entry_ID__c            = String.valueOf(Math.random()),
                                    TransAm_State__c                   = 'testState',
                                    TransAm_Status__c                  = status,
                                    TransAm_Original_Recruiter_Name__c = UserInfo.getUserId(),
                                    TransAm_Recruiter_Name__c          = 'Test Recruiter',
                                    TransAm_SSN__c                     = con.TransAm_SSN__c,
                                    TransAm_Email__c                   = 'testApplicant'+i+'@transam.com',
                                    TransAm_Phone__c                   = '16157185278'+i,
                                    TransAm_Primary_Application__c     = true,
                                    TransAm_Applicant__c               = con.Id,
                                    TransAm_ZIP__c                     = String.valueOf('22345'),
                                    TransAm_Date_Of_Birth__c           = Date.today().addYears(-22),
                					TransAm_Gender__c				   = 'Male',
                                    TransAm_Last_Name__c               = 'TestLNeh'+i,
                                    TransAm_MobilePhone__c            = '9898989898'+i,
                    TransAm_First_Name__c          = 'TestFNeh'+i,
                                    TransAm_Cell_Number__c             ='122424',
				    TransAm_Address__c		       = 'Test Addr1',
				    TransAm_Expiration_Date__c         = Date.today().addYears(+2),
				    TransAm_Driver_Type__c	       = 'Owner',
				    TransAm_Driver_Sub_Type__c	       = 'Owner',
				    TransAm_Type_Of_Driver__c	       = 'OO-Owner Operator',
				    TransAm_Position_Applying_For__c   = 'Experienced',
				    TransAm_Orientation_Location__c    = 'Olathe, KS',
				    TransAm_Emergency_First_Name__c    = 'Test EFN1eh',
				    TransAm_Emergency_Last_Name__c     = 'Test ELN1eh',
				    TransAm_Emergency_Phone__c         = '16157185278',
				    Emergency_Contact_1_Alternate_Phone__c = '16157185278',
				    Emergency_Contact_2_First_Name__c  = 'Test EFN2eh',
				    Emergency_Contact_2_Last_Name__c   = 'Test ELN2eh',
				    Emergency_Contact_2_Phone__c       = '16157185278',
				    Emergency_Contact_2_Alternate_Phone__c = '16157185278',
				    TransAm_LicenseNo__c	       = '9874563214',
				    TransAm_License_State__c	       = 'AL'
                                ));
        }
        if(insertRecords)
            insert lstApplications;
        return lstApplications;
    }


    /************************************************************************************
    * Method       :    createAccidents
    * Description  :    create a list of Accidents
    * Parameter    :    Integer, String, String, Boolean 
    * Return Type  :    List<TransAm_Accident__c>
    *************************************************************************************/
    public static List<TransAm_Accident__c> createAccidents(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_Accident__c> lstAccidents = new List<TransAm_Accident__c>();
        for(integer i = 0; i < total; i++){
            lstAccidents.add(new TransAm_Accident__c(
                                    TransAm_Application__c      = transamApplication.Id,
                                    TransAm_Accident_Type__c    = 'Backing',
                                    TransAm_Date__c             = System.today(),
                                    TransAm_EBE_Entry_ID__c     = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c    = transamApplication.TransAm_EBE_Entry_ID__c,
                                    TransAm_Location__c         = 'TestLocation',
                                    TransAm_Fatality__c         = 'No',
                                    TransAm_Hazardous_Spills__c = 'No',
                                    TransAm_Injuries__c         = 'Yes',
                                    TransAm_Preventable__c      = 'No'
                                ));
        }
        if(insertRecords)
            insert lstAccidents;
        return lstAccidents;
    }

    /************************************************************************************
    * Method       :    createFelonies
    * Description  :    create a list of Felony
    * Parameter    :    Integer, TransAm_Application__c, Boolean 
    * Return Type  :    List<TransAm_Felony__c>
    *************************************************************************************/
    public static List<TransAm_Felony__c> createFelonies(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_Felony__c> lstFelonies = new List<TransAm_Felony__c>();
        for(integer i = 0; i < total; i++){
            lstFelonies.add(new TransAm_Felony__c(
                                    TransAm_Application__c                      = transamApplication.Id,
                                    TransAm_Date__c                             = System.today(),
                                    TransAm_EBE_Entry_ID__c                     = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c                    = transamApplication.TransAm_EBE_Entry_ID__c,
                                  
                                    TransAm_Months_Parole_Probation_Served__c   = 1,
                                    TransAm_Months_Served__c                    = 1,
                                    TransAm_Served_Time__c                      = 'No'
                                ));
        }
        if(insertRecords)
            insert lstFelonies;
        return lstFelonies;
    }

    /************************************************************************************
    * Method       : createHiringAreasReferences
    * Description  :    create a list of HiringAreasReferences
    * Parameter    :    Integer,  TransAm_Application__c, Boolean 
    * Return Type  :    List<TransAm_Hiring_Areas_Reference__c>
    *************************************************************************************/
    public static List<TransAm_Hiring_Areas_Reference__c> createHiringAreasReferences(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_Hiring_Areas_Reference__c> lstHiringAreasReferences = new List<TransAm_Hiring_Areas_Reference__c>();
        for(integer i = 0; i < total; i++){
            lstHiringAreasReferences.add(new TransAm_Hiring_Areas_Reference__c(                                                                       
                TransAm_Zip__c = '223'+i));
        }
        if(insertRecords)
            insert lstHiringAreasReferences;
        return lstHiringAreasReferences;
    }
	public static List<TransAm_Hiring_Areas_Reference__c> insertHiringAreasReferences(Boolean insertRecords){
        List<TransAm_Hiring_Areas_Reference__c> lstHiringAreasReferences = new List<TransAm_Hiring_Areas_Reference__c>();
        lstHiringAreasReferences.add(new TransAm_Hiring_Areas_Reference__c(TransAm_Zip__c = '223'));
        if(insertRecords)
            insert lstHiringAreasReferences;
        return lstHiringAreasReferences;
    }

    /************************************************************************************
    * Method       :    createJobHistory
    * Description  :    create a list of Job History
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_Job_History__c>
    *************************************************************************************/
    public static List<TransAm_Job_History__c> createJobHistory(Integer total, TransAm_Application__c transamApplication, TransAm_Employer__c employer, Boolean insertRecords){
        List<TransAm_Job_History__c> lstJobHistory = new List<TransAm_Job_History__c>();
        for(integer i = 0; i < total; i++){
            lstJobHistory.add(new TransAm_Job_History__c(
                                    TransAm_Application__c      = transamApplication.Id,
                                    TransAm_EBE_Entry_ID__c     = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c    = transamApplication.TransAm_EBE_Entry_ID__c ,
                                    TransAm_Employer2__c        = employer.Id
                                ));
        }
        if(insertRecords)
            insert lstJobHistory;
        return lstJobHistory;
    }

    /************************************************************************************
    * Method       :    createMisdemeanor
    * Description  :    create a list of Misdemeanor
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_Misdemeanor__c>
    *************************************************************************************/
    public static List<TransAm_Misdemeanor__c> createMisdemeanor(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_Misdemeanor__c> lstMisdemeanor = new List<TransAm_Misdemeanor__c>();
        for(integer i = 0; i < total; i++){
            lstMisdemeanor.add(new TransAm_Misdemeanor__c(
                                    TransAm_Application__c      = transamApplication.Id,
                                    TransAm_EBE_Entry_ID__c     = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c    = transamApplication.TransAm_EBE_Entry_ID__c
                                    
                                    
                                ));
        }
        if(insertRecords)
            insert lstMisdemeanor;
        return lstMisdemeanor;
    }

    /************************************************************************************
    * Method       :    createViolations
    * Description  :    create a list of Violations
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_Misdemeanor__c>
    *************************************************************************************/
    public static List<TransAm_Violation__c> createViolations(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_Violation__c> lstViolations = new List<TransAm_Violation__c>();
        for(integer i = 0; i < total; i++){
            lstViolations.add(new TransAm_Violation__c(
                                    TransAm_Application__c              = transamApplication.Id,
                                    TransAm_EBE_Entry_ID__c             = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c            = transamApplication.TransAm_EBE_Entry_ID__c,
                                    TransAm_Conviction_Date__c          = System.today(),
                                    TransAm_Charges_for_speeding__c     = true,
                                    TransAm_Charges__c                  = 'CARELESS DRIVING',
                                    TransAm_Location__c                 = 'TestLocation',
                                    TransAm_Penalty__c                  = '200'
                                ));
        }
        if(insertRecords)
            insert lstViolations;
        return lstViolations;
    }
    
    /************************************************************************************
    * Method       :    createLicenses
    * Description  :    create a list of Licenses
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_License__c>
    *************************************************************************************/
    public static List<TransAm_License__c> createLicenses(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_License__c> lstLicenses = new List<TransAm_License__c>();
        for(integer i = 0; i < total; i++){
            lstLicenses.add(new TransAm_License__c(
                                    TransAm_Application__c         = transamApplication.Id,
                                    TransAm_Current__c             = 'Yes',
                                    TransAm_EBE_Entry_ID__c        = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c       = transamApplication.TransAm_EBE_Entry_ID__c,
                                    TransAm_Expiration_Date__c     = System.today()+1,
                                    TransAm_HazMat__c              = 'Yes',
                                    TransAm_LicenseNo__c           = '1234567',
                                    TransAm_License_Type__c        = 'Class A',
                                    TransAm_State__c               = 'DC'
                                ));
        }
        if(insertRecords)
            insert lstLicenses;
        return lstLicenses;
    }
    
    /************************************************************************************
    * Method       :    createPreviousAddresses
    * Description  :    create a list of PreviousAddresses
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_License__c>
    *************************************************************************************/
    public static List<TransAm_PreviousAddress__c> createPreviousAddresses(Integer total, TransAm_Application__c transamApplication, Boolean insertRecords){
        List<TransAm_PreviousAddress__c> lstPAddresses = new List<TransAm_PreviousAddress__c>();
        for(integer i = 0; i < total; i++){
            lstPAddresses.add(new TransAm_PreviousAddress__c(
                                    TransAm_Address__c         = 'NewYork',
                                    TransAm_Application__c     = transamApplication.Id,
                                    TransAm_City__c            = 'NewYork',
                                    TransAm_EBE_Entry_Id__c    = String.valueOf(Math.random()),
                                    TransAm_EBE_Parent_ID__c   = transamApplication.TransAm_EBE_Entry_ID__c,
                                    TransAm_State__c           = 'Atlanta',
                                    TransAm_Zip__c             =  String.valueOf('12345')                                 
                                ));
        }
        if(insertRecords)
            insert lstPAddresses;
        return lstPAddresses;
    }

    /************************************************************************************
    * Method       :    createEmployers
    * Description  :    create a list of Employers
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<TransAm_Employer__c>
    *************************************************************************************/
    public static List<TransAm_Employer__c> createEmployers(Integer total, Boolean insertRecords){
        List<TransAm_Employer__c> lstEmployers = new List<TransAm_Employer__c>();
        for(Integer i = 0; i < total; i++){
            lstEmployers.add(new TransAm_Employer__c(
                                    TransAm_Address1__c        = 'New Jersey '+i,
                                    TransAm_Address__c         = 'New Jersey '+i,
                                    TransAm_City__c            = 'New Jersey City '+i,
                                    TransAm_State__c           = 'New York',
                                    TransAm_Contact_Method__c  = 'Online',
                                    TransAm_Zip__c             =  String.valueOf('12345') ,
                                    TransAm_Email__c           = 'test@transam.com',
                                    TransAm_Fax__c             = '16157507314'                  
                                ));
        }
        if(insertRecords)
            insert lstEmployers;
        return lstEmployers;
    }

    /************************************************************************************
    * Method       :    createAttachments
    * Description  :    create a list of Attachments
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<Attachment>
    *************************************************************************************/
    public static List<Attachment> createAttachments(Integer total, Id attParentId, Boolean insertRecords){
        List<Attachment> lstAttachments = new List<Attachment>();
        for(Integer i = 0; i < total; i++){
            lstAttachments.add(new Attachment(        
                                    Name        = String.valueOf(attParentId)+i+'.pdf',
                                    Body        = Blob.valueOf('Unit Test Attachment Body'+i),
                                    ParentId    = attParentId              
                                ));
        }
        if(insertRecords)
            insert lstAttachments;
        return lstAttachments;
    }
    /************************************************************************************
    * Method       :    createRecruitmentDocuments
    * Description  :    create a list of Attachments
    * Parameter    :    Integer, Id, Boolean 
    * Return Type  :    List<Attachment>
    *************************************************************************************/
    public static List<TransAm_Recruitment_Document__c> createRecruitmentDocuments(Integer total, Id appId, String docType,Boolean insertRecords){
        List<TransAm_Recruitment_Document__c> lstDocs = new List<TransAm_Recruitment_Document__c>();
        for(Integer i = 0; i < total; i++){
            lstDocs.add(new TransAm_Recruitment_Document__c(        
                                    TransAm_Application__c        = appId,
                                    TransAm_Type__c 			  = docType              
                                ));
        }
        if(insertRecords)
            insert lstDocs;
        return lstDocs;
    }
    
    /************************************************************************************
    * Method       :    createHireRightOrders
    * Description  :    create a list of HireRight orders
    * Parameter    :    Integer, Id, String, Boolean 
    * Return Type  :    List<TransAm_HireRight_Orders__c>
    *************************************************************************************/
    public static List<TransAm_HireRight_Orders__c> createHireRightOrders(Integer total, Id appId, String docType,Boolean insertRecords){
        List<TransAm_HireRight_Orders__c> lstHROrders = new List<TransAm_HireRight_Orders__c>();
        for(Integer i = 0; i < total; i++){
            lstHROrders.add(new TransAm_HireRight_Orders__c(        
                                    TransAm_Application__c        = appId,
                                    TransAm_Report_Type__c 	= docType              
                                ));
        }
        if(insertRecords)
            insert lstHROrders;
        return lstHROrders;
    }

    /************************************************************************************
    * Method       :    createFormsEmailMappings
    * Description  :    create a list of Forms Email Mappings
    * Parameter    :    
    * Return Type  :    List<TransAm_Forms_EmailMapping__c>
    *************************************************************************************/
    public static List<TransAm_Forms_EmailMapping__c> createFormsEmailMappings(){
        List<TransAm_Forms_EmailMapping__c> lstFormEmailMappings = new List<TransAm_Forms_EmailMapping__c>();
        lstFormEmailMappings.add(new TransAm_Forms_EmailMapping__c( 
                                    Name = 'W-4',
                                    TransAm_SendEmail__c = TRUE,   
                                    TransAm_TeamEmail__c  = 'testEmail@test.com',
                                    TransAm_Form_Type__c  = 'W-4',
                                    TransAm_Email_Subject__c = 'New Hire W-4'            
                                ));
        insert lstFormEmailMappings;
        return lstFormEmailMappings;
    }

    /************************************************************************************
    * Method       :    createAppContactPrepopulateMappings
    * Description  :    create a list of Contact to App Prepopulate Mappings
    * Parameter    :    
    * Return Type  :    List<TransAm_AppContactPrepopulateMapping__c>
    *************************************************************************************/
    public static List<TransAm_AppContactPrepopulateMapping__c> createAppContactPrepopulateMappings(){
        List<TransAm_AppContactPrepopulateMapping__c> lstAppContactPrepopulateMappings = new List<TransAm_AppContactPrepopulateMapping__c>();
        lstAppContactPrepopulateMappings.add(new TransAm_AppContactPrepopulateMapping__c( 
                                            Name = 'TransAm_First_Name__c',
                                            TransAm_ContactField__c = 'FirstName',
                                            TransAm_AppFieldDatatype__c = 'text'
                                ));
        lstAppContactPrepopulateMappings.add(new TransAm_AppContactPrepopulateMapping__c( 
                                            Name = 'TransAm_Emergency_Contact_Verified__c',
                                            TransAm_ContactField__c = 'Emergency_contact_1_verified__c',
                                            TransAm_AppFieldDatatype__c = 'checkbox'
                                ));
        lstAppContactPrepopulateMappings.add(new TransAm_AppContactPrepopulateMapping__c( 
                                            Name = 'TransAm_Date_Of_Birth__c',
                                            TransAm_ContactField__c = 'BirthDate',
                                            TransAm_AppFieldDatatype__c = 'date'
                                ));
        lstAppContactPrepopulateMappings.add(new TransAm_AppContactPrepopulateMapping__c( 
                                            Name = 'TransAm_ReferredBy__c',
                                            TransAm_ContactField__c = 'TransAm_Driver_Id__r.Name',
                                            TransAm_AppFieldDatatype__c = 'relatedlookupname'
                                ));
        lstAppContactPrepopulateMappings.add(new TransAm_AppContactPrepopulateMapping__c( 
                                            Name = 'TransAm_Applicant__c',
                                            TransAm_ContactField__c = 'Name',
                                            TransAm_AppFieldDatatype__c = 'lookup'
                                ));
        insert lstAppContactPrepopulateMappings;
        return lstAppContactPrepopulateMappings;
    }

    /************************************************************************************
    * Method       :    createGenericCustomSetting
    * Description  :    create a list of Generic Custom Settings
    * Parameter    :    
    * Return Type  :    List<TransAm_Generic_Custom_Setting__c>
    *************************************************************************************/
    public static List<TransAm_Generic_Custom_Setting__c> createGenericCustomSetting(){
        List<TransAm_Generic_Custom_Setting__c> lstGenCustomSettings = new List<TransAm_Generic_Custom_Setting__c>();
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'No of Business Hours a Day',
                                    TransAm_Message_String__c = '8'            
                                ));
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'Recruiter Queue Name',
                                    TransAm_Message_String__c = 'Test'            
                                ));
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'Risk Mgmt Docs Number',
                                    TransAm_Message_String__c = '2'            
                                ));
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'Risk Mgmt Email Address',
                                    TransAm_Message_String__c = 'TestEmail@transam.com'            
                                ));
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'Risk Mgmt Email Docs',
                                    TransAm_Message_String__c = 'ICA.docx,OneBeaconEnrollForm.docx'            
                                ));
        lstGenCustomSettings.add(new TransAm_Generic_Custom_Setting__c( 
                                    Name = 'Risk Mgmt Lease Doc Types',
                                    TransAm_Message_String__c = 'Independent Contractor Agreement,OneBeacon Enrollment Form'            
                                ));
        insert lstGenCustomSettings;
        return lstGenCustomSettings;
    }

    /************************************************************************************
    * Method       :    createRecruiterUser
    * Description  :    create a list of Generic Custom Settings
    * Parameter    :    
    * Return Type  :    List<TransAm_Generic_Custom_Setting__c>
    *************************************************************************************/
    public static User createRecruiterUser(){
        Profile p =[SELECT Id FROM Profile WHERE Name = 'Recruiter'];
        User u = new User();
        u.Username = 'testRecUser@transam.com';
        u.LastName = 'RecUser';
        u.Email = 'testRecUser@transam.com';
        u.Alias = 'testrec';
        u.CommunityNickname = 'testrec';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.LocaleSidKey = 'en_us';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_us';
        u.ProfileId = p.Id;
        insert u;
        return u;
    }

    /************************************************************************************
    * Method       :    createBusinessHours
    * Description  :    create Business Hours
    * Parameter    :    
    * Return Type  :    List<TransAm_Generic_Custom_Setting__c>
    *************************************************************************************/
    public static BusinessHours createBusinessHours(){
        BusinessHours defaultBH = [Select Id From BusinessHours Where IsDefault = True];
        Holiday hol = new Holiday();
        hol.Name='Test holiday';
        hol.activitydate = System.Today();
        insert hol;
        return defaultBH;
    }

    /************************************************************************************
    * Method       :    createLeases
    * Description  :    create a list of Lease records
    * Parameter    :    Integer, TransAm_Application__c, String, Boolean
    * Return Type  :    List<TransAm_ONE_Leasing__c>
    *************************************************************************************/
    public static List<TransAm_ONE_Leasing__c> createLeases(Integer total, TransAm_Application__c trApp, String recordTypeName, Boolean insertRecords, TransAm_ONE_Leasing__c leaseRec){
        List<TransAm_ONE_Leasing__c> lstLeaseRecords = new List<TransAm_ONE_Leasing__c>();
        Id leaseRecTypeId = Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        for(Integer i = 0; i < total; i++){
            if(recordTypeName == 'Lease Agreement')
                lstLeaseRecords.add(new TransAm_ONE_Leasing__c(        
                                    RecordTypeId                = leaseRecTypeId,
                                    TransAm_Application_ELA__c  = trApp.Id,
                                    TransAm_Lifestyle__c        = 'Yes',
                                    TransAm_IC_Agreement__c     = leaseRec.Id            
                                ));
            else if(recordTypeName == 'IC Agreement')
                lstLeaseRecords.add(new TransAm_ONE_Leasing__c(        
                                    RecordTypeId                = leaseRecTypeId,
                                    TransAm_Application_IC__c      = trApp.Id      
                                ));
        }
        if(insertRecords)
            insert lstLeaseRecords;
        return lstLeaseRecords;
    }
}