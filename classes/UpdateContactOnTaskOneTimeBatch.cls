global class UpdateContactOnTaskOneTimeBatch implements Database.Batchable<sObject> {
    
    /*******************************************************************************************
    * Method        :   start
    * Description   :   This is the start() method of the batch class, used to query 
    *                   and retrieve all the Tasks where whoId=null.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('select id,whoId from Task where whoId=null'); //exectue query
    }
   
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   This is the execute method of the batch class, used to process and update contact on Task
    * Parameter     :   Database.BatchableContext, List<Task>
    * Return Type   :   Void
    * Modification Log:
    * ----------------------------------------------------------------------------
    *  * Developer                          Date               Description
    *  * ----------------------------------------------------------------------------                 
    *  * Ajay B R                         09/04/2017           Initial version.
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Task> taskList) {
        
        Set<Id> appIdSet = new Set<Id>();
        List<Task> newTaskList = new List<Task>();
        Map<ID,ID> appIdContactIdMap = new Map<ID,ID>();
        Map<ID, String> appIDConNameMap = new Map<ID, String>();
        map<Id, list<Task>> appTaskMap = new map<Id, list<Task>>();
        List<Task> taskToBeUpdatedList = new List<Task>();
        set<Id> taskIdSet = new set<Id>();
        String appPrefix = TransAm_Application__C.sobjecttype.getDescribe().getKeyPrefix();
        for(Task taskObj : [SELECT ID,Whoid, WhatId FROM Task WHERE ID IN:taskList]){           
            if(taskObj.whatId != null && String.valueOf(taskObj.whatId).startsWith(appPrefix)){
                newTaskList.add(taskObj);
                if(appTaskMap.containskey(taskObj.whatId)){
                    appTaskMap.get(taskObj.WhatId).add(taskObj);
                }
                else{
                    List<Task> appTaskList = new List<Task>();
                    appTaskList.add(taskObj);
                    appTaskMap.put(taskObj.WhatId, appTaskList);
                }                
            }
        }
        for(TransAm_Application__C appObj : [Select ID, TransAm_Applicant__c, TransAm_Primary_Application__c from TransAm_Application__C 
                                             where ID IN :appTaskMap.keyset() and TransAm_Primary_Application__c = True]){
            if(appTaskMap.containsKey(appObj.Id)){
                for(Task taskObj : appTaskMap.get(appObj.Id)){
                    taskObj.whoId = appObj.TransAm_Applicant__c;
                    taskToBeUpdatedList.add(taskObj);
                }                
            }
        }
        update taskToBeUpdatedList;
    }    

    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Finish() method of the batch class,generally used to implement post commit logics.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
    //completed
    }
}