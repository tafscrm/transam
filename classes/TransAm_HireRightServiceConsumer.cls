/********************************************************************************* 
* Class Name        :   TransAm_HireRightServiceConsumer
* Description       :     Utility class to invoke HireRight WebServices
* Created By        :     Monalisa Das
* Create Date       :     28/04/2017

* Modification Log  :
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Monalisa Das                      28/04/2017         Initial version.
*****************************************************************************************/
public class TransAm_HireRightServiceConsumer {
    
    
    /**********************************************************************************************
* Created By      :  Monalisa
* Create Date     :  28-Apr-2017
* Description     :  Invoke Hire Right Service to fetch HR Order Id
* Modification Log:  Initial version.
***********************************************************************************************/
    
    public static void sendRawRequest(String request, Id orderId) {
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest(); 
            HttpResponse res = new HttpResponse();   
            TransAm_HireRight_Orders__c order = [SELECT TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c WHERE ID = :orderId];
            
            Integer ContentLength = 0; 
            ContentLength = request.length();
            req.setMethod('POST'); 
            req.setEndPoint('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); 
            //req.setEndPoint('https://ows01.hireright.com/ws_gateway_stub/HireRightAPI/v/1/2'); 
            req.setHeader('Content-type','text/xml'); 
            req.setHeader('Content-Length',ContentLength.format()); 
            req.setHeader('SoapAction','Create');         
            req.setBody(request);
            System.Debug('req:'+req); 
            System.Debug('req body:'+req.getBody()); 
            res = h.send(req); 
            System.Debug('res:'+res); 
            String auth = res.getBody(); 
            System.Debug('Debug(auth:'+auth);
            
            String envNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            String hrNS = 'urn:enterprise.soap.hireright.com/objs';        
            if(res.getStatusCode() == 200) {
                Dom.Document doc = res.getBodyDocument();
                Dom.XmlNode soapEnv = doc.getRootElement();
                Dom.XmlNode node = soapEnv.getChildElement('Body', envNS);
                node = node.getChildElement('CreateResponse', hrNS);
                node = node.getChildElement('Result', hrNS);
                node = node.getChildElement('RegId', hrNS);
                order.TransAm_Hireright_Order_ID__c = node.getText();
                order.TransAm_Order_Status__c = 'In Progress';
				order.TransAm_IS_Manual_Update__c = true;
                //TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
                Database.update(order);
                //TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
            }
	    else
	    {
		Dom.Document doc = res.getBodyDocument();
                Dom.XmlNode soapEnv = doc.getRootElement();
                Dom.XmlNode bodyNode = soapEnv.getChildElement('Body', envNS);
		Dom.XmlNode faultNode = bodyNode.getChildElement('Fault', envNS);
		faultNode = faultNode.getChildElement('faultstring', null);
		
                order.TransAm_Service_Message__c = faultNode.getText();
		order.TransAm_Order_Status__c = 'Failed';
		Database.update(order);
	    }
        }catch(Exception e)
        {
            System.debug('Exception in sendRawRequest ::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightServiceConsumer',TransAm_Method_Name__c = 'sendRawRequest',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }
    
    /**********************************************************************************************
* Created By      :  Monalisa
* Create Date     :  28-Apr-2017
* Description     :  Invoke Hire Right Service to fetch HR Order Weblink URL
* Modification Log:  Initial version.
***********************************************************************************************/
    /*@future(callout=true)
    public static void sendRawRequestForWebLink(String request, Id orderId) {
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest(); 
            HttpResponse res = new HttpResponse();   
            TransAm_HireRight_Orders__c order = [SELECT TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c WHERE ID = :orderId];
            
            Integer ContentLength = 0; 
            ContentLength = request.length();
            req.setMethod('POST'); 
            req.setEndPoint('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); 
            //req.setEndPoint('https://ows01.hireright.com/ws_gateway_stub/HireRightAPI/v/1/2'); 
            req.setHeader('Content-type','text/xml'); 
            req.setHeader('Content-Length',ContentLength.format()); 
            req.setHeader('SoapAction','GenerateWebLink');  
            req.setBody(request);
            System.debug('Req in sendRawRequestForWebLink::'+request);
            res = h.send(req); 
            System.Debug('res:'+res); 
            String auth = res.getBody(); 
            // System.Debug('Debug(auth:'+auth);
            
            String envNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            String hrNS = 'urn:enterprise.soap.hireright.com/objs';        
            if(res.getStatusCode() == 200) {
                Dom.Document doc = res.getBodyDocument();
                Dom.XmlNode soapEnv = doc.getRootElement();
                Dom.XmlNode node = soapEnv.getChildElement('Body', envNS);
                node = node.getChildElement('GenerateWebLinkResponse', hrNS);
                node = node.getChildElement('Result', hrNS);
                node = node.getChildElement('ReportURL', hrNS);
                order.TransAm_HireRight_Reports__c = node.getText();
				order.TransAm_IS_Manual_Update__c = true;
                System.debug('order.TransAm_HireRight_Reports__c ::'+order.TransAm_HireRight_Reports__c);
                TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
                //System.debug('Calling download attachment');
                downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order);
                Database.update(order);
                //downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order.Id);
                TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
            }
        }
        catch(Exception e)
        {
            System.debug('Exception ::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightServiceConsumer',TransAm_Method_Name__c = 'sendRawRequestForWebLink',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }
    
   public static String getWeblink(String request) {
        HttpResponse res;
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest(); 
            res = new HttpResponse();   
            
            
            Integer ContentLength = 0; 
            ContentLength = request.length();
            req.setMethod('POST'); 
            req.setEndPoint('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); 
            //req.setEndPoint('https://ows01.hireright.com/ws_gateway_stub/HireRightAPI/v/1/2'); 
            req.setHeader('Content-type','text/xml'); 
            req.setHeader('Content-Length',ContentLength.format()); 
            req.setHeader('SoapAction','GenerateWebLink');  
            req.setBody(request);
            System.debug('Req in sendRawRequestForWebLink::'+request);
            res = h.send(req); 
            System.Debug('res:'+res); 
            String auth = res.getBody(); 
            // System.Debug('Debug(auth:'+auth);
            
            String envNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            String hrNS = 'urn:enterprise.soap.hireright.com/objs';        
            if(res.getStatusCode() == 200) {
                Dom.Document doc = res.getBodyDocument();
                Dom.XmlNode soapEnv = doc.getRootElement();
                Dom.XmlNode node = soapEnv.getChildElement('Body', envNS);
                node = node.getChildElement('GenerateWebLinkResponse', hrNS);
                node = node.getChildElement('Result', hrNS);
                node = node.getChildElement('ReportURL', hrNS);
                return node.getText();
                /*order.TransAm_HireRight_Reports__c = node.getText();
                System.debug('order.TransAm_HireRight_Reports__c ::'+order.TransAm_HireRight_Reports__c);
                TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
                //System.debug('Calling download attachment');
                downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order);
                Database.update(order);
                //downloadAttachment(order.TransAm_Application__c,order.TransAm_HireRight_Reports__c,order.Id);
                TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;*/
           /*}
        }
        catch(Exception e)
        {
            System.debug('Exception ::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightServiceConsumer',TransAm_Method_Name__c = 'sendRawRequestForWebLink',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
             return e.getMessage();
        }
        return 'Body ' + res.getBody();
    }*/
    
    /**********************************************************************************************************************
* Created By      :  Monalisa
* Create Date     :  28-Apr-2017
* Description     :  To download the attachment from weblink url, map to recruitment document & HR Order
* Modification Log:  Initial version.
***********************************************************************************************************************/
    /*public static void downloadAttachment(String appId,String attachmentURL,TransAm_HireRight_Orders__c hireRightOrder){
        try
        {
            System.debug('Inside download attachment');
            HttpRequest req = new HttpRequest();
            req.setEndpoint(attachmentURL);
            req.setMethod('GET');
            Http h = new Http();                 
            HttpResponse res = h.send(req);
            String nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            System.debug('Status code for ows ' + res.getStatusCode());
            
            // Attaching the PDF to Contact
            
            /*TransAm_Recruitment_Document__c recDoc = new TransAm_Recruitment_Document__c();
recDoc.TransAm_Application__c = appId;
if(!String.ISBLANK(hireRightOrder.TransAm_Report_Type__c))
{
System.debug('hireRightOrder.TransAm_Report_Type__c ::'+hireRightOrder.TransAm_Report_Type__c);
if(hireRightOrder.TransAm_Report_Type__c.contains('Express'))
recDoc.TransAm_Type__c = 'MVRExpress';
else if(hireRightOrder.TransAm_Report_Type__c.contains('Standard'))
recDoc.TransAm_Type__c = 'MVRStandard';
}
else
{
System.debug('hireRightOrder.TransAm_Report_Type__c is blank');
}
recDoc.Name='Master ' + hireRightOrder.TransAm_Application__r.Name + ' ' + recDoc.TransAm_Type__c;
Database.insert(recDoc);
System.debug('Rec Doc Id :: '+recDoc);*/
            
            /*String reportType ;
            if(!String.ISBLANK(hireRightOrder.TransAm_Report_Type__c))
            {
                if(hireRightOrder.TransAm_Report_Type__c.contains('Express'))
                    reportType = 'MVRExpress';
                else if(hireRightOrder.TransAm_Report_Type__c.contains('Standard'))
                    reportType = 'MVRStandard';
            }
            //String searchString = '%'+hireRightOrder.TransAm_Application__r.Name + '%'+reportType;
            String appName = hireRightOrder.TransAm_Application__r.Name;
            TransAm_Recruitment_Document__c recDoc = [select id from TransAm_Recruitment_Document__c where TransAm_Application__r.Name = :appName and TransAm_Type__c = :reportType Limit 1];
            
            
            Attachment att = new Attachment();
            att.Body = res.getBodyAsBlob();
            att.name = 'test.pdf';
            att.ParentId = recDoc.Id;
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = true;
            Database.insert(att);
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = false;
            System.debug('Attachment Id :: '+att);
            
            TransAm_HireRight_Orders__c order = new TransAm_HireRight_Orders__c(Id=hireRightOrder.Id);
            order.TransAm_Recruitment_Documents__c = recDoc.Id;
            order.TransAm_HireRight_Reports__c=attachmentURL;
            System.debug('Updating Hire Right Order :: '+hireRightOrder.Id);
            Database.update(order);
            
            
            
        }
        catch(Exception e)
        {
            System.debug('Exception in downloadAttachment::'+e.getStackTraceString() + e.getMessage() +e);
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightServiceConsumer',TransAm_Method_Name__c = 'downloadAttachment',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }*/
    
    /*public static Blob getAttachment(String attachmentURL){
            System.debug('Inside download attachment');
            HttpRequest req = new HttpRequest();
            req.setEndpoint(attachmentURL);
            req.setMethod('GET');
            Http h = new Http();                 
            HttpResponse res = h.send(req);
            String nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            nextEndpoint = res.getHeader('Location');
            req.setEndpoint(nextEndpoint);
            res = h.send(req);
            System.debug('Status code for ows ' + res.getStatusCode());
            return res.getBodyAsBlob();
    }*/
    
}