/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/15/2017
* Description  :     This test class ensures code coverage for TransAm_ContactTrigger trigger , TransAm_ContactHandler class and TransAm_ContactHelper class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest(seeAllData=false)
public class TransAm_ContactTriggerTest{ 
    public static User integrationUser;
    public static List<Account> accountList;
    public static List<Contact> contactList;

    @testSetup
    private static void testDataSetUp(){
        List<Account> accountList = TransAm_Test_DataUtility.createAccounts(2, true);
        accountList[0].TransAm_Driving_School_Code__c = String.valueOf(accountList[0].Id).substring(0,15);
        update accountList[0];

        List<Contact> contactList = TransAm_Test_DataUtility.createContacts(14, accountList[0].Id, 'Applicant', false);
        contactList[1].TransAm_SSN__c = '';
        contactList[2].Phone = '';
        contactList[2].TransAm_SSN__c = '';
        contactList[3].Phone = '';
        contactList[3].Email = '';
        contactList[3].TransAm_SSN__c = '';
        contactList[4].Phone = '';
        contactList[4].Email = '';
        contactList[4].TransAm_SSN__c = '';
        contactList[4].TransAm_FirstNameLastNameEmail__c = '';
        contactList[4].TransAm_FirstNameLastNamePhone__c = '';
        contactList[5].Email = '';
        contactList[5].TransAm_SSN__c = '';
        
        Id scRecId = [select id,developername from recordtype where sobjecttype = 'contact' and developername = 'TransAm_School_Contact'].Id;
        contactList[11].RecordTypeId = scRecId;
        contactList[11].TransAm_Main_Contact__c = true;
        contactList[11].TransAm_School_Id__c = String.valueOf(accountList[0].Id).substring(0,15);
        //contactList[11].accountId = null;
        contactList[12].RecordTypeId = scRecId;
        contactList[12].TransAm_Main_Contact__c = true;
        contactList[12].TransAm_School_Id__c = String.valueOf(accountList[0].Id).substring(0,15);
        contactList[12].accountId = null;
        insert contactList;
        
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        integrationUser = new User();
        integrationUser.firstname = 'Arun';
        integrationUser.lastname = 'Tyagi';
        integrationUser.username = 'xyzrtyu@abc.com';
        integrationUser.email = 'xyz@deloitte.com';
        integrationUser.alias = 'xyz';
        integrationUser.TimeZoneSidKey = 'America/Los_Angeles';
        integrationUser.LocaleSidKey = 'En_US';
        integrationUser.profileID = prof.Id;
        integrationUser.LanguageLocaleKey = 'En_US';
        integrationUser.EmailencodingKey = 'UTF-8';
        insert integrationUser;
    }

    public static TestMethod void testContactTrigger(){
        Test.startTest();
        Test.stopTest();
    }

    public static testMethod void testDeleteDuplicateContactsOnAfterInsert(){
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        List<User> u = [SELECT Id FROM User WHERE profileId = :prof.Id AND IsActive = TRUE];
        if(!u.isEmpty()){
			System.runAs(u[0]){
				List<Account> lstAcc = [SELECT Id FROM Account];
				TransAm_Test_DataUtility.createContacts(4, lstAcc[0].Id, 'Applicant', true);   
			}
		}
    }

    public static testMethod void testpreventDuplicateContactsOnBeforeUpdate(){
        List<Account> lstAcc = [SELECT Id FROM Account order by TransAm_Driving_School_Code__c];
        lstAcc[1].Name = 'Not Applicable 12';
        update lstAcc;
        List<Contact> lstCons = [SELECT Id, TransAm_SSN__c, Phone, Email FROM Contact ORDER BY Id];
        lstCons[6].TransAm_SSN__c = '223311443';
        lstCons[7].Phone = '161571852783';
        lstCons[7].TransAm_SSN__c = '';
        lstCons[8].Phone = '';
        lstCons[8].Email = 'testApplicant3@transam.com';
        lstCons[8].TransAm_SSN__c = '';
        lstCons[9].TransAm_SSN__c = '';
        lstCons[9].TransAm_FirstNameLastNameEmail__c = '';
        lstCons[9].TransAm_FirstNameLastNamePhone__c = '';
        lstCons[9].TransAm_FirstNameLastNameCellPhone__c = 'testftestl2323';
        lstCons[10].Email = 'test@t.com';
        lstCons[10].TransAm_SSN__c = '';
        lstCons[13].AccountId = lstAcc[1].Id;
        update lstCons;
    }
}