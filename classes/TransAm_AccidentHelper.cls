/* Class Name   : TransAm_AccidentHelper
 * Description  : Helper class for the Accident trigger
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_AccidentHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  15-Feb-2017
    * Description     :  populate the application Id on the accident record
    * Modification Log:  Initial version.
    ***************************************************************************************/
    static Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
    public static void populateApplicationOnAccidents(list<TransAm_Accident__c> accidentList){
    
        
        if(UserInfo.getProfileId()==profileId){       
            map<string,Id> applicationMap = new map<string,Id>();
            set<String> parentIdSet = new set<String>();           
            try{
                for(TransAm_Accident__c  accidentObj : accidentList){
                    parentIdSet.add(accidentObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_Accident__c accidentObj : accidentList){
                    if(applicationMap.containsKey(accidentObj.TransAm_EBE_Parent_ID__c)){
                        accidentObj.TransAm_Application__c = applicationMap.get(accidentObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_AccidentHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnAccidents';
                el.TransAm_Module_Name__c = 'Accident';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el;
            }
        }
    }
    public static void updateApplication(List<TransAm_Accident__c> accidentList){
        if(UserInfo.getProfileId()==profileId){ 
            List<Id> appIdList = new List<Id>();
            for(TransAm_Accident__c objAccident : accidentList){
                if(String.isNotBlank(objAccident.TransAm_Application__c))
                    appIdList.add(objAccident.TransAm_Application__c);
            }
            List<TransAm_Application__c> appList = [SELECT Id FROM TransAm_Application__c WHERE Id IN : appIdList];
            System.debug('appList-->'+ appList);
            Database.update(appList);
        }
    }
}