/* CLass Name   : TransAm_SendApplicationSummary_Test
 * Description  : Test class for SendApplicationSummary
 * Created By   : Karthik Gulla
 * Created On   : 13-Sep-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                13-Sep-2017            Initial version.
 *
 *****************************************************************************************/
@isTest
public class TransAm_SendApplicationSummary_Test{
     /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
    	List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(2, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0], true);
        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1,true);
        TransAm_Test_DataUtility.createJobHistory(16, lstApplications[0], lstEmployers[0], true);
    }

    /************************************************************************************
    * Method       :    testSendAppSummary
    * Description  :    Test Method to Send Application details for Driver for eSignature
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testSendAppSummary() {
        Test.startTest();
        List<TransAm_Application__c> lstApps = [Select Id, Name From TransAm_Application__c];
        TransAm_SendApplicationSummary.sendAppSummary(lstApps[0].Id);
        Test.stopTest();
    }
}