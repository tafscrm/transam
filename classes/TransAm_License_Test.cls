/* CLass Name   : TransAm_License_Test
 * Description  : Test class for License
 * Created By   : Pankaj Singh
 * Created On   : 24-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                24-Feb-2017              Initial version.  
 *  * Manish Jain                 05-Apr-2017              Second Version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_License_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);
        
    }

    /************************************************************************************
    * Method       :    testLicense
    * Description  :    Test Method to create License
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testLicense() {
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        User user = new User();
        user.FirstName = 'Neha';
        user.LastName = 'Jain';
        user.Username = 'Nehaj@deloitte.com';
        user.Email   = 'Nehajaiertyn@deloitte.com';
        user.Alias   = 'Neha';
        user.TimeZoneSidKey   = 'America/Los_Angeles';
        user.LocaleSidKey   = 'en_US';
        user.EmailEncodingKey   = 'UTF-8';
        user.ProfileId = p.Id;                               
        user.LanguageLocaleKey = 'en_US'; 
        user.TransAm_Exclude_from_Assignment__c = FALSE;  
        user.TransAm_Last_Application_Assigned__c = FALSE; 
        insert user;
        System.runAs(user){
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
            list<TransAm_License__c> licenseList = TransAm_Test_DataUtility.createLicenses(1, lstApplications[0], false);
            Test.startTest();
                insert licenseList;
            Test.stopTest();
        }
    }
    /************************************************************************************
    * Method       :    testLicenseException
    * Description  :    Test Method to cover exception block
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testLicenseException() {
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
    	User user = new User();
        user.FirstName = 'Neha';
        user.LastName = 'Jain';
        user.Username = 'Nehaj@deloitte.com';
        user.Email   = 'Nehajaicsaertyn@deloitte.com';
        user.Alias   = 'Neha';
        user.TimeZoneSidKey   = 'America/Los_Angeles';
        user.LocaleSidKey   = 'en_US';
        user.EmailEncodingKey   = 'UTF-8';
        user.ProfileId = p.Id;                               
        user.LanguageLocaleKey = 'en_US'; 
        user.TransAm_Exclude_from_Assignment__c = FALSE;  
        user.TransAm_Last_Application_Assigned__c = FALSE; 
        insert user;
        System.runAs(user){
            Test.startTest();
            try{
                TransAm_LicenseHelper.populateApplicationOnLicense(null);
            }catch(Exception e){}
            Test.stopTest();
        }
    }
    
     /************************************************************************************
    * Method       :    testLicenseOnExpirationDate
    * Description  :    Test Method to popupate latest expiration date from license to associated application object
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testLicenseOnExpirationDate() {
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        User user = new User();
        user.FirstName = 'Neha';
        user.LastName = 'Jain';
        user.Username = 'Nehaj@deloitte.com';
        user.Email   = 'Nehertfvtyn@deloitte.com';
        user.Alias   = 'Neha';
        user.TimeZoneSidKey   = 'America/Los_Angeles';
        user.LocaleSidKey   = 'en_US';
        user.EmailEncodingKey   = 'UTF-8';
        user.ProfileId = p.Id;                               
        user.LanguageLocaleKey = 'en_US'; 
        user.TransAm_Exclude_from_Assignment__c = FALSE;  
        user.TransAm_Last_Application_Assigned__c = FALSE; 
        insert user;
        System.runAs(user){
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_License_Expiration_Date__c,TransAm_EBE_Entry_ID__c, TransAm_State__c FROM TransAm_Application__c LIMIT 1];
            list<TransAm_License__c> licenseList = TransAm_Test_DataUtility.createLicenses(3, lstApplications[0], true);
            licenseList[0].TransAm_Expiration_Date__c = Date.Today().addDays(4);
            licenseList[1].TransAm_Expiration_Date__c = Date.Today().addDays(10);
            
            Test.startTest();
                UPDATE licenseList;
            Test.stopTest();
        }
    }
}