public class TransAm_Application_ChildRecords{
    
    public list<voilationsWrapper> listVoilations {get;set;}
    public List<TransAm_Violation__c> listVoilationsData;
    public boolean Selected{get;set;}
    public List<voilationsWrapper> listVoilationsSelected {get;set;}
    public List<TransAm_Violation__c> listMergeVoilations {get;set;}
    
    public Id MasterApp = 'a034D000000patE';
    public list<TransAm_Application__c> listApplications = [select id from TransAm_Application__c where TransAm_Master_Application__c =: MasterApp ]; 
    public TransAm_Application_ChildRecords(ApexPages.StandardController controller) {
        
        listVoilationsData =[Select id,TransAm_Application__r.Name,Name,TransAm_Charges__c,TransAm_Violation_Date__c,TransAm_Location__c,TransAm_Penalty__c,
                            TransAm_If_OverSpeeding_select_speed__c from TransAm_Violation__c where TransAm_Application__c in:listApplications];
        listVoilations = new list<voilationsWrapper>();
        for(TransAm_Violation__c obj : listVoilationsData ){
            listVoilations.add(new voilationsWrapper(obj));
        }  
    }
    
    public PageReference deselectAll() {
        return null;
    }


    public PageReference selectAll() {
        return null;
    }

    public class voilationsWrapper{
        public boolean selected{get;set;}
        public TransAm_Violation__c voilationsSelected{get;set;}
        
        public voilationsWrapper(TransAm_Violation__c voilations){       
            this.selected = false;
            this.voilationsSelected = voilations;
        }                
    
    }
    
    /*public void findVoilations()
    {
      for(voilationsWrapper obj: listVoilations )
      {
        voilationsWrapper voils = new voilationsWrapper(obj);
        listVoilationsSelected.add(voils);
      }   
    }*/  
    
    public pagereference mergeVoilations(){
    
         //findVoilations();
         listMergeVoilations = new List<TransAm_Violation__c>();
         for(voilationsWrapper obj : listVoilations){
             if(obj.selected){
                 listMergeVoilations.add(obj.voilationsSelected);
             }
         }
         for(TransAm_Violation__c obj : listMergeVoilations){
             obj.TransAm_Application__c=MasterApp;
         } 
         update listMergeVoilations;  
         return new PageReference('/apex/AccidentsPage');       
    }
}