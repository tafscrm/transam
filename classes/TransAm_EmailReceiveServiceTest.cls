@isTest(seeAllData=false)
public class TransAm_EmailReceiveServiceTest {

    @testsetup
    static void setup() {
        List<Account> accountList = TransAm_Test_DataUtility.createAccounts(1, True);
        List<Contact> contactList = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'Applicant', True);
        List<TransAm_Application__c> applicationList = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', contactList[0], False);
        insert applicationList;
      }
        
    public static testmethod void testEmailReceive() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        string appName = [select Id, Name from TransAm_Application__c][0].Name; 
        system.debug(LoggingLevel.ERROR, 'test appName ' + appName);
        
        email.subject = 'Equest';
        email.fromname = 'name';
        email.plainTextBody = 'COMPANY:0485535: NAME:  Trans Am Trucking\nACCOUNT: 471922\nSUB: 4888\nORDERED BY: Shawna Simpson\nFIRST NAME: Liam\nLAST NAME: Adams\nPO#: ' + appName.substring(3,appName.length()) + '\nSERVICE FROM: Chicago/IL\nDEPARTURE DATE: 0601\nDEPARTURE TIME:  1100AM\nSERVICE TO: Kansas City/MO\nPRICE: 123.00\nCONFIRMATION: 12345678\nSCHEDULE:     Re 11:00a 0,10:25 09:25p  BTW  1205   29 JL   DSM 00:20\nCOMMENTS: ';
        system.debug(LoggingLevel.ERROR, email.plainTextBody);
        env.fromaddress = 'commercial.sales@greyhound.com';
        TransAm_EmailReceiveService service = new TransAm_EmailReceiveService();
        service.handleInboundEmail(email, env);              
    }
}