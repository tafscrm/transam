/******************************************************************************************
* Create By       :     Pankaj Singh
* Create Date     :     03/09/2017
* Description     :     Utility class for Application round-robin assignment
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Pankaj SIngh                      03/09/2017         Initial version.
*****************************************************************************************/

public class TransAm_ApplicationAssignmentHelper{
    
    private static final string RECRUITER_PROFILE = 'Recruiter';

    public static void assignApplication(list<TransAm_Application__c> applicationList){
    
        list<Id> availableRecruiterList = new list<Id>();
        list<Id> assignedRecruiterList = new list<Id>();
        List<User> previousRecruiterList = new list<User>();
        map<string, TransAm_Application__c> ssnApplicationMap = new map<string, TransAm_Application__c>();
        map<string, TransAm_Application__c> emailMap = new map<string, TransAm_Application__c>();
        map<string, TransAm_Application__c> cellphoneMap = new map<string, TransAm_Application__c>();
        map<string, TransAm_Application__c> namePhoneMap = new map<string, TransAm_Application__c>();
        map<Id,User> userMap = new map<Id,User>();
        set<string> ssnSet = new set<string>();
        set<string> emailSet = new set<string>();
        set<string> phoneSet = new set<string>();
        set<string> cellPhoneSet = new set<string>();
        Integer count=0;
        Integer applicationCount = 0;
        try{                              
            for(TransAm_Application__c app : applicationList){
                if(String.IsNotBlank(app.TransAm_SSN__c))
                    ssnSet.add(app.TransAm_SSN__c);
                if(String.IsNotBlank(app.TransAm_Email__c))
                    emailSet.add(app.TransAm_Email__c);
                if(app.TransAm_First_Name__c!=null){ 
                    if(String.IsNotBlank(app.TransAm_Phone__c))             
                        phoneSet.add(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c);
                    if(String.IsNotBlank(app.TransAm_Cell_Number__c))
                        cellPhoneSet.add(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c);
                }
            }          
            System.debug('****ssnSet****'+ssnSet+'**emailSet***'+emailSet+'***cellPhoneSet***'+cellPhoneSet+'***phoneSet***'+phoneSet);     
            for(User userObj : [SELECT ID,Profile.Name,TransAm_Last_Application_Assigned__c,TransAm_Exclude_from_Assignment__c FROM User WHERE 
                                Profile.Name =:RECRUITER_PROFILE AND isActive = TRUE AND TransAm_Exclude_from_Assignment__c = FALSE]){            
                    userMap.put(userObj.Id,userObj);
                    if(!userObj.TransAm_Last_Application_Assigned__c){ 
                        availableRecruiterList.add(userObj.Id);    
                    }   
            }
            system.debug('availableRecruiterList::'+availableRecruiterList);
            for(User userObj : userMap.values()){
                if(userObj.TransAm_Last_Application_Assigned__c){
                    availableRecruiterList.add(userObj.Id);
                    userObj.TransAm_Last_Application_Assigned__c = false;
                    previousRecruiterList.add(userObj);    
                }        
            }
            system.debug('availableRecruiterList:: 2'+availableRecruiterList);
            if(!previousRecruiterList.isEmpty()){
                update previousRecruiterList;
            }                       
            for(TransAm_Application__c app : [SELECT ID,TransAm_SSN__c,TransAm_Cell_Number__c,TransAm_Last_Name__c,TransAm_First_Name__c,TransAm_Email__c,TransAm_MobilePhone__c,
                                              TransAm_Phone__c,OwnerId FROm TransAm_Application__c WHERE TransAm_SSN__c IN : ssnSet OR TransAm_firstnamelastnamecellphone__c IN : 
                                              cellPhoneSet OR TransAm_firstnamelastnameemail__c IN: emailSet OR TransAm_FirstNameLastNamePhone__c IN : phoneSet]){  
                System.debug('*****'+app);
                if(app.TransAm_SSN__c!=null){
                    ssnApplicationMap.put(app.TransAm_SSN__c,app);
                }
                if(app.TransAm_Email__c!=null){
                    emailMap.put(app.TransAm_Email__c,app);
                }
                if(app.TransAm_First_Name__c!=null){
                    if(app.TransAm_Cell_Number__c!=null){
                        cellphoneMap.put(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c,app);
                    }
                    if(app.TransAm_Phone__c!=null){
                        namePhoneMap.put(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c,app);
                    }                   
                }    
            }
            System.debug('****ssnApplicationMap****'+ssnApplicationMap+'**emailMap***'+emailMap+'***cellphoneMap***'+cellphoneMap+'***namePhoneMap***'+namePhoneMap);
            for(TransAm_Application__c app : applicationList){                  
                applicationCount++;   
                if(ssnApplicationMap.containsKey(app.TransAm_SSN__c) && userMap.containsKey((Id)ssnApplicationMap.get(app.TransAm_SSN__c).get('OwnerId'))){                 
                    if((Id)ssnApplicationMap.get(app.TransAm_SSN__c).get('OwnerId')!=null && !userMap.get((Id)ssnApplicationMap.get(app.TransAm_SSN__c).get('OwnerId')).TransAm_Exclude_from_Assignment__c){
                        app.OwnerId = (Id)ssnApplicationMap.get(app.TransAm_SSN__c).get('OwnerId');
                    }
                }                       
                else if(emailMap.containsKey(app.TransAm_Email__c) && userMap.containsKey((Id)emailMap.get(app.TransAm_Email__c).get('OwnerId'))){
                    if((Id)emailMap.get(app.TransAm_Email__c).get('OwnerId')!=null && !userMap.get((Id)emailMap.get(app.TransAm_Email__c).get('OwnerId')).TransAm_Exclude_from_Assignment__c){
                        app.OwnerId = (Id)emailMap.get(app.TransAm_Email__c).get('OwnerId');
                    }
                }                              
                else if(namePhoneMap.containsKey(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c) && userMap.containsKey((Id)namePhoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c).get('OwnerId'))){
                    if((Id)namePhoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c).get('OwnerId')!=null && !userMap.get((Id)namePhoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c).get('OwnerId')).TransAm_Exclude_from_Assignment__c){
                        app.OwnerId = (Id)namePhoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c).get('OwnerId');
                    }
                }
                else if(cellphoneMap.containsKey(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c) && userMap.containsKey((Id)cellphoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c).get('OwnerId'))){
                    if((Id)cellphoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c).get('OwnerId')!=null && !userMap.get((Id)cellphoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c).get('OwnerId')).TransAm_Exclude_from_Assignment__c){
                        app.OwnerId = (Id)cellphoneMap.get(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c).get('OwnerId');
                    }
                }  
                else{
                    if(availableRecruiterList.size()>0){ 
                        system.debug('printing count:::'+count); 
                        system.debug('printing owner id:::'+availableRecruiterList.get(count));  
                        if(!userMap.get(availableRecruiterList.get(count)).TransAm_Exclude_from_Assignment__c)                                      
                            app.OwnerId = availableRecruiterList.get(count);
                        if(app.TransAm_SSN__c!=null){
                            ssnApplicationMap.put(app.TransAm_SSN__c,app);
                        }
                        if(app.TransAm_Cell_Number__c!=null){
                            cellphoneMap.put(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Cell_Number__c,app);
                        }
                        if(app.TransAm_Phone__c!=null){
                            namePhoneMap.put(app.TransAm_First_Name__c + app.TransAm_Last_Name__c + app.TransAm_Phone__c,app);
                        }
                        if(app.TransAm_Email__c!=null){
                            emailMap.put(app.TransAm_Email__c,app);
                        } 
                    }
                }    
                if(applicationCount==applicationList.size()){
                     system.debug('inside count if::'+applicationCount);
                     //User recruiter =  userMap.get(availableRecruiterList.get(count)); 
                     previousRecruiterList.clear();
                     User recruiter =  userMap.get(app.ownerId); 
                     recruiter.TransAm_Last_Application_Assigned__c =true;
                     previousRecruiterList.add(recruiter); 
                     system.debug('inside recruiter if::'+recruiter);
                     
                }
                count++;                    
                if(count >= availableRecruiterList.size()){
                     count = 0;
                }  
                System.debug('****after assigning owner*'+app.OwnerId);             
            }
            if(previousRecruiterList!=null && !previousRecruiterList.isEmpty())
                update previousRecruiterList;
                  
        }catch(Exception excep){
            TransAm_Error_Log__c el = new TransAm_Error_Log__c();
            el.TransAm_Class_Name__c = 'TransAm_ApplicationAssignmentHelper';
            el.TransAm_Method_Name__c = 'assignApplication';
            el.TransAm_Module_Name__c = 'Round Robin Assignment';
            el.TransAm_Line_Number__c = excep.getLineNumber();
            el.TransAm_Exception_Message__c = excep.getMessage();
            insert el;
        }
    }
}