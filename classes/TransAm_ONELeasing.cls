/******************************************************************************************
* Create By    :     Karthik Gulla
* Create Date  :     03/10/2017
* Description  :     Webservice class to get RecordTypes
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                      03/10/2017         Initial version.
*****************************************************************************************/
global class TransAm_ONELeasing{
    Webservice static String getRecordTypeAndFormURL(String recTypeName, Id appId, Id leaseId) {
        String appName = '';
        String driverName;
        RecordType recType = [SELECT Id FROM RecordType WHERE SObjectType = 'TransAm_ONE_Leasing__c' AND Name = :recTypeName];

        Map<String,String> mapCompIdMappings = new Map<String,String>();
        String html = '';
        String url = '/' + TransAm_ONE_Leasing__c.SObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1&RecordType='+recType.Id;

        PageReference p = new PageReference(url);
        if(!test.isRunningTest()){
             html = p.getContent().toString();
        }else{
            html = '<td class="labelCol requiredInput"><label for="00NK0000000ap5l"><span class="requiredMark">*</span>Type</label></td>';
        }

        Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            mapCompIdMappings.put(label.removeStart('<span class="assistiveText">*</span>'), id);
        }
        
        TransAm_Application__c transamApp;
        if(appId != null && String.isNotBlank(appId)){
            transamApp = [SELECT Name, TransAm_Applicant__r.Name FROM TransAm_Application__c WHERE Id = :appId];
        }

        if(transamApp != null){
            appName = transamApp.Name;
        }
        
        if(recTypeName == 'Lease Agreement' 
            && mapCompIdMappings.containsKey('Application') 
            && mapCompIdMappings.containsKey('IC Agreement')
            && leaseId != null){            
            List<TransAm_ONE_Leasing__c> lstExistLA = [SELECT Id, Name FROM TransAm_ONE_Leasing__c WHERE RecordTypeId = :recType.Id AND TransAm_IC_Agreement__c = :leaseId];
            if(lstExistLA.size() > 0)
                return 'LeaseAlreadyExists';
            else{
                String leaseName = [SELECT Id, Name FROM TransAm_ONE_Leasing__c WHERE Id = :leaseId].Name;
                return url+'&'+mapCompIdMappings.get('Application')+'='+appName+'&'+mapCompIdMappings.get('IC Agreement')+'='+leaseName;
            }
        }
        else if(recTypeName == 'IC Agreement')
            return url+'&'+mapCompIdMappings.get('Application')+'='+appName;
        else
            return url+'&'+mapCompIdMappings.get('Application')+'='+appName;
    }
}
