/* CLass Name   : TransAm_JobHistory_Test
 * Description  : Test class for Job History
 * Created By   : Pankaj Singh
 * Created On   : 22-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                22-Feb-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_JobHistory_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);
        
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Arun';
        user_int.LastName = 'Tyagi';
        user_int.Username = 'xyzrtyu@abc.com';
        user_int.Email   = 'xyz@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
        
    }

    /************************************************************************************
    * Method       :    testJobHistory
    * Description  :    Test Method to create Job history
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testJobHistory() {
        List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);  
        List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
                
        User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Arun' LIMIT 1];
        TransAm_Employer__c empName= [SELECT NAME FROM TransAm_Employer__c LIMIT 1];
        Test.startTest();
        List<TransAm_Job_History__c> jobList = TransAm_Test_DataUtility.createJobHistory(1, lstApplications[0], lstEmployers[0], false);
        for(TransAm_Job_History__c jobHis: jobList){    
            jobHis.TransAm_Other_Employer__c=String.valueOf(empName.Name);
        }
        System.runAs(user_int){ 
            INSERT jobList;
            
        Test.stopTest();
    	}
    }
    /************************************************************************************
    * Method       :    testJobHistoryException
    * Description  :    Test Method to create Job history
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
     public static testmethod void testJobHistoryException() {
        User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Arun' LIMIT 1];
             System.runAs(user_int){  
            List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, true);  
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
            List<TransAm_Job_History__c> jobList = TransAm_Test_DataUtility.createJobHistory(2, lstApplications[0], lstEmployers[0], false);
           
            Test.startTest();
            
                TransAm_JobHistoryHelper.populateEmployerOnJobHistory(null);
            Test.stopTest();
         }
    }
    public static testmethod void testJobHistoryException2() {
        User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Arun' LIMIT 1];
             System.runAs(user_int){  
            List<TransAm_Employer__c> lstEmployers = TransAm_Test_DataUtility.createEmployers(1, false);  
            List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
            List<TransAm_Job_History__c> jobList = TransAm_Test_DataUtility.createJobHistory(1, lstApplications[0], lstEmployers[0], false);
            Test.startTest();
            
                TransAm_JobHistoryHelper.populateApplicationOnJobHistory(null);
            Test.stopTest();
         }
    }
}