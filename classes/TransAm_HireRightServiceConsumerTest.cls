/* CLass Name   : TransAm_HireRightServiceConsumerTest
 * Description  : Test class for TransAm_HireRightServiceConsumer
 * Created By   : Monalisa Das
 * Created On   : 09-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                09-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
 @isTest(seeAllData=false)
public class TransAm_HireRightServiceConsumerTest {

@testVisible static List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
@testVisible static List<TransAm_HireRight_Orders__c> lstHROrders = new List<TransAm_HireRight_Orders__c>();

 /************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
@testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], false);
        lstApplications[0].TransAm_First_Name__c = 'TestFNeh';
        insert lstApplications;        
        System.debug('lstApplications ::'+lstApplications);
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Test';
        user_int.LastName = 'User';
        user_int.Username = 'testHRuser@abc.com';
        user_int.Email   = 'testuser@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
        
	// List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
	TransAm_HireRightHandler.prohibitAfterInsertTrigger = true;
        lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'MVR Standard' ,True); 
    system.debug(LoggingLevel.ERROR, 'app id ' + lstApplications[0].Id);
    system.debug(LoggingLevel.ERROR,'app name ' + lstApplications[0].Name);
	TransAm_HireRightHandler.prohibitAfterInsertTrigger = false;
	TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
	TransAm_HireRight_Orders__c order = lstHROrders.get(0);
	order.TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3';
	update order;
	TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
	
	
    }
    
    
    public static testmethod void sendRawRequestTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
          
	    TransAm_HRServiceMock fakeResponse2 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:CreateResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs"><hr_objs:Result><hr_objs:Id>116551559</hr_objs:Id><hr_objs:RegId>HE-051017-JR3KT</hr_objs:RegId><hr_objs:Status>Ordered</hr_objs:Status><hr_objs:Services><hr_objs:Service count="1" id="MVR Standard" name="MVR Standard"/></hr_objs:Services></hr_objs:Result></hr_objs:CreateResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 null);
	    Test.setMock(HttpCalloutMock.class, fakeResponse2);
	    TransAm_Application__c app = [select id, Name, TransAm_Application_Restriction__c ,
                                    TransAm_City__c ,
                                    TransAm_EBE_Entry_ID__c,
                                    TransAm_State__c,
                                    TransAm_Status__c,
                                    TransAm_Original_Recruiter_Name__c,
                                    TransAm_Recruiter_Name__c,
                                    TransAm_SSN__c,
                                    TransAm_Email__c,
                                    TransAm_Phone__c,
                                    TransAm_Primary_Application__c,
                                    TransAm_Applicant__c,
                                    TransAm_ZIP__c,
                                    TransAm_Date_Of_Birth__c,
                                    TransAm_Last_Name__c,
				    TransAm_First_Name__c,
                                    TransAm_Cell_Number__c,
				    TransAm_Address__c,
				    TransAm_Expiration_Date__c,
				    TransAm_Driver_Type__c,
				    TransAm_Driver_Sub_Type__c,
				    TransAm_Type_Of_Driver__c,
				    TransAm_Position_Applying_For__c,
				    TransAm_Orientation_Location__c,
				    TransAm_Emergency_First_Name__c,
				    TransAm_Emergency_Last_Name__c,
				    TransAm_Emergency_Phone__c,
				    Emergency_Contact_1_Alternate_Phone__c,
				    Emergency_Contact_2_First_Name__c,
				    Emergency_Contact_2_Last_Name__c,
				    Emergency_Contact_2_Phone__c,
				    Emergency_Contact_2_Alternate_Phone__c,
				    TransAm_LicenseNo__c,
				    TransAm_AddressLine2__c,
				    TransAm_License_State__c from TransAm_Application__c
				    where TransAm_First_Name__c	       = 'TestFNeh' LIMIT 1];
	    
	    TransAm_HireRight_Orders__c order = [select id,Name,TransAm_Application__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	    
	    String soapRequest = TransAm_HRRequestService.getCreateRequest(app,order);   
        TransAm_HireRightServiceConsumer.sendRawRequest(soapRequest,order.Id);
            
           
        }
	 Test.stopTest();
    }
   
   
   public static testmethod void sendRawRequestExceptionTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
          
	    TransAm_HRServiceMock fakeResponse2 = new TransAm_HRServiceMock(500,
						'FAILURE',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><SOAP-ENV:Fault xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><faultcode>SOAP-ENV:Server</faultcode><faultstring>Invalid Data:*Instant Services - Instant Services: Driver license number</faultstring><detail><fns:fault xmlns:fns="urn:fault.enterprise.soap.hireright.com" xmlns:java="java" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="fns:ApiFault"><fns:exceptionCode xmlns:fns="urn:fault.enterprise.soap.hireright.com">INTERNAL_ERROR</fns:exceptionCode><fns:exceptionMessage xmlns:fns="urn:fault.enterprise.soap.hireright.com">Invalid Data:*Instant Services - Instant Services: Driver license number</fns:exceptionMessage><fns:logDataExchangeId xmlns:fns="urn:fault.enterprise.soap.hireright.com">4956426339846</fns:logDataExchangeId></fns:fault></detail></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 null);
	    Test.setMock(HttpCalloutMock.class, fakeResponse2);
	    TransAm_Application__c app = [select id, Name, TransAm_Application_Restriction__c ,
                                    TransAm_City__c ,
                                    TransAm_EBE_Entry_ID__c,
                                    TransAm_State__c,
                                    TransAm_Status__c,
                                    TransAm_Original_Recruiter_Name__c,
                                    TransAm_Recruiter_Name__c,
                                    TransAm_SSN__c,
                                    TransAm_Email__c,
                                    TransAm_Phone__c,
                                    TransAm_Primary_Application__c,
                                    TransAm_Applicant__c,
                                    TransAm_ZIP__c,
                                    TransAm_Date_Of_Birth__c,
                                    TransAm_Last_Name__c,
				    TransAm_First_Name__c,
                                    TransAm_Cell_Number__c,
				    TransAm_Address__c,
				    TransAm_Expiration_Date__c,
				    TransAm_Driver_Type__c,
				    TransAm_Driver_Sub_Type__c,
				    TransAm_Type_Of_Driver__c,
				    TransAm_Position_Applying_For__c,
				    TransAm_Orientation_Location__c,
				    TransAm_Emergency_First_Name__c,
				    TransAm_Emergency_Last_Name__c,
				    TransAm_Emergency_Phone__c,
				    Emergency_Contact_1_Alternate_Phone__c,
				    Emergency_Contact_2_First_Name__c,
				    Emergency_Contact_2_Last_Name__c,
				    Emergency_Contact_2_Phone__c,
				    Emergency_Contact_2_Alternate_Phone__c,
				    TransAm_LicenseNo__c,
				    TransAm_AddressLine2__c,
				    TransAm_License_State__c from TransAm_Application__c
				    where TransAm_First_Name__c	       = 'TestFNeh' LIMIT 1];
	    
	    TransAm_HireRight_Orders__c order = [select id,Name,TransAm_Application__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	    
	    String soapRequest = TransAm_HRRequestService.getCreateRequest(app,order);
            TransAm_HireRightServiceConsumer.sendRawRequest(soapRequest,order.Id);
        }
	 Test.stopTest();
    }
    /*public static testmethod void getWebLinkTest(){
    	User curUser = new user(Id=userinfo.getUserId()); 
        system.runAs(curUser){
            String req = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
            TransAm_HireRightServiceConsumer.getWeblink(req);
   	 	}
    }*/
    
   /* public static testmethod void sendRawRequestForWebLinkTest() {
        
       User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
          Map<String, String> headers = new map<String, String>();
            headers.put('Location', 'sforce');
	     TransAm_HRServiceMock fakeResponse3 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 headers);
	    Test.setMock(HttpCalloutMock.class, fakeResponse3);
	   
	    TransAm_HireRight_Orders__c order = [SELECT TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    //TransAm_HireRight_Orders__c order = [select id,Name, TransAm_Application__c, TransAm_Application__r.Name, TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	     String soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest('TN-050917-JH8M3');
	    TransAm_HireRightServiceConsumer.sendRawRequestForWebLink(soapRequest,order.Id);
            
           
        }
	 Test.stopTest();
    }*/

    
    
   
    
    /*public static testmethod void getAttachmentTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
	Map<String,String> responseHeaders = new Map<String,String>();
	responseHeaders.put('Location','https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A');
        System.runAs(user_int){ 
          
	     TransAm_HRServiceMock fakeResponse4 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 responseHeaders);
	    Test.setMock(HttpCalloutMock.class, fakeResponse4);
	    TransAm_HireRightServiceConsumer.getAttachment('https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A');
            
           
        }
	 Test.stopTest();
    }*/
    
    
    /*public static testmethod void downloadAttachmentTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
	Map<String,String> responseHeaders = new Map<String,String>();
	responseHeaders.put('Location','https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A');
        System.runAs(user_int){ 
          
	     TransAm_HRServiceMock fakeResponse5 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 responseHeaders);
	    Test.setMock(HttpCalloutMock.class, fakeResponse5);
	    TransAm_Application__c app = [select id,name from TransAm_Application__c
				    where TransAm_First_Name__c	       = 'TestFNeh' LIMIT 1];
	   TransAm_HireRight_Orders__c order = [select id,Name,TransAm_Application__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c,TransAm_HireRight_Reports__c,TransAm_Recruitment_Documents__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
	    
	    TransAm_HireRightServiceConsumer.downloadAttachment(app.Id,'https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A',order);
            
           
        }
	 Test.stopTest();
    }*/
    
   
}