/* Class Name   : TransAm_AttachmentHandler
 * Description  : Handler class for the Attachment trigger
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_AttachmentHandler{

    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    public static boolean prohibitBeforeInsertTrigger= false;
    public static boolean prohibitAfterDeleteTrigger= false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<Attachment> attachmentList){
        
        if(prohibitAfterInsertTrigger){
            return;
        } 
        TransAm_AttachmentHelper.transferReceivedFaxes(attachmentList); 
               
        
        TransAm_AttachmentHelper.updateRecruitmentDocumentExportFlag(attachmentList);
        TransAm_AttachmentHelper.updateOneLeaseDocumentExportFlag(attachmentList);
        //TransAm_AttachmentHelper.emailAttachmentToApplicationOwner(attachmentList);   
        TransAm_AttachmentHelper.triggerW4ToPayrollDept(attachmentList);
        TransAm_AttachmentHelper.countAttachments(attachmentList);
		TransAm_AttachmentHelper.incrementCountOneLease(attachmentList);//NHALLIYAL:Added method for user story 622364
        TransAm_AttachmentHelper.transferDoctoRDFromSentFax(attachmentList);
		prohibitAfterInsertTrigger = true;
    }
    public static void afterUpdateHandler(list<Attachment> attachmentList){
    
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        
        TransAm_AttachmentHelper.updateRecruitmentDocumentExportFlag(attachmentList);
        prohibitAfterUpdateTrigger = true;
    }
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<Attachment> attachmentList){
         
         system.debug('calling transfer attchment'+ prohibitBeforeInsertTrigger);
         if(prohibitBeforeInsertTrigger){
             return;
         }
         system.debug('calling transfer attchment'+ prohibitBeforeInsertTrigger);
         prohibitBeforeInsertTrigger = true; 
         TransAm_AttachmentHelper.transferAttachements(attachmentList);

         TransAm_AttachmentHelper.transferLeasingAttachments(attachmentList);

         //TransAm_AttachmentHelper.renameAttachments(attachmentList);                  
         TransAm_AttachmentHelper.CheckTheExtensionOfAttachment(attachmentList);            
    }
    
    /****************************************************************************************
    * Created By      :  Neha Jain
    * Create Date     :  03-Aug-2017
    * Description     :  handler method for after delete event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterDeleteHandler(list<Attachment> attachmentList){
         
         if(prohibitAfterDeleteTrigger){
             return;
         } 
         TransAm_AttachmentHelper.countAttachments(attachmentList);
         prohibitAfterDeleteTrigger = true;
    }
}