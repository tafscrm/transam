/******************************************************************************************
* Create By    :     Neha Jain
* Create Date  :     06/20/2017
* Description  :     Scheduler to execute batch class-- TransAm_ApplicationStatusNoContactBatch.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_ScheduleAppStatusNoContact implements Schedulable {   
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_ApplicationStatusNoContactBatch());
   }
}