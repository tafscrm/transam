/******************************************************************************************
* Class Name   :     TransAm_TaskHandler
* Created By   :     Ajay B R
* Create Date  :     08/16/2017
* Description  :     Task Handler class invokes the logics from Task_Helper that will be executed in Task Trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                         08/16/2017           Initial version.
*****************************************************************************************/
public class TransAm_TaskHandler{

    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;

    /*******************************************************************************************
   * Method        :   afterInsertHandler
   * Description   :   Purpose of this method is to call all after Insert methods from TransAm_TaskHelper class.
   * Parameter     :   List<Task>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterInsertHandler(list<Task> taskList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_TaskHelper.populateFollowUpDateOnApplication_Insert(taskList);
		TransAm_TaskHelper.populateContactOnTask_Insert(taskList);
        prohibitAfterInsertTrigger = true;
    }
    
    /*******************************************************************************************
   * Method        :   afterUpdateHandler
   * Description   :   Purpose of this method is to call all after update methods from TransAm_TaskHelper class.
   * Parameter     :   List<Task>, Map<ID, Task>
   * Return Type   :   Void
   ******************************************************************************************/
    public static void afterUpdateHandler(list<Task> taskList, Map<Id, Task> taskOldMap){     
       if(prohibitAfterUpdateTrigger ){
            return;
        }
        TransAm_TaskHelper.populateFollowUpDateOnApplication_Update(taskList, taskOldMap);
        prohibitAfterUpdateTrigger = true;
    }

}