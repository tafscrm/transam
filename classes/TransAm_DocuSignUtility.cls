/* Class Name   : TransAm_DocuSignUtility 
 * Description  : TUtility class for sending documents to contacts associated with leads using docusign
 * Created By   : Pankaj Singh
 * Created On   : 25-Jun-2016
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj SIngh                22-Jun-2016             Initial version.
 *
 *****************************************************************************************/
public with sharing class TransAm_DocuSignUtility {
    
    public string applicantId {get;set;}
    public String packetType { get; set; }
    private string receipientList = '';
    private decimal routingNo = 0;
    
    public TransAm_DocuSignUtility(){
        applicantId = ApexPages.currentPage().getParameters().get('Id');
    }
    public PageReference sendToDocuSign(){
    
        TransAm_Application__c applicationObj;
        applicationObj = [Select id,Owner.Name,TransAm_Driver_Type__c,TransAm_State__c,TransAm_Orientation_Location__c,TransAm_First_Name__c,Owner.Email,TransAm_Last_Name__c,TransAm_Email__c from TransAm_Application__c WHERE ID =: applicantId];        
  
        string templateId = '';
        string RC = '';
        string RSL='';
        string RSRO='';
        string RROS='';
        string CCRM='';
        string CCTM='';
        string CCNM='';
        string CRCL=''; 
        string CRL='';
        string OCO='';
        string DST='';
        string LA='';
        string CEM='';
        string CES='';
        string STB='';
        string SSB='';
        string SES='';
        string SEM='';string SRS='';string SCS ='';string RES=''; 
        Organization orgDetails = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        User activeUser = [SELECT ID, FirstName, LastName, Email FROM User Where Id =:UserInfo.getUserId() LIMIT 1];
        Map<String,TransAm_DocuSign_Template_Mappings__mdt> mapDocusignTemplateMappings = new Map<String,TransAm_DocuSign_Template_Mappings__mdt>();
        List<TransAm_DocuSign_Template_Mappings__mdt> lstDSTemplateMappings = [SELECT MasterLabel, TransAm_Docusign_TemplateId__c, TransAm_Is_Sandbox__c FROM TransAm_DocuSign_Template_Mappings__mdt WHERE TransAm_Is_Sandbox__c =:orgDetails.IsSandbox];
        
        for(TransAm_DocuSign_Template_Mappings__mdt dsMappings:lstDSTemplateMappings){
            String strMasterLabel = dsMappings.MasterLabel;
            if(orgDetails.IsSandbox)
                strMasterLabel = strMasterLabel.replace('_Demo','');
            mapDocusignTemplateMappings.put(strMasterLabel, dsMappings);       
        }
        
        receipientList = receipientList +'Email~'+applicationObj.TransAm_Email__c+';FirstName~'+applicationObj.TransAm_First_Name__c+';LastName~'+applicationObj.TransAm_Last_Name__c+';Role~Signer1;RoutingOrder~1;';
        receipientList = receipientList +','+'Email~'+activeUser.Email+';FirstName~'+activeUser.FirstName+';LastName~'+activeUser.LastName+';Role~Signer2;RoutingOrder~2';
        
        if(mapDocusignTemplateMappings.containsKey('OwnerOperator') && applicationObj.TransAm_Driver_Type__c == 'Owner'){ 
            templateId = mapDocusignTemplateMappings.get('OwnerOperator').TransAm_Docusign_TemplateId__c;
        }else if(mapDocusignTemplateMappings.containsKey('Kansas') && applicationObj.TransAm_Driver_Type__c == 'Company' && applicationObj.TransAm_Orientation_Location__c == 'Olathe, KS' && applicationObj.TransAm_State__c == 'KS'){
            templateId = mapDocusignTemplateMappings.get('Kansas').TransAm_Docusign_TemplateId__c;
        }else if(mapDocusignTemplateMappings.containsKey('Texas') && applicationObj.TransAm_Driver_Type__c == 'Company' && applicationObj.TransAm_Orientation_Location__c == 'Rockwall, TX'){
            templateId = mapDocusignTemplateMappings.get('Texas').TransAm_Docusign_TemplateId__c;
        }else if(mapDocusignTemplateMappings.containsKey('NonKansas') && applicationObj.TransAm_Driver_Type__c == 'Company' && applicationObj.TransAm_Orientation_Location__c == 'Olathe, KS' && applicationObj.TransAm_State__c != 'KS'){
            templateId = mapDocusignTemplateMappings.get('NonKansas').TransAm_Docusign_TemplateId__c;
        }
        //Adding Notes & Attachments 
        LA='0'; 
        //Custom Recipient List 
        /*Id userId = UserInfo.getUserId();
        User activeUser = new User();
        if(userId!=null){
            activeUser = [Select Id,Email,FirstName,LastName From User where Id = : userId limit 1];
        }*/
        
        CRL= receipientList + 'LoadDefaultContacts~0';
        
        // Show Email Subject (default in config) 
        SES = '1'; //Ex: '1' 
        // Show Email Message (default in config) 
        SEM = '1'; //Ex: '1' 
        // Show Tag Button (default in config) 
        STB = '1'; //Ex: '1' 
        // Show Chatter (default in config) 
        SCS = '0'; //Ex: '1' 
        OCO = ''; 
        PageReference pageRef = null;
        pageRef = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID='+applicantId+'&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+templateId+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES);
        pageRef.setRedirect(true);    
        return pageRef;
   }
}