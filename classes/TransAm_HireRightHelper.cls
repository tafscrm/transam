/* Class Name   : TransAm_HireRightHelper
* Description  : Helper class for the Accident trigger
* Created By   : Monalisa
* Created On   : 9-Apr-2017  
*
*  Modification Log :
*  --------------------------------------------------------------------------------------
*  * Developer                    Date                    Description
*  * ------------------------------------------------------------------------------------                 
*  * Monalisa Das               9-Apr-2017               Initial version.
*
*****************************************************************************************/
public class TransAm_HireRightHelper{
    
    /****************************************************************************************
* Created By      :  Monalisa
* Create Date     :  9-Apr-2017
* Description     :  Invoke Webservice and update Hire Right order
* Modification Log:  Initial version.
***************************************************************************************/
    @future(callout=true)
    public static void populateOrderIdOnHireRightOrders(Id[] orderIds){
        //Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        //if(UserInfo.getProfileId()==profileId){      
        //}
        try{
            for(Id orderId : orderIds) {
                TransAm_HireRight_Orders__c order = getOrder(orderId);
                TransAm_Application__c application = getApplication(order);
                String soapRequest = TransAm_HRRequestService.getCreateRequest(application,order);
                System.debug(soapRequest);
                System.debug('Order id is ' + order.Id);
                TransAm_HireRightServiceConsumer.sendRawRequest(soapRequest, order.Id);
            }
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightHelper',TransAm_Method_Name__c = 'populateOrderIdOnHireRightOrders',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        
    }
    @testVisible
    private static TransAm_HireRight_Orders__c getOrder(Id orderId) {
        TransAm_HireRight_Orders__c order = new TransAm_HireRight_Orders__c();
        try{
            order = [SELECT TransAm_Application__c, TransAm_Report_Type__c, Name from TransAm_HireRight_Orders__c WHERE ID = :orderId];
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightHelper',TransAm_Method_Name__c = 'getOrder',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        return order;
    }
    
    /****************************************************************************************
* Created By      :  Monalisa
* Create Date     :  9-Apr-2017
* Description     :  Invoke Webservice and update Hire Right order
* Modification Log:  Initial version.
***************************************************************************************/
    /*public static void populateWeblinkOnHireRightOrders(list<TransAm_HireRight_Orders__c> hireRightOrdersList){
        try{
            for(TransAm_HireRight_Orders__c order : hireRightOrdersList) {
                //Database.insert(order);
                if(String.isEmpty(order.TransAm_HireRight_Reports__c)){
                    System.debug('HR order id ' + order.TransAm_Hireright_Order_ID__c);
                    String soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest(order.TransAm_Hireright_Order_ID__c);
					TransAm_HireRightServiceConsumer.sendRawRequestForWebLink(soapRequest, order.Id);
                }
            }
        }
        catch(Exception e)
        {
            throw e;
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightHelper',TransAm_Method_Name__c = 'populateWeblinkOnHireRightOrders',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        
    }*/
    @testVisible
    private static TransAm_Application__c getApplication(TransAm_HireRight_Orders__c order) {
        
        TransAm_Application__c app = new TransAm_Application__c();
        try{
            Id appId = order.TransAm_Application__c;
            app = [SELECT Name,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Middle_Name__c,TransAm_Zip__c,TransAm_City__c,								TransAm_State__c,TransAm_Address__c, TransAm_AddressLine2__c,TransAm_Phone__c,TransAm_Email__c,TransAm_Cell_Number__c, TransAm_SSN__c,TransAm_Date_Of_Birth__c,TransAm_Gender__c, TransAm_License_State__c, 							 TransAm_LicenseNo__c FROM  TransAm_Application__c WHERE ID = :appId];
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HireRightHelper',TransAm_Method_Name__c = 'getApplication',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        return app;
    }
    
    
}