/* Class Name   : TransAm_MisdemeanorHandler
 * Description  : Handler class for the Misdemeanor trigger
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_MisdemeanorHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Misdemeanor__c> misdemeanorList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_MisdemeanorHelper.populateApplicationOnMisdemeanor(misdemeanorList);
        prohibitBeforeInsertTrigger = true;
    }

}