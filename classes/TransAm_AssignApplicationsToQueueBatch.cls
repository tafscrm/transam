/******************************************************************************************
* Class Name   :     TransAm_AssignApplicationsToQueueBatch
* Created By   :     Ajay B R
* Create Date  :     06/16/2017
* Description  :     Batch Class to assign Applications in "Recruiter Review" status to Driver Recruitement Queue 
*                    if it not changed by recruiters even after 72 hours.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                         06/16/2017           Initial version.
*  * Ajay B R                         07/03/2017           Changed the Logic to Business Days from 72 hrs
*  * Ajay B R                         07/14/2017           Modified the logic to make the batch run for all active statuses 
*                                                          and capture the age of application in each status.
*****************************************************************************************/
global class TransAm_AssignApplicationsToQueueBatch implements Database.Batchable<sObject> {
    
    //TransAm_Generic_Custom_Setting__c is a custom setting - It has the datasets for: 
        // Recruiter Queue Name
        // No of Business Hours
        // No of Business Days
        // Application status etc
    global Map<String, TransAm_Generic_Custom_Setting__c> genericCustomSettingsMap = TransAm_Generic_Custom_Setting__c.getAll();
    /*******************************************************************************************
    * Method        :   start
    * Description   :   This is the start() method of the batch class, used to execute a query 
    *                   to retrieve all applications in Recruiter Review status and return the query results to Execute method.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, TransAm_Status__c, OwnerId, TransAm_Last_Status_Change__c, TransAm_Follow_Up_Date__c, TransAm_Received_Date__c, CreatedDate FROM TransAm_Application__c where TransAm_Status__c Not IN (\'Application Complete\', \'DQ\', \'No Contact\', \'Incorrect Information\', \'No Show\', \'Not Interested\', \'Merged\') and transam_primary_application__c = true'); //exectue query
    }
   
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   This is the execute method of the batch class, used to process and assign the applications back to 
    *                   the Driver Recruiter Queue if they are in Recruiter Review Status for 72 hours or more.
    * Parameter     :   Database.BatchableContext, List<TransAm_Application__c>
    * Return Type   :   Void
    * Modification Log:
    * ----------------------------------------------------------------------------
    *  * Developer                          Date               Description
    *  * ----------------------------------------------------------------------------                 
    *  * Ajay B R                         06/16/2017           Initial version.
    *  * Ajay B R                         07/03/2017           Changed the Logic to Business Days from 72 hrs
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TransAm_Application__c> listOfApplications) {
        //check if the batch is not empty
        if(!listOfApplications.isEmpty() && genericCustomSettingsMap!=null){
            List<Group> recruiterQueueList;   //creating a list<Group> Var to store the Queue Details
            String queueName;    //String var to store the recruiter queue name
            Integer workingDaysToSkip;    //Integer var to store the ni of business days 
            Integer NoOfBusinessHoursADay;    //Integer var to store No of Business Hours a Day
            Decimal noOfWorkingDays;    //decimal var to calculate the noOfWorkingDays (businessDays)
            String applicationStatusAppReceived;   //String var to store the Application Received status
            String applicationStatusRecReview;   //String var to store the Recruiter Review status
            String appStatusNoShow; //String var to store the "No Show" status
            String appStatusNotInterested;  //String var to store the "Not Interested" status
            String appStatusSchOrientation;  //String var to store the "Not Interested" status
            Set<ID> updatedAppIdSet = new Set<ID>();    //set var to store all succesfully updated application Ids
            String taskRecTypeNonSchool; //string var to retrieve task record type name from custom setting
            Id taskRecTypeId; //var to store the taskRecTypeId
            String taskType;    //string var to store the task type
            List<Task> listTasksToBeClosed = new List<Task>();     //list var to store the list of open/pending past tasks to be closed
            
            if(genericCustomSettingsMap.containsKey('Recruiter Queue Name'))
                queueName = genericCustomSettingsMap.get('Recruiter Queue Name').TransAm_Message_String__c;   //assigning the queue name to a string var from the custom setting
            
            //check if the queueName is not blank
            if(String.isNotBlank(queueName)){
                //Query the Queue details and assign to the var
                recruiterQueueList = [select Id from Group where Name = :queueName and Type = 'Queue'];
            }
            
            //Creating a BusinessHour Variable
            list<BusinessHours> busHoursList = new list<BusinessHours>();
            //Query the latest BusinessHours configuration in the system
            busHoursList  = [SELECT Id FROM BusinessHours WHERE isActive = TRUE ORDER BY LastModifiedDate DESC LIMIT 1];
            
            //Id businessHoursId = [select Id from BusinessHours where isdefault = true ].id;
            
            //for every Application record retrieved in the batch            
            for(TransAm_Application__c applicationObj : listOfApplications){
                //********************Liam's Code Start Here
                integer elapsed;
                if(applicationObj.TransAm_Received_Date__c == null)
                    elapsed = getBusinessDays(applicationObj.CreatedDate.date(), busHoursList[0].Id);
                else
                    elapsed = getBusinessDays(applicationObj.TransAm_Received_Date__c, busHoursList[0].Id);
                system.debug('elapsed days ' + elapsed);
                applicationObj.TransAm_Application_Age__c = elapsed;
                //********************Liam's Code Ends Here
                
                //check if business hours is configured and the query has retreived it
                if(busHoursList.size()>0){
                    if(genericCustomSettingsMap.containsKey('Application Status No Show'))
                        appStatusNoShow = genericCustomSettingsMap.get('Application Status No Show').TransAm_Message_String__c;
                    if(genericCustomSettingsMap.containsKey('Application Status Not Interested'))
                        appStatusNotInterested = genericCustomSettingsMap.get('Application Status Not Interested').TransAm_Message_String__c;
                    if(String.isNotBlank(appStatusNoShow) && String.isNotBlank(appStatusNotInterested)){
                        if(applicationObj.TransAm_Status__c != appStatusNoShow && applicationObj.TransAm_Status__c != appStatusNotInterested){
                            if(applicationObj.TransAm_Last_Status_Change__c != null){
                                DateTime startTime = DateTime.newInstance(applicationObj.TransAm_Last_Status_Change__c.year(), applicationObj.TransAm_Last_Status_Change__c.month(), applicationObj.TransAm_Last_Status_Change__c.day());
                                DateTime endTime = DateTime.newInstance(System.now().year(), System.now().month(), System.now().day());
                                if(genericCustomSettingsMap.containsKey('No of Business Hours a Day'))
                                    NoOfBusinessHoursADay = Integer.valueOf(genericCustomSettingsMap.get('No of Business Hours a Day').TransAm_Message_String__c);
                                //check if NoOfBusinessHoursADay is not null
                                if(NoOfBusinessHoursADay != null){
                                    //calculate the businessDays based on sartTime and endtime
                                    applicationObj.TransAm_Current_Status_Age__c = BusinessHours.diff(busHoursList[0].Id, startTime, endTime)/(3600*NoOfBusinessHoursADay*1000);
                                }
                            }
                        }
                    }
                    
                    //assigning Recruiter Review app status from custom setting
                    if(genericCustomSettingsMap.containsKey('Application Recruiter Review Status'))
                        applicationStatusRecReview = genericCustomSettingsMap.get('Application Recruiter Review Status').TransAm_Message_String__c;   
                    if(genericCustomSettingsMap.containsKey('App Status Schedule Orientation'))
                        appStatusSchOrientation = genericCustomSettingsMap.get('App Status Schedule Orientation').TransAm_Message_String__c;   
                        
                    if((String.isNotBlank(applicationStatusRecReview) && applicationObj.TransAm_Status__c == applicationStatusRecReview) || (String.isNotBlank(appStatusSchOrientation) && applicationObj.TransAm_Status__c == appStatusSchOrientation)){
                        if(!recruiterQueueList.isEmpty() && applicationObj.TransAm_Current_Status_Age__c != null){
                            Date lastModifiedDateVar;
                            if(applicationObj.TransAm_Follow_Up_Date__c!= null)
                                lastModifiedDateVar = applicationObj.TransAm_Follow_Up_Date__c;
                            else if(applicationObj.TransAm_Last_Status_Change__c != null)
                                lastModifiedDateVar = applicationObj.TransAm_Last_Status_Change__c.date();
                            if(lastModifiedDateVar  != null && lastModifiedDateVar <= System.today()){
                                if(genericCustomSettingsMap.containsKey('No of Business Days To Skip'))
                                    workingDaysToSkip = Integer.valueOf(genericCustomSettingsMap.get('No of Business Days To Skip').TransAm_Message_String__c);    //assigning businessDays to skip from custom setting
                                if(genericCustomSettingsMap.containsKey('Queue Assignment Status Update'))
                                    applicationStatusAppReceived = genericCustomSettingsMap.get('Queue Assignment Status Update').TransAm_Message_String__c;   //assigning application status from custom setting
                                //check if the applicationStatusAppReceived is not blank and workingDaysToSkip is not null
                                if(workingDaysToSkip!=null && String.isNotBlank(applicationStatusAppReceived)){
                                    //check if the application is in recruiter review status for 3 Business Days or more based on the timestamp captured.
                                    if(applicationObj.TransAm_Current_Status_Age__c >= workingDaysToSkip){   
                                        applicationObj.ownerId = recruiterQueueList[0].Id; //if yes, assign the application to Driver Recruiter Queue (owner - reassigned)
                                        //Update the status back to Application Received - value fetched from the custom setting
                                        applicationObj.TransAm_Status__c = applicationStatusAppReceived;    
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            //update the batch (applications) to update the OwnerId and capture the results
            Database.SaveResult[] updateResults = Database.update(listOfApplications, false);
            
            //for every result captured
            for (Database.SaveResult sr : updateResults) {
                //if it is successfully updated, capture appIds to close open/pending current and past tasks
                if(sr.isSuccess()){
                    updatedAppIdSet.add(sr.getID());
                }
                //check if it is a failure
                else{
                    String errormsg;    //string var to capture error msg
                    for(Database.Error err : sr.getErrors()) {
                        errormsg += err.getMessage();   //get error message and capture in string var
                    }
                    //create an error log instance
                    TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                    el.TransAm_Class_Name__c = 'TransAm_AssignApplicationsToQueueBatch';
                    el.TransAm_Method_Name__c = 'exectue method - Update Batch DML Exception';
                    el.TransAm_Module_Name__c = 'Application';
                    el.TransAm_Exception_Message__c = 'Error Record ID: '+ sr.getID() + '; -- Error Message: ' + errormsg;
                    insert el;  //insert error log instance
                }
            }
            if(!updatedAppIdSet.isEmpty()){
                //if(genericCustomSettingsMap.containsKey('TaskRecordTypeNonSchool'))
                  //  taskRecTypeNonSchool = genericCustomSettingsMap.get('TaskRecordTypeNonSchool').TransAm_Message_String__c;
                //if(String.isNotBlank(taskRecTypeNonSchool))    
                  //  taskRecTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(taskRecTypeNonSchool).getRecordTypeId();
                if(genericCustomSettingsMap.containsKey('TaskTypeFollowUp'))
                    taskType = genericCustomSettingsMap.get('TaskTypeFollowUp').TransAm_Message_String__c;
                if(String.isNotBlank(taskType)){
                    listTasksToBeClosed = [select Id, Status from Task where status = 'Open' and whatId IN :updatedAppIdSet and type = :taskType and ActivityDate < today];
                    
                } 
            }
            if(!listTasksToBeClosed.isEmpty()){
                for(Task taskObj : listTasksToBeClosed){
                    taskObj.status = 'Completed';
                }
                
                try{
                    update listTasksToBeClosed;
                    System.debug('*** tasks after update '+listTasksToBeClosed);
                }Catch(System.DMLException dmlexcpn){
                    System.debug('*** Exception---> '+dmlexcpn.getMessage());
                    TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                    el.TransAm_Class_Name__c = 'TransAm_AssignApplicationsToQueueBatch';
                    el.TransAm_Method_Name__c = 'exectue method - Update Batch DML Exception - Occured when Closing Open tasks';
                    el.TransAm_Module_Name__c = 'Application';
                    el.TransAm_Exception_Message__c = dmlexcpn.getMessage();
                    el.TransAm_Line_Number__c = dmlexcpn.getLineNumber();
                    
                    insert el;
                }
            
            }
        }
    }   
    
    //********************Liam's Code Start Here
    private Integer getBusinessDays(Date dateReceived, Id businessHoursId) {
        if(dateReceived == null)
            return 365;
        Integer elapsed = 0;
        //Id businessHoursId = [select Id from BusinessHours where isdefault = true ].id;
        system.debug('Received date ' + dateReceived);
        Datetime receivedDate = Datetime.newInstance(dateReceived, Time.newInstance(12, 0 , 0, 0));
        system.debug('Received datetime ' + receivedDate);
        Datetime endpoint = Datetime.newInstance(Date.today(), Time.newInstance(12, 0 , 0, 0));
        system.debug('Endpoint ' + endpoint);
        if(receivedDate == endpoint)
            return 0;
        while(receivedDate < endpoint){
            receivedDate = receivedDate.addDays(1);
            if(BusinessHours.isWithin(businessHoursId, receivedDate)) elapsed++;
            //else receivedDate = BusinessHours.nextStartDate(businessHoursId, receivedDate);
        }
        return elapsed;
    }
    //********************Liam's Code Ends Here
    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Finish() method of the batch class,generally used to implement post commit logics.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
    //completed
    }
}