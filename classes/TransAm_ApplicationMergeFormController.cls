public class TransAm_ApplicationMergeFormController{
    
    public TransAm_Application__c MasterApp{get;set;}
    public list<appsWrapper> appsWrapperList{get;set;}    
    public List<TransAm_Application__c> ChildAppList;
    
    public TransAm_ApplicationMergeFormController(Apexpages.standardController controller){
    
        ChildAppList = new List<TransAm_Application__c>();
        MasterApp = new TransAm_Application__c();
        appsWrapperList = new list<appsWrapper>();
        
        MasterApp = (TransAm_Application__c)controller.getRecord();
        
        system.debug('##master app id is:'+MasterApp.id);
        
        ChildAppList = [select id, Name, TransAm_First_Name__c, TransAm_Last_Name__c, TransAm_Status__c, TransAm_Received_Date__c, TransAm_Score__c, TransAm_App_Source__c from TransAm_Application__c where TransAm_Master_Application__c =:MasterApp.id];        
        system.debug('##ChildAppList is:'+ChildAppList.size());
        
        for(TransAm_Application__c app : ChildAppList){
            appsWrapperList.add(new appsWrapper(app));
        }        
    }        
    
    public PageReference mergeFieldsPage(){
        integer i = 0;
        PageReference pr = new PageReference('/apex/TransAm_Application_Merge_Fields_Form');
        for(appsWrapper selectedWrapperApp : appsWrapperList){
            if(selectedWrapperApp.selected){
                i++;                
                pr.getParameters().put('Id'+i,selectedWrapperApp.childApp.id);
            }
        }
        if(i == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select at least one Application to Merge'));
            return null;
        } else if (i > 3){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You can select upto 3 Applications only to Merge'));
            return null;
        } else{
            system.debug('## MasterApp.id is:'+MasterApp.id);
            pr.getParameters().put('Id',MasterApp.id);
            pr.setRedirect(false);
            system.debug('## wrapper list:'+appsWrapperList);
        }
        return pr;
    }

    public PageReference mergeAppsFieldsPage(){
        integer i = 0;
        PageReference pr = new PageReference('/apex/TransAm_Application_MergeAppsFields_Form');
        for(appsWrapper selectedWrapperApp : appsWrapperList){
            if(selectedWrapperApp.selected){
                i++;
                pr.getParameters().put('Id'+i,selectedWrapperApp.childApp.id);
            }
        }
        system.debug('## MasterApp.id is:'+MasterApp.id);
        pr.getParameters().put('Id',MasterApp.id);
        pr.setRedirect(false);
        system.debug('## wrapper list:'+appsWrapperList);
        return pr;
    }
    
    public class appsWrapper{
        public boolean selected{get;set;}
        public TransAm_Application__c childApp{get;set;}
        
        public appsWrapper(TransAm_Application__c app){       
            this.selected = false;
            this.childApp = app;
        }                
    
    }
    
    
}