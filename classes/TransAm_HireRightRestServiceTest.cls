/* CLass Name   : TransAm_HireRightRestServiceTest
 * Description  : Test class for TransAm_HireRightRestService
 * Created By   : Monalisa Das
 * Created On   : 09-May-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa Das                09-May-2017              Initial version.
 *  
 *
 *****************************************************************************************/
 @isTest(seeAllData=false)
public class TransAm_HireRightRestServiceTest {
 /************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], false);
        lstApplications[0].TransAm_First_Name__c = 'TestFNeh';
        insert lstApplications;        
        
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Test';
        user_int.LastName = 'User';
        user_int.Username = 'testHRuser@abc.com';
        user_int.Email   = 'testuser@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
        
		 
        TransAm_HireRightHandler.prohibitAfterInsertTrigger = true;
		List<TransAm_HireRight_Orders__c> lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'MVR Standard' ,True); 
		TransAm_HireRightHandler.prohibitAfterInsertTrigger = false;
		TransAm_HireRightHandler.prohibitAfterUpdateTrigger = true;
		TransAm_HireRight_Orders__c order = lstHROrders.get(0);
		order.TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3';
		update order;
		TransAm_HireRightHandler.prohibitAfterUpdateTrigger = false;
    }
    
    
     /************************************************************************************
* Method       :    updateHireRightOrdersTest
* Description  :    Test Method to invoke Update hirerightOrder REST Service
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/ 
   public static testmethod void completeOrdersTest() {
        
        List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        Map<String,String> responseHeaders = new Map<String,String>();
		responseHeaders.put('Location','https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A');
        
       
        //insert lstHROrders;
        System.runAs(user_int){ 
		
            TransAm_HireRight_Orders__c queriedOrder = [select id, name from TransAm_HireRight_Orders__c where TransAm_Report_Type__c = 'MVR Standard' LIMIT 1];
            RestRequest req = new RestRequest();
            req.httpMethod = 'POST';
            req.requestUri = 'https://XXXX.salesforce.com/services/apexrest/public/'+queriedOrder.Name;
            req.requestBody = Blob.valueOf(JSON.serializePretty(''));
            RestContext.request = req;
            RestContext.response = new RestResponse();
            
	    
	        TransAm_HRServiceMock fakeResponse = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 responseHeaders);
												 
		
			Test.setMock(HttpCalloutMock.class, fakeResponse);
            TransAm_HireRightRestService.completeOrder();
            
            Test.stopTest();
        }
    }
	
	public static testmethod void setAttachmentTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        
       
      
        System.runAs(user_int){ 
           
	    
	    TransAm_HRServiceMock fakeResponse1 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><hr_objs:GenerateWebLinkResponse xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:Result xsi:type="hr_objs:ReportWL"><hr_objs:RegId>TN-050917-JH8M3</hr_objs:RegId><hr_objs:ReportContent>FULL</hr_objs:ReportContent><hr_objs:ReportURL contentType="application/pdf">https://ows01.hireright.com/screening_manager/entry?key=E1C083547A0E0B376420D5EE71B2AC6A</hr_objs:ReportURL></hr_objs:Result></hr_objs:GenerateWebLinkResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>',
                                                 null);
	    Test.setMock(HttpCalloutMock.class, fakeResponse1);
	    String soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest('TN-050917-JH8M3');
        //String url = TransAm_HireRightServiceConsumer.getWeblink(soapRequest);
		TransAm_Application__c app = [select id, Name from TransAm_Application__c
				    where TransAm_First_Name__c = 'TestFNeh' LIMIT 1];
	    TransAm_HireRight_Orders__c order = [select id,Name,TransAm_Application__r.Name,TransAm_Application__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c from TransAm_HireRight_Orders__c where TransAm_Hireright_Order_ID__c = 'TN-050917-JH8M3'];
         Blob blobVal = Blob.valueOf('fsfs');
		//TransAm_HireRightRestService.setAttachment(blobVal,order,app.Name);
            
            Test.stopTest();
        }
    }
	
	public static testmethod void getAccessTokenTest() {
        
       User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
        TransAm_HRServiceMock fakeResponse1 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'{"access_token":"abc"}',
                                                 null);
	    Test.setMock(HttpCalloutMock.class, fakeResponse1);
	   //TransAm_HireRightRestService.getAccessToken();
            
            Test.stopTest();
        }
    } 
	
	public static testmethod void callUpdateOrderTest() {
        
        User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){ 
        TransAm_HRServiceMock fakeResponse1 = new TransAm_HRServiceMock(200,
						'SUCCESS',
						'{"access_token":"abc"}',
                                                 null);
	    Test.setMock(HttpCalloutMock.class, fakeResponse1);
	   //TransAm_HireRightRestService.callUpdateOrder('abc','OID-000000123');
            
            Test.stopTest();
        }
    }   
}