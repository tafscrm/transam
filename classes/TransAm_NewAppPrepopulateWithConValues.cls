/******************************************************************************************
* Create By    :     Karthik Gulla
* Create Date  :     07/03/2017
* Description  :     Webservice class to prepopulate contact field values on to Create Application
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                           <Date>           Initial version.
*****************************************************************************************/
global class TransAm_NewAppPrepopulateWithConValues{
    Webservice static String formStandardNewAppCreateUrl(Id conId) {
        Set<String> setAppFields = new Set<String>();
        for(Schema.FieldSetMember f : SObjectType.TransAm_Application__c.FieldSets.TransAm_Contact_Information_AutoPopulate.getFields()) {
            setAppFields.add(f.getLabel());
        }

        System.debug('### setAppFields ###'+setAppFields);

        Map<String,TransAm_AppContactPrepopulateMapping__c> mapAppContactFieldMappings = TransAm_AppContactPrepopulateMapping__c.getAll();

        String query = 'SELECT ';
        for(String appFieldName: mapAppContactFieldMappings.keySet()){
            query += mapAppContactFieldMappings.get(appFieldName).TransAm_ContactField__c + ', ';
        }
        query += ' Id FROM Contact WHERE Id =\''+conId+'\'';
        Contact con = Database.query(query);

        Map<String,String> mapFieldIdMappings = new Map<String,String>();
        String url = '/' + TransAm_Application__c.SObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1';
        mapFieldIdMappings = getMappings();

        Map<String,String> urlMap = new Map<String,String>();
        for(String field:setAppFields){
            if(mapFieldIdMappings.containsKey(field) && mapFieldIdMappings.get(field) != null 
                && mapAppContactFieldMappings.containsKey(field) && mapAppContactFieldMappings.get(field) != null){
                if((mapAppContactFieldMappings.get(field).TransAm_AppFieldDataType__c).equals('checkbox')
                    && con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c) != null){
                    String fieldVal = Boolean.valueOf(con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c)) == true ? '1' : '0';
                    urlMap.put(mapFieldIdMappings.get(field),fieldVal);
                }
                else if((mapAppContactFieldMappings.get(field).TransAm_AppFieldDataType__c).equals('date')
                    && con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c) != null){
                    urlMap.put(mapFieldIdMappings.get(field), Date.ValueOf(con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c)).format());
                }
                else if((mapAppContactFieldMappings.get(field).TransAm_AppFieldDataType__c).equals('datetime')
                    && con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c) != null){
                    urlMap.put(mapFieldIdMappings.get(field), DateTime.ValueOf(con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c)).format());
                }
                else if((mapAppContactFieldMappings.get(field).TransAm_AppFieldDataType__c).equals('relatedlookupname')){
                    Schema.DescribeFieldResult fieldResult = Contact.TransAm_Driver_Id__c.getDescribe();
                    Schema.SObjectfield tfield = fieldResult.getSobjectField();
                    Contact relatedContact = (Contact)con.getSObject(tfield);
                    if(relatedContact!= null && relatedContact.Name != null)
                        urlMap.put(mapFieldIdMappings.get(field), String.valueOf(relatedContact.Name));
                }
                else if(con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c) != null){
                    urlMap.put(mapFieldIdMappings.get(field), String.valueOf(con.get(mapAppContactFieldMappings.get(field).TransAm_ContactField__c)));
                }
            }
        }

        for(String urlKey:urlMap.keySet()){
            url = url + '&' + urlKey + '=' + urlMap.get(urlKey);
        }

        System.debug('### url ###'+url);
        return url;
    }

    global static Map<String,String> getMappings(){
        Map<String,String> mapCompIdMappings = new Map<String,String>();
        String html = '';
        PageReference p = new PageReference('/' + TransAm_Application__c.SObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1');
        if(!test.isRunningTest()){
             html = p.getContent().toString();
        }else{
            html = '<td class="labelCol requiredInput"><label for="00NK0000000ap5l"><span class="requiredMark">*</span>Type</label></td>';
        }

        Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            mapCompIdMappings.put(label.removeStart('<span class="assistiveText">*</span>'), id);
        }
        return mapCompIdMappings; 
    }
}