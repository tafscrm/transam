/********************************************************************************* 
* Class Name   		: 	TransAm_HRRequestService
* Description     	:     Utility class to create request for HireRightService
* Created By      	:     Monalisa Das
* Create Date     	:     28/04/2017

* Modification Log	:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Monalisa Das                      28/04/2017         Initial version.
*****************************************************************************************/
public class TransAm_HRRequestService {
    
    private static String createPackageTemplate = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" SOAP-ENV:mustUnderstand="1"><wsse:UsernameToken wsu:Id="UsernameToken-37571A40C1EC9E28F614925237923833"><wsse:Username></wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"></wsse:Password><wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"></wsse:Nonce><wsu:Created></wsu:Created></wsse:UsernameToken></wsse:Security></SOAP-ENV:Header><SOAP-ENV:Body><hr_objs:Create xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><hr_objs:HRObject xsi:type="hr_objs:BackgroundCheck" duplicateCheckModel="None" fallbackModel="None" forceFallback="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><hr_objs:CompanyLogin></hr_objs:CompanyLogin><hr_objs:AccountId></hr_objs:AccountId><hr_objs:UserRefId></hr_objs:UserRefId><hr_objs:ClientApplicantId></hr_objs:ClientApplicantId><hr_objs:ClientRequestId></hr_objs:ClientRequestId><hr_objs:FlexFields><hr_objs:FlexField name="Customer_Reference"></hr_objs:FlexField><hr_objs:FlexField name="Sub_Account"></hr_objs:FlexField><hr_objs:FlexField name="ClientStatusReceiverURL"></hr_objs:FlexField></hr_objs:FlexFields><hr_objs:PackageId/><hr_objs:Payload><BackgroundCheck xmlns="http://ns.hr-xml.org/2006-02-28"><BackgroundSearchPackage><PersonalData><PersonName><GivenName></GivenName><MiddleName></MiddleName><FamilyName></FamilyName></PersonName><PostalAddress validFrom="2014-01"><CountryCode></CountryCode><PostalCode></PostalCode><Region></Region><Municipality></Municipality><DeliveryAddress><AddressLine></AddressLine></DeliveryAddress></PostalAddress><ContactMethod><Location></Location><Telephone><FormattedNumber></FormattedNumber></Telephone><InternetEmailAddress></InternetEmailAddress></ContactMethod><ContactMethod><Location></Location><Telephone><FormattedNumber></FormattedNumber></Telephone></ContactMethod><DemographicDetail><GovernmentId countryCode="US" issuingAuthority="SSN"></GovernmentId><DateOfBirth></DateOfBirth><Race></Race><GenderCode></GenderCode><Language></Language><EyeColor></EyeColor><HairColor></HairColor><Height unitOfMeasure="ft"></Height><Weight unitOfMeasure="lb"></Weight><BirthPlace><CountryCode></CountryCode><Region></Region><Municipality></Municipality></BirthPlace><Other type="HeightInches"></Other></DemographicDetail></PersonalData><SupportingDocumentation><Documentation type="release" resultType="x:signed" manifestName="Consent Form"/><Documentation type="release" resultType="x:FaxByRequestor" manifestName="DA Release Form"/></SupportingDocumentation><Screenings><Screening type="license" qualifier="cdlis"><CountryCode></CountryCode><Region></Region><SearchLicense><License><LicenseNumber/><LicensingAgency/></License></SearchLicense><AdditionalItems qualifier="SearchDepth"><Text></Text></AdditionalItems></Screening></Screenings></BackgroundSearchPackage></BackgroundCheck></hr_objs:Payload></hr_objs:HRObject></hr_objs:Create></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    private static String createNonPackageTemplate = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header><wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" 	xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken wsu:Id="UsernameToken-37571A40C1EC9E28F614925237923833"><wsse:Username></wsse:Username> <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"></wsse:Password> <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"></wsse:Nonce> <wsu:Created></wsu:Created> </wsse:UsernameToken> </wsse:Security> </SOAP-ENV:Header> <SOAP-ENV:Body> <hr_objs:Create xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> <hr_objs:HRObject xsi:type="hr_objs:BackgroundCheck" duplicateCheckModel="None" fallbackModel="None" forceFallback="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> <hr_objs:CompanyLogin></hr_objs:CompanyLogin> <hr_objs:AccountId></hr_objs:AccountId><hr_objs:UserRefId></hr_objs:UserRefId> <hr_objs:ClientApplicantId></hr_objs:ClientApplicantId> <hr_objs:ClientRequestId></hr_objs:ClientRequestId> <hr_objs:Services> <hr_objs:Service/> </hr_objs:Services><hr_objs:FlexFields><hr_objs:FlexField name="ClientStatusReceiverURL"></hr_objs:FlexField></hr_objs:FlexFields><hr_objs:Payload> <BackgroundCheck xmlns="http://ns.hr-xml.org/2006-02-28"> <BackgroundSearchPackage> <PersonalData> <PersonName> <GivenName></GivenName> <MiddleName></MiddleName> <FamilyName></FamilyName> </PersonName> <ContactMethod> <Location>Home</Location><Telephone><FormattedNumber></FormattedNumber></Telephone><InternetEmailAddress></InternetEmailAddress></ContactMethod><ContactMethod><Location>Office</Location><Telephone><FormattedNumber></FormattedNumber></Telephone></ContactMethod><DemographicDetail><GovernmentId countryCode="US" issuingAuthority="SSN"></GovernmentId><DateOfBirth></DateOfBirth><Race></Race><GenderCode></GenderCode><Language></Language><EyeColor></EyeColor><HairColor></HairColor><Height unitOfMeasure="ft"></Height><Weight unitOfMeasure="lb"></Weight><BirthPlace><CountryCode></CountryCode><Region></Region><Municipality></Municipality></BirthPlace><Other type="HeightInches"></Other></DemographicDetail><PostalAddress validFrom="2014-01"><CountryCode></CountryCode><PostalCode></PostalCode><Region></Region><Municipality></Municipality><DeliveryAddress><AddressLine></AddressLine></DeliveryAddress></PostalAddress></PersonalData><SupportingDocumentation><Documentation type="release" resultType="x:signed" manifestName="Consent Form"/><Documentation type="release" resultType="x:FaxByRequestor" manifestName="DA Release Form"/></SupportingDocumentation><Screenings><Screening type="license" qualifier="mvPersonal"><CountryCode></CountryCode><Region></Region><SearchLicense><License><LicenseNumber></LicenseNumber><LicensingAgency/></License></SearchLicense><AdditionalItems qualifier="SearchDepth"><Text>EXT</Text></AdditionalItems></Screening></Screenings></BackgroundSearchPackage></BackgroundCheck></hr_objs:Payload></hr_objs:HRObject></hr_objs:Create></SOAP-ENV:Body> </SOAP-ENV:Envelope>';
    private static String createWebLinkTemplate ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" 	xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken wsu:Id="UsernameToken-37571A40C1EC9E28F614925237923833"><wsse:Username></wsse:Username> <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"></wsse:Password> <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"></wsse:Nonce> <wsu:Created></wsu:Created> </wsse:UsernameToken> </wsse:Security></soapenv:Header><soapenv:Body><GenerateWebLink xmlns="urn:enterprise.soap.hireright.com/objs" xmlns:hr_objs="urn:enterprise.soap.hireright.com/objs" xmlns:objs="urn:enterprise.soap.hireright.com/objs"><HRWebLink xsi:type="hr_objs:ViewReportWL"><hr_objs:CompanyLogin></hr_objs:CompanyLogin>            <hr_objs:AccountId></hr_objs:AccountId><hr_objs:UserRefId></hr_objs:UserRefId><RegId></RegId><hr_objs:Format>PDF</hr_objs:Format></HRWebLink></GenerateWebLink></soapenv:Body></soapenv:Envelope>';
    private static String envNS = 'http://schemas.xmlsoap.org/soap/envelope/';
    private static String hrobjsNS = 'urn:enterprise.soap.hireright.com/objs';
    private static String bgNS = 'http://ns.hr-xml.org/2006-02-28';
    private static String wsseNS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    private static String wsuNS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
    
    
    /**********************************************************************************************
* Created By      :  Monalisa
* Create Date     :  28-Apr-2017
* Description     :  Create Hire Right Request to invoke HR Service which returns HR Order Id
* Modification Log:  Initial version.
***********************************************************************************************/
    public static String getCreateRequest(TransAm_Application__c app,TransAm_HireRight_Orders__c order){
        
        Dom.Document doc = new Dom.Document();
        try{
            Map<String, PackageReport__c> packageReports = PackageReport__c.getAll();
            if(!packageReports.isEmpty() && packageReports.containsKey(order.TransAm_Report_Type__c) && packageReports.get(order.TransAm_Report_Type__c).Is_Package__c)
            {
                doc.load(createPackageTemplate);
            }
            else
            {
                doc.load(createNonPackageTemplate);
            }
            Dom.XMLNode node = doc.getRootElement();
            Dom.XmlNode root = node;
            Dom.XMLNode backgroundSearchPkgNode;  
            Dom.XMLNode serviceNode; 
            node = node.getChildElement('Body', envNS);
            node = node.getChildElement('Create', hrobjsNS);
            node = node.getChildElement('HRObject', hrobjsNS);
            Dom.XmlNode hrObjNode = node;
            node = node.getChildElement('ClientApplicantId', hrobjsNS);
            //node.addTextNode('AppID_170320_1');
			node.addTextNode(app.Name);
			node = hrObjNode.getChildElement('ClientRequestId', hrobjsNS);
            //node.addTextNode('ReqID_170320_1');
			node.addTextNode(order.Name);
			node = hrObjNode.getChildElement('CompanyLogin', hrobjsNS);
            node.addTextNode(Label.HR_Company_Login);
            node = hrObjNode.getChildElement('AccountId', hrobjsNS);
            node.addTextNode(Label.HR_Account_Id);
            node = hrObjNode.getChildElement('UserRefId', hrobjsNS);
            id userId = UserInfo.getUserId();
            String userRefId = [Select UserRefId__c From User Where Id = :userId][0].UserRefId__c;
            system.debug('user id ' + userId + ' ref id ' + userRefId);
			if(!String.ISBLANK(userRefId))
				node.addTextNode(userRefId);
			else
				node.addTextNode('TAYA2');
            node = hrObjNode.getChildElement('FlexFields', hrobjsNS);
            for(Dom.XmlNode temp : node.getChildren()) {
                if('ClientStatusReceiverURL'.equals(temp.getAttributeValue('name', null))) {
                    String baseUrl = System.Url.getSalesforceBaseUrl().toExternalForm();
                    String url = Label.HR_Complete_Url + order.Name;
                    /*String token = getAccessToken();
                    url = url + ';_auth=' + token;
                    System.debug('callback url ' + url);*/
                    temp.addTextNode(url);
                }
                else if('Customer_Reference'.equals(temp.getAttributeValue('name', null)))
                    temp.addTextNode('myReference 123');
                else if('Sub_Account'.equals(temp.getAttributeValue('name', null)))
                    temp.addTextNode('subAccount TDAC2_abc');
            }
            //Setting Service Data
            /*node = hrObjNode.getChildElement('Services', hrobjsNS); 
serviceNode = node;
node = serviceNode.getChildElement('Service', hrobjsNS);
//node.setAttribute('id', 'CDLIS+');*/
            node = hrObjNode.getChildElement('Payload', hrobjsNS);         
            node = node.getChildElement('BackgroundCheck', bgNS);          
            node = node.getChildElement('BackgroundSearchPackage', bgNS);
            backgroundSearchPkgNode = node;
            
            Dom.XmlNode personalDataNode = node.getChildElement('PersonalData', bgNS);        
            Dom.XmlNode personNameNode = personalDataNode.getChildElement('PersonName', bgNS);
            node = personNameNode.getChildElement('GivenName', bgNS);
            node.addTextNode(app.TransAm_First_Name__c);
            node = personNameNode.getChildElement('FamilyName', bgNS);
            node.addTextNode(app.TransAm_Last_Name__c);
            
            if(!packageReports.isEmpty() && packageReports.containsKey(order.TransAm_Report_Type__c) && packageReports.get(order.TransAm_Report_Type__c).Is_Package__c)
            {
                PackageReport__c packageReport = packageReports.get(order.TransAm_Report_Type__c);
                node = hrObjNode.getChildElement('PackageId', hrobjsNS); 
                node.addTextNode(packageReport.Package_ID__c);
                //node.setAttribute('id', packageReport.Package_ID__c);
                Dom.XmlNode homeContactNode = personalDataNode.getChildElement('ContactMethod', bgNS);
                node = homeContactNode.getChildElement('Location', bgNS);
                node.addTextNode('home');
                node = homeContactNode.getChildElement('Telephone', bgNS);
                Dom.XmlNode homeTelephoneNode = node;
                node = homeTelephoneNode.getChildElement('FormattedNumber', bgNS);
                node.addTextNode(app.TransAm_Phone__c);
                node = homeContactNode.getChildElement('InternetEmailAddress', bgNS);
                node.addTextNode(app.TransAm_Email__c);
                
                /*Dom.XmlNode officeContactNode = personalDataNode.getChildElement('ContactMethod', bgNS);
node = officeContactNode.getChildElement('Telephone', bgNS);
Dom.XmlNode officeTelephoneNode = node;
node = officeTelephoneNode.getChildElement('FormattedNumber', bgNS);
node.addTextNode(app.TransAm_Cell_Number__c);	*/
                node = personalDataNode.getChildElement('DemographicDetail', bgNS);
                node = node.getChildElement('GenderCode', bgNS);
                node.addTextNode(app.TransAm_Gender__c);
            }
            else
            {
                node = hrObjNode.getChildElement('Services', hrobjsNS); 
                serviceNode = node;
                node = serviceNode.getChildElement('Service', hrobjsNS);
                node.setAttribute('id', order.TransAm_Report_Type__c);
            }
            //node.setAttribute('id', order.TransAm_Report_Type__c);
            // Setting Service Data complete      
            
            Dom.XmlNode demoNode = personalDataNode.getChildElement('DemographicDetail', bgNS);
            node = demoNode.getChildElement('GovernmentId', bgNS);
            node.addTextNode(app.TransAm_SSN__c);
            node = demoNode.getChildElement('DateOfBirth', bgNS);   
            Date dob = app.TransAm_Date_Of_Birth__c;
            DateTime dt = DateTime.newInstance(dob.year(), dob.month(), dob.day());
            String dobString = dt.formatGMT('yyyy-MM-dd');
            node.addTextNode(dobString);
            
            Dom.XmlNode addressNode = personalDataNode.getChildElement('PostalAddress', bgNS);
            node = addressNode.getChildElement('CountryCode', bgNS);
            node.addTextNode('US');
            node = addressNode.getChildElement('PostalCode', bgNS);
            node.addTextNode(app.TransAm_Zip__c);
            node = addressNode.getChildElement('Region', bgNS);
            node.addTextNode(app.TransAm_State__c);
            node = addressNode.getChildElement('Municipality', bgNS);
            node.addTextNode(app.TransAm_City__c);
            node = addressNode.getChildElement('DeliveryAddress', bgNS);
            node = node.getChildElement('AddressLine', bgNs);
            if(app.TransAm_AddressLine2__c != null)
                node.addTextNode(app.TransAm_Address__c + ' ' + app.TransAm_AddressLine2__c);
            else
                node.addTextNode(app.TransAm_Address__c);
            
            node = backgroundSearchPkgNode.getChildElement('Screenings',bgNS);
            for(Dom.XmlNode elem : node.getChildElements()) {
                String val = elem.getAttributeValue('type', null);
                if(val != null && val.equals('license')) {
                    node = elem;
                    break;
                }
            }
            node = node.getChildElement('Region', bgNS);
            System.debug('License Issuance Region ::'+app.TransAm_License_State__c);
            node.addTextNode(app.TransAm_License_State__c);  
            node = node.getParent().getChildElement('CountryCode', bgNS);
            node.addTextNode('US');
            node = node.getParent();
            node = node.getChildElement('SearchLicense', bgNS);
            node = node.getChildElement('License', bgNS);
            node = node.getChildElement('LicenseNumber', bgNS);
            node.addTextNode(app.TransAm_LicenseNo__c);        
            
            // Header parameters
            node = root.getChildElement('Header', envNS);
            setHeader(node);
        }
        catch(Exception e)
        {
            System.debug(e.getStackTraceString()); 
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HRRequestService',TransAm_Method_Name__c = 'getCreateRequest',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        return doc.toXmlString();
    }
    
    /*************************************************************************************************
* Created By      :  Monalisa
* Create Date     :  28-Apr-2017
* Description     :  Create Hire Right Request to invoke HR Service which returns HR weblink URL
* Modification Log:  Initial version.
**************************************************************************************************/
    public static String getGenerateWeblinkRequest(String orderId){
        
        Dom.Document doc = new Dom.Document();
        try{
            doc.load(createWebLinkTemplate);
            Dom.XMLNode node = doc.getRootElement();
            Dom.XmlNode root = node;
            node = node.getChildElement('Body', envNS);
            //node = node.getChildElement('GenerateWebLink', hrobjsNS);
            //node = node.getChildElement('HRWebLink', hrobjsNS);
            node = node.getChildElement('GenerateWebLink', hrobjsNS);
            node = node.getChildElement('HRWebLink', hrobjsNS);
            Dom.XmlNode hrObjNode = node;
            node = hrObjNode.getChildElement('CompanyLogin', hrobjsNS);
            node.addTextNode(Label.HR_Company_Login);
            node = hrObjNode.getChildElement('AccountId', hrobjsNS);
            node.addTextNode(Label.HR_Account_Id);
            node = hrObjNode.getChildElement('UserRefId', hrobjsNS);
			node.addTextNode('TAYA2');
            node = hrObjNode.getChildElement('FlexFields', hrobjsNS);
            node = hrObjNode.getChildElement('RegId', hrobjsNS);
            node.addTextNode(orderId);
            node = hrObjNode.getChildElement('Format', hrobjsNS);
            //node.addTextNode('PDF');
            
            // Header parameters
            node = root.getChildElement('Header', envNS);
            setHeader(node);
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HRRequestService',TransAm_Method_Name__c = 'getGenerateWeblinkRequest',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
        return doc.toXmlString();
    }
    
    private static void setHeader(Dom.XmlNode node) {
        try{
            node = node.getChildElement('Security', wsseNS);
            node = node.getChildElement('UsernameToken', wsseNS);
            Dom.XmlNode tokenNode = node;
            node = tokenNode.getChildElement('Username', wsseNS);
            node.addTextNode(Label.HR_Username);
            String nonce = generateNonce();
            Blob nonceBlob = EncodingUtil.base64Decode(nonce);
            String created = Datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            Blob createdBlob = Blob.valueOf(created);
            Blob passwordBlob = Blob.valueOf('Wh@+sUp@17');
            String combinedDataAsHex = EncodingUtil.convertToHex(nonceBlob) + EncodingUtil.convertToHex(createdBlob) + EncodingUtil.convertToHex(passwordBlob);
            Blob combinedDataAsBlob = EncodingUtil.convertFromHex(combinedDataAsHex);
            Blob hashTest = Crypto.generateDigest('SHA-1', combinedDataAsBlob);
            String password = EncodingUtil.base64encode(hashTest);
            node = tokenNode.getChildElement('Nonce', wsseNS);
            node.addTextNode(nonce);
            node = tokenNode.getChildElement('Created', wsuNS);
            node.addTextNode(created);
            node = tokenNode.getChildElement('Password', wsseNS);
            node.addTextNode(password);
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HRRequestService',TransAm_Method_Name__c = 'setHeader',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
        }
    }
    
    public static String generateNonce()
    {
        try{
            Long randomLong = Crypto.getRandomLong();
            System.debug('Nonce long is: ' + randomLong);
            return EncodingUtil.base64Encode(Blob.valueOf(String.valueOf(randomLong)));            
        }
        catch(Exception e)
        {
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HRRequestService',TransAm_Method_Name__c = 'generateNonce',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
            return null;
        }
    }
}