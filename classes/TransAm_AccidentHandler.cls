/* Class Name   : TransAm_AccidentHandler
 * Description  : Handler class for the Accident trigger
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_AccidentHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Accident__c> accidentList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_AccidentHelper.populateApplicationOnAccidents(accidentList);
        prohibitBeforeInsertTrigger = true;
    }
    
    public static void afterInsertHandler(list<TransAm_Accident__c> accidentList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_AccidentHelper.updateApplication(accidentList);
        prohibitAfterInsertTrigger = true;
    }
    
    public static void afterUpdateHandler(list<TransAm_Accident__c> accidentList){
    
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        TransAm_AccidentHelper.updateApplication(accidentList);
        prohibitAfterUpdateTrigger = true;
    }

}