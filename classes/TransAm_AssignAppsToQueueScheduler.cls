/******************************************************************************************
* Create By    :     Ajay B R
* Create Date  :     06/20/2017
* Description  :     Scheduler Class to schedule the TransAm_AssignApplicationsToQueueBatch class
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                         06/20/2017           Initial version.
*****************************************************************************************/
global class TransAm_AssignAppsToQueueScheduler implements Schedulable {
   global void execute(SchedulableContext SC) {
      Database.executeBatch(new TransAm_AssignApplicationsToQueueBatch(),100); 
   }
}