/* Class Name   : TransAm_FelonyHandler
 * Description  : Handler class for the Felony trigger
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_FelonyHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Felony__c> felonyList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_FelonyHelper.populateApplicationOnFelony(felonyList);
        prohibitBeforeInsertTrigger = true;
    }
    
    public static void afterInsertHandler(list<TransAm_Felony__c> felonyList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        //TransAm_FelonyHelper.updateApplication(felonyList);
        prohibitAfterInsertTrigger = true;
    }
    
    public static void afterUpdateHandler(list<TransAm_Felony__c> felonyList){
    
        if(prohibitAfterUpdateTrigger){
            return;
        }        
        //TransAm_FelonyHelper.updateApplication(felonyList);
        prohibitAfterUpdateTrigger = true;
    }
    
}