/******************************************************************************************
* Create By    :     Pranjal Singh
* Create Date  :     05/06/2017
* Description  :     Schedule class for TransAm_ScheduleRefreshRVIXMLBatch batch class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
global class TransAm_ScheduleRefreshRVIXMLBatch implements Schedulable {
   global void execute(SchedulableContext SC) {
        Database.executeBatch(new TransAm_RefreshRVIXMLBatch(),1);
   }
}