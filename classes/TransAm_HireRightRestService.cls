@RestResource(urlMapping='/public/*')
global class TransAm_HireRightRestService {

    @HttpPost(urlMapping='orderId')
    global static String completeOrder() {
        RestRequest req = RestContext.request;
    	String orderId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        TransAm_HireRight_Orders__c order = [SELECT TransAm_Application__c,TransAm_HireRight_Reports__c,TransAm_Report_Type__c,TransAm_Hireright_Order_ID__c, TransAm_Application__r.Name from TransAm_HireRight_Orders__c WHERE Name = :orderId];
        //String accessToken = getAccessToken();
        //String res = callUpdateOrder(accessToken, orderId);
        String soapRequest;
        String weblink;
        soapRequest = TransAm_HRRequestService.getGenerateWeblinkRequest(order.TransAm_Hireright_Order_ID__c);
        boolean useQueue = true;
        if(useQueue) {
            System.enqueueJob(new TransAm_GetWeblinkJob(orderId, soapRequest));
            return 'queueable';
        }
        /*boolean useFuture = true;
        if(useFuture) {
            TransAm_HireRightServiceConsumer.sendRawRequestForWebLink(soapRequest, order.Id);
            return 'future';
        }
        weblink = TransAm_HireRightServiceConsumer.getWeblink(soapRequest); 
        System.debug('weblink ::'+weblink);
        Blob attachment;
        try {
        attachment = TransAm_HireRightServiceConsumer.getAttachment(weblink);
        } catch(Exception e) {
            SYstem.debug('Exceptione ::'+e.getMessage() + e.getStackTraceString() + e);
            return weblink;
        }
        String attName;
        try {
        attName = setAttachment(attachment, order, order.TransAm_Application__r.Name);
        } catch(Exception e) {
            return e.getStackTraceString() + ' ' + e.getMessage() + ' ' + order.TransAm_Application__r.Name + ' ' + order.TransAm_Report_Type__c;
        }
        order.TransAm_HireRight_Reports__c=weblink;
        order.TransAm_Order_Status__c = 'Completed';
        try{
        Database.update(order);
        } catch(Exception e) {
            Id id1 = UserInfo.getProfileId();
            Profile profile = [select Name from profile where id = :id1];
            return e.getStackTraceString() + '\n\n' + e.getMessage();
        }
        //TransAm_HireRightHelper.populateWeblinkOnHireRightOrders(orders);
        //TransAm_HireRightUpdate.test(orderId);
        System.debug('in rest service');*/
        return 'success';
    }
    
    /*@testVisible
    private static String setAttachment(Blob attBlob, TransAm_HireRight_Orders__c hireRightOrder, String name) {
        String reportType ;
            if(!String.ISBLANK(hireRightOrder.TransAm_Report_Type__c))
            {
                if(hireRightOrder.TransAm_Report_Type__c.contains('Express'))
                    reportType = 'MVRExpress';
                else if(hireRightOrder.TransAm_Report_Type__c.contains('Standard'))
                    reportType = 'MVRStandard';
            }
            String appName = hireRightOrder.TransAm_Application__r.Name;
        	System.debug('Order application name ' + hireRightOrder.TransAm_Application__r.Name);
            TransAm_Recruitment_Document__c recDoc = [select id from TransAm_Recruitment_Document__c where TransAm_Application__r.Name = :appName and TransAm_Type__c = :reportType Limit 1];
            //Id recDocId = Id.valueOf('a0C4D0000005dMsUAI');
        	//TransAm_Recruitment_Document__c recDoc = [select id from TransAm_Recruitment_Document__c where Id = :recDocId];
            Attachment att = new Attachment();
            att.Body = attBlob;
            att.name = 'test.pdf';
            att.ParentId = recDoc.Id;
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = true;
            Database.insert(att);
            TransAm_AttachmentHandler.prohibitAfterInsertTrigger = false;
            System.debug('Attachment Id :: '+att);
            
            //TransAm_HireRight_Orders__c order = new TransAm_HireRight_Orders__c(Id=hireRightOrder.Id);
            hireRightOrder.TransAm_Recruitment_Documents__c = recDoc.Id;
            System.debug('Updating Hire Right Order :: '+hireRightOrder.Id);
        	return att.Name;
          //  Database.update(order);
    }
    
	@testVisible
    private static String callUpdateOrder(String token, String orderId) {
        HttpResponse res;
        try {
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndPoint('https://cs71.salesforce.com/services/apexrest/transam/hireright/' + orderId);
            req.setHeader('Authorization', 'OAuth ' + token);
            req.setBody('{}');
			Http h = new Http();
            res = h.send(req);            
        } catch(Exception e) {
            return 'Exception' + res.getBody();
        }
        return res.getBody();
    }
    @testVisible
    private static String getAccessToken() {
        Map<String, Object> data = new Map<String, Object>();
        try{
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(Label.Get_Access_Token);
            req.setBody('grant_type=password' + 
                        '&client_id=' + '3MVG9M6Iz6p_Vt2wnCMn6Xu5bY0JsJcrx_C4d6mCM9Z4_HuwXf_ddlY4ezYZk3NNnyz6crRY0eWK604z10tb0'+
                        '&client_secret=' + '1401790106614030644' +
                        '&username=' + 'liaadams@transamtruck.com.currentdev' +
                        '&password=' + 'zzDtnVTpbVHoB45aklfJ1xreFrsUB3G0ueFmLpprEUm3p');
            Http h = new Http();
            HTTPResponse res = h.send(req);
            data = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('Body ' + res.getBody());
        }
        catch(Exception e)
        {
            
            INSERT (new TransAm_Error_Log__c(TransAm_Class_Name__c = 'TransAm_HRRequestService',TransAm_Method_Name__c = 'getAccessToken',TransAm_Module_Name__c = 'HireRight',TransAm_Exception_Message__c = e.getMessage() + e.getStackTraceString()));
			return 'errorr';
        }
        return (String)data.get('access_token');	
    }*/
}