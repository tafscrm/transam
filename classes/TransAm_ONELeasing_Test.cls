/* CLass Name   : TransAm_ONELeasing_Test
 * Description  : Test class for TransAm_ONELeasing
 * Created By   : Karthik Gulla
 * Created On   : 19-Sep-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Karthik Gulla                19-Sep-2017            Initial version.
 *
 *****************************************************************************************/
@isTest
public class TransAm_ONELeasing_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
    	List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(2, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstContacts[0], true);
        TransAm_Test_DataUtility.createLeases(1, lstApplications[0], 'IC Agreement', true, null);  
    }

    /************************************************************************************
    * Method       :    testONELeasing
    * Description  :    Test Method to create lease records of different type
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    static testmethod void testONELeasing() {
        Test.startTest();
        List<TransAm_Application__c> lstApps = [Select Id, Name From TransAm_Application__c];
        lstApps[0].TransAm_Status__c = 'HR Review';
        update lstApps;

        TransAm_ONELeasing.getRecordTypeAndFormURL('IC Agreement',lstApps[0].Id, null);

        List<TransAm_ONE_Leasing__c> lstLease = [Select Id, Name FROM TransAm_ONE_Leasing__c];

        TransAm_ONELeasing.getRecordTypeAndFormURL('Lease Agreement',lstApps[0].Id, lstLease[0].Id);

        TransAm_ONELeasing.getRecordTypeAndFormURL('Lease Agreement',lstApps[0].Id, lstLease[0].Id);

        Test.stopTest();
    }
}