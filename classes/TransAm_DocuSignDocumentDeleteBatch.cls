/******************************************************************************************
* Create By    :     Naveenkanth B
* Create Date  :     04/06/2017
* Description  :     Batch Class to delete DocuSign attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Naveenkanth B                    04/06/2017           Initial version.
*****************************************************************************************/
global class TransAm_DocuSignDocumentDeleteBatch implements Database.Batchable<sObject>{
  
    global Database.QueryLocator start(Database.BatchableContext BC){ 
      String query = 'SELECT Id,Name,Parent.Type FROM Attachment WHERE Parent.Type =\'dsfs__DocuSign_Status__c\'  AND CreatedDate = LAST_N_DAYS : 50'; 
      return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        DELETE (List<Attachment>)scope;
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}