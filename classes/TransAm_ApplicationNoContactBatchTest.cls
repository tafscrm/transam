/******************************************************************************************
* Create By    :     Neha Jain
* Create Date  :     06/20/2017
* Description  :     Test class for TransAm_ApplicationStatusNoContactBatch batch class and TransAm_ScheduleAppStatusNoContact class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
@isTest
private class TransAm_ApplicationNoContactBatchTest{
     /*******************************************************************************************
    * Method        :   dataSetup
    * Description   :   Purpose of this method is to setup test data.
    * Parameter     :   null
    * Return Type   :   void
    ******************************************************************************************/
    public static @testSetup void dataSetup() {        
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        User integrationUser = new User();
        integrationUser.firstname = 'Arun';
        integrationUser.lastname = 'Tyagi';
        integrationUser.username = 'xyzrtyu@abc.com';
        integrationUser.email = 'xyz@deloitte.com';
        integrationUser.alias = 'xyz';
        integrationUser.TimeZoneSidKey = 'America/Los_Angeles';
        integrationUser.LocaleSidKey = 'En_US';
        integrationUser.profileID = prof.Id;
        integrationUser.LanguageLocaleKey = 'En_US';
        integrationUser.EmailencodingKey = 'UTF-8';
        
        insert integrationUser;
        
        
        /*for(TransAm_Application__c app : appList){       
            app.TransAm_State__c = 'AR';
            update appList;
        }*/

    } 

    /************************************************************************************
    * Method       :    testScheduleAppStatusBatch
    * Description  :    Test Method to test if the scheduler runs and updates the status fieldon Applications.
    * Parameter    :    Null
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testScheduleAppStatusBatch() {       
        
       /* TransAm_Application__c updatedApp = [select id, Name, TransAm_Status__c,CreatedDate from TransAm_Application__c where TransAm_State__c = 'AR' and TransAm_Status__c = 'Application Received' limit 1];
        
        system.debug('****updatedApp'+updatedApp);
        system.assertEquals(updatedApp.TransAm_Status__c, 'Application Received');*/
        
            
        User intUser = [Select Id from User where FirstName='Arun' limit 1];
        system.runAs(intUser){
            List<TransAm_Hiring_Areas_Reference__c> hiringAreaRef = new LisT<TransAm_Hiring_Areas_Reference__c>();
            hiringAreaRef.add(new TransAm_Hiring_Areas_Reference__c(TransAm_Zip__c = '223'));
            insert hiringAreaRef;
            List<Account> accList = TransAm_Test_DataUtility.createAccounts(1, true);
            List<Contact> conList = TransAm_Test_DataUtility.createContacts(1, accList[0].Id, 'Applicant',true);
            List<TransAm_Application__c> appsList = TransAm_Test_DataUtility.createApplications(10, 'Independent Contractor Only', 'Application Received', conList[0], false);
            for(TransAm_Application__c app : appsList){
                app.TransAm_State__c = 'KS';      
            }
            
                insert appsList;
            
            Set<ID> appIDSet = new Set<ID>();
            List<TransAm_Application__c> appList2 = [select id, Name, TransAm_Status__c,createdDate, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Application Received'];
            System.assert(appList2.size()>0);
            for(TransAm_Application__c app : appList2 ){
                appIDSet.add(app.Id);
                Test.setCreatedDate(app.Id, DateTime.newInstance(2013,12,12));
            }
            
            test.startTest();
            List<TransAm_Application__c> updatedAppList2 = [select id, Name,createdDate, TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where ID IN: appIDSet];
            system.assertEquals(DateTime.newInstance(2013,12,12), updatedAppList2[0].createddate, 'Date not set');
            Database.executebatch(new TransAm_ApplicationStatusNoContactBatch(),200);
            
            
            //System.assertEquals('No Contact', updatedAppList2[0].TransAm_Status__c, 'Records Not Updated' );
            Test.stopTest();
            
        }
        //Test.startTest();
            //TransAm_Application__c app1 = [SELECT ID, createddate from TransAm_Application__c LIMIT 1 ];
            //system.debug('****updatedAppcreateddate'+app1.createddate);
            //Database.executebatch(new TransAm_ApplicationStatusNoContactBatch(),200);
            //
            //String scheduleTime = '0 0 23 * * ?';
            //System.schedule('Test AppSchedule Batch', scheduleTime, applicationstatusUpdate);
        
        
    }
}