/* Class Name   : TransAm_ViolationHandler
 * Description  : Handler class for the Violation trigger
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_ViolationHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
    public static boolean prohibitafterUpdateTrigger = false;
    public static boolean prohibitafterInsertTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Violation__c> violationList){
    
        if(prohibitBeforeInsertTrigger){
            return;
        }        
        TransAm_ViolationHelper.populateApplicationOnViolation(violationList);
        prohibitBeforeInsertTrigger = true;
    }
    
    public static void afterInsertHandler(list<TransAm_Violation__c> violationList){
    
        if(prohibitafterInsertTrigger){
            return;
        }        
        TransAm_ViolationHelper.updateApplication(violationList);
        prohibitafterInsertTrigger = true;
    }
    
    public static void afterUpdateHandler(list<TransAm_Violation__c> violationList){
    
        if(prohibitafterUpdateTrigger){
            return;
        }        
        TransAm_ViolationHelper.updateApplication(violationList);
        prohibitafterUpdateTrigger = true;
    }

}