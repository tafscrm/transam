global class TransAm_EmailReceiveService implements Messaging.InboundEmailHandler {
    
        global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                           Messaging.Inboundenvelope envelope) {
		String body = email.plainTextBody;
        system.debug('email body ' + body);
        List<string> lines = body.split('\n');
        system.debug('tokens ' + lines);
        boolean foundApp = false;	
        integer i = 0;
        TransAm_Application__c app = null;
        integer messageCount = 0;	
        string orderedBy = null;
        string confirmationNumber = null;
        string firstName = null;
        string lastName = null;
        for(string s : lines) {
            list<string> elems = s.split(':');
            system.debug('current string ' + s);                
            if(s.contains('PO#')) {
                if(messageCount > 0)
                    break;
                messageCount++;
                system.debug('inside PO');
                string appNameString = elems[1].trim();
                appNameString = 'AP-' + appNameString;
                system.debug('app name ' + appNameString);
                app = [select Id, TransAm_Greyhound_Confirmation_Number__c, TransAm_Transportation_Cost__c
                                     from TransAm_Application__c where Name = :appNameString];
            }
            else if(s.contains('PRICE') && elems.size() > 1) {
                app.TransAm_Transportation_Cost__c = decimal.valueOf(elems[1].trim());
            }
            else if(s.contains('CONFIRMATION') && elems.size() > 1) {
                confirmationNumber = elems[1];
                app.TransAm_Greyhound_Confirmation_Number__c = elems[1];
            }
            else if(s.contains('SCHEDULE') && elems.size() > 1) {
                system.debug('schedule string ' + elems[1]);
                string sched = '';
                for(Integer j = 1; j < elems.size() ; j++) 
                	sched += elems[j];
                system.debug('sched ' + sched);
                app.TransAm_Greyhound_Memo__c = sched;
            }
            else if(s.contains('COMMENTS') && elems.size() > 1) {
                app.TransAm_Greyhound_Memo__c = elems[1];
            }
            else if(s.contains('ORDERED BY') && elems.size() > 1)
                orderedBy = elems[1];
            else if(s.contains('FIRST NAME') && elems.size() > 1)
                firstName = elems[1];
            else if(s.contains('LAST NAME') && elems.size() > 1)
                lastName = elems[1];
            i++;
        }
        app.TransAm_Send_Greyhound_Email__c = false;
        app.TransAm_Greyhound_Confirmation_Date__c = date.today();
        update app;
        notifyRecruiter(orderedBy, app.id, confirmationNumber, firstName, lastName);
        return new Messaging.InboundEmailResult();                                                       
    }
    
    private void notifyRecruiter(String orderedBy, id appId, String confirmationNumber, 
                                 string firstName, string lastName) {
        orderedBy = orderedBy.trim();
        String email = [select email from user where name = :orderedBy][0].email;
        string emailBody = null;
        string instanceUrl = URL.getSalesforceBaseUrl().toExternalForm();
        if(string.isNotEmpty(confirmationNumber))
        	emailBody = 'Greyhound order confirmed for ' + instanceUrl + '/' + appId;
        else
            emailBody = 'Greyhound order failed for ' + instanceUrl + '/' + appId;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(new String[] {email});
        message.subject = 'Greyhound request for ' + firstName.trim() + ' ' + lastName.trim();
        message.plainTextBody = emailBody;
        Messaging.sendEmail(new Messaging.SingleEmailmessage[] {message});
    }

}