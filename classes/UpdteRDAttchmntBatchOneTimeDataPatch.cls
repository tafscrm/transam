/******************************************************************************************
* Class Name   :     TransAm_UpdateRDAttachmentCountBatch
* Created By   :     Ajay B R
* Create Date  :     09/04/2017
* Description  :     Batch Class to Count and update the number of attachments a Recruitment Document has.
*
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                         09/04/2017           Initial version.
*****************************************************************************************/
global class UpdteRDAttchmntBatchOneTimeDataPatch implements Database.Batchable<sObject> {
    
    //TransAm_Generic_Custom_Setting__c is a custom setting - It has the datasets for: 
    //global Map<String, TransAm_Generic_Custom_Setting__c> genericCustomSettingsMap = TransAm_Generic_Custom_Setting__c.getAll();
    /*******************************************************************************************
    * Method        :   start
    * Description   :   This is the start() method of the batch class, used to query 
    *                   and retrieve all RDs with TransAm_Attachment_count__c null.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Database.QueryLocator
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, TransAm_Attachment_count__c from TransAm_Recruitment_Document__c where TransAm_Attachment_count__c = null'); //exectue query
    }
   
    /*******************************************************************************************
    * Method        :   execute
    * Description   :   This is the execute method of the batch class, used to process and update the count of attachments under RD
    * Parameter     :   Database.BatchableContext, List<TransAm_Recruitment_Document__c>
    * Return Type   :   Void
    * Modification Log:
    * ----------------------------------------------------------------------------
    *  * Developer                          Date               Description
    *  * ----------------------------------------------------------------------------                 
    *  * Ajay B R                         09/04/2017           Initial version.
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<TransAm_Recruitment_Document__c> listOfRDs) {
        Set<ID> rdIdSet = new Set<ID>();
        List<Attachment> attachmentObjList = new List<Attachment>();
        Map<Id, List<Attachment>> rdIdAttchmntListMap = new Map<ID, List<Attachment>>();
        if(!listOfRDs.isEmpty()){
            for(TransAm_Recruitment_Document__c rdObj : listOfRDs){
                rdIdSet.add(rdObj.Id);
            }
            if(!rdIdSet.isEmpty()){
                attachmentObjList = [Select Id, ParentId from Attachment where ParentId IN :rdIdSet];
            }
            if(!attachmentObjList.isEmpty()){
                for(Attachment attachmntObj: attachmentObjList){
                    if(rdIdAttchmntListMap.containsKey(attachmntObj.ParentId)){
                        rdIdAttchmntListMap.get(attachmntObj.ParentId).add(attachmntObj);
                    }
                    else{
                        List<Attachment> attachmentObjList2 = new List<Attachment>();
                        attachmentObjList2.add(attachmntObj);
                        rdIdAttchmntListMap.put(attachmntObj.ParentId, attachmentObjList2);
                    }
                }
            }
            if(!rdIdAttchmntListMap.isEmpty()){
                for(TransAm_Recruitment_Document__c rdObj : listOfRDs){
                    if(rdIdAttchmntListMap.containsKey(rdObj.Id)){
                        rdObj.TransAm_Attachment_count__c = rdIdAttchmntListMap.get(rdObj.Id).size();
                    }
                }
            }
            Database.SaveResult[] updateResults = Database.update(listOfRDs, false);
            
            //for every result captured
            for (Database.SaveResult sr : updateResults) {
                //check if it is a failure
                if (!sr.isSuccess()){
                    String errormsg;    //string var to capture error msg
                    for(Database.Error err : sr.getErrors()) {
                        errormsg += err.getMessage();   //get error message and capture in string var
                    }
                    //create an error log instance
                    TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                    el.TransAm_Class_Name__c = 'TransAm_UpdateRDAttachmentCountBatch';
                    el.TransAm_Method_Name__c = 'exectue method - Update Batch DML Exception';
                    el.TransAm_Module_Name__c = 'Recruitement Document';
                    el.TransAm_Exception_Message__c = 'Error Record ID: '+ sr.getID() + '; -- Error Message: ' + errormsg;
                    insert el;  //insert error log instance
                }
            }
        }   
    }    

    
    /*******************************************************************************************
    * Method        :   finish
    * Description   :   Finish() method of the batch class,generally used to implement post commit logics.
    * Parameter     :   Database.BatchableContext
    * Return Type   :   Void
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC){
    //completed
    }
}