/* CLass Name   : TransAm_DriverConversionTest
 * Description  : Test class for Driver Conversion
 * Created By   : Neha Jain
 * Created On   : 17-Apr-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Neha Jain                17-Apr-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
public class TransAm_DriverConversionTest{

    @testSetup static void setup() {
   
    }

/************************************************************************************
 * Method       :    testDriverConversion
 * Description  :    Test Method to call Driver Conversion class
 * Parameter    :    NIL    
 * Return Type  :    void
 *************************************************************************************/
    
    public static testmethod void testDriverConversion(){
        
        Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        TransAm_Application__c objApp =new TransAm_Application__c();
        objApp = TransAm_Test_DataUtility.createApplications(1, 'Independent', 'Application Received',con, True)[0];  
        objApp.TransAm_Primary_Application__c=false;
        objApp.TransAm_DriverID__c = '15263';
        objApp.TransAm_EBE_Entry_ID__c = '';
        update objApp;
        
        TransAm_PreviousAddress__c prevAdd = new TransAm_PreviousAddress__c(TransAm_Application__c=objApp.id,TransAm_Address__c='My sweet home',TransAm_City__c='city of hearts',
                                            TransAm_State__c='Big Heart State',TransAm_Zip__c='11111');
        insert prevAdd;
        
        
        TransAm_Accident__c accident= new TransAm_Accident__c(TransAm_Application__c=objApp.id,TransAm_Accident_Type__c='Roll Away' , TransAm_Date__c=system.today());
        insert accident;
        
        TransAm_Felony__c felony = new TransAm_Felony__c(TransAm_Application__c=objApp.id,TransAm_Date__c=system.today(),TransAm_Felony_Type__c='Bad Check');
        insert felony;
        
        TransAm_Job_History__c jobs = new TransAm_Job_History__c(TransAm_Application__c=objApp.id, TransAm_Employed_From__c=system.today(),TransAm_Employed_To__c=system.today(),TransAm_Other_Employer__c='Test');
        insert jobs;
        
        TransAm_License__c licenses = new TransAm_License__c(TransAm_Application__c=objApp.id,TransAm_LicenseNo__c='1231',TransAm_License_Type__c='Class A',TransAm_State__c='Big Heart State',TransAm_Expiration_Date__c=system.today());
        insert licenses;
        
        TransAm_Misdemeanor__c misd = new TransAm_Misdemeanor__c(TransAm_Application__c=objApp.id,TransAm_Date__c=system.today());
        insert misd;
        
        TransAm_Violation__c voils = new TransAm_Violation__c(TransAm_Application__c=objApp.id, TransAm_Charges__c='Careless Driving',TransAm_Conviction_Date__c=system.today());
        insert voils;
        
        system.debug('objApp+++'+objApp);
        Test.startTest();
        TransAm_DriverConversion.cloneApplication(objApp.Id);
        Test.stopTest();
    }
    
    public static testmethod void testDriverConversion1(){
        List<Account> accList = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> conList = TransAm_Test_DataUtility.createContacts(1, accList[0].Id, 'Applicant',true);
        List<TransAm_Application__c> appsList = TransAm_Test_DataUtility.createApplications(1, 'Independent Contractor Only', 'Application Received', conList[0], false);
        appsList[0].TransAm_Status__c = 'null';
        insert appsList;

        List<TransAm_Recruitment_Document__c> lstRecrDocuments = [SELECT Id, TransAm_Type__c FROM TransAm_Recruitment_Document__c 
                                                                    WHERE TransAm_Application__c =: appsList[0].Id 
                                                                    AND (TransAm_Type__c = 'MVRStandard' 
                                                                        OR TransAm_Type__c ='Previous Employment Verification'
                                                                        OR TransAm_Type__c = 'School Verification Form'
                                                                        OR TransAm_Type__c = 'Telephone Reference')];
       
        
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[0].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[1].Id, true);
        TransAm_Test_DataUtility.createAttachments(1, lstRecrDocuments[2].Id, true); 

        Test.startTest();
        TransAm_DriverConversion.cloneApplication(appsList[0].Id);
        Test.stopTest();
        
    }
}