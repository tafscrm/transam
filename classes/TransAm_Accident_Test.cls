/* CLass Name   : TransAm_Accident_Test
 * Description  : Test class for Job History
 * Created By   : Pankaj Singh
 * Created On   : 22-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                22-Feb-2017              Initial version.
 *  * Manish Jain                 20-Apr-2017              Second Version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_Accident_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);        
    
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Aruna123';
        user_int.LastName = 'Tyagi';
        user_int.Username = 'xyz112dsaf23@abc.com';
        user_int.Email   = 'xyzcssfdcsa23r2@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
       
    }

    /************************************************************************************
    * Method       :    testAccidents
    * Description  :    Test Method to create Accidents
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testAccidents() {
        
        List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
                
         User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Aruna123' LIMIT 1];
        Test.startTest();
        //list<TransAm_Accident__c> accidentList = TransAm_Test_DataUtility.createAccidents(1, lstApplications[0], false);
        System.runAs(user_int){
            list<TransAm_Accident__c> accidentList = TransAm_Test_DataUtility.createAccidents(1, lstApplications[0], true);            
            accidentList[0].TransAm_Location__c = 'Location Updated';
            update accidentList; 
            TransAm_AccidentHelper.populateApplicationOnAccidents(accidentList);
            TransAm_AccidentHelper.updateApplication(accidentList);

        Test.stopTest();
        }
    }
    /************************************************************************************
    * Method       :    testAccidentException
    * Description  :    Test Method to cover exception block
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testAccidentException() {
    
        Test.startTest();
        try{
            TransAm_AccidentHelper.populateApplicationOnAccidents(null);
        }catch(Exception e){}
        Test.stopTest();
    }
    
    
    public static testmethod void testAccidentsprohibitAccountHandler() {
        
        List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
                
         User user_int = [SELECT ID FROM USER  WHERE FirstName =: 'Aruna123' LIMIT 1];
        Test.startTest();
        System.runAs(user_int){
            TransAm_AccidentHandler.prohibitBeforeInsertTrigger = true;
            TransAm_AccidentHandler.prohibitAfterInsertTrigger  = true;
            TransAm_AccidentHandler.prohibitAfterUpdateTrigger  = true;
            list<TransAm_Accident__c> accidentList = TransAm_Test_DataUtility.createAccidents(1, lstApplications[0], true);            
            accidentList[0].TransAm_Location__c = 'Location Updated';
            update accidentList; 
            TransAm_AccidentHelper.populateApplicationOnAccidents(accidentList);
            TransAm_AccidentHelper.updateApplication(accidentList);
           

        Test.stopTest();
        }
    }
}