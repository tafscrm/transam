/******************************************************************************************
* Class Name   :     TransAm_AssignApplicationsToQueueBatchTest
* Create By    :     Ajay B R
* Create Date  :     06/21/2017
* Description  :     Test class for TransAm_AssignApplicationsToQueueBatch class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                          06/21/2017          Initial version.
*****************************************************************************************/
@isTest(seeAllData = False)
private class TransAm_AssignAppsToQueueBatchTest{

 /*******************************************************************************************
    * Method        :   dataSetup
    * Description   :   Purpose of this method is to setup test data.
    * Parameter     :   null
    * Return Type   :   void
    ******************************************************************************************/
@testSetup
    private static void testDataSetUp(){
        Group recQueue = new Group(name = 'Driver Recruiter Queue');
        insert recQueue;
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        User integrationUser = new User();
        integrationUser.firstname = 'Arun';
        integrationUser.lastname = 'Tyagi';
        integrationUser.username = 'xyzrtyu@abc.com';
        integrationUser.email = 'xyz@deloitte.com';
        integrationUser.alias = 'xyz';
        integrationUser.TimeZoneSidKey = 'America/Los_Angeles';
        integrationUser.LocaleSidKey = 'En_US';
        integrationUser.profileID = prof.Id;
        integrationUser.LanguageLocaleKey = 'En_US';
        integrationUser.EmailencodingKey = 'UTF-8';
        
        insert integrationUser;
    }

/************************************************************************************
    * Method       :    testAssignAppsToQueueBatch
    * Description  :    This method is used to test the PopulateDummyAccountOnContact_Update.
    * Parameter    :    NIL    
    * Return Type  :    void
*************************************************************************************/
    public static testMethod void testAssignAppsToQueueBatch(){
        List<TransAm_Hiring_Areas_Reference__c> hiringAreaRef = TransAm_Test_DataUtility.insertHiringAreasReferences(true);
        List<Account> accList = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> conList = TransAm_Test_DataUtility.createContacts(1, accList[0].Id, 'Applicant',true);
        List<TransAm_Application__c> appsList = TransAm_Test_DataUtility.createApplications(10, 'Independent Contractor Only', 'Application Received', conList[0], false);
        for(TransAm_Application__c app : appsList){
            app.TransAm_State__c = 'KS';      
        }
        appsList[0].TransAm_Follow_Up_Date__c = System.today() - 1;  
        if(appsList.size()>0){
                insert appsList;
        }
        system.assertNotEquals(0, database.countQuery('select count() from TransAm_Application__c'), 'Records not created!!');
        List<TransAm_Generic_Custom_Setting__c> customSetingObjList = new List<TransAm_Generic_Custom_Setting__c>();
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Queue Assignment Duration', TransAm_Message_String__c = '0'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Queue Assignment Status Update', TransAm_Message_String__c = 'Application Received'));
        //customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Recruiter Queue Assignment Query', TransAm_Message_String__c = 'SELECT Id, TransAm_Status__c, OwnerId, TransAm_Last_Status_Change__c,TransAm_Follow_Up_Date__c FROM TransAm_Application__c where TransAm_Status__c = \'Recruiter Review\''));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Recruiter Queue Name', TransAm_Message_String__c = 'Driver Recruiter Queue'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'No of Business Days To Skip', TransAm_Message_String__c = '3'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'No of Business Hours a Day', TransAm_Message_String__c = '10'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Application Recruiter Review Status', TransAm_Message_String__c = 'Recruiter Review'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Application Status No Show', TransAm_Message_String__c = 'No Show'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Application Status Not Interested', TransAm_Message_String__c = 'Not Interested'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'App Status Schedule Orientation', TransAm_Message_String__c = 'Schedule Orientation'));
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'TaskTypeFollowUp', TransAm_Message_String__c = 'Follow-up'));
        insert customSetingObjList;
        
        User intUser = [Select Id from User where FirstName='Arun' limit 1];
        system.runAs(intUser){
            test.startTest();
                List<TransAm_Application__c> updatedAppList = [select id, Name, TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Application Received'];
                for(TransAm_Application__c app : updatedAppList){
                    app.TransAm_Status__c = 'Recruiter Review';
                }
                update updatedAppList;
                system.assertNotEquals(0,updatedAppList.size() , 'Records not updated!!');
                List<TransAm_Application__c> updatedAppList3 = [select id, Name, TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Recruiter Review'];
                //system.assertNotEquals(0, database.countQuery('select count() from TransAm_Application__c where TransAm_Status__c = \'Recruiter Review\''), 'No records to process!!');
                if(updatedAppList3!=null){
                    for(TransAm_Application__c app : updatedAppList3){
                        //if(app!=null)
                        Task taskObj = new Task();
                        taskObj.subject = 'Call';
                        taskObj.status = 'Open';
                        taskObj.activityDate = Date.today().addDays(-2);
                        taskObj.priority = 'Normal';
                        taskObj.whatId = app.Id;
                        //taskObj.whoId = intUser.Id;
                        taskObj.type = 'Follow-up';
                        
                        insert taskObj;
                            app.TransAm_Last_Status_Change__c = System.now()-10;
                    }
                    
                    update updatedAppList3;
                }
                system.assertNotEquals(0, database.countQuery('select count() from TransAm_Application__c where TransAm_Status__c = \'Recruiter Review\''), 'No records to process!!');
            TransAm_AssignAppsToQueueScheduler schedulerObj = new TransAm_AssignAppsToQueueScheduler();
            String scheduleTime = '0 5 * * * ?';
            System.schedule('Test Apps Queue assignment Schedule', scheduleTime, schedulerObj);
            test.stopTest();
            List<TransAm_Application__c> updatedAppList2 = [select id, Name, ownerId,TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Application Received'];
            //TransAm_Application__c updatedApp = [select id, Name, TransAm_Status__c, TransAm_Last_Status_Change__c from TransAm_Application__c where TransAm_Status__c = 'Recruiter Review' limit 1];
            Group recQueue = [select id from Group where name = 'Driver Recruiter Queue' and Type='Queue'];
            for(TransAm_Application__c app1: updatedAppList2){
                system.assertEquals(app1.ownerID, recQueue.Id);
            }
        }
    }
}