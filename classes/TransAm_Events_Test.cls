/* CLass Name   : TransAm_Events_Test
 * Description  : Test class for Events trigger
 * Created By   : Pankaj Singh
 * Created On   : 22-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh                22-Feb-2017              Initial version.
 *
 *****************************************************************************************/
@isTest
private class TransAm_Events_Test{
    /************************************************************************************
    * Method       :    setup
    * Description  :    setup test data
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup static void setup() {
        list<Account> accountList = TransAm_Test_DataUtility.createAccounts(1,True);  
        List<Contact> contactList = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'School Contact', True);        
    }

    /************************************************************************************
    * Method       :    testAccidents
    * Description  :    Test Method to create events
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testAccidents() {
        
        List<Account> accountList = [SELECT ID FROM Account LIMIT 1];
        list<Contact> contactList = [SELECT ID FROM Contact LIMIT 1];
        list<Event> eventList = TransAm_Test_DataUtility.createEvents(1,contactList[0].Id,accountList[0].Id,'Test',false);
        Test.startTest();
            insert eventList;
        Test.stopTest();
    }
    /************************************************************************************
    * Method       :    testEventHelperException
    * Description  :    Test Method to cover exception block
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testmethod void testEventHelperException() {
    
        Test.startTest();
        try{
            TransAm_EventHelper.populateVisitCountOnSchool(null);
            TransAm_EventHelper.populateVisitCountOnDriver(null);
        }catch(Exception e){}
        Test.stopTest();
    }
}