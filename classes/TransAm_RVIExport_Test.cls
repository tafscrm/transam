@isTest(seeAllData=false)
private class TransAm_RVIExport_Test {
    @testSetup
    private static void testRVIExport(){
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstApplicantContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Hiring_Areas_Reference__c> lstHiringArea = TransAm_Test_DataUtility.insertHiringAreasReferences(true);
        List<TransAm_Application__c> lstApplications = new List<TransAm_Application__c>();
        lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Application Received', lstApplicantContacts[0],true);
        List<TransAm_Recruitment_Document__c> recDocList = new List<TransAm_Recruitment_Document__c>();
        recDocList = TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Agreement',false);
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Application Summary',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Background Reports',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Fuel Card Agreement',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Hotel Agreement',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Initial Urinalysis Consent Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Kansas I-9',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Texas Arbitration',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Benefit Information',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Web In Transit Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'Settlement Designation Authorization Form',false));
        recDocList.addAll(TransAm_Test_DataUtility.createRecruitmentDocuments(1,lstApplications[0].Id,'System Document',false));
        
        INSERT recDocList;
        List<Attachment> lstAttachments = new List<Attachment>();
        for(TransAm_Recruitment_Document__c rec : recDocList){
            lstAttachments.add(new Attachment(        
                                    Name        = 'Test.pdf',
                                    Body        = Blob.valueOf('Unit Test Attachment Body'+ rec.TransAm_Type__c),
                                    ParentId    = rec.Id              
                                ));

        }
        INSERT lstAttachments;
    }
    
    private Static testmethod void testStatusDQDocGeneration(){
        test.startTest();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test DQ';
        currentApp.TransAm_Status__c ='DQ';
        currentApp.TransAm_Driver_Type__c = 'Company';
        currentApp.TransAm_SSN__c = '989898989';
        UPDATE currentApp;
        TransAm_RVIExport.generateXML(currentApp.Id);
        List<Attachment> att = [SELECT Id from Attachment WHERE ParentId IN (SELECT Id FROM TransAm_Recruitment_Document__c 
                                                                             WHERE TransAm_Type__c =: 'System Document' AND TransAm_Application__c =: currentApp.Id)];
        test.stopTest();
    }
    
    private Static testmethod void testApplicationCompleteDocGeneration(){
        test.startTest();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='Application Complete';
        currentApp.TransAm_Driver_Type__c = 'Company';
        currentApp.TransAm_SSN__c = '989898989';
        currentApp.Transam_Hire_date__c = DAte.today();
        UPDATE currentApp;
        System.debug('currentApp' + currentApp.TransAm_Status__c);
        TransAm_RVIExport.generateXML(currentApp.Id);
        test.stopTest();
        
    }
    private Static testmethod void testApplicationCompleteDocGeneration2(){
        test.startTest();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='Application Complete';
        currentApp.TransAm_Driver_Type__c = 'Owner';
        currentApp.TransAm_Orientation_Date__c = Date.today();
        currentApp.TransAm_SSN__c = '989898989';
        currentApp.Transam_Hire_date__c = DAte.today();
        UPDATE currentApp;
        TransAm_RVIExport.generateXML(currentApp.Id);
        test.stopTest();
        
    }
    private Static testmethod void testInProgressDocGeneration(){
        test.startTest();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='Application Received';
        currentApp.TransAm_Driver_Type__c = 'Owner';
        currentApp.TransAm_SSN__c = '989898989';
        UPDATE currentApp;
        TransAm_RVIExport.generateXML(currentApp.Id);
        test.stopTest();
        
    }
    private Static testmethod void testNoContactDocGeneration(){
        test.startTest();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='No Contact';
        currentApp.TransAm_Driver_Type__c = 'Owner';
        currentApp.TransAm_SSN__c = '989898989';
        currentApp.TransAm_Orientation_Date__c = Date.today();
        UPDATE currentApp;
        TransAm_RVIExport.generateXML(currentApp.Id);
        test.stopTest();
        
    }
    private Static testmethod void testScheduleRVIDocGeneration(){
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,TransAm_Archived_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='DQ';
        currentApp.TransAm_Driver_Type__c = 'Owner';
        currentApp.TransAm_SSN__c = '989898989';
        currentApp.TransAm_Orientation_Date__c = Date.today();
        currentApp.TransAm_Archived_Date__c = Date.today();
        UPDATE currentApp;
        Test.startTest();
        TransAm_ScheduleRefreshRVIXMLBatch rviBatch = new TransAm_ScheduleRefreshRVIXMLBatch();
        String scheduleTime = '0 0 23 * * ?';
        System.schedule('TestRVI SCBatch', scheduleTime, rviBatch);
        Test.stopTest();            
    }
    private Static testmethod void testBatchRVIDocGeneration(){
        
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,TransAm_Archived_Date__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c
                                             FROM TransAm_Application__c LIMIT 1];
        currentApp.TransAm_First_Name__c ='Test Complete';
        currentApp.TransAm_Status__c ='DQ';
        currentApp.TransAm_Driver_Type__c = 'Owner';
        currentApp.TransAm_SSN__c = '989898989';
        currentApp.TransAm_Orientation_Date__c = Date.today();
        currentApp.TransAm_Archived_Date__c = Date.today();
        UPDATE currentApp;
        Test.startTest();
        String query = 'Select id from TransAm_Application__c LIMIT 1';
        TransAm_RefreshRVIXMLBatch rviBatch = new TransAm_RefreshRVIXMLBatch(query);
        Database.executeBatch(rviBatch,1);
        Test.stopTest();            
    }
}