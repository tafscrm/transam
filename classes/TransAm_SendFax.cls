global class TransAm_SendFax {

    Static final String RD_TYPE = 'Previous Employment Verification';
    webservice static void sendFaxToEmployer(Id jobHistoryId, String faxNumber, string faxSubject){
        system.debug('faxnumber**'+faxNumber+'    faxsubject&&&  '+faxSubject);
        ID signedDocId, verifyDocId;
        Boolean addBarcode;
        TransAm_Job_History__c objApp = [SELECT TransAm_Application__c FROM TransAm_Job_History__c WHERE ID=:jobHistoryId LIMIT 1];
        Id appId = objApp.TransAm_Application__c;
        List<TransAm_Recruitment_Document__c> recrList = [Select Id,TransAm_Type__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =: appId AND (TransAm_Type__c = 'Background Check Release Form' OR TransAm_Type__c ='Previous Employment Verification')];
        for(TransAm_Recruitment_Document__c objR : recrList){
            if(objR.TransAm_Type__c == 'Background Check Release Form')
                signedDocId =objR.Id;
            else if(objR.TransAm_Type__c == 'Previous Employment Verification')
                verifyDocId = objR.Id;
        }
        if(signedDocId != null){
        	List<Attachment> attList = new list<Attachment>();
        	Attachment att;
            if(faxSubject.contains('HireRight')){
            	attList = [Select Id FROM Attachment Where ParentId =: jobHistoryId AND name LIKE 'HR_Emp_Ver_%' ORDER BY CreatedDate DESC LIMIT 1]; 	
            	addBarcode = FALSE;
            }
            else{
        		attList = [Select Id FROM Attachment Where ParentId =: jobHistoryId AND name LIKE 'Past_Emp_Ver_%' ORDER BY CreatedDate DESC LIMIT 1];	
            	addBarcode = TRUE;
            }
                system.debug('attList***'+attList);
        	if(!attList.isEmpty()){
                att = attList[0];
	            System.debug('###'+att);
	            insertFaxRecord(att.Id,faxNumber,verifyDocId,faxSubject,addBarcode);
        	}
            else{
				if(!test.isRunningTest())
					throw new TransAM_CustomException('No Attachment');	
            }
        }       
    }
    
    webservice static void sendFaxToSchoolContact(Id appId){
        ID signedDocId, verifyDocId;
        List<TransAm_Recruitment_Document__c> recrList = [Select Id,TransAm_Type__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =: appId AND (TransAm_Type__c = 'School Verification Release Form' OR TransAm_Type__c = 'School Verification Form')];
        System.debug('%%%'+recrList);
        for(TransAm_Recruitment_Document__c objR : recrList){
            if(objR.TransAm_Type__c == 'School Verification Release Form')
                signedDocId =objR.Id;
            else if(objR.TransAm_Type__c == 'School Verification Form')
                verifyDocId = objR.Id;
            System.debug('##RA'+signedDocId +'-->'+verifyDocId);
        }
        System.debug('##RA2 -->'+signedDocId +'-->'+verifyDocId);
        if(signedDocId != null){
            Attachment att;
            List<Attachment> attList = [Select Id FROM Attachment Where PArentId =: signedDocId LIMIT 1];
            if(!attList.isEmpty())
                att = attList[0];
            TransAm_Application__c objApp = [SELECT Id, TransAm_Applicant__r.Account.Fax FROM TransAm_Application__c WHERE ID=: appId LIMIT 1];
      
                String faxNumber = objApp.TransAm_Applicant__r.Account.Fax;
                
                efaxapp__Sent_Fax__c fax = new efaxapp__Sent_Fax__c(efaxapp__Fax_Number__c = faxNumber,efaxapp__Subject__c = 'Fax to School',efaxapp__Send_Date__c = Datetime.now(),efaxapp__Attachment_ID__c = att.Id,efaxapp__Status__c = 'Sending',
                                                                    efaxapp__Sent_With_Outbound_Message__c = True, 
                                                                    efaxapp__Org_Fax_Number__c = Label.TransAm_Org_Fax_Number, 
                                                                    efaxapp__Barcode_Position_Top__c = '535', 
                                                                    efaxapp__Barcode_Position_Left__c = '325',
                                                                    efaxapp__Barcode_Size_Width__c = '80',
                                                                    efaxapp__Preview_Image_URL__c = '/servlet/servlet.FileDownload?file='+att.Id,
                                                                    efaxapp__Fax_URL__c = '/servlet/servlet.FileDownload?file='+att.Id,
                                                                    TransAm_Recruitment_Document__c = verifyDocId
                                                                   );
                System.debug('efaxapp__Sent_Fax__c -->'+fax);
                insert fax;
            
        }
    }
	/** 
     *  Method to insert sent fax record to send fax.
     *  @param: Application Id
     *  @return: Void
     *	@author : Nitesh
     */
    public static void insertFaxRecord(String attachmentid, String FaxNumber, String RDId, String FaxSubject, Boolean addBarcode){
    	
	            efaxapp__Sent_Fax__c fax = new efaxapp__Sent_Fax__c(
            	efaxapp__Attachment_ID__c = attachmentid,
            	efaxapp__Org_Fax_Number__c = label.TransAm_Org_Fax_Number,
            	TransAm_Recruitment_Document__c = RDId,
            	efaxapp__Send_Date__c = Datetime.now(),
            	efaxapp__Fax_Number__c = faxNumber,
            	efaxapp__Subject__c = FaxSubject,
                efaxapp__Sent_With_Outbound_Message__c = True,
            	efaxapp__Status__c = 'Sending',                 
                efaxapp__Preview_Image_URL__c = '/servlet/servlet.FileDownload?file='+attachmentid,
                efaxapp__Fax_URL__c = '/servlet/servlet.FileDownload?file='+attachmentid
            );
        if(addBarcode){
            fax.efaxapp__Barcode_Position_Top__c = '56';
            fax.efaxapp__Barcode_Position_Left__c = '385';
            fax.efaxapp__Barcode_Size_Width__c = '72';
        }
    	try{
    		insert fax;
    		System.debug('efaxapp__Sent_Fax__c -->'+fax);
    	}
        catch(Exception e){
            TransAm_Error_Log__c logFaxError = new TransAm_Error_Log__c( 
            TransAm_Class_Name__c = 'TransAm_SendFax',
            TransAm_Exception_Message__c = e.getMessage(),
            TransAm_Method_Name__c = 'sendFaxToEmployer',
            TransAm_Module_Name__c = 'Send Fax to HireRight/Employer'
            );
            insert logFaxError;
            if(!test.isRunningTest())
				throw e;    
        }
    }
}