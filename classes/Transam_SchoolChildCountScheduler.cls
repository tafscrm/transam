/******************************************************************************************
* Create By    :     Raushan Anand
* Create Date  :     03/07/2017
* Description  :     Scheduler Class to delete archived attachments
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Raushan Anand                    03/07/2017           Initial version.
*****************************************************************************************/
global class Transam_SchoolChildCountScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new Transam_SchoolChildCount(),5);
    }

}