global class TransAm_RVIExport{
    webservice static void generateXML(Id appId){
		Map<String,String> docTypeMap = new Map<String,String>();
        TransAm_Application__c currentApp = [SELECT Id,TransAm_First_Name__c,TransAm_Last_Name__c,TransAm_Status__c,TransAm_Orientation_Date__c,TransAm_Hire_Date__c,TransAm_Status_Reason__c,
                                             Name,TransAm_DriverID__c,TransAm_Driver_Type__c,TransAm_RVI_Export__c,TransAm_RVI_Export_Date__c,TransAm_SSN__c,TransAm_RVI_Export_Hour__c
                                             FROM TransAm_Application__c WHERE ID=:appId LIMIT 1];
        List<TransAm_Recruitment_Document__c> recruitmentDocList = [SELECT Id, TransAm_Type__c, TransAm_SSN__c, TransAm_Application__c,TransAm_Is_Exported__c FROM TransAm_Recruitment_Document__c WHERE TransAm_Application__c =:appId];
    	validateAttachmentNames(currentApp);
        List<DocumentTypeMapping__mdt> metaDocSetting = [SELECT Id,
                                                            MasterLabel,
                                                  			Doc_Type__c
                                                            FROM DocumentTypeMapping__mdt];
        for(DocumentTypeMapping__mdt objMD : metaDocSetting){
            docTypeMap.put(objMD.MasterLabel,objMD.Doc_Type__c);
        }
        List<Attachment> attachList = [SELECT Id, Name, ParentId FROM Attachment where ParentId IN: recruitmentDocList];
        List<RVI_XML_Settings__mdt> metasetting = [SELECT Id, TransAm_All_Drivers_Hired_System__c, TransAm_Company_Hired_System__c,TransAm_Document_Name__c,
                                                  TransAm_Owner_Hired_System__c,TransAm_RVI_Scan_Code__c,TransAm_Task_21_RejectedDQed__c,TransAm_Task_25_No_Contact__c,TransAm_Task_X_In_Progress__c
                                                  FROM RVI_XML_Settings__mdt];
        List<formatXML> xmlNodes = new List<formatXML>();
        List<Attachment> updateAttachList = new List<Attachment>();
        for(Attachment objAttachment : attachList){
            for(TransAm_Recruitment_Document__c objR : recruitmentDocList){
                if(objR.Id == objAttachment.ParentId && objR.TransAm_Type__c != 'System Document'){
                    formatXML objXML = new formatXML();
                    objXML.recruitDocType = objR.TransAm_Type__c;
                    objXML.docName = objAttachment.Name;
                    objXML.docId = objR.Id;
                    String nameOfDoc = objAttachment.Name.substringAfterLast('.').toUpperCase();
                    if(docTypeMap.containsKey(nameOfDoc))
                    	objXML.typeofDoc = docTypeMap.get(nameOfDoc);
                    else
                        objXML.typeofDoc ='O';
                    xmlNodes.add(objXML);
                    if(!System.isBatch())
                    	objR.TransAm_Is_Exported__c = true;
                }
            }
        }
        System.debug('###RA--> currentApp.TransAm_Status__c'+currentApp.TransAm_Status__c + currentApp.TransAm_Status_Reason__c);
        if(currentApp.TransAm_Status__c == 'Application Complete'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_All_Drivers_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_All_Drivers_Hired_System__c;   
                    }
                    if(currentApp.TransAm_Driver_Type__c == 'Company'){
                        if(currentApp.TransAm_Hire_Date__c!=null)
                            objXML.hireDate = (datetime.newInstance(currentApp.TransAm_Hire_Date__c.year(), currentApp.TransAm_Hire_Date__c.month(),currentApp.TransAm_Hire_Date__c.day())).format('MMddyy');
                        if(String.isNotBlank(objSetting.TransAm_Company_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                            objXML.driverType = objSetting.TransAm_Company_Hired_System__c;
                            objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                            //if(currentApp.TransAm_Orientation_Date__c!=null)
                            //objXML.hireDate = (datetime.newInstance(currentApp.TransAm_Orientation_Date__c.year(), currentApp.TransAm_Orientation_Date__c.month(),currentApp.TransAm_Orientation_Date__c.day())).format('MM/dd/yy');
                        }
                    }
                    else if(currentApp.TransAm_Driver_Type__c == 'Owner'){
                        if(currentApp.TransAm_Hire_Date__c != null){
                                Date hireUpdatedDate=currentApp.TransAm_Hire_Date__c.addDays(2);
                            	objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy') ;
                        }
                    	if(String.isNotBlank(objSetting.TransAm_Owner_Hired_System__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                            objXML.driverType = objSetting.TransAm_Owner_Hired_System__c;
                            objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                            /*if(currentApp.TransAm_Orientation_Date__c != null){
                                Date hireUpdatedDate=currentApp.TransAm_Orientation_Date__c.addDays(2);
                            	objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MM/dd/yy') ;
                            }*/
                    	}
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'Application Received'|| currentApp.TransAm_Status__c == 'Recruiter Review' || currentApp.TransAm_Status__c == 'Manager Review' || currentApp.TransAm_Status__c == 'Application Approved - Pending' 
               || currentApp.TransAm_Status__c == 'Schedule Orientation' || currentApp.TransAm_Status__c == 'Pending Orientation' || currentApp.TransAm_Status__c == 'HR Review' || currentApp.TransAm_Status__c == 'HR Approval' || currentApp.TransAm_Status__c == 'Merged'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_X_In_Progress__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_X_In_Progress__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                        
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'DQ'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_21_RejectedDQed__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_21_RejectedDQed__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                    }
                }
                
            }
        }
        else if(currentApp.TransAm_Status__c == 'No Contact' || currentApp.TransAm_Status__c == 'Incorrect Information' || currentApp.TransAm_Status__c == 'Not Interested'){
            for(RVI_XML_Settings__mdt objSetting : metasetting){
                for(formatXML objXML : xmlNodes){
                    if(String.isNotBlank(objSetting.TransAm_Task_25_No_Contact__c) && objXML.recruitDocType == objSetting.TransAm_Document_Name__c){
                        objXML.scanCode = objSetting.TransAm_RVI_Scan_Code__c;
                        objXML.folderLocation = objSetting.TransAm_Task_25_No_Contact__c;
                        Date hireUpdatedDate=Date.today();
                        objXML.hireDate = (datetime.newInstance(hireUpdatedDate.year(), hireUpdatedDate.month(),hireUpdatedDate.day())).format('MMddyy');
                    }
                }
                
            }
        }
        System.debug('##RA-->'+xmlNodes);
        String fileContent,fileContent2;
        for(formatXML objXML : xmlNodes){
            if(String.isBlank(objXML.driverType)){
                objXML.SC=objXML.folderLocation;
                if(String.isBlank(fileContent)){
                    fileContent=getxmlFormattedString(currentApp,objXML);
                    
                }
                else{
                    fileContent +=getxmlFormattedString(currentApp,objXML);
                    
                }
            }
            else{
                objXML.SC=objXML.driverType;
                if(String.isBlank(fileContent2)){
                    fileContent2=getxmlFormattedString(currentApp,objXML);
                    System.debug('##RA fileContent2-->1'+fileContent2);
                    
                }
                else{
                    fileContent2 +=getxmlFormattedString(currentApp,objXML);
                    System.debug('##RA fileContent2-->2'+fileContent2);
                }
            }
        }
        TransAm_Recruitment_Document__c objRecruit;
        objRecruit = [SELECT Id FROM TransAm_Recruitment_Document__c WHERE TransAm_Type__c = 'System Document' AND TransAm_Application__c =: appId LIMIT 1];
        System.debug('objRecruit'+objRecruit);
        if(String.isNotBlank(fileContent)){
            fileContent = fileContent.remove('null');
            List<Attachment> existingSystemDoc = [SELECT ID FROM Attachment WHERE ParentId =: objRecruit.Id];
            if(existingSystemDoc.size()>0){
                DELETE existingSystemDoc;
            }
            UPDATE recruitmentDocList;
            Attachment att = new Attachment(Name = currentApp.Name+'_Index.xml', body = Blob.valueOf(fileContent), parentId = objRecruit.Id);
            updateAttachList.add(att);
        }
        if(String.isNotBlank(fileContent2)){
            fileContent2 = fileContent2.remove('null');
            Attachment att2 = new Attachment(Name = currentApp.Name+'_Index_2.xml', body = Blob.valueOf(fileContent2), parentId = objRecruit.Id);
            updateAttachList.add(att2);
            if(String.isBlank(fileContent)){
                List<Attachment> existingSystemDoc = [SELECT ID FROM Attachment WHERE ParentId =: objRecruit.Id];
            	if(existingSystemDoc.size()>0){
                	DELETE existingSystemDoc;
            	}
            }
        }
        if(!updateAttachList.isEmpty()){
            Database.INSERT(updateAttachList);
            currentApp.TransAm_RVI_Export_Date__c = Date.today();
            currentApp.TransAm_RVI_Export__c = true;
            currentApp.TransAm_RVI_Export_Hour__c = DateTime.now().Hour();
            UPDATE currentApp;
        }
    }
    public static String getxmlFormattedString(TransAm_Application__c currentApp,formatXML xmlObj){
        try{
            String resourceName='server_'+xmlObj.SC;
            String content=[SELECT Body FROM StaticResource WHERE Name =:resourceName].Body.toString();
            // update value in content from current application
            if(currentApp!=null){
               Map<String, object> currentAppMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(currentApp));
               for(String currentAppField: currentAppMap.keyset()){
                   content=content.replace('{currentApp.'+currentAppField+'}', (JSON.serialize(currentAppMap.get(currentAppField))).replace('"',''));
               }
            }
            // update value in content from format xml object
            if(xmlObj!=null){
               Map<String, object> xmlObjMap=(Map<String, object>)JSON.deserializeUntyped(JSON.serialize(xmlObj));
               for(String xmlObjField: xmlObjMap.keyset()){
                   content=content.replace('{objXML.'+xmlObjField+'}', (JSON.serialize(xmlObjMap.get(xmlObjField))).replace('"',''));
               }
            }
            content=(content).replaceAll('\\{[0-9A-Za-z\\_.]*\\}','');
            return content;
        }catch(Exception e){
            return '';
        }
    }
    private static void validateAttachmentNames(Transam_Application__c currentApp){
        List<Attachment> attList = [SELECT Id, Name FROM Attachment 
                                    WHERE PARENTID IN
                                    (SELECT ID FROM TransAm_Recruitment_Document__c 
                                    WHERE Transam_Application__c =: currentApp.Id)];
        for(Attachment objAtt : attList){
            objAtt.Name = objAtt.Name.Replace(' ','_');
            Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9_.-]');
            Matcher matcher = nonAlphanumeric.matcher(objAtt.Name);
            objAtt.Name = matcher.replaceAll('');
            if(!objAtt.Name.contains(currentApp.Name)){
                objAtt.Name = currentApp.Name+'_'+objAtt.Name;
            }
            if(objAtt.Name.length()>Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)){
                objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')).substring(0,Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)-5)+'.'+objAtt.Name.substringafterlast('.');
            }
            for(Attachment dupAtt : attList){
                if(objAtt.Name == dupAtt.Name && objAtt.Id != dupAtt.Id){
                    if(objAtt.Name.length() < Integer.valueOf(Label.Transam_RVI_Attachment_Name_Length)-5){
                        objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')) + String.ValueOf(Math.random()).Substring(3,7)+'.'+objAtt.Name.substringafterlast('.');
                    }
                    else{
                        objAtt.Name = objAtt.Name.Substring(0,objAtt.Name.LastIndexOf('.')).substring(0,28)+String.ValueOf(Math.random()).Substring(3,7)+'.'+objAtt.Name.substringafterlast('.');
                    }
                }
            }
        }
        UPDATE attList;
    }
    global class formatXML{
        public String recruitDocType{get;set;}
        public Id docId {get;set;}
        public String docName {get;set;}
        public String scanCode{get;set;}
        public String typeofDoc{get;set;}
        public String folderLocation{get;set;}
        public String driverType {get;set;}
        public String hireDate {get;set;}
        public String SC{get; set;}
        
    }

}