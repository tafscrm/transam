/* CLass Name   : TransAm_HireRightUpdateTest
* Description  : Test class for TransAm_HireRightUpdate
* Created By   : Monalisa Das
* Created On   : 09-May-2017
*
*  Modification Log :
*  --------------------------------------------------------------------------------------
*  * Developer                    Date                    Description
*  * ------------------------------------------------------------------------------------                 
*  * Monalisa Das                09-May-2017              Initial version.
*  
*
*****************************************************************************************/
@isTest(seeAllData=false)
public class TransAm_HireRightUpdateTest {
    
    /************************************************************************************
* Method       :    setup
* Description  :    setup test data
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/
    @testSetup static void setup() {
        List<Account> lstAccounts = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> lstContacts = TransAm_Test_DataUtility.createContacts(1, lstAccounts[0].Id, 'Applicant', true);
        List<TransAm_Application__c> lstApplications = TransAm_Test_DataUtility.createApplications(1, 'Company Driver Only', 'Prospect', lstContacts[0], True);        
        
        Profile p = [SELECT id,name FROM Profile where name = 'TransAm_Integration_User'];
        system.assert(p.Id != null);
        User user_int = new User();
        user_int.FirstName = 'Test';
        user_int.LastName = 'User';
        user_int.Username = 'testHRuser@abc.com';
        user_int.Email   = 'testuser@deloitte.com';
        user_int.Alias   = 'xyz';
        user_int.TimeZoneSidKey   = 'America/Los_Angeles';
        user_int.LocaleSidKey   = 'en_US';
        user_int.EmailEncodingKey   = 'UTF-8';
        user_int.ProfileId = p.Id;                               
        user_int.LanguageLocaleKey = 'en_US';    
        System.debug('Logged In Userid:'+ UserInfo.getUserId());
        insert user_int;
        
        
    }
    
    
    /************************************************************************************
* Method       :    updateHireRightOrdersTest
* Description  :    Test Method to invoke Update hirerightOrder REST Service
* Parameter    :    NIL    
* Return Type  :    void
*************************************************************************************/ 
    /*public static testmethod void updateHireRightOrdersTest() {
        
        List<TransAm_Application__c> lstApplications = [SELECT ID , TransAm_EBE_Entry_ID__c FROM TransAm_Application__c LIMIT 1];
        
       User user_int = [SELECT ID FROM USER  WHERE Username =: 'testHRuser@abc.com' LIMIT 1];
        Test.startTest();
        
        
        List<TransAm_HireRight_Orders__c> lstHROrders = TransAm_Test_DataUtility.createHireRightOrders(1, lstApplications[0].Id, 'MVR Standard' ,True); 
        //insert lstHROrders;
        System.runAs(user_int){ 
            TransAm_HireRight_Orders__c queriedOrder = [select id, name from TransAm_HireRight_Orders__c where id=:lstHROrders[0].id];
            RestRequest req = new RestRequest();
            req.httpMethod = 'POST';
            req.requestUri = 'https://XXXX.salesforce.com/services/apexrest/transam/hireright/HireRightOrderID/'+queriedOrder.Name;
            req.requestBody = Blob.valueOf(JSON.serializePretty(''));
            RestContext.request = req;
            RestContext.response = new RestResponse();
            
            TransAm_HireRightUpdate.updateHireRightOrders();
            TransAm_HireRightUpdate.test(queriedOrder.Name);
            
            
            Test.stopTest();
        }
    }*/
}