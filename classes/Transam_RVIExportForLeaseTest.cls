/******************************************************************************************
* Class Name   :     Transam_RVIExportForLeaseTest
* Create By    :     Ajay B R
* Create Date  :     09/28/2017
* Description  :     Test class is the test class for Transam_RVIExportForLease class.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                          09/28/2017          Initial version.
*****************************************************************************************/
@isTest(seeAllData = False)
public class Transam_RVIExportForLeaseTest{

    /*******************************************************************************************
    * Method        :   dataSetup
    * Description   :   Purpose of this method is to setup test data.
    * Parameter     :   null
    * Return Type   :   void
    ******************************************************************************************/
    @testSetup  
    private static void testDataSetUp(){
        Profile prof = [Select Id, name from Profile where name = 'TransAm_Integration_User'];
        User integrationUser = new User();
        integrationUser.firstname = 'Arun';
        integrationUser.lastname = 'Tyagi';
        integrationUser.username = 'xyzrtyu@abc.com';
        integrationUser.email = 'xyz@deloitte.com';
        integrationUser.alias = 'xyz';
        integrationUser.TimeZoneSidKey = 'America/Los_Angeles';
        integrationUser.LocaleSidKey = 'En_US';
        integrationUser.profileID = prof.Id;
        integrationUser.LanguageLocaleKey = 'En_US';
        integrationUser.EmailencodingKey = 'UTF-8';
        
        insert integrationUser;
        
        List<TransAm_Generic_Custom_Setting__c> customSetingObjList = new List<TransAm_Generic_Custom_Setting__c>();
        customSetingObjList.add(new TransAm_Generic_Custom_Setting__c(name = 'Risk Mgmt Email Docs', TransAm_Message_String__c = 'ICA.docx,OneBeaconEnrollForm.docx'));
        insert customSetingObjList ;
    }
    
    /************************************************************************************
    * Method       :    testRVIExportLeaseGenerateXML
    * Description  :    This method is used to test the generateXML method.
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    public static testMethod void testRVIExportLeaseGenerateXML(){
        List<TransAm_Hiring_Areas_Reference__c> hiringAreaRef = TransAm_Test_DataUtility.insertHiringAreasReferences(true);
        List<Account> accList = TransAm_Test_DataUtility.createAccounts(1, true);
        List<Contact> conList = TransAm_Test_DataUtility.createContacts(1, accList[0].Id, 'Applicant',true);
        List<TransAm_Application__c> listAppObj = TransAm_Test_DataUtility.createApplications(1, 'Independent Contractor Only', 'Application Received', conList[0], false);
            listAppObj[0].TransAm_State__c = 'KS';      
            insert listAppObj;
        User intUser = [Select Id from User where FirstName='Arun' limit 1];
        //Id conDriverRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Driver').getRecordTypeId();
        //RecordType rt = [select id,Name from RecordType where SobjectType='TransAm_ONE_Leasing__c' and Name='A' Limit 1];
        //Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.TransAm_ONE_Leasing__c; 
        Map<String,Schema.RecordTypeInfo> oneLeasingRecordTypeInfo = Schema.SObjectType.TransAm_ONE_Leasing__c.getRecordTypeInfosByName(); 
        String leaseAgreementRecType = oneLeasingRecordTypeInfo.get('Lease Agreement').getName();
        String icAgreementRecType = oneLeasingRecordTypeInfo.get('IC Agreement').getName();
        
        System.runAs(intUser){
        
            TransAm_ONE_Leasing__c oneLeaseObjIC = new TransAm_ONE_Leasing__c();
            oneLeaseObjIC.recordTypeID = oneLeasingRecordTypeInfo.get('IC Agreement').getRecordTypeId();
            oneLeaseObjIC.TransAm_Active_IC__c = TRUE;
            oneLeaseObjIC.TransAm_Application_IC__c = listAppObj[0].Id;
            oneLeaseObjIC.TransAm_IC_Make__c = 'Kenworth';
            oneLeaseObjIC.TransAm_IC_Model__c = 'T680';
            oneLeaseObjIC.TransAm_IC_Model_Year__c = '2018';
            oneLeaseObjIC.TransAm_IC_Assignee__c = 'Carol George';
            oneLeaseObjIC.TransAm_Contract_Start_Date__c = System.today();
            oneLeaseObjIC.TransAm_IC_VIN__c = 'abc';
            oneLeaseObjIC.TransAm_IC_Base_License__c = 'lisc';
            
            insert oneLeaseObjIC;
            
            List<TransAm_ONE_Leasing_Document__c> listOneLeaseDocObjIC = [Select Id, name from TransAm_ONE_Leasing_Document__c where TransAm_Lease__c = :oneLeaseObjIC.Id];
            
            List<Attachment> listAttachObj = new List<Attachment>();
            
                Blob b = Blob.valueOf('Test Data');
                Attachment attachObj = new Attachment();
                attachObj.ParentId = listOneLeaseDocObjIC[0].Id;
                attachObj.Name = 'ICA' + '.docx';
                attachObj.Body = b;
                listAttachObj.add(attachObj);
                
                Attachment attachObj2 = new Attachment();
                attachObj2.ParentId = listOneLeaseDocObjIC[0].Id;
                attachObj2.Name = 'OneBeaconEnrollForm' + '.docx';
                attachObj2.Body = b;
                listAttachObj.add(attachObj2);
            
            
            insert(listAttachObj);
            Transam_RVIExportForLease.generateXML(oneLeaseObjIC.Id, icAgreementRecType);
        }
    }
}