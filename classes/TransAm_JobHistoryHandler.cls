/* Class Name   : TransAm_JobHistoryHandler
 * Description  : Handler class for the Violation trigger
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_JobHistoryHandler{

    public static boolean prohibitBeforeInsertTrigger = false;
	public static boolean prohibitAfterInsertTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  17-Feb-2017
    * Description     :  handler method for before insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void beforeInsertHandler(list<TransAm_Job_History__c> jobList){
    
        /*if(prohibitBeforeInsertTrigger){
            return;
        }*/        
        TransAm_JobHistoryHelper.populateApplicationOnJobHistory(jobList);
        TransAm_JobHistoryHelper.populateEmployerOnJobHistory(jobList);
        //prohibitBeforeInsertTrigger = true;
    }
	public static void beforeUpdatetHandler(list<TransAm_Job_History__c> jobList){
    
        TransAm_JobHistoryHelper.populateEmployerOnJobHistory(jobList);
    }
/****************************************************************************************
* Created By      :  Nitesh Halliyal
* Create Date     :  11-July-2017
* Description     :  handler method for after insert event
* Modification Log:  Initial version.
***************************************************************************************/
    public static void afterInsertHandler(list<TransAm_Job_History__c> jobList){
    system.debug('inside handler');
        if(prohibitAfterInsertTrigger){
            return;
        }        
        TransAm_JobHistoryHelper.createAttachmentFromRD(jobList);
        prohibitAfterInsertTrigger = true;
    }
}