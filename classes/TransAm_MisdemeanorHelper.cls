/* Class Name   : TransAm_Misdemeanor Helper
 * Description  : Helper class for the Misdemeanor trigger
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_MisdemeanorHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  17-Feb-2017
    * Description     :  populate the application Id on the Misdemeanor record
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateApplicationOnMisdemeanor(list<TransAm_Misdemeanor__c> misdemeanorList){
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        if(UserInfo.getProfileId()==profileId){       
            map<string,Id> applicationMap = new map<string,Id>();
            set<String> parentIdSet = new set<String>();         
            try{
                for(TransAm_Misdemeanor__c misObj : misdemeanorList){
                    parentIdSet.add(misObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_Misdemeanor__c misdemeanorObj : misdemeanorList){
                    if(applicationMap.containsKey(misdemeanorObj.TransAm_EBE_Parent_ID__c)){
                        misdemeanorObj.TransAm_Application__c = applicationMap.get(misdemeanorObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_MisdemeanorHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnMisdemeanor';
                el.TransAm_Module_Name__c = 'Misdemeanor';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el;  
            }
        }
    }
}