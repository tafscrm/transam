/******************************************************************************************
* Create By    :     Neha Jain
* Create Date  :     03/26/2017
* Description  :     controller for TransAm_Job_History__c page.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_JobHistoryController {

    public TransAm_Job_History__c objJobHistory{get;set;}
    public ID recordId;
    public TransAm_JobHistoryController (Apexpages.standardcontroller controller){
        
        recordId = ApexPages.currentPage().getParameters().get('id');
        if(recordId !=null){ 
                objJobHistory = [SELECT ID, TransAm_Employed_From__c,TransAm_Type_of_Equipment_operated__c ,TransAm_Application__c,TransAm_Employed_To__c,TransAm_Current_Employer__c,TransAm_Position_Held__c, TransAm_Date_of_Reference_Check__c,
                                                TransAm_No_of_States_Driven__c,TransAm_Dump_Trailer__c,TransAm_Geographical_Areas_of_Operation__c,TransAm_Flatbed__c,TransAm_Person_Contacted_at_Employer__c,
                                                TransAm_Would_you_rehire__c,TransAm_Reefer_Truck__c,TransAm_Reason_for_Leaving__c,TransAm_Tanker__c,TransAm_Phone_Verified_Create_Image__c,TransAm_References_Checked_by__c,
                                                TransAm_Van__c,TransAm_Reasons_For_Leaving_Notes__c,TransAm_Straight_Truck__c,TransAm_Employer2__c,TransAm_Other_Employer__c,TransAm_Address__c,
                                                TransAm_May_we_contact_this_employer__c,TransAm_City__c,TransAm_Contact_Method__c,TransAm_State__c,TransAm_Contact_Method_Manual_Override__c,
                                                TransAm_Zip__c,TransAm_Fax__c,TransAm_Email__c,TransAm_Phone__c,TransAm_DOT_regulated__c,TransAm_Frequency_Of_Request__c,TransAm_Subject_to_DOT_Alcohol_Testing__c,
                                                TransAm_Request_Attempts__c,TransAm_Was_General_Conduct_Satisfactory__c,TransAM_Was_verification_success__c,TrasAM_Was_verification2_success__c,TransAMDate_Time_Verification_Attempt_1__c,
                                                TransAMDate_Time_Verification_Attempt_2__c,TransAm_Validated_DOT_Alcohol_Testing__c ,TransAm_Validated_Accidents__c ,TransAm_Leaving_Notes__c
                                                FROM TransAm_Job_History__c 
                                                WHERE ID =:recordId] ;
        }
        else{
            objJobHistory= (TransAm_Job_History__c)controller.getRecord();
        }
    }
    
    public PageReference save() {
        
        try{
            if(objJobHistory!= null){
                upsert objJobHistory;
            }
                        
       }
       catch(Exception e){ 
           TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c(); TransAmErrorLogObj.TransAm_Class_Name__c = 'JobHistoryController'; TransAmErrorLogObj.TransAm_Class_Name__c = 'JobHistoryController'; TransAmErrorLogObj.TransAm_Exception_Message__c = e.getMessage();TransAmErrorLogObj.TransAm_Module_Name__c = 'Job History'; TransAmErrorLogObj.TransAm_Module_Name__c = 'Job History'; insert TransAmErrorLogObj;  ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0)); ApexPages.addMessage(msg);return null;   
       }
        if(objJobHistory.id!=null){
            return new PageReference('/'+ ApexPages.currentPage().getParameters().get('retURL'));
        }
        else{
            return null;
        }
    }
    
    public PageReference saveNew() {
        
        try{
            if(objJobHistory!= null){
                upsert objJobHistory;
            }
        }
        catch(Exception e){ 
            
            TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c(); TransAmErrorLogObj.TransAm_Class_Name__c = 'JobHistoryController'; TransAmErrorLogObj.TransAm_Class_Name__c = 'JobHistoryController';  TransAmErrorLogObj.TransAm_Exception_Message__c = e.getMessage(); TransAmErrorLogObj.TransAm_Module_Name__c = 'Job History'; TransAmErrorLogObj.TransAm_Module_Name__c = 'Job History'; insert TransAmErrorLogObj;  ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0)); ApexPages.addMessage(msg);return null;
        }
        if(objJobHistory.id!=null){
            return new PageReference('/'+objJobHistory.getSObjectType().getDescribe().getKeyPrefix()+'/e');
        }
        else{ return null;
        }
    }
    
    public PageReference cancel() {
        
        return new PageReference('/'+ ApexPages.currentPage().getParameters().get('retURL'));
    }
    
     public void addressInfo(){
         
         List<TransAm_Employer__c> listAddress = [select id,Name,TransAm_Address1__c,TransAm_City__c,TransAm_Email__c,TransAm_Contact_Method__c,TransAm_Fax__c,TransAm_Phone__c,TransAm_State__c,TransAm_Zip__c from TransAm_Employer__c where id=: objJobHistory.TransAm_Employer2__c ];
         if(listAddress.size()>0){ 
             for(TransAm_Employer__c obj : listAddress){
                 objJobHistory.TransAm_Address__c= obj.TransAm_Address1__c;
                 objJobHistory.TransAm_City__c= obj.TransAm_City__c;
                 objJobHistory.TransAm_Email__c= obj.TransAm_Email__c;
                 objJobHistory.TransAm_Fax__c= obj.TransAm_Fax__c;
                 objJobHistory.TransAm_Phone__c= obj.TransAm_Phone__c;
                 objJobHistory.TransAm_State__c= obj.TransAm_State__c;
                 objJobHistory.TransAm_Zip__c= obj.TransAm_Zip__c;
                 objJobHistory.TransAm_Other_Employer__c= obj.Name;
                 objJobHistory.TransAm_Contact_Method__c = obj.TransAm_Contact_Method__c;
             }
         }
     }
     
     /*public void otherEmployer(){
     
         if(objJobHistory.TransAm_Employer2__c==null){
             objJobHistory.TransAm_Address__c= '';
             objJobHistory.TransAm_City__c= '';
             objJobHistory.TransAm_Email__c= '';
             objJobHistory.TransAm_Fax__c= '';
             objJobHistory.TransAm_Phone__c= '';
             objJobHistory.TransAm_State__c= '';
             objJobHistory.TransAm_Zip__c= '';
             objJobHistory.TransAm_Contact_Method__c = '';
         }
     }*/  
}