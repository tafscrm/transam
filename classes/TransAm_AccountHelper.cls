/******************************************************************************************
* Create By    :     Pardeep Rao
* Create Date  :     03/06/2017
* Description  :     Helper class for TransAm_AccountTrigger trigger.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/

public class TransAm_AccountHelper{
    
public static List<TransAm_Private_School_Recruiter_Notes__c> recruiterNotes = new List<TransAm_Private_School_Recruiter_Notes__c>();
    
    /*******************************************************************************************
   * Method        :   insertPrivateRecruiterComment
   * Description   :   Purpose of this method is to insert dummy record in Private Recruiter Notes object.
   * Parameter     :   list<account>
   * Return Type   :   void
   ******************************************************************************************/
    public static void insertPrivateRecruiterComment(list<account> newAccountList){
        
     for (account objAcc: newAccountList)
     {
        recruiterNotes.add(new TransAm_Private_School_Recruiter_Notes__c(School_Name__c=objAcc.Id));
           
     }
       
       
        if (recruiterNotes!= null && recruiterNotes.size() > 0){            
            try{
               Database.SaveResult[] insertResultList = Database.insert(recruiterNotes, false);
                
               for (Database.SaveResult sr : insertResultList ) {
                    if (sr.isSuccess()) {                        
                        System.debug('Successfully inserted account. TransAm_Private_School_Recruiter_Notes__c ID: ' + sr.getId());                
                    } else {                
                        list<TransAm_Error_Log__c> ErrorLogObjList = new list<TransAm_Error_Log__c>();
                        
                        // Operation failed, so get all errors
                        for(Database.Error err : sr.getErrors()) {                            
                            TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c();
                            
                            TransAmErrorLogObj.TransAm_Class_Name__c = 'TransAm_AccountHelper';
                            TransAmErrorLogObj.TransAm_Exception_Message__c = err.getMessage();
                            TransAmErrorLogObj.TransAm_Method_Name__c = 'insertPrivateRecruiterComment';
                            TransAmErrorLogObj.TransAm_Module_Name__c = 'School/Private-Notes';
                            
                            ErrorLogObjList.add(TransAmErrorLogObj);
                        }
                        insert ErrorLogObjList;
                    }
                }
            } catch(Exception e){

                TransAm_Error_Log__c TransAmErrorLogObj = new TransAm_Error_Log__c();
                            
                TransAmErrorLogObj.TransAm_Class_Name__c = 'TransAm_AccountHelper';
                TransAmErrorLogObj.TransAm_Exception_Message__c = e.getMessage();
                TransAmErrorLogObj.TransAm_Method_Name__c = 'insertPrivateRecruiterComment';
                TransAmErrorLogObj.TransAm_Module_Name__c = 'School/Private-Notes';
                
                insert TransAmErrorLogObj;
            }                
        }
        
    }  
    
}