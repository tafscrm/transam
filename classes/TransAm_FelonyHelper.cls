/* Class Name   : TransAm_License Helper
 * Description  : Helper class for the License trigger
 * Created By   : Pankaj Singh
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               15-Feb-2017              Initial version.
 *
 *****************************************************************************************/
public class TransAm_FelonyHelper{

    /****************************************************************************************
    * Created By      :  Pankaj Singh
    * Create Date     :  15-Feb-2017
    * Description     :  populate the application Id on the felony record
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void populateApplicationOnFelony(list<TransAm_Felony__c > felonyList){
        
        Id profileId = [SELECT ID FROM Profile WHERE Name = 'TransAm_Integration_User'].Id;
        if(UserInfo.getProfileId()==profileId){       
            map<string,Id> applicationMap = new map<string,Id>();
            set<String> parentIdSet = new set<String>();         
            try{
                for(TransAm_Felony__c  felonyObj : felonyList){
                    parentIdSet.add(felonyObj.TransAm_EBE_Parent_ID__c);        
                }
                for(TransAm_Application__c applicationObj : [SELECT ID, TransAm_EBE_Entry_ID__c FROM TransAm_Application__c WHERE TransAm_EBE_Entry_ID__c IN : parentIdSet]){
                    applicationMap.put(applicationObj.TransAm_EBE_Entry_ID__c,applicationObj.Id);
                }
                for(TransAm_Felony__c felonyObj : felonyList){
                    if(applicationMap.containsKey(felonyObj.TransAm_EBE_Parent_ID__c)){
                        felonyObj.TransAm_Application__c = applicationMap.get(felonyObj.TransAm_EBE_Parent_ID__c);
                    }
                }
            }catch(Exception excep){
                TransAm_Error_Log__c el = new TransAm_Error_Log__c();
                el.TransAm_Class_Name__c = 'TransAm_FelonyHelper';
                el.TransAm_Method_Name__c = 'populateApplicationOnFelony';
                el.TransAm_Module_Name__c = 'Felony';
                el.TransAm_Exception_Message__c = excep.getMessage();
                insert el;   
            }
        }
    }
    /*public static void updateApplication(List<TransAm_Felony__c> felonyList){
        List<Id> appIdList = new List<Id>();
        for(TransAm_Felony__c objFelony : felonyList){
            if(String.isNotBlank(objFelony.TransAm_Application__c))
                appIdList.add(objFelony.TransAm_Application__c);
        }
        List<TransAm_Application__c> appList = [SELECT Id FROM TransAm_Application__c WHERE Id IN : appIdList];
        System.debug('appList-->'+ appList);
        Database.update(appList);
    }*/
}