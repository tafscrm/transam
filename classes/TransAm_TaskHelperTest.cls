/********************************************************************************************** 
 *  CLass Name   : TransAm_TaskHelperTest
 * Description  : Test class for TransAm_TaskHelper class
 * Created By   : Neha Jain
 * Created On   : 26-Sept-2017
 *
 *  Modification Log :
 *  ------------------------------------------------------------------------------------------
 *  * Developer         Modification ID            Modified Date               Description
 *  * ----------------------------------------------------------------------------------------                 
 *  * Neha Jain          00001                      26-Sept-2017               Initial version.
 *
 **********************************************************************************************/

@isTest
public class TransAm_TaskHelperTest{

    @testSetup static void setup() {
    
        List<Task> taskList = new List<Task>();
        Map<Id,Task> taskMap = new Map<Id,Task>();
        TransAm_Generic_Custom_Setting__c custSetting1 = new TransAm_Generic_Custom_Setting__c();
        custSetting1.Name='TaskRecordTypeNonSchool';
        insert custSetting1;
        TransAm_Generic_Custom_Setting__c custSetting2 = new TransAm_Generic_Custom_Setting__c();
        custSetting2.Name='TaskStatusOpen';
        insert custSetting2;
        TransAm_Generic_Custom_Setting__c custSetting3 = new TransAm_Generic_Custom_Setting__c();
        custSetting3.Name='TaskTypeFollowUp';
        insert custSetting3;
        TransAm_Generic_Custom_Setting__c custSetting4 = new TransAm_Generic_Custom_Setting__c();
        custSetting4.Name='TaskStatusCompleted';
        insert custSetting4;
        TransAm_Generic_Custom_Setting__c custSetting5 = new TransAm_Generic_Custom_Setting__c();
        custSetting5.Name='AppObjAPIName';
        insert custSetting5;
        custSetting1.TransAm_Message_String__c='Driver Recruiting';
        update custSetting1;
        custSetting2.TransAm_Message_String__c='Open';
        update custSetting2;
        custSetting3.TransAm_Message_String__c='Follow-up';
        update custSetting3;
        custSetting5.TransAm_Message_String__c='TransAm_Application__c';
        update custSetting5;
        
        Id user = [select id from user where profile.name='HR Manager' Limit 1].id;
        Account acc = new Account();
        acc=TransAm_Test_DataUtility.createAccounts(1,true)[0];
        Contact con = new Contact();
        con=TransAm_Test_DataUtility.createContacts(1,acc.id,'Applicant',true)[0];
        TransAm_Application__c app = new TransAm_Application__c();
        app = TransAm_Test_DataUtility.createApplications(1, 'Independent', 'Application Received',con, True)[0];
        RecordType recordTypId = [Select id,Name from recordType where sObjectType ='Task' AND Name='Driver Recruiting' Limit 1];
        Task newTask = new Task(Subject='call',ActivityDate=system.today().addDays(-2),whatId=app.id,recordTypeId=recordTypId.id,
                                    type='Follow-up');
        insert newTask;
    }
    
    public static testmethod void testTaskHelper(){
    
        Task newTask = [select id,ActivityDate from Task where type='Follow-up' Limit 1];
        Test.startTest();
        newTask.ActivityDate=system.today().addDays(2);
        update newTask;
        TransAm_TaskHandler handler2 = new TransAm_TaskHandler();
        Test.stopTest();
    }
}