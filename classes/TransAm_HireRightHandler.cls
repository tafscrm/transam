/* Class Name   : TransAm_HireRightHandler
 * Description  : Handler class of HireRightOrder trigger
 * Created By   : Monalisa
 * Created On   : 9-Apr-2017  
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Monalisa               9-Apr-2017                Initial version.
 *
 *****************************************************************************************/
public class TransAm_HireRightHandler{

    public static boolean prohibitAfterInsertTrigger = false;
    public static boolean prohibitAfterUpdateTrigger = false;
    
    /****************************************************************************************
    * Created By      :  Monalisa
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for after insert event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterInsertHandler(list<TransAm_HireRight_Orders__c> hireRightOrdersList){
    
        if(prohibitAfterInsertTrigger){
            return;
        }        
        Id[] orderIds = new List<Id>();
        for(TransAm_HireRight_Orders__c order : hireRightOrdersList)
            orderIds.add(order.Id);
        TransAm_HireRightHelper.populateOrderIdOnHireRightOrders(orderIds);
        //prohibitAfterInsertTrigger = true;
    }
    
    /****************************************************************************************
    * Created By      :  Monalisa
    * Create Date     :  13-Feb-2017
    * Description     :  handler method for before update event
    * Modification Log:  Initial version.
    ***************************************************************************************/
    public static void afterUpdatetHandler(list<TransAm_HireRight_Orders__c> hireRightOrdersList){
    
        if(prohibitAfterUpdateTrigger){
            System.debug('TransAm_HireRightHandler.prohibitAfterUpdateTrigger in handler::'+TransAm_HireRightHandler.prohibitAfterUpdateTrigger );
            return;
        }        
       // TransAm_HireRightHelper.populateWeblinkOnHireRightOrders(hireRightOrdersList);
        prohibitAfterUpdateTrigger = true;
    }

}