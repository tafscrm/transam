/********************************************************************************************** 
 *  CLass Name   : Transam_SchoolChildCount_Test
 * Description  : Test class for Transam_SchoolChildCount class
 * Created By   : Raushan Anand
 * Created On   : 03-Jul-2017
 *
 *  Modification Log :
 *  ------------------------------------------------------------------------------------------
 *  * Developer         Modification ID            Modified Date               Description
 *  * ----------------------------------------------------------------------------------------                 
 *  * Raushan Anand           00001                 03-Jul-2017               Initial version.
 *
 **********************************************************************************************/
@isTest(SeeAllData=false)
public class Transam_SchoolChildCount_Test {
	/************************************************************************************
    * Method       :    testDataSetUp
    * Description  :    This method is used to create test data.
    * Parameter    :    NIL    
    * Return Type  :    void
    *************************************************************************************/
    @testSetup
    private static void testDataSetUp(){
        List<Account> accountList = TransAm_Test_DataUtility.createAccounts(1, True);
        List<Contact> contactList = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'Prospect', True);
        List<Contact> contactList1 = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'Applicant', True);
        List<Contact> contactList2 = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'Driver', True);
        contactList2[0].Transam_Driver_Status__c = 'Hired';
        UPDATE contactList2;
        //List<Contact> contactList = TransAm_Test_DataUtility.createContacts(1, accountList[0].Id, 'Applicant', True);
        
    }
    private static testmethod void testCountBatch(){
        Test.startTest();
       	String sch = '0 0 23 * * ?'; 
        system.schedule('ertyuibn980_edfyugbhjTest Schedule', sch, new Transam_SchoolChildCountScheduler());	
        //Database.executeBatch(new Transam_SchoolChildCount(), 50);
        Test.stopTest();
       	/*List<Account> accList = [Select Id,Transam_Total_Applicants__c,Transam_Total_Drivers__c,Transam_Total_Prospects__c,Transam_Current_Active_Drivers__c from Account];
        System.debug('aa'+accList[0]);
        System.assertEquals(1,accList[0].Transam_Total_Prospects__c);*/
    }
}