<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NY</label>
    <protected>false</protected>
    <values>
        <field>Transam_State__c</field>
        <value xsi:type="xsd:string">NY</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">([a-zA-Z]{1}[0-9]{18}|[0-9]{9}|[0-9]{16})?$</value>
    </values>
</CustomMetadata>
