<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>KS_ICC_Exempt_Cert</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Document_Type__c</field>
        <value xsi:type="xsd:string">KS ICC Exempt Certificate</value>
    </values>
</CustomMetadata>
