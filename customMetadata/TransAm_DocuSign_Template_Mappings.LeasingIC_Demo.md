<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LeasingIC_Demo</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Docusign_TemplateId__c</field>
        <value xsi:type="xsd:string">ce965eba-d9fd-438d-978f-9f770350f665</value>
    </values>
    <values>
        <field>TransAm_Is_Sandbox__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
