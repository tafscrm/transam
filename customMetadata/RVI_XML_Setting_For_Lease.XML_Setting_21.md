<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>XML Setting 21</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Document_Name__c</field>
        <value xsi:type="xsd:string">Unplanned Mandatory Repairs Election Form</value>
    </values>
    <values>
        <field>TransAm_Owner_Hired_System__c</field>
        <value xsi:type="xsd:string">a</value>
    </values>
    <values>
        <field>TransAm_RVI_Scan_Code__c</field>
        <value xsi:type="xsd:string">UME</value>
    </values>
</CustomMetadata>
