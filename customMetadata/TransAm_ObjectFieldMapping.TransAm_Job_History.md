<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Job History</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Fieldset_Name__c</field>
        <value xsi:type="xsd:string">TransAm_Job_History_Field_Set</value>
    </values>
</CustomMetadata>
