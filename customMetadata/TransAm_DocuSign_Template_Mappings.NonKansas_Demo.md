<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NonKansas_Demo</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Docusign_TemplateId__c</field>
        <value xsi:type="xsd:string">5772f0d6-2a76-488c-91c8-1726f7355fec</value>
    </values>
    <values>
        <field>TransAm_Is_Sandbox__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
