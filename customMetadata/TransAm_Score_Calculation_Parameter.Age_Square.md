<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Age Square</label>
    <values>
        <field>TransAm_Field_Type__c</field>
        <value xsi:type="xsd:string">Age Square</value>
    </values>
    <values>
        <field>TransAm_Field_Value__c</field>
        <value xsi:type="xsd:double">-8.438E-5</value>
    </values>
    <values>
        <field>TransAm_Participate_in_Calculation__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
