<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MI</label>
    <values>
        <field>TransAm_Field_Type__c</field>
        <value xsi:type="xsd:string">State</value>
    </values>
    <values>
        <field>TransAm_Field_Value__c</field>
        <value xsi:type="xsd:double">0.1296927</value>
    </values>
    <values>
        <field>TransAm_Participate_in_Calculation__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
