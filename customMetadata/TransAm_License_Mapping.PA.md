<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PA</label>
    <protected>false</protected>
    <values>
        <field>Transam_State__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">(0[0-9]{7}|1[0-9]{7}|2[0-9]{7}|3[0-9]{7}|6[0-9]{7})?$</value>
    </values>
</CustomMetadata>
