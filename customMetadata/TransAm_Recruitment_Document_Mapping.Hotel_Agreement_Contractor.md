<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Hotel_Agreement_(C)</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Document_Type__c</field>
        <value xsi:type="xsd:string">Hotel Agreement</value>
    </values>
</CustomMetadata>
