<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NE</label>
    <protected>false</protected>
    <values>
        <field>Transam_State__c</field>
        <value xsi:type="xsd:string">NE</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">([a-zA-Z]{1}[0-9]{3}|[a-zA-Z]{1}[0-9]{4}|[a-zA-Z]{1}[0-9]{5}|[a-zA-Z]{1}[0-9]{6}|[a-zA-Z]{1}[0-9]{7}|[a-zA-Z]{1}[0-9]{8})?$</value>
    </values>
</CustomMetadata>
