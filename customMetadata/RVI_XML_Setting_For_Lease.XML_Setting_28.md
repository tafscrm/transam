<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>XML Setting 28</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Document_Name__c</field>
        <value xsi:type="xsd:string">OneBeacon One Off Form</value>
    </values>
    <values>
        <field>TransAm_Owner_Hired_System__c</field>
        <value xsi:type="xsd:string">a</value>
    </values>
    <values>
        <field>TransAm_RVI_Scan_Code__c</field>
        <value xsi:type="xsd:string">OCA</value>
    </values>
</CustomMetadata>
