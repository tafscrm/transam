<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DocusignProd</label>
    <protected>false</protected>
    <values>
        <field>TransAm_AccountId__c</field>
        <value xsi:type="xsd:string">4ca1d59b-ce0b-46e3-8c41-a314ce98ed0b</value>
    </values>
    <values>
        <field>TransAm_BaseUrl__c</field>
        <value xsi:type="xsd:string">https://na2.docusign.net/restApi</value>
    </values>
    <values>
        <field>TransAm_Integrator_Key__c</field>
        <value xsi:type="xsd:string">TRAN-3b58fb0b-5bcc-42e4-83ef-b9462f08f631</value>
    </values>
    <values>
        <field>TransAm_Password__c</field>
        <value xsi:type="xsd:string">transam123</value>
    </values>
    <values>
        <field>TransAm_TemplateId__c</field>
        <value xsi:type="xsd:string">0db4627b-03e4-46bb-a28c-cb2c19739038</value>
    </values>
    <values>
        <field>TransAm_Username__c</field>
        <value xsi:type="xsd:string">gagupte@deloitte.com</value>
    </values>
</CustomMetadata>
