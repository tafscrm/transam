<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ND</label>
    <protected>false</protected>
    <values>
        <field>Transam_State__c</field>
        <value xsi:type="xsd:string">ND</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">([0-9]{9}|([A-Za-z]{3}[0-9]{6}))?$</value>
    </values>
</CustomMetadata>
