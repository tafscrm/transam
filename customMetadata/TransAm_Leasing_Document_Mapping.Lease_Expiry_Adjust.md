<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lease_Expiry_Adjust</label>
    <protected>false</protected>
    <values>
        <field>TransAm_Document_Type__c</field>
        <value xsi:type="xsd:string">Lease Expiration Date Adjustment</value>
    </values>
</CustomMetadata>
