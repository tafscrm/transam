<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>XML Setting 51</label>
    <protected>false</protected>
    <values>
        <field>TransAm_All_Drivers_Hired_System__c</field>
        <value xsi:type="xsd:string">3</value>
    </values>
    <values>
        <field>TransAm_Company_Hired_System__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>TransAm_Document_Name__c</field>
        <value xsi:type="xsd:string">Previous Employer Drug Testing (Contractor)</value>
    </values>
    <values>
        <field>TransAm_Owner_Hired_System__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>TransAm_RVI_Scan_Code__c</field>
        <value xsi:type="xsd:string">DA</value>
    </values>
    <values>
        <field>TransAm_Task_21_RejectedDQed__c</field>
        <value xsi:type="xsd:string">q</value>
    </values>
    <values>
        <field>TransAm_Task_25_No_Contact__c</field>
        <value xsi:type="xsd:string">q</value>
    </values>
    <values>
        <field>TransAm_Task_X_In_Progress__c</field>
        <value xsi:type="xsd:string">q</value>
    </values>
</CustomMetadata>
