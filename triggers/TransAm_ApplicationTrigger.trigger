/******************************************************************************************
* Create By    :     Suresh M
* Create Date  :     02/17/2017
* Description  :     A common trigger for Application__c object for all the events.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * <Name>                             <Date>           Initial version.
*****************************************************************************************/
trigger TransAm_ApplicationTrigger on TransAm_Application__c (before insert, before update,after update, after undelete, after insert) {

    if(trigger.isBefore && trigger.isInsert){
        TransAm_ApplicationHandler.beforeInsertHandler(trigger.new, trigger.newMap, true);            
    }
    
    if(trigger.isBefore && trigger.isUpdate){
        TransAm_ApplicationHandler.beforeUpdateHandler(trigger.new, trigger.newMap, trigger.oldMap, true);
        //TransAm_ApplicationHandler.afterUpdateHandler(trigger.new,trigger.oldMap,trigger.newMap);
        
    }
    
    if(trigger.isAfter && trigger.isUpdate){
         TransAm_ApplicationHandler.afterUpdateHandler(trigger.new, trigger.oldMap, true);       
    }
    
    if(trigger.isAfter && trigger.isInsert){
        TransAm_ApplicationHandler.afterInsertHandler(trigger.new, trigger.newMap, true);
    }
    if(trigger.isAfter && trigger.isDelete){
        TransAm_ApplicationHandler.afterDeleteHandler(trigger.old, trigger.oldMap, true);
    }
}