/* Trigger Name : TransAm_MisdemeanorTrigger
 * Description  : Trigger for Misdemeanor object
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_MisdemeanorTrigger on TransAm_Misdemeanor__c(before insert) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_MisdemeanorHandler.beforeInsertHandler(trigger.new);
        }
    }

}