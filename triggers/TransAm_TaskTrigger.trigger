/******************************************************************************************
* Create By    :     Ajay B R
* Create Date  :     08/16/2017
* Description  :     A common trigger for Task object for all the events.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Ajay B R                           08/16/2017           Initial version.
*****************************************************************************************/
trigger TransAm_TaskTrigger on Task (after insert, after update) {
    if(Trigger.isAfter){
        if(Trigger.isinsert){
            TransAm_TaskHandler.afterInsertHandler(trigger.new);
        }
        if(Trigger.isUpdate){
            TransAm_TaskHandler.afterUpdateHandler(trigger.new, trigger.oldMap);
        }
    }
}