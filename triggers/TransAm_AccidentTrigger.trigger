/* Trigger Name : TransAm_AccidentTrigger
 * Description  : Trigger for Accident object
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_AccidentTrigger on TransAm_Accident__c(before insert, after Insert, after Update) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_AccidentHandler.beforeInsertHandler(trigger.new);
        }
        if(Trigger.isAfter){
            TransAm_AccidentHandler.afterInsertHandler(trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            TransAm_AccidentHandler.afterUpdateHandler(trigger.new);
        }
        
    }

}