/* Trigger Name : TransAm_JobHistoryTrigger
 * Description  : Trigger for job history object
 * Created By   : Pankaj Singh
 * Created On   : 17-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               17-Feb-2017              Initial version.
 *    Nitesh Halliyal            11-Jul-2017              Added after insert logic
 *
 *****************************************************************************************/
trigger TransAm_JobHistoryTrigger on TransAm_Job_History__c(before insert,after insert,before update) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_JobHistoryHandler.beforeInsertHandler(trigger.new);
        }
        else if(Trigger.isAfter){
            TransAm_JobHistoryHandler.afterInsertHandler(trigger.new);
        }
    }
	if(Trigger.isUpdate && Trigger.isBefore){
        TransAm_JobHistoryHandler.beforeUpdatetHandler(trigger.new);
    }
}