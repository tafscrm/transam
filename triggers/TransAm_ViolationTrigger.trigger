/* Trigger Name : TransAm_ViolationTrigger
 * Description  : Trigger for Violation object
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_ViolationTrigger on TransAm_Violation__c(before insert, before Update, after Insert, after Update) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_ViolationHandler.beforeInsertHandler(trigger.new);
        }
        if(Trigger.isAfter){
            TransAm_ViolationHandler.afterInsertHandler(trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            TransAm_ViolationHandler.afterUpdateHandler(trigger.new);
        }
    }

}