/* Trigger Name : TransAm_FelonyTrigger
 * Description  : Trigger for Felony object
 * Created By   : Pankaj Singh
 * Created On   : 16-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               16-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_FelonyTrigger on TransAm_Felony__c(before insert, after Insert, after Update) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_FelonyHandler.beforeInsertHandler(trigger.new);
        }
        if(Trigger.isAfter){
            TransAm_FelonyHandler.afterInsertHandler(trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            TransAm_FelonyHandler.afterUpdateHandler(trigger.new);
        }
    }

}