trigger Transam_SentFax_Trigger on efaxapp__Sent_Fax__c (after insert,after Update) {
    if(Trigger.IsUpdate){
        if(Trigger.isAfter){
            set<ID> sentFaxIdSet = new set<id>();
            for(efaxapp__Sent_Fax__c sf : trigger.new){
                if(sf.efaxapp__Status__c == 'Delivered' && trigger.oldmap.get(sf.Id).efaxapp__Status__c!= 'Delivered' ||
                sf.efaxapp__Status__c == 'Not Delivered' && trigger.oldmap.get(sf.Id).efaxapp__Status__c!= 'Not Delivered'){                    
                    sentFaxIdSet.add(sf.Id); 
                }   
            }
            if(sentFaxIdSet.size()>0){
                TransAm_SentFaxHandler.ddpGenerationHandler(sentFaxIdSet, UserInfo.getSessionId());
            }
            Transam_SentFaxHandler.afterUpdateHandler(trigger.new, Trigger.oldMap);                 
        }
    }
}