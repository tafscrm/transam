/* Trigger Name : TransAm_LicenseTrigger
 * Description  : Trigger for License object
 * Created By   : Pankaj Singh
 * Created On   : 15-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               15-Feb-2017              Initial version.
 *  * MJ                         04-April-2017             Second version.
 *
 *****************************************************************************************/
trigger TransAm_LicenseTrigger on TransAm_License__c(before insert,before Update,After insert,After update) {
    if(Trigger.isInsert && Trigger.isBefore){
            TransAm_LicenseHandler.beforeInsertHandler(trigger.new);     
    }
    if(Trigger.isAfter && Trigger.isInsert){
        TransAm_LicenseHandler.afterInsertHandler(trigger.new);        
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        TransAm_LicenseHandler.afterUpdateHandler(trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        TransAm_LicenseHandler.beforeUpdateHandler(trigger.new);
    }
}