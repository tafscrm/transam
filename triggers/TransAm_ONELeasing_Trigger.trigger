/******************************************************************************************
* Create By    :     Karthik Gulla
* Create Date  :     08/10/2017
* Description  :     A common trigger for ONE Leasing object for all the events.
* Modification Log:
* ----------------------------------------------------------------------------
*  * Developer                          Date               Description
*  * ----------------------------------------------------------------------------                 
*  * Karthik Gulla                      08/10/2017         Initial version.
*****************************************************************************************/
trigger TransAm_ONELeasing_Trigger on TransAm_ONE_Leasing__c (after Update, after Insert) { 
    if(trigger.isAfter && trigger.isInsert){
        TransAm_ONELeasing_Handler.afterInsertHandler(trigger.new, trigger.newMap, true);
    }

    if(trigger.isAfter && trigger.isUpdate){
        TransAm_ONELeasing_Handler.afterUpdateHandler(trigger.new, trigger.oldMap);
    }
}