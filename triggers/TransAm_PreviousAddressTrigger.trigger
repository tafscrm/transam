/* Trigger Name : TransAm_PreviousAddressTrigger
 * Description  : Trigger for previous address object
 * Created By   : Pankaj Singh
 * Created On   : 14-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               14-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_PreviousAddressTrigger on TransAm_PreviousAddress__c (before insert) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            TransAm_PreviousAddressHandler.beforeInsertHandler(trigger.new);
        }
    }
}