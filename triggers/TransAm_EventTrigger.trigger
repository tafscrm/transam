/* Trigger Name : TransAm_EventTrigger
 * Description  : Trigger for the Event object
 * Created By   : Pankaj Singh
 * Created On   : 13-Feb-2017
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pankaj Singh               13-Feb-2017              Initial version.
 *
 *****************************************************************************************/
trigger TransAm_EventTrigger on Event (before insert,before update, after insert, after delete) {
    if(Trigger.isinsert){
        if(Trigger.isBefore){
            TransAm_EventHandler.beforeInsertHandler(trigger.new);
            system.debug('before insert helper::');
        }
        if(Trigger.isAfter){
            TransAm_EventHandler.afterInsertHandler(trigger.new);
        }
    }
    if(Trigger.isDelete){
        if(Trigger.isAfter){
            TransAm_EventHandler.afterDeleteHandler(trigger.old);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            TransAm_EventHandler.beforeUpdateHandler(trigger.new);
        }
    }
}