<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_School_Driving_Code</fullName>
        <field>TransAm_Driving_School_Code__c</field>
        <formula>UPPER( TransAm_Driving_School_Code__c )</formula>
        <name>Update School Driving Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TransAm_Driving_School_Code_Uppercase_Mechanism</fullName>
        <actions>
            <name>Update_School_Driving_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.TransAm_Driving_School_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
