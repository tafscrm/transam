<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TransAm_Update_Applicant_Id</fullName>
        <description>Update Applicant Id on contact with the combination of First Name, Last Name, Phone.</description>
        <field>TransAm_Applicant_ID__c</field>
        <formula>IF(ISNEW(), FirstName&amp;LastName&amp;Phone, if(ISCHANGED(FirstName) || ISCHANGED(LastName) || ISCHANGED(Phone) , FirstName&amp; LastName&amp;Phone, TransAm_Applicant_ID__c))</formula>
        <name>Update Applicant Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TransAm_Populate_Applicant_ID</fullName>
        <actions>
            <name>TransAm_Update_Applicant_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 ) AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.FirstName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Applicant</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
