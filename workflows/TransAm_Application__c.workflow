<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Air_Travel_Confirmation_Olathe</fullName>
        <description>Air Travel Confirmation ,Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Flight</template>
    </alerts>
    <alerts>
        <fullName>Air_Travel_Confirmation_Rockwall</fullName>
        <description>Air Travel Confirmation ,Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Flight</template>
    </alerts>
    <alerts>
        <fullName>Bus_Confirmation_For_Rockwall</fullName>
        <description>Bus Confirmation For Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Bus</template>
    </alerts>
    <alerts>
        <fullName>Bus_Travel_Confirmation_To_Travelers_For_Olathe</fullName>
        <description>Bus Travel Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Bus</template>
    </alerts>
    <alerts>
        <fullName>No_Car_Hotel_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel And No Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Olathe_Air_without_hotel</fullName>
        <description>Olathe Air without hotel</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Flight_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Olathe_Bus_without_hotel</fullName>
        <description>Olathe Bus without hotel</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Bus_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Ola</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Roc</fullName>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Car_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Travel_Confirmation_To_Travelers_For_Olathe</fullName>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Personal_Vehicle_Rental_Car_Welcome_Mail</fullName>
        <description>Personal Vehicle/ Rental Car Welcome Mail Without Lodging Details</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Rockwall_Air_without_hotel</fullName>
        <description>Rockwall Air without hotel</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Flight_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>Rockwall_Bus_without_hotel</fullName>
        <description>Rockwall Bus without hotel</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Rockwall_Bus_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_by_Fight_to_Texas</fullName>
        <description>TransAm Email To Drivers travelling by Fight to Texas</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Air_Travel_Confirmation_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_to_Olathe</fullName>
        <description>TransAm  Welcome Email To Drivers Travelling To Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Olathe</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_To_Drivers_travelling_to_Rockwall</fullName>
        <description>TransAm Travel Confirmation For Applicants Travelling To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Travel_Confirmation_to_Olathe</fullName>
        <description>TransAm Travel Confirmation to Olathe</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Olathe</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Welcome_Email_To_Drivers_Travelling_To_Rockwall</fullName>
        <description>TransAm  Welcome Email To Drivers Travelling To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_For_Drivers_Travelling_by_Bus_To_Rockwall</fullName>
        <description>Welcome Email For Drivers Travelling by Bus To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Bus_Travel_Confirmation_For_Rockwall</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_For_Drivers_Travelling_by_Car_Without_Lodging_To_Rockwall</fullName>
        <description>Welcome Email For Drivers Travelling by Car Without Lodging To Rockwall</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Olathe_Car_No_Hotel</template>
    </alerts>
    <alerts>
        <fullName>car_Hotel_KS</fullName>
        <description>car Hotel KS</description>
        <protected>false</protected>
        <recipients>
            <field>TransAm_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pardot_Olathe_Car_Hotel</template>
    </alerts>
	<fieldUpdates>
        <fullName>Update_EBE_flag</fullName>
        <field>TransAm_EBE_Iseries__c</field>
        <name>Update EBE flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_to_Queue</fullName>
        <description>Assign OwnerId to Driver Recruiter Queue</description>
        <field>OwnerId</field>
        <lookupValue>TransAm_Driver_Recruiter_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ONE_Leasing_Status_Update</fullName>
        <description>Workflow field update to update &apos;ONE Leasing Status&apos; on Application to &apos;New&apos;, if Driver Type = &apos;Owner&apos;</description>
        <field>TransAm_One_Leasing_Status__c</field>
        <literalValue>New</literalValue>
        <name>ONE Leasing Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Application_Uncheck</fullName>
        <field>TransAm_Is_status_Changed_Through_Merge__c</field>
        <literalValue>0</literalValue>
        <name>Application_Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_ICC_Export</fullName>
        <field>TransAm_ICC_Export__c</field>
        <literalValue>1</literalValue>
        <name>ICC Export</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_ICC_Export_Date</fullName>
        <field>TransAm_ICC_Export_Date__c</field>
        <formula>TODAY()</formula>
        <name>ICC Export Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_PhoneUpdate_From_OtherNumber</fullName>
        <field>TransAm_Phone__c</field>
        <formula>IF(
AND( 
ISBLANK(TransAm_Phone__c), 
NOT(ISBLANK(TransAm_Other_Number__c))
),TransAm_Other_Number__c, TransAm_Phone__c)</formula>
        <name>TransAm_PhoneUpdate_From_OtherNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_StatusFlag_Update</fullName>
        <field>TransAm_Status_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Status Flag Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Status_Flag_Update</fullName>
        <field>TransAm_Status_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Status Flag Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Update_Archive_Date</fullName>
        <field>TransAm_Archived_Date__c</field>
        <formula>Today()</formula>
        <name>Update Archive Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transam_Update_Current_Status_Age</fullName>
        <description>This will update the Update Current Status Age field to 0 whenever the status is changed</description>
        <field>TransAm_Current_Status_Age__c</field>
        <formula>0</formula>
        <name>Update Current Status Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_By_Pass_Validation_Checkbox</fullName>
        <field>TransAm_By_Pass_Validation__c</field>
        <literalValue>1</literalValue>
        <name>Update By Pass Validation Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Received_Date_for_Manual_Apps</fullName>
        <field>TransAm_Received_Date__c</field>
        <formula>Today()</formula>
        <name>Update Received Date for Manual Apps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Category_to_Job_Board</fullName>
        <description>update the referral category to Job Board when the application Source is Job Board</description>
        <field>TransAm_Referral_Source__c</field>
        <literalValue>Job Board</literalValue>
        <name>Update Referral Category to Job Board</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_child_app_flag_to_false</fullName>
        <field>TransAm_Update_child_app_flag__c</field>
        <literalValue>0</literalValue>
        <name>Update child app flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
        <fullName>Update EBE Flag To Blank</fullName>
        <actions>
            <name>Update_EBE_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the EBE Flag to Blank after there is an update on the application from interface.</description>
        <formula>NOT(ISBLANK(TransAm_EBE_Iseries__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Air Travel Confirmation To Olathe</fullName>
        <actions>
            <name>Air_Travel_Confirmation_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Olathe</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )) &amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; NOT(ISPICKVAL(Transam_Lodging_Address__c,&apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Air Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Air_Travel_Confirmation_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Traveler Going To Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))&amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; NOT(ISPICKVAL(Transam_Lodging_Address__c,&apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Bus Travel Confirmation To Travelers For Olathe</fullName>
        <actions>
            <name>Bus_Travel_Confirmation_To_Travelers_For_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Olathe</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))&amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; NOT(ISPICKVAL(Transam_Lodging_Address__c,&apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Bus Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Bus_Confirmation_For_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))&amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; NOT(ISPICKVAL(Transam_Lodging_Address__c,&apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Completed Applicaton Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>Application Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TransAm_Update_Archive_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Disqualified Application</fullName>
        <actions>
            <name>TransAm_Update_Archive_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>DQ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>In Progress Application Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Status__c</field>
            <operation>equals</operation>
            <value>Application Received,Recruiter Review,Manager Review,Schedule Orientation,Pending Orientation,HR Review,HR Approval,Application Approved - Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TransAm_Update_Archive_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>50</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Olathe Air without hotel</fullName>
        <actions>
            <name>Olathe_Air_without_hotel</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Olathe</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;) &amp;&amp; ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;) &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;)&amp;&amp; (TransAm_Transportation_confirmation__c = True) &amp;&amp; OR( ISPICKVAL(TransAm_Lodging__c,&apos;&apos;),ISPICKVAL( TransAm_Lodging__c , &apos;N/A&apos;)) &amp;&amp; OR (ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ) ,ISPICKVAL( Transam_Lodging_Address__c , &apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Olathe Bus Without Hotel</fullName>
        <actions>
            <name>Olathe_Bus_without_hotel</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Olathe</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;) &amp;&amp; ISPICKVAL(TransAm_Orientation_Location__c,&apos;Olathe, KS&apos;) &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp; (TransAm_Transportation_confirmation__c = True) &amp;&amp; OR( ISPICKVAL(TransAm_Lodging__c,&apos;&apos;),ISPICKVAL( TransAm_Lodging__c , &apos;N/A&apos;)) &amp;&amp; OR (ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ) ,ISPICKVAL( Transam_Lodging_Address__c , &apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Travel_And_Lodging_Confirmation_To_Travelers_For_Roc</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel And Lodging Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;))&amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos;))&amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; NOT(ISPICKVAL(Transam_Lodging_Address__c,&apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel Confirmation To Travelers For Olathe</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Travel_Confirmation_To_Travelers_For_Olathe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Olathe</description>
        <formula>AND(ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;),ISPICKVAL(TransAm_Orientation_Location__c ,&apos;Olathe, KS&apos;),OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;)),(TransAm_Transportation_confirmation__c = True),(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) ,( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )), (ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)), (ISPICKVAL(Transam_Lodging_Address__c, &apos;N/A&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Personal Vehicle%2F Rental Car Travel Confirmation To Travelers For Rockwall</fullName>
        <actions>
            <name>Personal_Vehicle_Rental_Car_Welcome_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Personal Vehicle/ Rental Car Travel Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;))&amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   (ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  ( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )) &amp;&amp; (ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)) &amp;&amp; (ISPICKVAL(Transam_Lodging_Address__c, &apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rockwall Air without hotel</fullName>
        <actions>
            <name>Rockwall_Air_without_hotel</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Air Travel Confirmation To Traveler Going To Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;) &amp;&amp; ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;) &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp; (TransAm_Transportation_confirmation__c = True) &amp;&amp; OR( ISPICKVAL(TransAm_Lodging__c,&apos;&apos;),ISPICKVAL( TransAm_Lodging__c , &apos;N/A&apos;)) &amp;&amp; OR (ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ) ,ISPICKVAL( Transam_Lodging_Address__c , &apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rockwall Bus Without Hotel</fullName>
        <actions>
            <name>Rockwall_Bus_without_hotel</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Bus Travel Confirmation To Travelers For Rockwall</description>
        <formula>ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;) &amp;&amp; ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;) &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp; (TransAm_Transportation_confirmation__c = True) &amp;&amp; OR( ISPICKVAL(TransAm_Lodging__c,&apos;&apos;),ISPICKVAL( TransAm_Lodging__c , &apos;N/A&apos;)) &amp;&amp; OR (ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ) ,ISPICKVAL( Transam_Lodging_Address__c , &apos;N/A&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status Flag Update</fullName>
        <actions>
            <name>TransAm_ICC_Export</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TransAm_ICC_Export_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TransAm_StatusFlag_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(TransAm_Status__c),&quot;Pending Orientation&quot;),ISPICKVAL(TransAm_Status__c,&quot;HR Review&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Application Queue Assignment</fullName>
        <actions>
            <name>Assign_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow assigns all applications with status &quot;Applications Received&quot; to Driver Recruiter queue</description>
        <formula>AND($Profile.Name = &apos;TransAm_Integration_User&apos;,  isPickval(TransAm_Status__c,  &apos;Application Received&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TransAm ONE Leasing Status update</fullName>
        <actions>
            <name>ONE_Leasing_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to update the ONE Leasing status based on Driver type</description>
        <formula>AND(
NOT(ISNEW()),
TransAm_Primary_Application__c,   
NOT(ISBLANK(TransAm_DriverID__c)), 
ISBLANK(PRIORVALUE(TransAm_DriverID__c)), 
ISPICKVAL(TransAm_Driver_Type__c,&apos;Owner&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Reset Current Status Age</fullName>
        <actions>
            <name>Transam_Update_Current_Status_Age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resets the current status age field to 0 when the Application status is changed</description>
        <formula>ISCHANGED(TransAm_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Travel Confirmation For Applicants Travelling To Olathe</fullName>
        <actions>
            <name>car_Hotel_KS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;),ISPICKVAL(TransAm_Orientation_Location__c ,&apos;Olathe, KS&apos;),OR(ISPICKVAL(TransAm_Transportation__c ,&apos;Personal Vehicle&apos;),ISPICKVAL(TransAm_Transportation__c ,&apos;Rental Car&apos;)),(TransAm_Transportation_confirmation__c = True),NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) ,NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; )), NOT(ISPICKVAL(TransAm_Lodging__c, &apos;N/A&apos;)), NOT(ISPICKVAL(Transam_Lodging_Address__c, &apos;N/A&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Welcome Mail For Air Travellers For Rockwall</fullName>
        <actions>
            <name>TransAm_Email_To_Drivers_travelling_by_Fight_to_Texas</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>TransAm Welcome Mail to the Applicants Travelling By Air To Rockwall</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Flight&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;   NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Welcome Mail For Applicants Travelling By Bus to Rockwall</fullName>
        <actions>
            <name>Welcome_Email_For_Drivers_Travelling_by_Bus_To_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Welcome Mail To Applicants Travelling To Rockwall By Bus</description>
        <formula>ISPICKVAL(PRIORVALUE(TransAm_Status__c),&apos;Schedule Orientation&apos;)  &amp;&amp; ISPICKVAL(TransAm_Status__c ,&apos;Pending Orientation&apos;)  &amp;&amp;   ISPICKVAL(TransAm_Orientation_Location__c,&apos;Rockwall, TX&apos;)   &amp;&amp; ISPICKVAL(TransAm_Transportation__c ,&apos;Bus&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp;  NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Welcome Mail For Applicants Travelling To Rockwall</fullName>
        <actions>
            <name>TransAm_Welcome_Email_To_Drivers_Travelling_To_Rockwall</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>TransAm Welcome Mail to the Applicants Coming to Rockwall,TX</description>
        <formula>ISPICKVAL( PRIORVALUE( TransAm_Status__c ) ,&apos;Schedule Orientation&apos;)  &amp;&amp;  ISPICKVAL(TransAm_Status__c,&apos;Pending Orientation&apos;)  &amp;&amp;  ISPICKVAL(TransAm_Orientation_Location__c ,&apos;Rockwall,TX&apos;) &amp;&amp;  (TransAm_Transportation_confirmation__c = True) &amp;&amp; NOT(ISPICKVAL(TransAm_Lodging__c,&apos;&apos;)) &amp;&amp;  NOT( ISPICKVAL(Transam_Lodging_Address__c,&apos;&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm_Assign_Task_To_Recruiter</fullName>
        <actions>
            <name>TransAm_Follow_up_with_the_applicant</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), ISPICKVAL(PRIORVALUE(TransAm_Status__c), &apos;Manager Review&apos;), ISPICKVAL(TransAm_Status__c , &apos;Recruiter Review&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm_Uncheck_Is_Status_Changed_Through_Merge</fullName>
        <actions>
            <name>TransAm_Application_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TransAm_Is_status_Changed_Through_Merge__c &amp;&amp; !ISBLANK( TransAm_Master_Application__c) &amp;&amp; !ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TransAm_UpdatePhone_From_OtherNumber</fullName>
        <actions>
            <name>TransAm_PhoneUpdate_From_OtherNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow rule updates the Phone from Other Number if Phone is blank</description>
        <formula>AND(
 ISBLANK( TransAm_Phone__c ),
 NOT(ISBLANK(TransAm_Other_Number__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Transam Updated Referral Category Based on Application Source</fullName>
        <actions>
            <name>Update_Referral_Category_to_Job_Board</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_App_Source__c</field>
            <operation>equals</operation>
            <value>Job Board</value>
        </criteriaItems>
        <description>This rule will update the Referral Category based on Application Source</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update By Pass Validation Checkbox</fullName>
        <actions>
            <name>Update_By_Pass_Validation_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
TransAm_Primary_Application__c, 
ISBLANK(TransAm_Master_Application__c), 
NOT(TransAm_By_Pass_Validation__c) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Received Date on manually Created Applications</fullName>
        <actions>
            <name>Update_Received_Date_for_Manual_Apps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will populate Received Date with current date when application is not created through integration</description>
        <formula>CreatedBy.Profile.Name &lt;&gt; &apos;TransAm_Integration_User&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update child app flag to prevent editing child Apps</fullName>
        <actions>
            <name>Update_child_app_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_Application__c.TransAm_Update_child_app_flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>TransAm_Follow_up_with_the_applicant</fullName>
        <assignedToType>owner</assignedToType>
        <description>Follow up with the applicant.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Follow up with the applicant</subject>
    </tasks>
</Workflow>
