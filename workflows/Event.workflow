<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Type</fullName>
        <description>This copies the type value from the standard field to the custom field</description>
        <field>TransAm_Type_Copy__c</field>
        <formula>TEXT(Type)</formula>
        <name>Copy Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Type</fullName>
        <actions>
            <name>Copy_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow rules copies the value from the standard type field into a custom type field so that it can be exposed in custom report types.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TransAm Copy Event Type</fullName>
        <actions>
            <name>Copy_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow rules copies the value from the standard type field into a custom type field so that it can be exposed in custom report types.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
