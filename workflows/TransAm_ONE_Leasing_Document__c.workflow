<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Archival_Date_on_IC_ONE_Leasing</fullName>
        <field>Transam_Archive_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Archival Date on IC/ONE Leasing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>TransAm_Lease__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Archival Date on ONE Leasing</fullName>
        <actions>
            <name>Update_Archival_Date_on_IC_ONE_Leasing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_ONE_Leasing_Document__c.TransAm_Archive_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow will update archival date on &apos;IC/ONE Leasing&apos; record whenever there is a new document uploaded after original RVI export</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
