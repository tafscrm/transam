<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Field_Update_of_Base_License</fullName>
        <field>TransAm_IC_Base_License__c</field>
        <formula>UPPER( TransAm_IC_Base_License__c )</formula>
        <name>Field Update of Base License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TransAm_Update_VIN_and_License</fullName>
        <field>TransAm_IC_VIN__c</field>
        <formula>UPPER( TransAm_IC_VIN__c )</formula>
        <name>Update VIN and License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Archive_Date_for_One_Lease</fullName>
        <field>Transam_Archive_Date__c</field>
        <formula>TODAY() + 30</formula>
        <name>Update Archive Date for One Lease</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Archival on One Leasing</fullName>
        <actions>
            <name>Update_Archive_Date_for_One_Lease</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_ONE_Leasing__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Changing Value to Upper case</fullName>
        <actions>
            <name>Field_Update_of_Base_License</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TransAm_Update_VIN_and_License</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( !ISBLANK( TransAm_IC_Base_License__c ) ,!ISBLANK( TransAm_IC_VIN__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
