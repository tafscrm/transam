<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
   <alerts>
        <fullName>Send_Email_For_Received_Fax_No_Barcode</fullName>
        <description>Send Email For Received Fax No Barcode</description>
        <protected>false</protected>
        <recipients>
            <recipient>deploymentuser@transamtruck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/TransAm_Email_Notification_for_Received_Fax_HTML_NOBarcode</template>
   </alerts>
   <alerts>
        <fullName>TransAm_Email_Alert_For_Received_Fax</fullName>
        <description>Email Alert For Received Fax</description>
        <protected>false</protected>
        <recipients>
            <recipient>deploymentuser@transamtruck.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_Email_Notification_for_Received_Fax_HTML</template>
    </alerts>
    <alerts>
        <fullName>efaxapp__Received_Fax_Notification_Email_Alert</fullName>
        <description>Received Fax Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>efaxapp__eFax_App_Email_Templates/efaxapp__Received_Fax_Notification_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Email Alert For Received Fax</fullName>
        <actions>
            <name>TransAm_Email_Alert_For_Received_Fax</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sending email to recipients once the fax is received</description>
        <formula>AND(NOT(ISBLANK(Id)),NOT(ISBLANK( TransAM_Application_Id__c )))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>efaxapp__Received Fax Notification</fullName>
        <actions>
            <name>efaxapp__Received_Fax_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>NOT( ISBLANK ( efaxapp__Fax_URL__c ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Email_Received_Fax_Link</fullName>
        <actions>
            <name>Send_Email_For_Received_Fax_No_Barcode</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT(ISBLANK(Id)),
ISBLANK(TransAM_Application_Id__c)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>  
