<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_To_Record_Creator</fullName>
        <description>Email To Record Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_HireRight_Email</template>
    </alerts>
    <alerts>
        <fullName>TransAm_Email_On_Status_Change_to_Record_Creator</fullName>
        <description>TransAm Email On Status Change to Record Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_HireRight_Email</template>
    </alerts>
    <alerts>
        <fullName>TransAm_HireRight_Mail</fullName>
        <description>TransAm HireRight Mail</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TransAm_HireRight_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Hire_RIght_Order_Status_Update</fullName>
        <description>Update the Hire Right order status to In Progress from Ordered</description>
        <field>TransAm_Order_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Hire RIght Order Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Manual_Update</fullName>
        <field>TransAm_IS_Manual_Update__c</field>
        <literalValue>0</literalValue>
        <name>Update Is Manual Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Of Status</fullName>
        <actions>
            <name>Email_To_Record_Creator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR( AND(ISCHANGED( TransAm_Order_Status__c ), ISPICKVAL(TransAm_Order_Status__c ,&quot;Completed&quot;)),

AND(ISCHANGED(TransAm_HireRight_Reports__c), NOT(ISBLANK(PRIORVALUE(TransAm_HireRight_Reports__c)))
))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HireRight order manual update</fullName>
        <actions>
            <name>Update_Is_Manual_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TransAm_HireRight_Orders__c.TransAm_IS_Manual_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Resetting the flag to false if set to true.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>email the client</fullName>
        <actions>
            <name>TransAm_HireRight_Mail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TransAm_HireRight_Orders__c.TransAm_Order_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
